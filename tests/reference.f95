program reference
  implicit none
  integer, parameter :: prec = 8
  integer, parameter :: np = 7360
  real(kind=prec), dimension(np) :: s
  real(kind=prec), dimension(np, 2) :: der,errdersta,errdersys, deg,errdegsta,errdegsys
#if VERSION > 12
  complex(kind=prec), external :: cggvapx, cegvapx
#else
  complex(kind=prec), external :: cggvap, cegvap
#endif
  complex(kind=prec), dimension(np, 2) :: cder,cerrdersta,cerrdersys, cdeg,cerrdegsta,cerrdegsys
  complex(kind=prec), dimension(np, 2) :: cegA, cegH
  complex(kind=prec), dimension(np, 2) :: dcegA, dcegH, dcegAsta, dcegAsys, dcegHsta, dcegHsys
  complex(kind=prec), dimension(np, 2) :: cggA, cggH
  complex(kind=prec), dimension(np, 2) :: dcggA, dcggH, dcggAsta, dcggAsys, dcggHsta, dcggHsys
  real (kind=prec) :: sW2 = 0.23122_prec
  character(len=3) LEPTONflag
  integer iLEP,iwarnings
  real(kind=prec) :: st2,mtop,als
  common /lepton/iLEP,iwarnings,LEPTONflag
  common /parm/st2,als,mtop
  integer i
  character(len=100) :: arg
  call get_command_argument(1, arg)
  open(unit=8, file="svalues.rec", form="unformatted")
  read(8) s
  close(8)
#if VERSION > 12
  call constants
  call rdata
#endif

  der = 0.
  errdersta = 0.
  errdersys = 0.
  deg = 0.
  errdegsta = 0.
  errdegsys = 0.
  cder = 0.
  cerrdersta = 0.
  cerrdersys = 0.
  cdeg = 0.
  cerrdegsta = 0.
  cerrdegsys = 0.
  cegA = 0.
  cegH = 0.
  dcegA = 0.
  dcegH = 0.
  dcegAsta = 0.
  dcegAsys = 0.
  dcegHsta = 0.
  dcegHsys = 0.
  cggA = 0.
  cggH = 0.
  dcggA = 0.
  dcggH = 0.
  dcggAsta = 0.
  dcggAsys = 0.
  dcggHsta = 0.
  dcggHsys = 0.

  do i=1,np
#if VERSION > 12
    call dhadr5x(+sqrt(s(i)),sw2,der(i,1),errdersta(i,1),errdersys(i,1),deg(i,1),errdegsta(i,1),errdegsys(i,1))
    call dhadr5x(-sqrt(s(i)),sw2,der(i,2),errdersta(i,2),errdersys(i,2),deg(i,2),errdegsta(i,2),errdegsys(i,2))

    call chadr5x(+sqrt(s(i)),sw2, cder(i,1),cerrdersta(i,1),cerrdersys(i,1), cdeg(i,1),cerrdegsta(i,1),cerrdegsys(i,1))
    call chadr5x(-sqrt(s(i)),sw2, cder(i,2),cerrdersta(i,2),cerrdersys(i,2), cdeg(i,2),cerrdegsta(i,2),cerrdegsys(i,2))
#else
    call dhadr5n12(+sqrt(s(i)),sw2,der(i,1),errdersta(i,1),errdersys(i,1),deg(i,1),errdegsta(i,1),errdegsys(i,1))
    call dhadr5n12(-sqrt(s(i)),sw2,der(i,2),errdersta(i,2),errdersys(i,2),deg(i,2),errdegsta(i,2),errdegsys(i,2))

    call chadr5n12(+sqrt(s(i)),sw2, cder(i,1),cerrdersys(i,1),cdeg(i,1),cerrdegsys(i,1))
    call chadr5n12(-sqrt(s(i)),sw2, cder(i,2),cerrdersys(i,2),cdeg(i,2),cerrdegsys(i,2))
#endif

    st2 = sw2

    LEPTONflag = 'had'
#if VERSION > 12
    cegH(i,1) = cegvapx(+s(i), dcegH(i,1), dcegHsta(i,1), dcegHsys(i,1))
    cegH(i,2) = cegvapx(-s(i), dcegH(i,2), dcegHsta(i,2), dcegHsys(i,2))
    cggH(i,1) = cggvapx(+s(i), dcggH(i,1), dcggHsta(i,1), dcggHsys(i,1))
    cggH(i,2) = cggvapx(-s(i), dcggH(i,2), dcggHsta(i,2), dcggHsys(i,2))
#else
    cegH(i,1) = cegvap(+s(i), dcegH(i,1))
    cegH(i,2) = cegvap(-s(i), dcegH(i,2))
    cggH(i,1) = cggvap(+s(i), dcggH(i,1))
    cggH(i,2) = cggvap(-s(i), dcggH(i,2))
#endif

    LEPTONflag = 'all'
#if VERSION > 12
    cegA(i,1) = cegvapx(+s(i), dcegA(i,1), dcegAsta(i,1), dcegAsys(i,1))
    cegA(i,2) = cegvapx(-s(i), dcegA(i,2), dcegAsta(i,2), dcegAsys(i,2))
    cggA(i,1) = cggvapx(+s(i), dcggA(i,1), dcggAsta(i,1), dcggAsys(i,1))
    cggA(i,2) = cggvapx(-s(i), dcggA(i,2), dcggAsta(i,2), dcggAsys(i,2))
#else
    cegA(i,1) = cegvap(+s(i), dcegA(i,1))
    cegA(i,2) = cegvap(-s(i), dcegA(i,2))
    cggA(i,1) = cggvap(+s(i), dcggA(i,1))
    cggA(i,2) = cggvap(-s(i), dcggA(i,2))
#endif
  enddo

  open(unit=8, file=trim(arg), action="write", form="unformatted")
  write(8) der
  write(8) errdersta
  write(8) errdersys
  write(8) deg
  write(8) errdegsta
  write(8) errdegsys
  write(8) cder
  write(8) cerrdersta
  write(8) cerrdersys
  write(8) cdeg
  write(8) cerrdegsta
  write(8) cerrdegsys
  write(8) cegA
  write(8) cegH
  write(8) dcegA
  write(8) dcegH
  write(8) dcegAsta
  write(8) dcegAsys
  write(8) dcegHsta
  write(8) dcegHsys
  write(8) cggA
  write(8) cggH
  write(8) dcggA
  write(8) dcggH
  write(8) dcggAsta
  write(8) dcggAsys
  write(8) dcggHsta
  write(8) dcggHsys
  close(8)
end program
