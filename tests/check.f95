program check
  implicit none
  integer, parameter :: prec = 8
  character(len=100) :: arg, func
  integer :: histogram(-30:10)
  integer ntest

  call get_command_argument(1, arg)
  call get_command_argument(2, func)

  histogram = 0

  ntest = 1
  do
    call get_command_argument(ntest, arg)
    call get_command_argument(ntest+1, func)

    if (len_trim(arg) == 0) exit
    if (len_trim(func) == 0) then
      write(0, *) "Invalid arguments"
      stop 1
    endif

    call perform_test(arg, func)
    ntest = ntest + 2
  enddo

  call print_histogram
  if(sum(histogram,1) .ne. histogram(lbound(histogram,1))) then
    stop 10
  endif

contains

  ! recursive need because body is larger than stack size
  recursive subroutine perform_test(arg, func)
  integer, parameter :: np = 7360
  real(kind=prec), dimension(np) :: s
  real(kind=prec), dimension(np, 2) :: der,errdersta,errdersys, deg,errdegsta,errdegsys
  complex(kind=prec), dimension(np, 2) :: cder,cerrdersta,cerrdersys, cdeg,cerrdegsta,cerrdegsys
  complex(kind=prec), dimension(np, 2) :: cegA, cegH
  complex(kind=prec), dimension(np, 2) :: dcegA, dcegH, dcegAsta, dcegAsys, dcegHsta, dcegHsys
  complex(kind=prec), dimension(np, 2) :: cggA, cggH
  complex(kind=prec), dimension(np, 2) :: dcggA, dcggH, dcggAsta, dcggAsys, dcggHsta, dcggHsys

  real(kind=prec), dimension(2) :: deri,errderstai,errdersysi, degi,errdegstai,errdegsysi
  complex(kind=prec), dimension(2) :: cderi,cerrderstai,cerrdersysi, cdegi,cerrdegstai,cerrdegsysi
  complex(kind=prec), dimension(2) :: cegAi, cegHi
  complex(kind=prec), dimension(2) :: dcegAi, dcegHi, dcegAstai, dcegAsysi, dcegHstai, dcegHsysi
  complex(kind=prec), dimension(2) :: cggAi, cggHi
  complex(kind=prec), dimension(2) :: dcggAi, dcggHi, dcggAstai, dcggAsysi, dcggHstai, dcggHsysi
  real (kind=prec) :: sW2 = 0.23122_prec
  integer i
  character(len=100) :: arg, func
  integer version

  ! cat check.f95| perl -ne 'print if s/.* ([^ ]*)\(.sqrt\(s.*/\1/g' | sort -u
  complex(kind=prec), external :: cegvap_qedc12, cegvapx_qedc17, cegvapx_qedc19, cegvapx_qedc23, &
                                  cggvap_qedc12, cggvapx_qedc17, cggvapx_qedc19, cggvapx_qedc23
  external :: chadr5n12_qedc12, chadr5x_qedc17, chadr5x_qedc19, chadr5x_qedc23
  external :: dhadr5n12_qedc12, dhadr5x_qedc17, dhadr5x_qedc19, dhadr5x_qedc23


  character(len=3) LEPTONflag_qedc12
  integer iLEP_qedc12,iwarnings_qedc12
  real(kind=prec) :: st2_qedc12,als_qedc12,mtop_qedc12
  common /lepton_qedc12/iLEP_qedc12,iwarnings_qedc12,LEPTONflag_qedc12
  common /parm_qedc12/st2_qedc12,als_qedc12,mtop_qedc12

  character(len=3) LEPTONflag_qedc17
  integer iLEP_qedc17,iwarnings_qedc17
  real(kind=prec) :: st2_qedc17,als_qedc17,mtop_qedc17
  common /lepton_qedc17/iLEP_qedc17,iwarnings_qedc17,LEPTONflag_qedc17
  common /parm_qedc17/st2_qedc17,als_qedc17,mtop_qedc17
  integer  ior_qedc17,nf_qedc17
  real *8 pi_qedc17,alinp_qedc17,einp_qedc17,mtopr_qedc17
  common/pqcdhs_qedc17/pi_qedc17,alinp_qedc17,einp_qedc17,mtopr_qedc17,ior_qedc17,nf_qedc17

  character(len=3) LEPTONflag_qedc19
  integer iLEP_qedc19,iwarnings_qedc19
  real(kind=prec) :: st2_qedc19,als_qedc19,mtop_qedc19
  common /lepton_qedc19/iLEP_qedc19,iwarnings_qedc19,LEPTONflag_qedc19
  common /parm_qedc19/st2_qedc19,als_qedc19,mtop_qedc19
  integer  ior_qedc19,nf_qedc19
  real *8 pi_qedc19,alinp_qedc19,einp_qedc19,mtopr_qedc19
  common/pqcdhs_qedc19/pi_qedc19,alinp_qedc19,einp_qedc19,mtopr_qedc19,ior_qedc19,nf_qedc19

  character(len=3) LEPTONflag_qedc23
  integer iLEP_qedc23,iwarnings_qedc23
  real(kind=prec) :: st2_qedc23,als_qedc23,mtop_qedc23
  common /lepton_qedc23/iLEP_qedc23,iwarnings_qedc23,LEPTONflag_qedc23
  common /parm_qedc23/st2_qedc23,als_qedc23,mtop_qedc23
  integer  ior_qedc23,nf_qedc23
  real *8 pi_qedc23,alinp_qedc23,einp_qedc23,mtopr_qedc23
  common/pqcdhs_qedc23/pi_qedc23,alinp_qedc23,einp_qedc23,mtopr_qedc23,ior_qedc23,nf_qedc23


  read(arg,*) version

  select case(version)
    case(12)
    case(17)
      call constants_qedc17
      call rdata_qedc17
      pi_qedc17 = 0.
      alinp_qedc17 = 0.
      einp_qedc17 = 0.
      mtopr_qedc17 = 0.
      ior_qedc17 = 0
      nf_qedc17 = 0
    case(19)
      call constants_qedc19
      call rdata_qedc19
      pi_qedc19 = 0.
      ALINP_qedc19 = 0.
      EINP_qedc19 = 0.
      MTOPr_qedc19 = 0.
      IOR_qedc19 = 0
      NF_qedc19 = 0
    case(23)
      call constants_qedc23
      call rdata_qedc23
      pi_qedc23 = 0.
      ALINP_qedc23 = 0.
      EINP_qedc23 = 0.
      MTOPr_qedc23 = 0.
      IOR_qedc23 = 0
      NF_qedc23 = 0

    case default
      write(0, *) "Unknown alphaQED version"
      stop 1
  end select

  open(unit=8, file="alphaQEDc"//trim(arg)//".rec", form="unformatted")
  read(8) der
  read(8) errdersta
  read(8) errdersys
  read(8) deg
  read(8) errdegsta
  read(8) errdegsys
  read(8) cder
  read(8) cerrdersta
  read(8) cerrdersys
  read(8) cdeg
  read(8) cerrdegsta
  read(8) cerrdegsys
  read(8) cegA
  read(8) cegH
  read(8) dcegA
  read(8) dcegH
  read(8) dcegAsta
  read(8) dcegAsys
  read(8) dcegHsta
  read(8) dcegHsys
  read(8) cggA
  read(8) cggH
  read(8) dcggA
  read(8) dcggH
  read(8) dcggAsta
  read(8) dcggAsys
  read(8) dcggHsta
  read(8) dcggHsys
  close(8)

  open(unit=8, file="svalues.rec", form="unformatted")
  read(8) s
  close(8)

  deri = 0.
  errderstai = 0.
  errdersysi = 0.
  degi = 0.
  errdegstai = 0.
  errdegsysi = 0.
  cderi = 0.
  cerrderstai = 0.
  cerrdersysi = 0.
  cdegi = 0.
  cerrdegstai = 0.
  cerrdegsysi = 0.
  cegAi = 0.
  cegHi = 0.
  dcegAi = 0.
  dcegHi = 0.
  dcegAstai = 0.
  dcegAsysi = 0.
  dcegHstai = 0.
  dcegHsysi = 0.
  cggAi = 0.
  cggHi = 0.
  dcggAi = 0.
  dcggHi = 0.
  dcggAstai = 0.
  dcggAsysi = 0.
  dcggHstai = 0.
  dcggHsysi = 0.

  do i=1,np
    select case(func)
      case ("dhadr5")
        select case(version)
          case(12)
            call dhadr5n12_qedc12(+sqrt(s(i)),sw2,deri(1),errderstai(1),errdersysi(1),degi(1),errdegstai(1),errdegsysi(1))
            call dhadr5n12_qedc12(-sqrt(s(i)),sw2,deri(2),errderstai(2),errdersysi(2),degi(2),errdegstai(2),errdegsysi(2))
          case(17)
            call dhadr5x_qedc17(+sqrt(s(i)),sw2,deri(1),errderstai(1),errdersysi(1),degi(1),errdegstai(1),errdegsysi(1))
            call dhadr5x_qedc17(-sqrt(s(i)),sw2,deri(2),errderstai(2),errdersysi(2),degi(2),errdegstai(2),errdegsysi(2))
          case(19)
            call dhadr5x_qedc19(+sqrt(s(i)),sw2,deri(1),errderstai(1),errdersysi(1),degi(1),errdegstai(1),errdegsysi(1))
            call dhadr5x_qedc19(-sqrt(s(i)),sw2,deri(2),errderstai(2),errdersysi(2),degi(2),errdegstai(2),errdegsysi(2))
          case(23)
            call dhadr5x_qedc23(+sqrt(s(i)),sw2,deri(1),errderstai(1),errdersysi(1),degi(1),errdegstai(1),errdegsysi(1))
            call dhadr5x_qedc23(-sqrt(s(i)),sw2,deri(2),errderstai(2),errdersysi(2),degi(2),errdegstai(2),errdegsysi(2))
        end select

        call check2R(i, "der", deri, der(i,:))
        call check2R(i, "errdersta", errderstai, errdersta(i,:))
        call check2R(i, "errdersys", errdersysi, errdersys(i,:))
        call check2R(i, "deg", degi, deg(i,:))
        call check2R(i, "errdegsta", errdegstai, errdegsta(i,:))
        call check2R(i, "errdegsys", errdegsysi, errdegsys(i,:))

      case ("chadr5")
        select case(version)
          case(12)
            call chadr5n12_qedc12(+sqrt(s(i)),sw2, cderi(1),cerrdersysi(1),cdegi(1),cerrdegsysi(1))
            call chadr5n12_qedc12(-sqrt(s(i)),sw2, cderi(2),cerrdersysi(2),cdegi(2),cerrdegsysi(2))
          case(17)
            call chadr5x_qedc17(+sqrt(s(i)),sw2, cderi(1),cerrderstai(1),cerrdersysi(1), cdegi(1),cerrdegstai(1),cerrdegsysi(1))
            call chadr5x_qedc17(-sqrt(s(i)),sw2, cderi(2),cerrderstai(2),cerrdersysi(2), cdegi(2),cerrdegstai(2),cerrdegsysi(2))
          case(19)
            call chadr5x_qedc19(+sqrt(s(i)),sw2, cderi(1),cerrderstai(1),cerrdersysi(1), cdegi(1),cerrdegstai(1),cerrdegsysi(1))
            call chadr5x_qedc19(-sqrt(s(i)),sw2, cderi(2),cerrderstai(2),cerrdersysi(2), cdegi(2),cerrdegstai(2),cerrdegsysi(2))
          case(23)
            call chadr5x_qedc23(+sqrt(s(i)),sw2, cderi(1),cerrderstai(1),cerrdersysi(1), cdegi(1),cerrdegstai(1),cerrdegsysi(1))
            call chadr5x_qedc23(-sqrt(s(i)),sw2, cderi(2),cerrderstai(2),cerrdersysi(2), cdegi(2),cerrdegstai(2),cerrdegsysi(2))
        end select

        call check2C(i, "cder", cderi, cder(i,:))
        call check2C(i, "cerrdersta", cerrderstai, cerrdersta(i,:))
        call check2C(i, "cerrdersys", cerrdersysi, cerrdersys(i,:))
        call check2C(i, "cdeg", cdegi, cdeg(i,:))
        call check2C(i, "cerrdegsta", cerrdegstai, cerrdegsta(i,:))
        call check2C(i, "cerrdegsys", cerrdegsysi, cerrdegsys(i,:))

      case ("cegvapxH")
        select case(version)
          case(12)
            LEPTONflag_qedc12 = 'had'
            st2_qedc12 = sw2
            cegHi(1) = cegvap_qedc12(+s(i), dcegHi(1))
            cegHi(2) = cegvap_qedc12(-s(i), dcegHi(2))
          case(17)
            LEPTONflag_qedc17 = 'had'
            st2_qedc17 = sw2
            cegHi(1) = cegvapx_qedc17(+s(i), dcegHi(1), dcegHstai(1), dcegHsysi(1))
            cegHi(2) = cegvapx_qedc17(-s(i), dcegHi(2), dcegHstai(2), dcegHsysi(2))
          case(19)
            LEPTONflag_qedc19 = 'had'
            st2_qedc19 = sw2
            cegHi(1) = cegvapx_qedc19(+s(i), dcegHi(1), dcegHstai(1), dcegHsysi(1))
            cegHi(2) = cegvapx_qedc19(-s(i), dcegHi(2), dcegHstai(2), dcegHsysi(2))
          case(23)
            LEPTONflag_qedc23 = 'had'
            st2_qedc23 = sw2
            cegHi(1) = cegvapx_qedc23(+s(i), dcegHi(1), dcegHstai(1), dcegHsysi(1))
            cegHi(2) = cegvapx_qedc23(-s(i), dcegHi(2), dcegHstai(2), dcegHsysi(2))
        end select

        call check2C(i, "cegH", cegHi, cegH(i,:))
        call check2C(i, "dcegH", dcegHi, dcegH(i,:))
        call check2C(i, "dcegHsta", dcegHstai, dcegHsta(i,:))
        call check2C(i, "dcegHsys", dcegHsysi, dcegHsys(i,:))

      case ("cggvapxH")
        select case(version)
          case(12)
            LEPTONflag_qedc12 = 'had'
            st2_qedc12 = sw2
            cggHi(1) = cggvap_qedc12(+s(i), dcggHi(1))
            cggHi(2) = cggvap_qedc12(-s(i), dcggHi(2))
          case(17)
            LEPTONflag_qedc17 = 'had'
            st2_qedc17 = sw2
            cggHi(1) = cggvapx_qedc17(+s(i), dcggHi(1), dcggHstai(1), dcggHsysi(1))
            cggHi(2) = cggvapx_qedc17(-s(i), dcggHi(2), dcggHstai(2), dcggHsysi(2))
          case(19)
            LEPTONflag_qedc19 = 'had'
            st2_qedc19 = sw2
            cggHi(1) = cggvapx_qedc19(+s(i), dcggHi(1), dcggHstai(1), dcggHsysi(1))
            cggHi(2) = cggvapx_qedc19(-s(i), dcggHi(2), dcggHstai(2), dcggHsysi(2))
          case(23)
            LEPTONflag_qedc23 = 'had'
            st2_qedc23 = sw2
            cggHi(1) = cggvapx_qedc23(+s(i), dcggHi(1), dcggHstai(1), dcggHsysi(1))
            cggHi(2) = cggvapx_qedc23(-s(i), dcggHi(2), dcggHstai(2), dcggHsysi(2))
        end select
        call check2C(i, "cggH", cggHi, cggH(i,:))
        call check2C(i, "dcggH", dcggHi, dcggH(i,:))
        call check2C(i, "dcggHsta", dcggHstai, dcggHsta(i,:))
        call check2C(i, "dcggHsys", dcggHsysi, dcggHsys(i,:))

      case ("cegvapxA")
        select case(version)
          case(12)
            LEPTONflag_qedc12 = 'all'
            st2_qedc12 = sw2
            cegAi(1) = cegvap_qedc12(+s(i), dcegAi(1))
            cegAi(2) = cegvap_qedc12(-s(i), dcegAi(2))
          case(17)
            LEPTONflag_qedc17 = 'all'
            st2_qedc17 = sw2
            cegAi(1) = cegvapx_qedc17(+s(i), dcegAi(1), dcegAstai(1), dcegAsysi(1))
            cegAi(2) = cegvapx_qedc17(-s(i), dcegAi(2), dcegAstai(2), dcegAsysi(2))
          case(19)
            LEPTONflag_qedc19 = 'all'
            st2_qedc19 = sw2
            cegAi(1) = cegvapx_qedc19(+s(i), dcegAi(1), dcegAstai(1), dcegAsysi(1))
            cegAi(2) = cegvapx_qedc19(-s(i), dcegAi(2), dcegAstai(2), dcegAsysi(2))
          case(23)
            LEPTONflag_qedc23 = 'all'
            st2_qedc23 = sw2
            cegAi(1) = cegvapx_qedc23(+s(i), dcegAi(1), dcegAstai(1), dcegAsysi(1))
            cegAi(2) = cegvapx_qedc23(-s(i), dcegAi(2), dcegAstai(2), dcegAsysi(2))
        end select
        call check2C(i, "cegA", cegAi, cegA(i,:))
        call check2C(i, "dcegA", dcegAi, dcegA(i,:))
        call check2C(i, "dcegAsta", dcegAstai, dcegAsta(i,:))
        call check2C(i, "dcegAsys", dcegAsysi, dcegAsys(i,:))

      case ("cggvapxA")
        select case(version)
          case(12)
            LEPTONflag_qedc12 = 'all'
            st2_qedc12 = sw2
            cggAi(1) = cggvap_qedc12(+s(i), dcggAi(1))
            cggAi(2) = cggvap_qedc12(-s(i), dcggAi(2))
          case(17)
            LEPTONflag_qedc17 = 'all'
            st2_qedc17 = sw2
            cggAi(1) = cggvapx_qedc17(+s(i), dcggAi(1), dcggAstai(1), dcggAsysi(1))
            cggAi(2) = cggvapx_qedc17(-s(i), dcggAi(2), dcggAstai(2), dcggAsysi(2))
          case(19)
            LEPTONflag_qedc19 = 'all'
            st2_qedc19 = sw2
            cggAi(1) = cggvapx_qedc19(+s(i), dcggAi(1), dcggAstai(1), dcggAsysi(1))
            cggAi(2) = cggvapx_qedc19(-s(i), dcggAi(2), dcggAstai(2), dcggAsysi(2))
          case(23)
            LEPTONflag_qedc23 = 'all'
            st2_qedc23 = sw2
            cggAi(1) = cggvapx_qedc23(+s(i), dcggAi(1), dcggAstai(1), dcggAsysi(1))
            cggAi(2) = cggvapx_qedc23(-s(i), dcggAi(2), dcggAstai(2), dcggAsysi(2))
        end select
        call check2C(i, "cggA", cggAi, cggA(i,:))
        call check2C(i, "dcggA", dcggAi, dcggA(i,:))
        call check2C(i, "dcggAsta", dcggAstai, dcggAsta(i,:))
        call check2C(i, "dcggAsys", dcggAsysi, dcggAsys(i,:))
      case default
        write(0, *) "Unknown function"
        stop 1
    end select
  enddo

  end subroutine

  subroutine print_histogram
  integer :: nhist(lbound(histogram,1) : ubound(histogram,1))
  real(kind=prec), parameter :: height = 50
  integer, parameter :: stepsize = 6
  integer i
  nhist = ceiling(height * real(histogram, kind=prec) / real(sum(histogram), kind=prec))

  do i=lbound(histogram,1), ubound(histogram,1)
    if(mod(i, stepsize) .eq. 0) then
      write(*, 900) i/2, repeat("#", nhist(i))
    else
      write(*, 901) repeat("#", nhist(i))
    endif
  enddo

900 FORMAT(I4, " |", A)
901 FORMAT(4x, " |", A)
  end subroutine print_histogram

  subroutine check1R(i, name, region, value, reference)
  integer, intent(in) :: i
  character(len=*), intent(in) :: name
  character(len=*), intent(in) :: region
  real(kind=prec), intent(in) :: value
  real(kind=prec), intent(in) :: reference
  real(kind=prec), parameter :: zero = 1e-15
  real(kind=prec), parameter :: hard_threshold = 1e-6
  real(kind=prec) :: ratio
  integer ind

  if( isnan(value) .and. isnan(reference)) then
    ! okay
    histogram(lbound(histogram)) = histogram(lbound(histogram)) + 1
  elseif( abs(value) < zero .and. abs(reference) < zero ) then
    ! okay
    histogram(lbound(histogram)) = histogram(lbound(histogram)) + 1
  elseif( abs(value) < zero ) then
    histogram(ubound(histogram)) = histogram(ubound(histogram)) + 1
    write(*, 900) name, i, region, reference
  elseif( abs(reference) < zero ) then
    histogram(ubound(histogram)) = histogram(ubound(histogram)) + 1
    write(*, 901) name, i, region, reference
  else
    ratio = abs(value / reference - 1)
    if(ratio < zero) then
      histogram(lbound(histogram)) = histogram(lbound(histogram)) + 1
    else
      ind = floor(2*log10(ratio))
      ind = max(lbound(histogram, 1), ind)
      ind = min(ubound(histogram, 1), ind)
      histogram(ind) = histogram(ind) + 1
      if(ratio > hard_threshold) then
        write(*, 903) name, i, region, ratio
      endif
    endif
  endif

900 FORMAT("Function ", A, ", index ", I0, " (", A, "): zero but shouldn't (ref=", ES20.4, ")")
901 FORMAT("Function ", A, ", index ", I0, " (", A, "): non-zero but shouldn't (value=", ES20.4, ")")
903 FORMAT("Function ", A, ", index ", I0, " (", A, "): agreement is ", ES20.4)
  end subroutine check1R

  subroutine check2R(i, name, value, reference)
  integer, intent(in) :: i
  character(len=*), intent(in) :: name
  real(kind=prec), intent(in) :: value(2)
  real(kind=prec), intent(in) :: reference(2)

  call check1R(i, name, "time-like", value(1), reference(1))
  call check1R(i, name, "spacelike", value(2), reference(2))
  end subroutine check2R

  subroutine check2C(i, name, value, reference)
  integer, intent(in) :: i
  character(len=*), intent(in) :: name
  complex(kind=prec), intent(in) :: value(1:2)
  complex(kind=prec), intent(in) :: reference(1:2)

  call check2R(i, name // "_r", real(value, kind=prec), real(reference, kind=prec))
  call check2R(i, name // "_i", aimag(value), aimag(reference))

  end subroutine check2C
end program
