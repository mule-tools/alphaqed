#!/bin/sh
set -e

function prepare {
    c=$(docker run --rm -d registry.gitlab.com/mule-tools/alphaqed/builder sleep infinity)
    docker exec $c git clone https://gitlab.com/mule-tools/alphaQED.git
    docker exec -w /alphaQED $c git checkout $1
    docker cp reference.f95 $c:/alphaQED
    docker cp svalues.rec $c:/alphaQED
    echo $c
}

function perform_test {
    c=$1
    version=$2
    shift 2
    docker exec -w /alphaQED $c gfortran -fdefault-double-8 -fdefault-real-8 -cpp -DVERSION=$version $@
    docker exec -w /alphaQED $c ./a.out alphaQEDc$version.rec
    docker cp $c:/alphaQED/alphaQEDc$version.rec .
    docker kill $c
}


# alphaQEDc12
c=$(prepare 1a5a09b)
docker exec -w /alphaQED/alphaQED -e FORTRAN="gfortran -fdefault-double-8 -fdefault-real-8" $c make -C rhad-1.01
docker exec -w /alphaQED/alphaQED -e FORTRAN="gfortran -fdefault-double-8 -fdefault-real-8" $c make libqed
perform_test $c 12 alphaQED/libqed.so reference.f95


# alphaQEDc17
c=$(prepare ea23fc0)
docker exec -w /alphaQED/alphaQED -e FORTRAN="gfortran -fdefault-double-8 -fdefault-real-8 -O" $c make -C rhad-1.01
docker exec -w /alphaQED/alphaQED -e FORTRAN="gfortran -fdefault-double-8 -fdefault-real-8" $c make libqed
perform_test $c 17 alphaQED/libqed.so reference.f95


# alphaQEDc19
c=$(prepare fc5e7dd)
docker exec -w /alphaQED/alphaQED $c sed -i "1s/.*/FC=gfortran -fdefault-double-8 -fdefault-real-8/" Makefile
docker exec -w /alphaQED/alphaQED $c make libqed.a
perform_test $c 19 reference.f95 alphaQED/libqed.a

# alphaQEDc23
c=$(prepare c284ff8)
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o r012.o r012.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o r34.o r34.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o runal.o runal.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o funcs.o funcs.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o vegas-rhad.o vegas-rhad.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o parameters.o parameters.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o rhad.o rhad.f
docker exec -w /alphaQED/alphaQEDc23/rhad $c gfortran -O -fdefault-double-8 -fdefault-real-8 -fno-second-underscore -fbounds-check -c -o rqcdHSx.o rqcdHSx.f
docker exec -w /alphaQED/alphaQEDc23 -e FORTRAN="gfortran -fdefault-double-8 -fdefault-real-8" $c make
perform_test $c 23 reference.f95 \
  alphaQEDc23/leptons.o \
  alphaQEDc23/hadr5n23.o \
  alphaQEDc23/hadr5n12.o \
  alphaQEDc23/hadr5n09.o \
  alphaQEDc23/hadr5n03.o \
  alphaQEDc23/cerrortot.o \
  alphaQEDc23/constants.o \
  alphaQEDc23/constants_qcd.o \
  alphaQEDc23/Rdat_all.o \
  alphaQEDc23/Rdat_fun.o \
  alphaQEDc23/Rdat_fit.o \
  alphaQEDc23/resonances_dat.o \
  alphaQEDc23/funalpqedcx.o \
  alphaQEDc23/funalp2smcx.o \
  alphaQEDc23/funalpqedrx.o \
  alphaQEDc23/funalp2smrx.o \
  alphaQEDc23/chadr5n23.o \
  alphaQEDc23/dggvapx.o \
  alphaQEDc23/cggvapx.o \
  alphaQEDc23/resonances_renr.o \
  alphaQEDc23/resonances_renc.o \
  alphaQEDc23/FSRfun.o \
  alphaQEDc23/R3G0322.o \
  alphaQEDc23/R330322.o \
  alphaQEDc23/RN20322.o \
  alphaQEDc23/RGG0322.o \
  alphaQEDc23/RS3G_fun.o \
  alphaQEDc23/RS33_fun.o \
  alphaQEDc23/RSN2_fun.o \
  alphaQEDc23/RSGG_fun.o \
  alphaQEDc23/RS3G_fit.o \
  alphaQEDc23/RS33_fit.o \
  alphaQEDc23/RSN2_fit.o \
  alphaQEDc23/RSGG_fit.o \
  alphaQEDc23/alphaQEDcx_sub.o \
  alphaQEDc23/renotab2.o \
  alphaQEDc23/renooldnew.o \
  alphaQEDc23/printscan.o \
  alphaQEDc23/printdatarray.o \
  alphaQEDc23/hadr5reson_ana.o \
  alphaQEDc23/bwintanalytic.o \
  alphaQEDc23/resfitfunx.o \
  alphaQEDc23/romegafun.o \
  alphaQEDc23/rphifun.o \
  alphaQEDc23/rphifun3g.o \
  alphaQEDc23/rphifun33.o \
  alphaQEDc23/qed.a \
  alphaQEDc23/rhad/r012.o \
  alphaQEDc23/rhad/r34.o \
  alphaQEDc23/rhad/runal.o \
  alphaQEDc23/rhad/funcs.o \
  alphaQEDc23/rhad/vegas-rhad.o \
  alphaQEDc23/rhad/parameters.o \
  alphaQEDc23/rhad/rhad.o \
  alphaQEDc23/rhad/rqcdHSx.o
