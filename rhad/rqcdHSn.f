      subroutine rqcdHS(s,R,ALINP,EINP,MTOP,IOR,NFext, version)
c used by function RS(s) in Rdat_fun.f and Rdat_fit.f
c interface which allows to provide most relevant parameters locally
c overwriting settings in parameters.f
c At given energy E=sqrt(s), contibution from NF=NFext flavors only
c e.g. NFext=3 only uds quarks taken into account, NFext=5 means no top contribution
c even above top threshold, etc
c NFext1 and nf (should agree) gives the number of active flavors at a given energy with flafor-thresholds
c provided by th(nf) via common quama 
c pQCD parameters set in constants_qcd.f
c..
c..   ***********
c..   "rhad: a program for the evaluation of the hadronic
c..      R-ratio in the perturbative regime of QCD"
c..
c..   by Robert V. Harlander and Matthias Steinhauser
c..   ***********
c..
c..   To compile, copy this file to the source directory of rhad, and say
c..
c..   gmake prog=rqcdHS
c..   The object will be named rqcdHS.o
c..
      implicit real*8(a-h,m-z)
      implicit integer(i,j)
      implicit character*60(k)
      implicit logical(l)
      integer IOR,NFext,NFext1,nf, version
      real *8 mq(6),mp(6),th(6),dmp(6,3)
      real *8 s,R,Rless,ALINP,EINP,MTOP
      real *8 muinp,mp4inp,mp5inp
      common /iecheck/iecheck

      include 'common.f'

c eventually provide quark masses and thresholds here mq msbar, mp pole, th flavor threshold
      real *8 mq_12(6),mp_12(6),th_12(6)
      common /quama_qedc12/mq_12,mp_12,th_12
      real *8 mq_17(6),mp_17(6),th_17(6)
      common /quama_qedc17/mq_17,mp_17,th_17
      real *8 mq_19(6),mp_19(6),th_19(6)
      common /quama_qedc19/mq_19,mp_19,th_19
      real *8 mq_23(6),mp_23(6),th_23(6)
      common /quama_qedc23/mq_23,mp_23,th_23

c for error handing in renormalization scale, c_mass and b_mass change at runtime
c uncomment relevant lines in this file
      common /input/muinp,mp4inp,mp5inp
      data ifirst /0/

      select case (version)
        case(12)
          mq = mq_12
          mp = mp_12
          th = th_12
        case(17)
          mq = mq_17
          mp = mp_17
          th = th_17
        case(19)
          mq = mq_19
          mp = mp_19
          th = th_19
        case(23)
          mq = mq_23
          mp = mp_23
          th = th_23
        case default
          print*, "Unknown version"
          stop 11
      end select

      sqrts = sqrt(s)
      scms  = sqrts*sqrts

      call parameters(scms, version)

      NFext1=6
      do while (sqrts.lt.th(NFext1))
         NFext1=NFext1-1         
      enddo 
c      write (19,*) ' rqcdHS:',sqrts,NFext1
c
c Settings in parameters; overwriting defalts called by parameters
c
c      alphasmz  = 0.118d0

      alphasmz  = ALINP

c.. order in pQCD
c      iord  = 4 ! default

      iord=IOR


c..   use MS-bar or pole quark mass?  (.true. == MS-bar mass)
c      lmsbar    = .false.

      lmsbar    = .false.

c.. msbar renormalization scale

c      mu=muinp

c..   masses

      massc      = mp(4)       ! charm
      massb      = mp(5)       ! bottom
      masst      = MTOP        ! top

c      massc      = mp4inp       ! charm
c      massb      = mp5inp       ! bottom
c      masst      = MTOP         ! top

c..   renormalization scale:
      mu        = dsqrt(scms)

c..   decoupling scales:
c      muc       = 2.d0*massc    ! charm
c      mub       = massb         ! bottom
c      mut       = masst         ! top

c..   threshold for open quark production
c      thrc = 4.8d0
c      thrb = 11.2d0
c      thrt = 2*masst+10.d0

      thrc = th(4)
      thrb = th(5)
      thrt = th(6)
c      write (*,*) ' INPUT:',muinp,mp4inp,mp5inp
c..   lower bound of quark threshold region
c      thrclow = 3.73d0
c      thrblow = 10.52d0
c      thrtlow = 2*masst-10.d0

c..   minimum allowed cms energy:
c      sqmin     = 1.8d0
      call init(scms)
      if (ifirst.eq.0) then
         call printpar(scms)
         ifirst=1
      endif

      rall = rhad(scms)
      ru = ruqrk(scms)
      rd = rdqrk(scms)
      rs = rsqrk(scms)
      rc = rcqrk(scms)
      rb = rbqrk(scms)
      rt = rtqrk(scms)
      rsg = rsinglet(scms)
      rem = rqed(scms)
      if (rs.eq.0.d0) then
         write (*,*) ' less than 3 flavors: pQCD fails! '
      else if (rc.eq.0.d0) then
         nf=3
      else if (rb.eq.0.d0) then
         nf=4
      else if (rt.eq.0.d0) then
         nf=5
      else
         nf=6
      endif
c      write (19,*) ' rqcdHSn: n_f=',nf
c if NFext not set elsewhere set it here
      if (iecheck.eq.1) write (*,*) sqrts,rall
      if (NFext.eq.6) R=rall
      if (NFext.eq.5) R=rall-rt
      if (NFext.eq.4) R=rall-rt-rb
      if (NFext.eq.3) R=rall-rt-rb-rc
      if (NFext.eq.2) R=rall-rt-rb-rc-rs
      if (NFext.eq.6) Rless=rall-rt
      if (NFext.eq.5) Rless=rall-rt-rb
      if (NFext.eq.4) Rless=rall-rt-rb-rc
      if (NFext.eq.3) Rless=rall-rt-rb-rc-rs
      if (NFext.eq.2) Rless=rall-rt-rb-rc-rs
      return
      end

      subroutine rqcdHS3x(s,R,R3G,R33,ALINP,EINP,MTOP,IOR,NFext, version)
c extended version of the interface rqcdHSn.f to be used in conjunction with objects which
c require flavor separation RS3G_fun.f, RS33_fun.f, RS3G_fit.f and RS33_fit.f
c..
c..   ***********
c..   "rhad: a program for the evaluation of the hadronic
c..      R-ratio in the perturbative regime of QCD"
c..
c..   by Robert V. Harlander and Matthias Steinhauser
c..   ***********
c..
c..   To compile, copy this file to the source directory of rhad, and say
c..
c..   gmake prog=rqcdHS
c..   The object will be named rqcdHS.o
c..
      implicit real*8(a-h,m-z)
      implicit integer(i,j)
      implicit character*60(k)
      implicit logical(l)
      integer IOR,NFext,NFext1,nf, version
      real *8 mq(6),mp(6),th(6),dmp(6,3)
      real *8 s,R,Rless,ALINP,EINP,MTOP
      real *8 muinp,mp4inp,mp5inp
      real *8 ru1,rd1,rs1,rc1,rb1,rt1,rsg1,rem1
      common /iecheck/iecheck

      real *8 mq_12(6),mp_12(6),th_12(6)
      common /quama_qedc12/mq_12,mp_12,th_12
      real *8 mq_17(6),mp_17(6),th_17(6)
      common /quama_qedc17/mq_17,mp_17,th_17
      real *8 mq_19(6),mp_19(6),th_19(6)
      common /quama_qedc19/mq_19,mp_19,th_19
      real *8 mq_23(6),mp_23(6),th_23(6)
      common /quama_qedc23/mq_23,mp_23,th_23

      include 'common.f'
      common /weights/v3uds1,v3charm1,v3bottom1,v3top1
c eventually provide quark masses and thresholds here mq msbar, mp pole, th flavor threshold
      common /quama/mq,mp,th
c for error handing in renormalization scale, c_mass and b_mass change at runtime
c uncomment relevant lines in this file
      common /rhadparts/ru1,rd1,rs1,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /input/muinp,mp4inp,mp5inp
      data ifirst /0/
      data ini/0/

      select case (version)
        case(12)
          mq = mq_12
          mp = mp_12
          th = th_12
        case(17)
          mq = mq_17
          mp = mp_17
          th = th_17
        case(19)
          mq = mq_19
          mp = mp_19
          th = th_19
        case(23)
          mq = mq_23
          mp = mp_23
          th = th_23
        case default
          print*, "Unknown version"
          stop 11
      end select

      if (ini.eq.0) goto 2
 1    v3uds1   =v3uds  
      v3charm1 =v3charm 
      v3bottom1=v3bottom
      v3top1   =v3top
      sqrts = sqrt(s)
      scms  = sqrts*sqrts

      call parameters(scms, version)

      NFext1=6
      do while (sqrts.lt.th(NFext1))
         NFext1=NFext1-1
      enddo
c
c Settings in parameters; overwriting defalts called by parameters
c
c      alphasmz  = 0.118d0

      alphasmz  = ALINP

c.. order in pQCD
c      iord  = 4 ! default

      iord=IOR


c..   use MS-bar or pole quark mass?  (.true. == MS-bar mass)
c      lmsbar    = .false.

      lmsbar    = .false.

c.. msbar renormalization scale

c      mu=muinp

c..   masses

      massc      = mp(4)       ! charm
      massb      = mp(5)       ! bottom
      masst      = MTOP        ! top

c      massc      = mp4inp       ! charm
c      massb      = mp5inp       ! bottom
c      masst      = MTOP         ! top

c..   renormalization scale:
      mu        = dsqrt(scms)

c..   decoupling scales:
c      muc       = 2.d0*massc    ! charm
c      mub       = massb         ! bottom
c      mut       = masst         ! top

c..   threshold for open quark production
c      thrc = 4.8d0
c      thrb = 11.2d0
c      thrt = 2*masst+10.d0

      thrc = th(4)
      thrb = th(5)
      thrt = th(6)
c      write (*,*) ' INPUT:',muinp,mp4inp,mp5inp
c..   lower bound of quark threshold region
c      thrclow = 3.73d0
c      thrblow = 10.52d0
c      thrtlow = 2*masst-10.d0

c..   minimum allowed cms energy:
c      sqmin     = 1.8d0
      call init(scms)
      if (ifirst.eq.0) then
         call printpar(scms)
         ifirst=1
      endif
c active NF

      nf = inffin

      if (NFext.eq.0) NFext=nf

         NFext=nf

      if (nf.eq.NFext) then
c calculate R(s), R3g(s) and R33(s) for nf and for nf-1 flavors used in non-perturbative regions
c assuming that only last active flavor is non-perturbative
         if (nf.eq.6) then
            x3gem =x3gem6
            x33em =x33em6
            x3gemm=x3gem5
            x33emm=x33em5
         endif
         if (nf.eq.5) then
            x3gem =x3gem5
            x33em =x33em5
            x3gemm=x3gem4
            x33emm=x33em4
         endif
         if (nf.eq.4) then
            x3gem =x3gem4
            x33em =x33em4
            x3gemm=x3gem3
            x33emm=x33em3
         endif
         if (nf.eq.3) then
            x3gem =x3gem3
            x33em =x33em3
            x3gemm=x3gem3
            x33emm=x33em3
         endif

         rall  = rhad(scms)
         ru    = ruqrk(scms)
         rd    = rdqrk(scms)
         rs    = rsqrk(scms)
         rc    = rcqrk(scms)
         rb    = rbqrk(scms)
         rt    = rtqrk(scms)
         rsg   = rsinglet(scms)
         rem   = rqed(scms)
         r3gsg = r3gsinglet(scms,r3gsgm)
         r33sg = r33singlet(scms,r33sgm)

         rs3g=xu *ru+xd* rd+xs* rs+xc* rc+xb* rb+xt* rt+r3gsg+x3gem*rem
         rs33=xu2*ru+xd2*rd+xs2*rs+xc2*rc+xb2*rb+xt2*rt+r33sg+x33em*rem

         if (nf.eq.3) then
         else if (nf.eq.4) then
            lcharm = .false.
         else if (nf.eq.5) then
            lbottom = .false.
         else if (nf.eq.6) then
            ltop = .false.
         endif
         if (nf.gt.3) then
            rallm  = rhad(scms)
            rum    = ruqrk(scms)
            rdm    = rdqrk(scms)
            rsm    = rsqrk(scms)
            rcm    = rcqrk(scms)
            rbm    = rbqrk(scms)
            rtm    = rtqrk(scms)
            rsgm   = rsinglet(scms)
            remm   = rqed(scms)

            rs3gm=xu *rum+xd* rdm+xs* rsm+xc* rcm+xb* rbm+xt* rtm+
     &           r3gsgm+x3gemm*remm
            rs33m=xu2*rum+xd2*rdm+xs2*rsm+xc2*rcm+xb2*rbm+xt2*rtm+
     &           r33sgm+x33emm*remm

         else
            rallm  = rall
            rum    = ru
            rdm    = rd
            rsm    = rs
            rcm    = rc
            rbm    = rb
            rtm    = rt
            rsgm   = rsg
            remm   = rem
            r3gsgm = r3gsg
            r33sgm = r33sg
            rs3gm  = rs3g
            rs33m  = rs33
         endif
         if (nf.eq.3) then
         else if (nf.eq.4) then
            lcharm = .true.
         else if (nf.eq.5) then
            lbottom = .true.
         else if (nf.eq.6) then
            ltop = .true.
         endif
         R=rall
         R3G=rs3g
         R33=rs33
         Rless=rallm
         R3Gless=rs3gm
         R33less=rs33m
      else
         if (nfext.eq.3) then
            lcharm = .false.
            lbottom = .false.
            ltop = .false.
            x3gem =x3gem3
            x33em =x33em3
         else if (nfext.eq.4) then
            lcharm = .true.
            lbottom = .false.
            ltop = .false.
            x3gem =x3gem4
            x33em =x33em4
         else if (nfext.eq.5) then
            lcharm = .true.
            lbottom = .true.
            ltop = .false.
            x3gem =x3gem5
            x33em =x33em5
         else if (nfext.eq.6) then
            lcharm = .true.
            lbottom = .true.
            ltop = .true.
            x3gem =x3gem6
            x33em =x33em6
         endif
         rall  = rhad(scms)
         ru    = ruqrk(scms)
         rd    = rdqrk(scms)
         rs    = rsqrk(scms)
         rc    = rcqrk(scms)
         rb    = rbqrk(scms)
         rt    = rtqrk(scms)
         rsg   = rsinglet(scms)
         rem   = rqed(scms)
         r3gsg = r3gsinglet(scms,r3gsgm)
         r33sg = r33singlet(scms,r33sgm)

         rs3g=xu *ru+xd* rd+xs* rs+xc* rc+xb* rb+xt* rt+r3gsg+x3gem*rem
         rs33=xu2*ru+xd2*rd+xs2*rs+xc2*rc+xb2*rb+xt2*rt+r33sg+x33em*rem

         R=rall
         R3G=rs3g
         R33=rs33
         Rless=rall
         R3Gless=rs3g
         R33less=rs33
      endif
      ru1    = ru
      rd1    = rd
      rs1    = rs
      rc1    = rc
      rb1    = rb
      rt1    = rt
      rsg1   = rsg
      rem1   = rem
      return
c
c weak current nomalized here to j=1/4(u-d) etc
c
 2    v3uds   =-1.d0/4.d0       ! v3up + v3down + v3strange = 1/4-1/4-1/4 = -1/4
      v3charm = 1.d0/4.d0
      v3bottom=-1.d0/4.d0
      v3top   = 1.d0/4.d0
c rem rewighting 3g
      x3gem3=5.d0/12.d0
      x3gem4=27.d0/68.d0
      x3gem5=57.d0/140.d0
      x3gem6=27.d0/68.d0
c rem rewighting 33
      x33em3=3.d0/16.d0
      x33em4=45.d0/272.d0
      x33em5=99.d0/560.d0
      x33em6=45.d0/272.d0
      xu=0.375d0
      xd=0.750d0
      xs=xd
      xb=xd
      xc=xu
      xt=xu
      xu2=xu*xu
      xd2=xd*xd
      xs2=xd2
      xb2=xd2
      xc2=xu2
      xt2=xu2
c      ini=1
c      write (*,*) ' rqcdHS3x coefficients initialized'
      goto 1
      end

C-{{{ function r3gsinglet:

      function r3gsinglet(scms,r3gsingles)
c..
c..   singlet terms
c..
c..   scms:  center-of-mass energy squared
c..
      implicit	real*8(a-h,m-z)
      implicit	integer(i,j)
      implicit	character*60(k)
      implicit  logical(l)

      include 'common.f'
      common /weights/v3uds,v3charm,v3bottom,v3top

      if (.not.la3sing) then
         r3gsinglet = 0.d0
         r3gsingles = 0.d0
         return
      endif

c      vguds   = 0.d0         ! vup  + vdown  + vstrange  = 2/3-1/3-1/3 = 0
c      v3uds   =-1.d0/4.d0    ! v3up + v3down + v3strange = 1/4-1/4-1/4 = -1/4
c      v3charm = 1.d0/4.d0
c      v3bottom=-1.d0/4.d0
c      v3top   = 1.d0/4.d0

      rtmp = 0.d0
      rtmp_sav = 0.d0
      
      if (lcharm) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*v3uds*vcharm * rv3sing(scms,mc,0
     &        .d0)+ 2*vcharm*v3charm * rv3sing(scms,mc,mc)
      endif
         
      if (lbottom) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*v3uds*vbottom * rv3sing(scms,mb
     &        ,0.d0)+ 2*v3charm*vbottom * rv3sing(scms,mb,0.d0)+
     &        2*(v3bottom*vbottom+vcharm*v3bottom) * rv3sing(scms,mb,mb)
      endif

      if (ltop) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*v3uds*vtop * rv3sing(scms,mt,0
     &        .d0)+ 2*v3charm*vtop * rv3sing(scms,mt,0.d0)+ 2*v3bottom
     &        *vtop * rv3sing(scms,mt,0.d0)+ 2*v3top*vtop * rv3sing(scms
     &        ,mt,mt)
         rtmp = rtmp 
     &        + 2*vcharm*v3top * rv3sing(scms,mt,0.d0)+ 2*vbottom
     &        *v3top * rv3sing(scms,mt,0.d0)
      endif

      r3gsingles = api**3 * nc * rtmp_sav
      r3gsinglet = api**3 * nc * rtmp

      return
      end

C-}}}
C-{{{ function r33singlet:

      function r33singlet(scms,r33singles)
c..
c..   singlet terms
c..
c..   scms:  center-of-mass energy squared
c..
      implicit	real*8(a-h,m-z)
      implicit	integer(i,j)
      implicit	character*60(k)
      implicit  logical(l)

      include 'common.f'
      common /weights/v3uds,v3charm,v3bottom,v3top

      if (.not.la3sing) then
         r33singles = 0.d0
         r33singlet = 0.d0
         return
      endif

      rtmp = 0.d0
      rtmp_sav = 0.d0

      rtmp = rtmp 
     &     + (v3uds)**2 * rv3sing(scms,0.d0,0.d0)
         
      if (lcharm) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*(v3uds)*v3charm * rv3sing(scms,mc,0
     &        .d0)+ v3charm**2 * rv3sing(scms,mc,mc)
      endif
         
      if (lbottom) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*(v3uds)*v3bottom * rv3sing(scms,mb
     &        ,0.d0)+ 2*v3charm*v3bottom * rv3sing(scms,mb,0.d0)+
     &        v3bottom**2 * rv3sing(scms,mb,mb)
      endif

      if (ltop) then
         rtmp_sav=rtmp
         rtmp = rtmp 
     &        + 2*(v3uds)*v3top * rv3sing(scms,mt,0
     &        .d0)+ 2*v3charm*v3top * rv3sing(scms,mt,0.d0)+ 2*v3bottom
     &        *v3top * rv3sing(scms,mt,0.d0)+ v3top**2 * rv3sing(scms
     &        ,mt,mt)
      endif

      r33singles = api**3 * nc * rtmp_sav
      r33singlet = api**3 * nc * rtmp

      return
      end

C-}}}
