      FUNCTION RS3G_qedc23(s,ier)
c FJ 27/12/2014 update/changes:
c initialization of common rhadnfmin1 supplemented:
c      IF (E.GE.EHIGH) THEN
cc call R to initialize some parameters
c         RSGG1=RS3x_qedc23(s,R3G,R33)
c proper PHI background directly implemented in R3G0322.f
c FJ 27/01/2023 phi resonance vs. background-only options
c major data update; new optionflags iomegaphidat,iintRd      
c     subroutine  omegaphir3gdat() may be called
c     for omega & phi data with (fort.1001) 
c      or without 3pi and KKc+KKn channels (fort.1002)      
      IMPLICIT NONE
      INTEGER i,j,jj,ini,IER,iresonances,IRESON,NFext,NFext1,nf,
     &     iomegaphidat,iintRd,iintRd1
      DOUBLE PRECISION s,E,RS40_qedc23,RS3G_qedc23,Rless,R3Gless,R33less,RS3x_qedc23,BW_qedc23,
     &     null,fac,rbw,rebw,Rinicall,UGM2,sMeV,ELOW,
     &     RSGG1,RS3G1,R3G,R33,F3G,rphifun_qedc23,frphi_qedc23
      DOUBLE PRECISION res(3),fsta(5),fsys(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      INTEGER N3G,N
      PARAMETER(N3G=577)
      DOUBLE PRECISION X3G(N3G),Y3Gdat(N3G,3),Y3G(N3G,3)
      DOUBLE PRECISION MOM,GOM,POM,MFI,GFI,PFI
      DOUBLE PRECISION CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH,ECUTFIT
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      integer  itestphi,jphi
      real *8  E0,E1,EX,dephi,SX
      integer ny
      parameter(ny=15)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc23/EMI,EMA
      COMMON/BWOM_qedc23/MOM,GOM,POM/BWFI_qedc23/MFI,GFI,PFI
      COMMON/RESRELERR_qedc23/FSTA,FSYS
c resonances flag iresonances=1 include resonances as data here; 0 include them
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc23/iresonances
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd1
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      COMMON /R3GDAT_qedc23/X3G,Y3Gdat,Y3G,N
      external RS40_qedc23,RS3x_qedc23,BW_qedc23,rphifun_qedc23,frphi_qedc23
c      common /rhadparts/ru,rd,rs,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      common /quama_qedc23/mq,mp,th
      iintRd1=iintRd
c      data ini /0/
c      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM_qedc23,BWFI_qedc23,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series
c
      call resonances_data_qedc23()
c
c called from resonances_dat.f
************************************************************************
      null=0.d0
      UGM2=1.D6
      xc=0.375d0
      xb=0.750d0
      xt=xc
      EOMM1=EOMM
      EOMP1=EOMP
      EFIM1=EFIM
      EFIP1=EFIP
      j=IER
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo
      itestphi=0
      if (itestphi.eq.1) then
         E0=EFIM-GFI
         E1=EFIP+GFI
         EMI(3)=E0
         EMA(3)=E1
         dephi=(E1-E0)/50.d0
         EX=E0
         do jphi=1,50
            SX=EX**2
            rbw=fac*BW_qedc23(sX,MFI,GFI,PFI)
            rbw=3.d0/4.d0*rbw
            write (99,*) ex,rbw,rbw*fsta(3),rbw*fsys(3)
            ex=ex+dephi
         enddo
         EMI(3)=EFIM
         EMA(3)=EFIP
      endif
      RSGG1=null
      RS3G1=null
      E=SQRT(S)
      ELOW =X3G(1)                ! 0.318
c      EHIGH=X3G(N3G)              ! 3.188 [ using up to 2.130]
c after dropping old incl data below lowest KEDR point
      EHIGH=1.841d0               ! lowest KEDR point
      CHPTCUTLOC=DMAX1(ELOW,CHPTCUTLOC)  ! do not go below recombined data set R3G0322.f
c bridge the clash between 1.841 (lowest KEDR point) and 2.000 GeV
      ECHARMTH=th(4)  ! 4.8 GeV
      ERXYDAT=2.130d0
      ECUTFIT=3.2d0
      RS3G_qedc23=null
      IF (E.LE.ELOW) THEN
         RS3G_qedc23=0.5d0*RS40_qedc23(s,IER)  ! 9/20 pQCD --> 10/20 from isospin decomposition
         RETURN
      ELSE IF ((E.GT.ELOW).and.(E.LE.ERXYDAT)) THEN
         call getindex_qedc23(E,N3G,X3G,I)
         if (iomegaphidat.eq.1) then ! phi data directly included (all channels)
            if (I.LT.N3G) then
               res(j)=Y3Gdat(I,j)+(Y3Gdat(I+1,j)-Y3Gdat(I,j))
     &              /(X3G(I+1)-X3G(I))*(E-X3G(I))
               if (j.eq.3) then
                  res(j)=res(j)*(Y3Gdat(I,1)+(Y3Gdat(I+1,1)-Y3Gdat(I,1))
     &                 /(X3G(I+1)-X3G(I))*(E-X3G(I)))
               endif
            else if (I.EQ.N3G) then
               res(j)=Y3Gdat(N3G,j)
               if (j.eq.3) then
                  res(j)=res(j)*Y3Gdat(N3G,1)
               end if
            endif
            RS3G_qedc23=res(j)
            RETURN
         else  ! background only; include or not phi separate as BW_qedc23+PDG
c for iomegaphidat=-1,0,2
            if (I.LT.N3G) then
               res(j)=Y3G(I,j)+(Y3G(I+1,j)-Y3G(I,j))
     &              /(X3G(I+1)-X3G(I))*(E-X3G(I))
               if (j.eq.3) then
                  res(j)=res(j)*(Y3G(I,1)+(Y3G(I+1,1)-Y3G(I,1))
     &                 /(X3G(I+1)-X3G(I))*(E-X3G(I)))
               endif
            else if (I.EQ.N3G) then
               res(j)=Y3G(N3G,j)
               if (j.eq.3) then
                  res(j)=res(j)*Y3G(N3G,1)
               endif
            endif
c <3G> has no omega contribution skipped
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
            if (iintRd.eq.0) then
               if ((iresonances.eq.1).and.(iomegaphidat.ne.-1)) then
                  if ((E.GE.EFIM).and.(E.LT.EFIP)) then
                     if (iomegaphidat.eq.2) then
                        rbw=frphi_qedc23(s,ier)*0.609723d0
                        if (ier.eq.2) then
                           res(j)=sqrt(res(j)**2+rbw**2)
                        else
                           res(j)=res(j)+rbw
                        endif
                     else if (iomegaphidat.eq.0) then
C      rbw=rphifun_qedc23(s,j)*0.63d0 ! 3/4*phi 83 % KKc+KKn 15% 3pi, 1% eta-gamma
                        rbw=fac*BW_qedc23(sMeV,MFI,GFI,PFI)*0.61449d0
                        if (ier.eq.2) then
                           rbw=rbw*fsta(3)
                           res(j)=sqrt(res(j)**2+rbw**2)
                        else
                           if (ier.eq.3) rbw=rbw*fsys(3)
                           res(j)=res(j)+rbw
                        endif
                     endif
C     rebw=rbw
                  endif
               endif
            endif               ! end skip phi
            RS3G_qedc23=res(j)
            RETURN
         endif
      ELSE IF (E.GT.ERXYDAT) THEN
c call R to initialize some parameters
         RSGG1=RS3x_qedc23(s,R3G,R33)
         IF (E.LE.ECUTFIT) THEN
            RS3G_qedc23=0.5d0*RS40_qedc23(s,IER)
         ELSE IF (E.GT.ECUTFIT) THEN
            if (nf.le.3) then
               nf=3
               RS3G_qedc23=0.5d0*RS40_qedc23(s,IER)
            else
               RSGG1=RS3x_qedc23(s,R3G,R33)
               F3G=R3G/RSGG1
               IF (IER.EQ.1) THEN
                  if (nf.eq.4) then
                     RS3G_qedc23=(RS40_qedc23(s,IER)-Rless)*xc+R3Gless
                  else if (nf.eq.5) then
                     RS3G_qedc23=(RS40_qedc23(s,IER)-Rless)*xb+R3Gless
                  else if (nf.ge.6) then
                     RS3G_qedc23=(RS40_qedc23(s,IER)-Rless)*xt+R3Gless
                  endif
               ELSE
                  RS3G_qedc23=RS40_qedc23(s,IER)*F3G
               ENDIF
            endif
         ENDIF
         RETURN
      ENDIF
      END
c
      FUNCTION RS3GpQCD_qedc23(s,IER)
c normalization of weak current j=1/4(u-d) etc
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,R,R3G,R33,RS3GpQCD_qedc23,RS3x_qedc23,ALINP,EINP,MTOP,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc23/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      external RS3x_qedc23
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      if (IER.eq.1) then
         ALINP1=ALS
         R=RS3x_qedc23(S,R3G,R33)
         res(IER)=R3G
      else if (IER.eq.2) then
         res(IER)=0.d0
      else  if (IER.eq.3) then
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         R=RS3x_qedc23(S,R3G,R33)
         RSP=R3G
         ALINP1=ALS-EST
         R=RS3x_qedc23(S,R3G,R33)
         RSM=R3G
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
      else
      endif
      RS3GpQCD_qedc23=res(IER)
      return
      end
c
      function RS3x_qedc23(s,R3Gout,R33out)
c as xRS_qedc23(s) with extended functionality calculating R3G and R33 as well
      implicit none
      integer  IOR,NF,NFext1,nf1
      real *8 pi,s,RS3x_qedc23,R,R3G,R33,xRS_qedc23,ALINP,EINP,MTOP
      real *8 Rless,R3Gless,R33less,R3Gout,R33out
      common/pqcdHS_qedc23/pi,ALINP,EINP,MTOP,IOR,NF
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf1
c adopte normalization j=1/4(u-d) etc for weak current i.e. 1/2  normal isospin
      call rqcdHS3x(s,R,R3G,R33,ALINP,EINP,MTOP,IOR,NF,23)
      RS3x_qedc23=R
      R3Gout=R3G
      R33out=R33
      return
      end

      subroutine  omegaphir3gdat_qedc23()
      implicit none
      integer ier,I,J,K,jj,count,iomegaphidat,iintRd,Nall,n1,n2
      double precision E,res(3),rex(3),
     &     omegaphidat(3),EOMM1,EOMP1,EFIM1,EFIP1
      parameter(Nall=2193)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      INTEGER N3G,N
      PARAMETER(N3G=577)
      DOUBLE PRECISION X3G(N3G),Y3Gdat(N3G,3),Y3G(N3G,3)
      COMMON /R3GDAT_qedc23/X3G,Y3Gdat,Y3G,N
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      include 'xRdat-extended.f' ! just DATA X/.../ statement
      call getindex_qedc23(EFIM1,Nall,X,n1)
      call getindex_qedc23(EFIP1,Nall,X,n2)
      count=0
      do jj=n1,n2
         E=X(jj)
         if ((E.GE.EFIM1).and.(E.LE.EFIP1))  then
            call getindex_qedc23(E,N3G,X3G,I)
            count=count+1
            do j=1,3
               res(j)=Y3Gdat(I,j)+(Y3Gdat(I+1,j)-Y3Gdat(I,j))
     &              /(X3G(I+1)-X3G(I))*(E-X3G(I))
               if (j.eq.3) then
                  res(j)=res(j)*(Y3Gdat(I,1)
     &                 +(Y3Gdat(I+1,1)-Y3Gdat(I,1))
     &                 /(X3G(I+1)-X3G(I))*(E-X3G(I)))
               endif
c     also get background for separating omega and phi resonance data
               rex(j)=Y3G(I,j)+(Y3G(I+1,j)-Y3G(I,j))
     &              /(X3G(I+1)-X3G(I))*(E-X3G(I))
               if (j.eq.3) then
                  rex(j)=rex(j)*(Y3G(I,1)+(Y3G(I+1,1)-Y3G(I,1))
     &                 /(X3G(I+1)-X3G(I))*(E-X3G(I)))
               endif
               omegaphidat(j)=res(j)-rex(j)
            enddo
            write (1001,123) e,omegaphidat
            write (1001,123) e,res
            write (1002,123) e,rex
         endif
      enddo
      write (1001,123) 0.d0,0.d0,0.d0,0.d0
      write (1001,*) ' count=',count
      return
 123  format(1x,1pe15.8,3(2x,1pe13.6))
      end

