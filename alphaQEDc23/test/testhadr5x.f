      program testhadr5x
C calculates Delta alpha from 5 quark sector, using tabled in hadr5n17.f 
      implicit none
      integer i,n,ier
      double precision e,st2,der,errder,errdersta,errdersys,
     &     deg,errdeg,errdegsta,errdegsys,de,emax,emin
      st2=0.23153d0
      n=300
      n=200
      n=400
      write (1,*) '      3.00000 '
      write (1,*) '      1.00000 '
      write (1,*) '   ',float(n)
      write (1,*) 'c energy(GeV),    der     ,   tot_error'
      write (2,*) '      3.00000 '
      write (2,*) '      1.00000 '
      write (2,*) '   ',float(n)
      write (1,*) 'c energy(GeV),    deg     ,   tot_error'
      emin= -120.00d0
      emax= 0.0d0
C     emin= 0.32d0
C      emax= 1.2d0
C      emin= 0.5d0
C      emax= 0.8d0
      de=(emax-emin)/n
      ier=0
      e=emin
      do i=1,n 
         call dhadr5x(e,st2,der,errdersta,errdersys,
     &        deg,errdegsta,errdegsys)
         errder=sqrt(errdersta**2+errdersys**2)
         errdeg=sqrt(errdegsta**2+errdegsys**2)
         write (1,98) e,der,errder
         write (2,98) e,deg,errdeg
         e=e+de
      enddo
      write (*,*) ' Results: der on fort.1, deg on fort.2'
 98   format(3(2x,1pe12.5))
      end
