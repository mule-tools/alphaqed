#!/bin/csh -fx
if ("$1" == "") then
set emin = 0.689
else
set emin= ($1)
endif
if ("$2" == "") then
set emax = 1.06
else
set emax= ($2)
endif
if ("$3" == "") then
set iomegaphidat = 2
else
set iomegaphidat = $3
endif
if ("$4" == "") then
set iresonances = 1
else
set iresonances = $4
endif
if ("$5" == "") then
set IRESON = 1
else
set IRESON = $5
endif

cd ..
make
cd -

echo $emin, $emax > Rdat.inp
echo $iomegaphidat, $iresonances, $IRESON >> Rdat.inp

make -f Makefile_testRdat prog=test_RSN2

echo ' run  test_RSN2'
time ./test_RSN2 < ./Rdat.inp

cat fort.1 > testRSN2_1.dat
cat fort.2 > testRSN2_2.dat
cat fort.3 > testRSN2_3.dat

make -f Makefile_testRdat prog=test_R3Gdat

echo ' run  test_R3Gdat'
time ./test_R3Gdat < ./Rdat.inp


cat fort.1 > testRS3G_1.dat
cat fort.2 > testRS3G_2.dat
cat fort.3 > testRS3G_3.dat


make -f Makefile_testRdat prog=test_R33dat

echo ' run  test_R33dat'
time ./test_R33dat < ./Rdat.inp


cat fort.1 > testRS33_1.dat
cat fort.2 > testRS33_2.dat
cat fort.3 > testRS33_3.dat

make -f  Makefile_testRdat prog=test_RGGdat

echo ' run  test_RGGdat'
time ./test_RGGdat < Rdat.inp

cat fort.1 > testRSGG_1.dat
cat fort.2 > testRSGG_2.dat
cat fort.3 > testRSGG_3.dat

make -f Makefile_testRdat

echo ' run  test_Rdat'
time ./test_Rdat < ./Rdat.inp


cat fort.1 > testRdat_1.dat
cat fort.2 > testRdat_2.dat
cat fort.3 > testRdat_3.dat

echo "Results in testSSN2_i.dat testRS3G_i.dat testRS33_i.dat testRdat_i.dat i=1,2,3"

#./x-alt-tab-mru

cat testRXYgnuplot.in > testRdat_gnu.in
cat >> testRdat_gnu.in <<_EOF_
set ylabel "RN2(s)"
plot "testRSN2_1.dat" skip 4 using 1:2:3:4 with yerrorbars ls 1 t "R data",\
  "testRSN2_2.dat" skip 4 using 1:2 with lines ls 3 t "fit", '' skip 4 using 1:3 with lines ls 3 t "", '' skip 4 using 1:4 with lines ls 3 t "",\
  "testRSN2_3.dat" skip 4 using 1:2 with lines ls 4 t "QCD", '' skip 4 using 1:3 with lines ls 4 t "", '' skip 4 using 1:4 with lines ls 4 t ""
pause -1
clear
_EOF_

gnuplot testRdat_gnu.in

Echo "Next figure? press RETURN"
set ans1=$<
pkill -x gnuplot

cat testRXYgnuplot.in > testRdat_gnu.in
cat >> testRdat_gnu.in <<_EOF_
set ylabel "R3G(s)"
plot "testRS3G_1.dat" skip 4 using 1:2:3:4 with yerrorbars ls 1 t "experimental",\
  "testRS3G_2.dat" skip 4 using 1:2 with lines t "fit", '' skip 4 using 1:3 with lines ls 3 t "", '' skip 4 using 1:4 with lines ls 3 t "",\
  "testRS3G_3.dat" skip 4 using 1:2 with lines ls 4 t "QCD", '' skip 4 using 1:3 with lines ls 4 t "", '' skip 4 using 1:4 with lines ls 4 t ""
pause -1
clear
_EOF_


gnuplot testRdat_gnu.in

echo -n "Next figure? press RETURN"
set ans1=$<
pkill -x gnuplot

cat testRXYgnuplot.in > testRdat_gnu.in
cat >> testRdat_gnu.in <<_EOF_
set ylabel "R33(s)"
plot "testRS33_1.dat" skip 4 using 1:2:3:4 with yerrorbars ls 1 t "experimental",\
  "testRS33_2.dat" skip 4 using 1:2 with lines t "fit", '' skip 4 using 1:3 with lines ls 3 t "", '' skip 4 using 1:4 with lines ls 3 t "",\
  "testRS33_3.dat" skip 4 using 1:2 with lines ls 4 t "QCD", '' skip 4 using 1:3 with lines ls 4 t "", '' skip 4 using 1:4 with lines ls 4 t ""
pause -1
clear
_EOF_

gnuplot testRdat_gnu.in

echo -n "Next figure? press RETURN"
set ans1=$<

cat testRXYgnuplot.in > testRdat_gnu.in
cat >> testRdat_gnu.in <<_EOF_
set ylabel "R(s)"
plot "testRdat_1.dat" skip 4 using 1:2:3:4 with yerrorbars ls 1 t "experimental",\
  "testRdat_2.dat" skip 4 using 1:2 with lines t "fit", '' skip 4 using 1:3 with lines ls 3 t "", '' skip 4 using 1:4 with lines ls 3 t "",\
  "testRdat_3.dat" skip 4 using 1:2 with lines ls 4 t "QCD", '' skip 4 using 1:3 with lines ls 4 t "", '' skip 4 using 1:4 with lines ls 4 t ""
pause -1
clear
_EOF_

gnuplot testRdat_gnu.in 

echo -n "Next figure? press RETURN"
set ans1=$<

cat testRXYgnuplot.in > testRdat_gnu.in
cat >> testRdat_gnu.in <<_EOF_
set ylabel "RGG(s)"
plot "testRSGG_1.dat" skip 4 using 1:2:3:4 with yerrorbars ls 1 t "experimental",\
  "testRSGG_2.dat" skip 4 using 1:2 with lines t "fit", '' skip 4 using 1:3 with lines ls 3 t "", '' skip 4 using 1:4 with lines ls 3 t "",\
  "testRSGG_3.dat" skip 4 using 1:2 with lines ls 4 t "QCD", '' skip 4 using 1:3 with lines ls 4 t "", '' skip 4 using 1:4 with lines ls 4 t ""
pause -1
clear
_EOF_

gnuplot testRdat_gnu.in 

echo ' For different setup adapt testRXYgnuplot.in '

exit 0
usage:
test_Rdat.sh emin emax iomegaphidat iresonances IRESON
exit 2

