      program test_rphifun
c     test phi resonance fit between 1.0 and 1.04 GeV
c     link rphifun and resfitfunx  
      implicit none
      integer ier,j,i,n
      double precision s,e,de,EFIM,EFIP,rphifun,resfi,res(4)
      external rphifun
      n=200
      ier=1
      j=ier
      EFIM=1.00d0
      EFIP=1.04d0
      write (1,*) '      2.0000'
      write (1,*) '      1.0000'
      write (1,*) '   ',float(n)
      e=efim
      de=(efip-efim)/float(n)
      do i=1,n
         if ((E.GE.EFIM).and.(E.LE.EFIP)) then ! apply fit of phi
            s=e*e
            do j=1,3 
               res(j)=rphifun(s,j)
               if (j.eq.1) resfi=res(j)
               if (j.gt.1) then
                  res(j)=abs(res(j)-resfi)
               endif
            enddo
            res(4)=sqrt(res(2)**2+res(3)**2)
            write (1,99) e,res(1),(res(1)-res(4)),
     &           (res(1)+res(4)),res(3),i
            e=e+de
         else
            write (*,*) ' Note range;',efim,efip 
         endif
      enddo
 99   format (1X,F10.7,4(2x,1PE11.5),2x,i4)
      end
