      program testadlerhadr5x
C calculates Delta alpha from 5 quark sector, using tabled in hadr5n17.f 
      implicit none
      integer i,n,ier
      double precision e,adler,errtot,errsta,errsys,de,emax,emin
      n=300
      n=200
      n=400
      write (1,*) '      3.00000 '
      write (1,*) '      1.00000 '
      write (1,*) '   ',float(n)
      write (1,*) 'c energy(GeV),   adlerfun ,   tot_error'
      emin= -100.00d0
      emax=    0.0d0
      de=(emax-emin)/n
      ier=0
      e=emin
      do i=1,n 
         call dadlerhadr5x(e,adler,errsta,errsys)
         errtot=sqrt(errsta**2+errsys**2)
         write (1,98) e,adler,errtot
         e=e+de
      enddo
      write (*,*) ' Results on fort.1'
 98   format(3(2x,1pe12.5))
      end
