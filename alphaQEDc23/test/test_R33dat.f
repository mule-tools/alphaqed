      program test_R33dat
c tests RENOoldnew and produces output for rwrenooldnew translating tables for renotab2[1]
c FJ 28/01/2012 recalculated Nall=2055 -> 2177
c FJ 27/01/2023 updates phi resonance vs. backgrould-only option
      implicit none
      character*7 plotvia
      integer i,j,n,Nall,Ndat,Nper,Nxxx,icount,IGS,iso,Ismooth,iffpp,
     &     npm,nmi,IORIN,NFIN,iomegaphidat,iintRd,
     &     iresonances_sav,IRESON_SAV,iomegaphidat_sav
      parameter(Nall=2193,Ndat=971,Nper=500)
      double precision pi1,ALINP,ERRAL,EINP,MTOP1,reno
      real *8 e,de,s,rs33,rs33smoothed,RS33pQCD,
     &     rs40smoothed,emi,ema,e0,e1,ex,alpima
      real *8 res(4),rfi(4),rpe(4),mq(6),mp(6),th(6)
      real *8 X(Nall),XD(Ndat)
      INTEGER NA,NB,NF,IOR,ICHK
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      REAL *8 CHPTCUT,EC,ECUT,EOMM1,EOMP1,EFIM1,EFIP1
      REAL *8 ALS2,EST,ESY,MZINP,MTOP2
      double precision ralphaold,uri,mplus,mp2,fapp,frf,renoold,renonew
      double complex calphanew,cggvapx,cdummy,cerror,cerrorsta,cerrorsys
      double precision st2,als,mtop
      double complex calept,cahadr,caltop,cDalphaweak1MSb
      external rs33,rs33smoothed,RS33pQCD
      external rs40smoothed,cggvap
c      FUNCTION RSpQCD(s,IER) in Rdat_fun.f requires QCD parameters incl errors
c provided via the following common block:
      COMMON/QCDPA/ALS2,EST,ESY,MZINP,MTOP2,NF,IOR,ICHK ! global INPUT here
c to set QCD parameters alpha_s with stat and sys error
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z, top mass,
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s)
c to be calculated, ICHK is dummy here
c (in some programs used for alternative routines calculating R(s))
      common/var1/pi1,ALINP,ERRAL,EINP,MTOP1,IORIN,NFIN ! IOR an NF as relevant for ALINP determination
      COMMON /RCUTS/CHPTCUT,EC,ECUT
      COMMON/GUSA/IGS,iso
      COMMON /OMEPHIDAT/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      COMMON /ERR/J
      common /quama_qedc23/mq,mp,th
      common /oldnew/ralphaold,calphanew,renoold,renonew
c
      include '../common.h'
      common /parm/st2,als,mtop
      common /cres/calept,cahadr,caltop,cDalphaweak1MSb
      COMMON/SMOO/Ismooth
      call constants()
      call constants_qcd() ! fills commons var and RCUTS
      include '../xRdat-extended.f'
      include '../xRdat-nonres.f'
      call Rdata()
c      plotvia='gnuplot'
      plotvia='graphx '
c fill common QCDPA
      URI=1.D-3              ! convert MeV into GeV
      MPLUS=139.56995D0*URI  ! pi^+ mass
      MP2=MPLUS**2
      ALS2=ALINP
      EST=ERRAL
      ESY=0.D0
      MZINP=EINP
      MTOP2=MTOP1
      IOR=IORIN       ! =4 default
      NF =NFIN        ! =5 default
      IGS=0           ! Gounaris-sakurai parametrisation for pipi channel
      iso=2           ! e^+ e^- data GS fit; iso=0,1 yields isospin 0, isovector part
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
      IRESON=1        ! include narrow resonances in fits
      iresonances=1   ! include narrow resonances in data sets
c      IRESON=0        ! exclude narrow resonances in fits
C      iintRd=1        ! resonances switched off
      iintRd=0        ! resonances included 
C     iomegaphidat=0   ! use BW with PDG parameters for omega and phi
C      iomegaphidat=1  ! use omega and phi data contribution to R
C      iomegaphidat=-1 ! replace BW resonances of option 0 by resonance data
      iomegaphidat=2  ! replace BW resonances of option 0 by resonance data
      icount=0
      EC=9.46d0       ! EC=9.5 (used as upper limit of N_f=4 pQCD region) -> show data
      ECUT=40.d0
      iffpp=1
      iffpp=0
      write (*,*) ' Enter energy range: ',
     &     'emin,emax'
      read (*,*) emi,ema
      write (*,*) ' Include resonances in data and fit: ',
     &     'iomegaphidat,iresonances,IRESON'
      read (*,*) iomegaphidat,iresonances,IRESON
      if (iomegaphidat.eq.0) iresonances=1
      if (iomegaphidat.eq.0) IRESON=1
      if (iomegaphidat.eq.1) iresonances=0
      if (iomegaphidat.eq.1) IRESON=0
      if (iomegaphidat.eq.-1) iresonances=0
      if (iomegaphidat.eq.-1) IRESON=0
      if (iomegaphidat.eq.2) iresonances=1
      if (iomegaphidat.eq.2) IRESON=1
      if ((iomegaphidat.ge.1).or.(iresonances.eq.1)) then
         Nxxx=Nall
         call getindex(emi,Nall,X,nmi)
         call getindex(ema,Nall,X,npm)
      else
         Nxxx=Ndat
         call getindex(emi,Ndat,XD,nmi)
         call getindex(ema,Ndat,XD,npm)
      endif
c may choose range by indices here
c      nmi=2
c      npm=340
c      nmi=1
c      npm=Nall
      Nxxx=npm-nmi+1
c write header for graphx data files fort.1,2,3
      if (plotvia.eq.'graphx ') then
      write (1,*)  '       3.0000'
      write (1,*)  '       1.0000'
      write (1,*)  '   ',float(Nxxx)
      write (1,*)  ' RS33 data'
      write(11,*)  '       2.0000'
      write(11,*)  '       1.0000'
      write(11,*)  '   ',float(Nxxx)
      write (2,*)  '       3.0000'
      write (2,*)  '       1.0000'
      write (2,*)  '   ',float(Nxxx)
      write (2,*)  ' RS33 fit'
      write (3,*)  '       3.0000'
      write (3,*)  '       1.0000'
      write (3,*)  '   ',float(Nper)
      write (3,*)  ' RS33 pQCD'
      endif
      write (*,*) ' test_R33dat:'
      write (*,*) ' -- 1 -- iomegaphidat=',iomegaphidat,
     &     ' -- iresonances=',iresonances,
     &     ' -- IRESON=',IRESON
      iresonances_sav=iresonances
      IRESON_SAV=IRESON
      iomegaphidat_sav=iomegaphidat
      do i=nmi,npm
C         iresonances=iresonances_sav
C         IRESON=IRESON_SAV
C         iomegaphidat=iomegaphidat_sav
         if ((iresonances.eq.1).or.(iomegaphidat.ge.1)) then
            e=X(i)
         else
            e=XD(i)
         endif
         ex=e
C assume for large E we may apply space-like s
c       if (E.GT.3.D0) ex=-e
c         reno=1.d0
c         CALL RENOoldnew(Ex,reno)
c         if (e.gt.ECUT) goto 10
         icount=icount+1
         s=e*e
         do j=1,3
            res(j)=rs33(s,j)
         enddo
c plot total error
         res(4)=sqrt(res(2)**2+res(3)**2)
C Conversion of |F_Pi|^2 to R!
         FAPP =(SQRT(1.D0-4.D0*MP2/S))**3/4.D0
C Conversion R(s) to |F_Pi|^2
C       ffpp=rs/fapp
         frf=1.d0/fapp
         if (iffpp.eq.0) frf=1.d0
         write (1,99) e,res(1)*frf,
     &        (res(1)-res(4))*frf,(res(1)+res(4))*frf,res(3)*frf,i
C         write (1,99) e,res(1)*frf,res(4)*frf,
C     &        res(4)*frf,res(3)*frf,i
         alpima=AIMAG(calphanew)
         if (ex.lt.x(1)) alpima=0.d0
         write(11,98) e,reno,ralphaold,DREAL(calphanew),alpima
      enddo
      write (1,*) icount
      icount=0
      write (*,*) ' -- 2 -- iomegaphidat=',iomegaphidat,
     &     ' -- iresonances=',iresonances,
     &     ' -- IRESON=',IRESON
      do i=nmi,npm
C         iresonances=iresonances_sav
C         IRESON=IRESON_SAV
C         iomegaphidat=iomegaphidat_sav
         if ((iresonances.eq.1).or.(iomegaphidat.ge.1)) then
            e=X(i)
         else
            e=XD(i)
         endif
         icount=icount+1
         s=e*e
         do j=1,3
            rfi(j)=rs33smoothed(s,j)
         enddo
c plot total error
         rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
C Conversion of |F_Pi|^2 to R!
         FAPP =(SQRT(1.D0-4.D0*MP2/S))**3/4.D0
C Conversion R(s) to |F_Pi|^2
C       ffpp=rs/fapp
         frf=1.d0/fapp
         if (iffpp.eq.0) frf=1.d0
         write (2,99) e,rfi(1)*frf,
     &        (rfi(1)-rfi(4))*frf,(rfi(1)+rfi(4))*frf,rfi(3)*frf,i
      enddo
c choose lower and upper limit for pQCD plot range
c      emi=1.8d0
c      ema=2.9d0
c may adjust N_f -> N_f+1 pQCD "thersholds" (matching points) here
c      th(4)=4.0d0
c      th(5)=11.d0
      if (ema.gt.1.8d0) then
      if (emi.le.1.8d0) emi=1.8001d0
      if (ema.le.emi) ema=13.d0
      de=(ema-emi)/Nper
      e=emi
      do i=1,Nper
         s=e*e
         do j=1,3
            rpe(j)=rs33pqcd(s,j)
         enddo
         rpe(4)=sqrt(rpe(2)**2+rpe(3)**2)
         write (3,99) e,rpe(1),rpe(1)-rpe(4),rpe(1)+rpe(4),rpe(3),i
         e=e+de
      enddo
      else
         write (*,*) ' emax < 1.8 GeV out of pQCD range ! '
      endif
      write (1,*) icount
      write (*,*) ' -- 3 -- iomegaphidat=',iomegaphidat,
     &     ' -- iresonances=',iresonances,
     &     ' -- IRESON=',IRESON
      stop 10
 99   format (1X,F10.7,4(2x,1PE11.4),2x,i4)
 98   format (1X,5(2x,1pe15.8))
      end
