
c      ++++ epemggfit1425 ++++  

c   1.375 -- 1.850 update oct 2022 
       DATA areslow/   
     &      2.032108d0, 0.212291d0,-0.385854d0,-0.054880d0, 0.032712d0,
     &      0.066392d0, 0.026796d0,-0.036695d0,-0.009545d0, 0.012694d0,
     &      0.012583d0,-0.006934d0/
       DATA astalow/   
     &      0.027987d0, 0.010261d0,-0.003240d0,-0.000808d0,-0.000798d0,
     &      0.001292d0,-0.000036d0,-0.000697d0,-0.000075d0,-0.000060d0,
     &      0.000239d0, 0.000007d0/
       DATA asyslow/   
     &      0.095135d0, 0.032139d0,-0.019406d0,-0.008849d0, 0.002171d0,
     &      0.007775d0, 0.001988d0,-0.001541d0,-0.001449d0, 0.000159d0,
     &      0.001094d0, 0.000612d0/
      DATA emima1 /
     &      1.375000d0, 1.837000d0/

c   1.750 -- 1.930 update oct 2022 
       DATA ares/      
     &      2.031826d0, 0.060373d0,-0.273655d0, 0.191905d0, 0.090077d0,
     &     -0.031557d0,-0.062915d0, 0.029121d0, 0.042647d0,-0.019347d0,
     &     -0.025377d0, 0.000000d0/
       DATA asta/      
     &      0.032620d0, 0.012577d0,-0.003203d0,-0.002174d0,-0.002918d0,
     &     -0.002386d0,-0.002022d0, 0.001708d0, 0.001691d0, 0.000633d0,
     &     -0.000460d0, 0.000000d0/
       DATA asys/      
     &      0.077540d0,-0.011229d0,-0.038618d0, 0.002465d0, 0.013698d0,
     &      0.002071d0,-0.006761d0,-0.002264d0,-0.000563d0,-0.000068d0,
     &      0.002284d0, 0.000000d0/
      DATA emima2 /
     &      1.395000d0, 2.000000d0/

c   1.845 -- 2.130 update oct 2022 
       DATA fres2023 / 
     &      2.075175d0, 0.215250d0, 0.029410d0,-0.054500d0,-0.017605d0,
     &      0.027696d0/
       DATA fsta2023 / 
     &      0.046072d0, 0.000029d0,-0.000019d0, 0.002251d0,-0.003610d0,
     &      0.000610d0/
       DATA fsys2023 / 
     &      0.044111d0,-0.021737d0, 0.005120d0,-0.000834d0, 0.003656d0,
     &     -0.004371d0/
      DATA emima3 /
     &      1.845000d0, 2.125000d0/
