c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; cggvap.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sat Jan  3 00:12:55 2015
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Wed Feb 12 11:09:02 2020
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2015 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
c **********************************************************************
c *         F. Jegerlehner, Humboldt Univerity Berlin, Grermany        *
c **********************************************************************
       FUNCTION cggvapx_qedc23(s,cerror,cerrorsta,cerrorsys)
C      -----------------------------
c Updated March 2012: hadr5n09 --> hadr5n12
c                     - includes BaBar and KLOE pipi ISR spectra; new compilation of Rdat_all.f
c                     - improved resolution of some resonance regions
c                     - 2010 Review of Particle Proprties resonace parameters
c                     - new version alpha_2 [<gamma 3> vacuum polarization] based on (ud) and (s) flavor separation        
c                     - <3 3> also available based of recombined flavor  separation
c calculate complex 1PI photon vacuum polarization
c  --- combines complex hadronic part (chadr5n12) with leptonic part (leptons) ---
C Fermionic contributions to the running of alpha
C Function collecting 1-loop, 2-loop, 3-loop leptonic contributions (e, mu, tau) 
C + hadronic contribution from light hadrons (udscb)
C + top quark contribution (t); here at 1-loop (good up to 200 GeV LEP I/II) 
C error is cerrder: central value cggvap +/- cerrder -> upper/lower bound
c Update December 2014: hadr5n12 --> hadr5n14 
c  -- included new data by KLOE 2012, BaBar 2012...2014, Novosibirsk 2014
c  -- up-to-date PDG parameters for omega and phi, 
c  -- option for using directly the omega and phi data
c  -- using local complex VP subtraction for BW_qedc23 resonances (numerical integration by NAG routine)  
       implicit none
       real*8 dggvapx_qedc23,s
       real*8 c0,st2,als,null
       real*8 mtop,mtop2
       real*8 dallepQED1_qedc23,dallepQED2_qedc23,dallepQED3l_qedc23,
     &        dalQED1ferm_qedc23,res_re,res_im
       real*8 r1real,r1imag,r2real,r2imag,imag
       real*8 r1reall,r1imagl,r2reall,r2imagl,r3reall,r3imagl
       real*8 fac,ee,e,der,errdersta,errdersys,deg,errdegsta,errdegsys
       real*8 dalept,daltop,dahadr,hfun,MW2,fourMW2,Dalphaweak1MSb
       double complex cnull,cder,cdeg,cerrder,cerrdersta,cerrdersys,
     &      cerrdeg,cerrdegsta,cerrdegsys,
     &      cerrdeg1,cerrdeg1sta,cerrdeg1sys,
     &      cahadr,calept,caltop,cggvapx_qedc23,cDalphaweak1MSb
       double complex cerror,cerrorsta,cerrorsys,cghadr,cegvap
       include 'common.h'      
       common /parm_qedc23/st2,als,mtop
c keep partial results here; as calculated in this routine        
       common /resu_qedc23/dalept,dahadr,daltop,Dalphaweak1MSb
       common /cres_qedc23/calept,cahadr,caltop,cDalphaweak1MSb
       common /cdegx_qedc23/cghadr,cerrdeg1,cerrdeg1sta,cerrdeg1sys,cegvap
       common /lep123_qedc23/r1real,r2real,r3reall,r1imag,r2imag,r3imagl
       null=0.d0
       cnull=DCMPLX(null,null)
       e=dsqrt(dabs(s))
       if (s.lt.0.d0) e=-e
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'had')) then
          call chadr5x_qedc23(e,st2,cder,cerrdersta,cerrdersys,
     &         cdeg,cerrdegsta,cerrdegsys)
          cahadr=cder
          call cerrortot_qedc23(cerrder,cerrdersta,cerrdersys)
          cghadr=cdeg
          call cerrortot_qedc23(cerrdeg,cerrdegsta,cerrdegsys)
          cerrdeg1sta=cerrdegsta
          cerrdeg1sys=cerrdegsys
          cerrdeg1   =cerrdeg
       else 
          cahadr=cnull
          cghadr=cnull
          cerrdeg1sta=cnull
          cerrdeg1sys=cnull
       endif
       if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
          calept=cnull
       else 
          r1real =dallepQED1_qedc23(s,r1imag)
          r2real =dallepQED2_qedc23(s,r2imag)
          r3reall=dallepQED3l_qedc23(s,r1reall,
     &                          r1imagl,r2reall,r2imagl,r3imagl)
          res_re=r1real+r2real+r3reall
          res_im=r1imag+r2imag+r3imagl
          calept=DCMPLX(res_re,res_im)
       endif
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'top')) then
          mtop2=mtop*mtop
          daltop=dalQED1ferm_qedc23(s,mtop2,imag)*4.d0/3.d0
          caltop=DCMPLX(daltop,imag)
       else 
          caltop=cnull
       endif
c MSbar alpha for one-loop weak contribution [WWgamma]
c       Dalphaweak1MSb=-adp/4.d0*(7.d0*log(mu^2/MW2)+2.d0/3.d0) ! OS value for s>>MW2
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'wea')) then
c          MW=80.398d0 now set in constants.f [via common.h]
          MW2=MW*MW
          fourMW2=MW2
          if (xMW.ne.null) fourMW2=xMW**2
          if (abs(s).gt.fourMW2) then
             Dalphaweak1MSb=-adp*(7.d0/4.d0*log(abs(s)/MW2))
          else 
             Dalphaweak1MSb=0.0d0
          endif
          cDalphaweak1MSb=DCMPLX(Dalphaweak1MSb,0.0d0)
       endif
       cerrorsta=cerrdersta
       cerrorsys=cerrdersys
       cerror   =cerrder
       cggvapx_qedc23=calept+cahadr
       RETURN
       END


       FUNCTION cegvapx_qedc23(s,cerror,cerrorsta,cerrorsys)
C      -----------------------------
c complex Delta g SM SU(2) coupling: interface for cggvap 
c Update Decembber 2014: new flavor separation and recombination 
c redesigned after cross check on the lattice by Harvey Meyer        
       implicit none
       real*8 s,null,fac
       real*8 st2,als,mtop
       double complex cggvapx_qedc23,cegvapx_qedc23,cegvap1,cfac,ccc,
     &      cer,cersta,cersys,
     &      cerrdeg,cerrdegsta,cerrdegsys
       double complex calept,cahadr,cahadr1,caltop,cDalphaweak1MSb
       double complex cglept,cghadr,cghadr1,cgetop,cDalpha2weak1MSb
       double complex cerror,cerrorsta,cerrorsys
       external cggvapx_qedc23
       common /parm_qedc23/st2,als,mtop
       common /cres_qedc23/calept,cahadr,caltop,cDalphaweak1MSb ! input
       common /cesg_qedc23/cglept,cghadr1,cgetop,cDalpha2weak1MSb ! output
       common /cdegx_qedc23/cghadr,cerrdeg,cerrdegsta,cerrdegsys,cegvap1
       null=0.d0
       fac=1.d0/4.d0/st2
       cfac=DCMPLX(fac,null)
       ccc=cggvapx_qedc23(s,cer,cersta,cersys)  ! fills common cres and part of cdeg
       cglept=cfac*calept
       cghadr1=cghadr
       cgetop=cfac*caltop*DCMPLX(3.d0/2.d0,null)
       cDalpha2weak1MSb=cfac*cDalphaweak1MSb
     &        *DCMPLX(86.d0/21.d0,null)
       cegvap1=cglept+cghadr
       cerrorsta=cerrdegsta
       cerrorsys=cerrdegsys
       cerror   =cerrdeg
       cegvapx_qedc23=cegvap1
       RETURN
       END

