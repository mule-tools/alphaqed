      FUNCTION RS33_qedc23(s,ier)
c FJ 27/12/2014 update/changes:
c initialization of common rhadnfmin1 supplemented:
c      IF (E.GE.EHIGH) THEN
cc call R to initialize some parameters
c         RSGG1=RS3x_qedc23(s,R3G,R33)
c proper PHI background directly implemented in R330322.f
c FJ 27/01/2023 phi resonance vs. background-only options
c               major data update      
      IMPLICIT NONE
      INTEGER i,j,jj,ini,IER,iresonances,IRESON,NFext,NFext1,nf,
     &     iomegaphidat,iintRd,iintRd1
      DOUBLE PRECISION s,E,RS40_qedc23,RS33_qedc23,Rless,R3Gless,R33less,RS3x_qedc23,BW_qedc23,
     &     null,fac,rbw,rebw,Rinicall,UGM2,sMeV,ELOW,
     &     RSGG1,RS331,R3G,R33,F33,rphifun_qedc23,frphi_qedc23
      DOUBLE PRECISION res(3),fsta(5),fsys(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      INTEGER N33,N
      PARAMETER(N33=577)
      DOUBLE PRECISION X33(N33),Y33dat(N33,3),Y33(N33,3)
      DOUBLE PRECISION MOM,GOM,POM,MFI,GFI,PFI
      DOUBLE PRECISION CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH,ECUTFIT
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      COMMON/BWOM_qedc23/MOM,GOM,POM/BWFI_qedc23/MFI,GFI,PFI
      COMMON/RESRELERR_qedc23/FSTA,FSYS
c resonances flag iresonances=1 include resonances as data here; 0 include them
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc23/iresonances
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd1
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      COMMON /R33DAT_qedc23/X33,Y33dat,Y33,N
      external RS40_qedc23,RS3x_qedc23,BW_qedc23,rphifun_qedc23,frphi_qedc23
c      common /rhadparts/ru,rd,rs,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      common /quama_qedc23/mq,mp,th
      iintRd1=iintRd
c      data ini /0/
c      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM_qedc23,BWFI_qedc23,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series
c
      call resonances_data_qedc23()
c
c called from resonances_dat.f
************************************************************************
      null=0.d0
      UGM2=1.D6
      xc=0.375d0**2
      xb=0.750d0**2
      xt=xc
      EOMM1=EOMM
      EOMP1=EOMP
      EFIM1=EFIM
      EFIP1=EFIP
      j=IER
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo
      RSGG1=null
      RS331=null
      E=SQRT(S)
      ELOW =X33(1)                ! 0.318
c      EHIGH=X33(N33)              ! 3.188 [ using up to 2.130]
c after dropping old incl data below lowest KEDR point
      EHIGH=1.841d0               ! lowest KEDR point
      CHPTCUTLOC=DMAX1(ELOW,CHPTCUTLOC)  ! do not go below recombined data set R330322.f
c bridge the clash between 1.841 (lowest KEDR point) and 2.000 GeV
      ECHARMTH=th(4)  ! 4.8 GeV
      ERXYDAT=2.130d0
      ECUTFIT=3.2d0
      RS33_qedc23=null
      IF (E.LE.ELOW) THEN
         RS33_qedc23=0.25d0*RS40_qedc23(s,IER)  ! 9/40 pQCD --> 10/40 isospin relation
         RETURN
      ELSE IF ((E.GT.ELOW).and.(E.LE.ERXYDAT)) THEN
         call getindex_qedc23(E,N33,X33,I)
         if ((iomegaphidat.eq.1).and.(iintRd.eq.0)) then ! phi data directly included (all channels)
            if (I.LT.N33) then
               res(j)=Y33dat(I,j)+(Y33dat(I+1,j)-Y33dat(I,j))
     &              /(X33(I+1)-X33(I))*(E-X33(I))
               if (j.eq.3) then
                  res(j)=res(j)*(Y33dat(I,1)+(Y33dat(I+1,1)-Y33dat(I,1))
     &                 /(X33(I+1)-X33(I))*(E-X33(I)))
               endif
            else if (I.EQ.N33) then
               res(j)=Y33dat(N33,j)
               if (j.eq.3) then
                  res(j)=res(j)*Y33dat(N33,1)
               endif
            endif
            RS33_qedc23=res(j)
            RETURN
         else  ! background only; include or not phi separate as BW_qedc23+PDG
c for iomegaphidat=-1,0,2
            if (I.LT.N33) then
               res(j)=Y33(I,j)+(Y33(I+1,j)-Y33(I,j))
     &              /(X33(I+1)-X33(I))*(E-X33(I))
               if (j.eq.3) then
                  res(j)=res(j)*(Y33(I,1)+(Y33(I+1,1)-Y33(I,1))
     &                 /(X33(I+1)-X33(I))*(E-X33(I)))
               endif
            else if (I.EQ.N33) then
               res(j)=Y33(N33,j)
               if (j.eq.3) then
                  res(j)=res(j)*Y33(N33,1)
               endif
            endif
c <33> has no omega contribution skipped
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
            if (iintRd.eq.0) then
               if ((iresonances.eq.1).and.(iomegaphidat.ne.-1)) then
                  if ((E.GE.EFIM).and.(E.LT.EFIP)) then
                     if (iomegaphidat.eq.2) then
                        rbw=frphi_qedc23(s,ier)*0.457286d0
                        if (ier.eq.2) then
                           res(j)=sqrt(res(j)**2+rbw**2)
                        else
                           res(j)=res(j)+rbw
                        endif
                     else if (iomegaphidat.eq.0) then
C  83 % KKc+KKn at peak
                        rbw=fac*BW_qedc23(sMeV,MFI,GFI,PFI)*0.46084d0
                        if (ier.eq.2) then
                           rbw=rbw*fsta(3)
                           res(j)=sqrt(res(j)**2+rbw**2)
                        else
                           if (ier.eq.3) rbw=rbw*fsys(3)
                           res(j)=res(j)+rbw
                        endif
                     endif
C     rebw=rbw
                  endif
               endif
            endif               ! end skip phi
            RS33_qedc23=res(j)
            RETURN
         endif
      ELSE IF (E.GT.ERXYDAT) THEN
c call R to initialize some parameters
         RSGG1=RS3x_qedc23(s,R3G,R33)
         IF (E.LE.ECUTFIT) THEN
            RS33_qedc23=9.d0/32.d0*RS40_qedc23(s,IER)
         ELSE IF (E.GT.ECUTFIT) THEN
            if (nf.le.3) then
               nf=3
               RS33_qedc23=9.d0/32.d0*RS40_qedc23(s,IER)
            else
               RSGG1=RS3x_qedc23(s,R3G,R33)
               F33=R33/RSGG1
               IF (IER.EQ.1) THEN
                  if (nf.eq.4) then
                     RS33_qedc23=(RS40_qedc23(s,IER)-Rless)*xc+R33less
                  else if (nf.eq.5) then
                     RS33_qedc23=(RS40_qedc23(s,IER)-Rless)*xb+R33less
                  else if (nf.ge.6) then
                     RS33_qedc23=(RS40_qedc23(s,IER)-Rless)*xt+R33less
                  endif
               ELSE
                  RS33_qedc23=RS40_qedc23(s,IER)*F33
               ENDIF
            endif
         ENDIF
         RETURN
      ENDIF
      END
c
      FUNCTION RS33pQCD_qedc23(s,IER)
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,R,R3G,R33,RS33pQCD_qedc23,RS3x_qedc23,ALINP,EINP,MTOP,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc23/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      external RS3x_qedc23
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      if (IER.eq.1) then
         ALINP1=ALS
         R=RS3x_qedc23(S,R3G,R33)
         res(IER)=R33
      else if (IER.eq.2) then
         res(IER)=0.d0
      else  if (IER.eq.3) then
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         R=RS3x_qedc23(S,R3G,R33)
         RSP=R33
         ALINP1=ALS-EST
         R=RS3x_qedc23(S,R3G,R33)
         RSM=R33
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
      else
      endif
      RS33pQCD_qedc23=res(IER)
      return
      end
