c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; chadr5n23.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Thu Jul  6 12:37:04 2017
c;; Last Modified By: Fred Jegerlehner
c;; Last Modified On: Fri Aug 18 00:28:28 2023
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2022 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
c **********************************************************************
c *         F. Jegerlehner, Humboldt Univerity Berlin, Grermany        *
c **********************************************************************
       subroutine chadr5x_qedc23(de,dst2,cder,cerrdersta,cerrdersys,
     &     cdeg,cerrdegsta,cerrdegsys)
c Update March 2012
c Provides imaginary part of 5 flavor hadronic contribution to photon vacuum polarization
c and converts dhadr5n12 to double complex variables in chadr5n12
c
c R(s) data/fits in Rdat_all.f [data] via interface Rdat_fun.f / Rdat_fit.f [fits]
c
c Pi' (q^2) photon vacuum polarization function ~ Pi(q^2)/q^2
c Convention Re alpha = - [Re Pi (q^2)-Pi (0)];
c            Im alpha =- Im Pi' (q^2) = -1/3 alpha * R(q^2)  [alpha = classical zero momentum feine structure constent]
c Options for R(q^2): iresonances=0,1 switch off,on narrow resonances
c omega, phi, psi(i) i=1,2,3, Upsilon(i) i=1,2,3,4,5,6
c Ismooth=0,1 data,smoothed data [mostly Chebyshev fits], IGS=0,1
c use [1] or not [0] Gounaris-Sakurai fit for rho resonance
c 08/10/13 save and restore environment variables changed temporarily by this routine
c Update December 2014: hadr5n12 --> hadr5n14 
c  -- included new data by KLOE 2012, BaBar 2012...2014, Novosibirsk 2014
c  -- up-to-date PDG parameters for omega and phi, 
c  -- option for using directly the omega and phi data
c  -- using local complex VP subtraction for BW_qedc23 resonances (numerical integration by NAG routine)  
       implicit none
       integer i,IRESON,iintRd,iresonances,iresonances1,Ismooth,
     &      IGS,IGS1,iso,IER,IER_SAV,IOR,NF,ICHK,IORIN,NFIN,
     &      IOR_SAV,IRESON_SAV,iresonances_sav,iso_sav,IGS_SAV
       real *8 de,dst2,dder,ddeg,derrdersta,derrdersys,
     &      derrdegsta,derrdegsys
       real *8 s,RSDATn_qedc23,sthres,null,fac,rder,rdeg,
     &      rerrdersta,rerrdegsta,rerrdersys,rerrdegsys
       data null/0./
       real *8 rer,ider,ideg,ierrdersta,ierrdersys,
     &      ierrdegsta,ierrdegsys,ierrder1,rrer
       real *8 RR(3),RG(3),RES(3),REBW,fac2
       double precision pi,ALINP,ERRAL,EINP,MTOP
       double precision CHPTCUT,EC,ECUT
c       double precision R3(3),rdgg,errdgg,ierrdgg,idgg
       double complex cder,cdeg,cerrdersta,cerrdegsta,
     &      cerrdersys,cerrdegsys
c     &      ,cdgg,cerrdgg
       double precision ALS2,EST,ESY,MZINP,MTOP2
       double precision dalept,dahadr,daltop,Dalphaweak1MSb
       double precision alp,adp,adp2,adp3,facbw,ml(3)
       double precision Ruds,Rudsc,Rudscb,Rc,Rb
       double precision cuds,cc,cb,crho,comega,cphi,cpsi,cups
       double precision MPLUS,MZERO,RS3GFUN_qedc23
       external dggvapx_qedc23,RSDATn_qedc23,RS3GFUN_qedc23
       common /params_qedc23/ALP,adp,adp2,adp3,ml
       common /resu_qedc23/dalept,dahadr,daltop,Dalphaweak1MSb
c      FUNCTION RSpQCD_qedc23(s,IER) in Rdat_fun.f requires QCD parameters incl errors
c provided via the following common block:
       COMMON/QCDPA_qedc23/ALS2,EST,ESY,MZINP,MTOP2,NF,IOR,ICHK ! global INPUT here
c to set QCD parameters alpha_s with stat and sys error
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z, top mass,
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s)
c to be calculated, ICHK is dummy here
c (in some programs used for alternative routines calculating R(s))
       common/var1_qedc23/pi,ALINP,ERRAL,EINP,MTOP,IORIN,NFIN
c commons needed to get R(s)
       COMMON/RCUTS_qedc23/CHPTCUT,EC,ECUT
c resonances flag iresonances=1 include resonances as data here;
c                            =0 include them elsewhere
       COMMON/RESFIT_qedc23/facbw,IRESON,iintRd
       COMMON/RES_qedc23/iresonances
       COMMON/RBW_qedc23/REBW
       COMMON/ERR_qedc23/IER
       common/srdeg_qedc23/fac,sthres
c switch for using Gounaris-Sakurai parametrization: IGS=1, iso=2
c provides e^+e^- data fit
c iso=1 isovector part, iso=0 isoscalar part
       COMMON /GUSA_qedc23/IGS,iso
c flag for imaginary part Ismooth=0 R(s) data,=1 R(s) fits
       COMMON/SMOO_qedc23/Ismooth ! to be set in main program

       call constants_qcd_qedc23()  ! fills commons var and RCUTS

c save environment and restore on return
       IOR_SAV=IOR
       IRESON_SAV=IRESON
       iresonances_sav=iresonances
       iso_sav=iso
       IGS_SAV=IGS

       IOR=4           ! don't change for consistency with real part calculation
       IRESON=1        ! don't change provides full R including narrow resonances in fits
       iresonances=1   ! don't change provides full R including narrow resonances in data
       iso=2           ! don't change
       IGS=0           ! Chebyshev fits to be preferred
       iintRd=0
c fill common QCDPA
       ALS2=ALINP
       EST=ERRAL
       ESY=0.D0
       MZINP=EINP
       MTOP2=MTOP
       IOR=IORIN                ! =4 default
       NF =NFIN                 ! =5 default
       MPLUS=0.13957018D0       ! PDG 2004 now set in resonances_dat.f via common/pions/
       sthres=(2.d0*MPLUS)**2   ! threshold for imaginary part [very small between m_\pi^0 and 2 m_\pi]
       fac=-alp/3.d0
       fac2=fac/dst2
       do i=1,3
          RR(i) =null
          RG(i) =null
          RES(i)=null
       enddo 
       ider=null
       ideg=null
       ierrdersta=null
       ierrdersys=null
       ierrdegsta=null
       ierrdegsys=null
       s=de*abs(de)
c provide double precision version for real part
       call dhadr5x_qedc23(de,dst2,dder,derrdersta,derrdersys,
     &      ddeg,derrdegsta,derrdegsys)
       if (s.gt.sthres) then
c get R(s)
          IER_SAV=IER
          do i=1,3
             IER=i
c R(s) incl resonances (if iresonances=1)
             RR(i)=RSDATn_qedc23(s,IER,Ismooth)
             RG(i)=RS3GFUN_qedc23(s,IER,Ismooth)
C separate narrow resonance contribution needed in flavor splitting for SU(2) coupling  
             RES(i)=rebw
          enddo
          IER=IER_SAV
          rerrdersta=sqrt(RR(2)**2)
          rerrdersys=sqrt(RR(3)**2)
          rerrdegsta=sqrt(RG(2)**2)
          rerrdegsys=sqrt(RG(3)**2)
          rder=RR(1)
          rdeg=RG(1)
       else 
          rerrdersta=null
          rerrdersys=null
          rerrdegsta=null
          rerrdegsys=null
          rder=null
          rdeg=null
       endif
       ider   =fac*rder
       ideg   =fac2*rdeg
       ierrdersta=abs(fac*rerrdersta)
       ierrdersys=abs(fac*rerrdersys)
       ierrdegsta=abs(fac2*rerrdegsta)
       ierrdegsys=abs(fac2*rerrdegsys)
       cder=DCMPLX(dder,ider)
       cdeg=DCMPLX(ddeg,ideg)
       cerrdersta=DCMPLX(derrdersta,ierrdersta)
       cerrdersys=DCMPLX(derrdersys,ierrdersys)
       cerrdegsta=DCMPLX(derrdegsta,ierrdegsta)
       cerrdegsys=DCMPLX(derrdegsys,ierrdegsys)

       IOR=IOR_SAV
       IRESON=IRESON_SAV
       iresonances=iresonances_sav
       iso=iso_sav
       IGS=IGS_SAV

       return
       end
C
      FUNCTION RSDATn_qedc23(s,IER,Ismooth)
C      --------------------------------
c interface for switching between data and fits
c returns NULL in chosen pQCD ranges [RCUTS]
      IMPLICIT NONE
      INTEGER IER,IER1,Ismooth
      double precision E,S,RSDATn_qedc23,RS40_qedc23,RS40smoothed_qedc23
      EXTERNAL RS40_qedc23,RS40smoothed_qedc23
      COMMON/ERR_qedc23/IER1
      IER1=IER
      E=SQRT(S)
      if (Ismooth.eq.1) then
         RSDATn_qedc23=RS40smoothed_qedc23(s,IER)
      else
         RSDATn_qedc23=RS40_qedc23(s,IER)
      endif
      RETURN
      END
C
       FUNCTION RS3GFUN_qedc23(s,IER,Ismooth)
C      -----------------
c interface for imaginary part of 3 gamma correlator (deg)
c Update Decembber 2014: new flavor separation and recombination 
c redesigned after cross check on the lattice by Harvey Meyer        
       IMPLICIT NONE
       INTEGER IER,Ismooth
       double precision E,S,RS3GFUN_qedc23,RS3G_qedc23,RS3Gsmoothed_qedc23
       EXTERNAL RS3G_qedc23,RS3Gsmoothed_qedc23
       E=SQRT(S)
       if (Ismooth.eq.1) then
          RS3GFUN_qedc23=RS3Gsmoothed_qedc23(s,IER)
       else
          RS3GFUN_qedc23=RS3G_qedc23(s,IER)
       endif
       RETURN
       END
C
       FUNCTION RS33FUN_qedc23(s,IER,Ismooth)
C      -----------------
c interface for imaginary part of 3 3 correlator (dgg)
c Update Decembber 2014: new flavor separation and recombination 
c redesigned after cross check on the lattice by Harvey Meyer        
       IMPLICIT NONE
       INTEGER IER,Ismooth
       double precision E,S,RS33FUN_qedc23,RS33_qedc23,RS33smoothed_qedc23
       EXTERNAL RS33_qedc23,RS33smoothed_qedc23
       E=SQRT(S)
       if (Ismooth.eq.1) then
          RS33FUN_qedc23=RS33smoothed_qedc23(s,IER)
       else
          RS33FUN_qedc23=RS33_qedc23(s,IER)
       endif
       RETURN
       END

C tools for calculating R(s) in pQCD, needed for imaginary part of complex alpha_em
c
C Download the Harlander-Steinhauser program rhad from http://www.rhad.de/
c expanding the tar file creates directory ./rhad-1.01/ with all files
c put the following subroutine into that directory and compile it like the example
c..   by Robert V. Harlander and Matthias Steinhauser,
c..   Comp. Phys. Comm. 153 (2003) 244,
c..   CERN-TH/2002-379, DESY 02-223, hep-ph/0212294.
c..   Changes: [March 30, 2009] v1.01
c..            - exact results at alphas^4 included
c
c choose option lverbose = .false. in parameters.f
c you may also want to switch off warnings on non-perturbative regions
c just before format statement 2003 in rhad.f, I use iwarn flag to switch it off
c         if (iwarn.eq.1) then
c            write(6,2003) dsqrt(scms)
c         endif
c also change in parameters.f iunit = 6 to something not used by this package
c copy rqcdHSn.f from current directory to ./rhad/ compile and link it

