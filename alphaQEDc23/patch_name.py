import re
import tqdm
rhad = "rhad"


src = [
  'funalpqedcx.f',
  'leptons.f',
  'hadr5n23.f',
  'dggvapx.f',
  'constants.f',
  'constants_qcd.f',
  'Rdat_all.f',
  'Rdat_fun.f',
  'Rdat_fit.f',
  'resonances_dat.f',
  'chadr5n23.f',
  'cggvapx.f',
  'resonances_renc.f',
  'RS3G_fun.f',
  'RS33_fun.f',
  'RS3G_fit.f',
  'RS33_fit.f',
  'RSN2_fun.f',
  'resfitfunx.f',
  'romegafun.f',
  'rphifun.f',
  'rphifun3g.f',
  'rphifun33.f',
  'cerrortot.f',
  'alphaQEDc23.f',
]

src += [
  rhad + '/r012.f',
  rhad + '/r34.f',
  rhad + '/runal.f',
  rhad + '/funcs.f',
  rhad + '/vegas-rhad.f',
  rhad + '/parameters.f',
  rhad + '/rhad.f',
  rhad + '/rqcdHSn.f'
]

src += [
  'common.h',
  'xRdat-extended.f',
  'Rdat_upd.f',
  'epemrdata1n_updatehigh.f',
  'epemfit1432.h',
  'epemfit079081.h',
  'epemfit32cut.h',
  'epemfit0814new.h',
  'ffppmfit0810.h',
  'ffppg2fit0810.h',
  'rfitrhohig.h',
  'rfitrholow.h',
  'epem3gfit079081.h',
  'epem3gfit0814new.h',
  'epem3gfit1425.h',
  'r3gfitrholow.h',
  'r3gfitrhohig.h',
  'epem33fit079081.h',
  'epem33fit0814new.h',
  'epem33fit1425.h',
  'r33fitrhohig.h',
  'r33fitrholow.h',
  'epemggfit079081.h',
  'epemggfit0814new.h',
  'epemggfit1425.h',
  'rggfitrholow.h',
  'rggfitrhohig.h',
  'epemn2fit079081.h',
  'epemn2fit0814new.h',
  'epemn2fit1425.h',
  'rn2fitrhohig.h',
  'rn2fitrholow.h',
  'rfitchptail.h',
  'fpi2fitchptail.h',
  'reso_reno.dat',
  'xRdat-spacelikehigh.f',
  'omegafit.h',
  'phifit.h',
  'phifit3g.h',
  'phifit33.h',
  'dalhadslow23_5.f',
  'deghadslow23_5.f',
  'dalhadshigh23_5.f',
  'deghadshigh23_5.f',
  'dalhadt23_5.f',
  'deghadt23_5.f',
  'dalhadthigh23_5.f',
  'deghadthigh23_5.f',
  'omfiRdat.f',
]

def read(fn):
    print(fn)
    with open(fn, "r") as fp:
        return fp.read()


def search(needle):
    return set(
        map(
            lambda x: x.lower(),
            re.findall(needle, allcode, re.M | re.I),
        )
    )


allcode = "".join([read(f) for f in src])

subroutines = search(r"^ *subroutine +([^\(\n]*)[\(\n]")
called_subroutines = search(r"^[^c].*call +([^\(\n ]*)[\(\n]") - {'derivs', 'vegas1'}
functions = search(r"^ *function +([^\(\n]*)[\(\n]")
commonblocks = search(r"common */([^/]*)/")

print(functions)
def add_suffix(s):
    return s.group(1) + "_qedc23" + s.group(2)
def add_suffix1(s):
    return s.group(1) + "_qedc23"


for i in tqdm.tqdm(src):
    with open(i) as fp:
        code = fp.read()

    code = re.sub(r"^([^c] *subroutine +[^\(\n]*)([\(\n ])", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c].*\bcall +[^\(\n ]*)([\(\n ])", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c] *common */[^/]*)(/)", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c] *save */[^/]*)(/)", add_suffix, code, flags=re.I|re.M)
    for fn in sorted(functions, key=lambda a: -len(a)):
        code = re.sub(r"(\b"+fn+r"\b)", add_suffix1, code, flags=re.I|re.M)

    with open(i, "w") as fp:
        fp.write(code)
