
c      ++++ epemn2fit1425 ++++  

c   1.375 -- 1.841 update oct 2022 
       DATA areslow/   
     &      0.349803d0,-0.019629d0,-0.054134d0, 0.010237d0,-0.004325d0,
     &     -0.000092d0, 0.007253d0, 0.000182d0,-0.000730d0,-0.001946d0,
     &     -0.001029d0,-0.000667d0/
       DATA astalow/   
     &      0.004728d0, 0.001150d0,-0.000294d0, 0.000135d0,-0.000256d0,
     &      0.000156d0, 0.000012d0,-0.000003d0, 0.000004d0,-0.000078d0,
     &      0.000012d0, 0.000005d0/
       DATA asyslow/   
     &      0.011805d0, 0.001192d0,-0.001209d0, 0.000391d0,-0.000414d0,
     &      0.000266d0, 0.000326d0, 0.000275d0,-0.000190d0,-0.000167d0,
     &     -0.000140d0, 0.000113d0/
      DATA emima1 /
     &      1.375000d0, 1.838000d0/

c   1.841 -- 2.000 update oct 2022 
       DATA ares/      
     &      0.307383d0, 0.024817d0, 0.012182d0, 0.006143d0,-0.001676d0,
     &      0.000849d0,-0.000339d0, 0.000378d0, 0.000689d0,-0.001418d0,
     &      0.000113d0, 0.000000d0/
       DATA asta/      
     &      0.006813d0, 0.000053d0,-0.000799d0, 0.000179d0,-0.000162d0,
     &      0.000108d0,-0.000025d0,-0.000046d0, 0.000065d0,-0.000050d0,
     &      0.000002d0, 0.000000d0/
       DATA asys/      
     &      0.013920d0, 0.002148d0, 0.000760d0, 0.000359d0, 0.000130d0,
     &     -0.000037d0,-0.000178d0,-0.000025d0, 0.000015d0,-0.000118d0,
     &     -0.000054d0, 0.000000d0/
      DATA emima2 /
     &      1.838000d0, 2.000000d0/

c   2.000 -- 2.130 update oct 2022 
       DATA fres2023 / 
     &      0.410317d0, 0.024171d0,-0.014958d0, 0.009759d0,-0.006742d0,
     &      0.004658d0/
       DATA fsta2023 / 
     &      0.008406d0, 0.000507d0,-0.000545d0, 0.000439d0,-0.000422d0,
     &      0.000298d0/
       DATA fsys2023 / 
     &      0.020693d0, 0.002031d0,-0.001148d0, 0.000067d0,-0.000093d0,
     &      0.000385d0/
      DATA emima3 /
     &      2.000000d0, 2.125000d0/
