c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; alphaQEDc17.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Fri Jul  7 23:59:48 2017
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Tue Mar 28 01:26:42 2023
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2019 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      PROGRAM alphaQEDc23  ! updated jan 2023
C Calculating the running QED coupling alpha(E)
C Leptons: 1--loop, 2--loop exact, 3--loop in high energy approximation
C Quarks form routine dhadr5n including effective s--channel shifts in
c low energy range
C r1....,r2....,r3.... : oneloop, twoloop, threeloop result
C ..real: realpart, ..imag: imaginary part;
C ......l: light fermions only (high energy approximation for 3--loops)
C dallepQED1n=r1real,dallepQED2n=r2real,dallepQED3l_qedc23=r3reall
C dallepQED3l_qedc23 also returns oneloop and twoloop high energy (light
c fermion) approximations 
c 21/09/2009: common pQCD parameter setting via constants_qcd.f FJ 
c 28/01/2012: recalculated Nall=2055 --> 2177
c 29/01/2012: common resonance parameters setting via resonances_dat.f
c 21/09/2012: new option for W contribution: iOSW=0 strict MSbar, iOSW=1
c including high energy asymptotic constant of the OS W contribution at
c s>>MW2 
c 12/12/2013: 
c new flavor separation scheme implemented, perturbative rewighting
c (equivalent to limit where OZI violation in uds sector would be
c negligible) has been disproved by lattice data (Maiz group by H. Meyer
c and Zeuthen group by K. Jansen) affects "deg" from hadr5n.f and
c effective sin^2 Theta calculations
c 26/08/2017:
c hadr5n17.f version now provides statistical and systematic errors
c separately as is required for a proper error estimate of integrals via
c intRdatx.f 
c 28/12/2019: 
c hader5n19.f data upgrade particularly in the 1.4 to 2.1 GeV region;
c improved flvor separation into I_0,I_1 and I_s, by modified nonet vs
c ideal us vs s mixing, in dataset0821.f routione.

      implicit none
      integer i,j,N,stflag,logflag,Ismooth,Nall,ndat,N1,N2,ifullrange,
     &     iOSW,iomegaphidat,iintRd,IORIN,NFIN
C      parameter(Nall=2187)
      parameter(Nall=2193)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB),XXX(NB)
      real*8 dggvapx_qedc23,s,e,de,null,Dalphaweak1MSb,cvap2,rvap2
      real*8 st2,als,mtop,dvpt_higx,dvpt_lowx
      real*8 dalept,dahadr,daltop,dvpt_new,dvpt_hig,dvpt_low
      real*8 emax,emin,elogl,elogu,delog,elog,fep,fem,fractionalerror
      real *8 alphip,alphim,alphic, alphie
      double precision CHPTCUT,EC,ECUT,EOMM1,EOMP1,EFIM1,EFIP1
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      double complex calept,cahadr,caltop,cvpt_new,cvpt_hig,cvpt_low
      double precision r1real,r2real,r3reall,r1imag,r2imag,r3imagl
      double complex alphac,alphah,alphal,alphact,alphaht,alphalt,
     &     calept1,cahadr1,caltop1,cggvapx_qedc23,cone,calp,cDalphaweak1MSb
      double complex cerror,cerrorsta,cerrorsys,comPi
      REAL*8 eps,PiQ2,ePiQ2
      double complex r1c,r1l,r1h
      double precision reno,renl,renh,BWRENO_qedc23,renfac,fracerror,edhadr
      external cggvapx_qedc23,BWRENO_qedc23
********************* extended header for real, complex as well as resonance data access **************
      REAL*8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI
      REAL*8 PSI(6),MPS(6),GPS(6),PPS(6),YPI(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 RMA(15),RGA(15),RPA(15),EREM(15),EREG(15),EREP(15),
     &     EMI(15),EMA(15)
      integer nx,ny
      parameter(nx=400,ny=15)
      integer nres(ny),ini_reso_reno_dat
c      double complex r1cx(nx,ny)
      double precision renpts(nx,ny),renofa(nx,ny),fracerr(nx,ny)
      double complex aleptc,rlc
      double precision renolept,e2QED      
      include 'common.h'      
c results detailed
      common /resu_qedc23/dalept,dahadr,daltop,Dalphaweak1MSb
      common /cres_qedc23/calept,cahadr,caltop,cDalphaweak1MSb
      common /lep123_qedc23/r1real,r2real,r3reall,r1imag,r2imag,r3imagl
c QCD parameters pQCD version of R(s), top contribution      
      common/var1_qedc23/pi1,ALINP,ERRAL,EINP,MTOP1,IORIN,NFIN
      common /parm_qedc23/st2,als,mtop
c commons needed to get R(s) , where to use pQCD, data vs fits
      COMMON/RCUTS_qedc23/CHPTCUT,EC,ECUT
      COMMON/SMOO_qedc23/Ismooth
      COMMON/IOSW_qedc23/iOSW
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
c parameters resonances
      COMMON/BWRO_qedc23/MRO,GRO,PRO/BWOM_qedc23/MOM,GOM,POM/BWFI_qedc23/MFI,GFI,PFI
      COMMON/PSYPPAR_qedc23/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESALL_qedc23/RMA,RGA,RPA,EREM,EREG,EREP
      COMMON/RESRAN_qedc23/EMI,EMA
      common/renormBW_qedc23/renpts,renofa,fracerr,nres,ini_reso_reno_dat
      call constants_qedc23()
      call constants_qcd_qedc23() ! fills commons var and RCUTS
      call resonances_data_qedc23() ! fills commons resonance parameters
c      sin2ell=0.23153 ! pm 0.00016 LEPEEWG Phys Rep 427 (2006) 257
      st2=0.23153d0   ! Reference value for weak mixing parameter
      e2QED=4.d0*pi*alp
      als=ALINP       ! alpha strong
      mtop=MTOP1      ! top quark mass
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
      ini_reso_reno_dat=1 ! recalculate renormalization factors table via alphaQED via resonsnces_count.f
C      ini_reso_reno_dat=0 ! use renormalization factors table reso_reno.dat
c following two entried set as default in chad5n14.f
c      IRESON=1        ! include narrow resonances in fits
c      iresonances=1   ! include narrow resonances in data sets
      call Rdata_qedc23()     ! R data compilation
c      call resonances_renoreal()
      call resonances_renocomplex_qedc23() ! fill common renormBW VP subtraction on physical resonances
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment in main the following 2 lines for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau
c      iLEP=LFLAG_qedc23(LEPTONflag)
************************************************************************
c the parameter xMW [provided via common.h] allows to change W threshold default M_W, e.g. to xMW=2.d0*MW
c      xMW=null is set in constants ! default M_W
      xMW=2.d0*MW
c      xMW=MZ
c local option for including W contribution
      iOSW=0          ! =0 W in MSbar, =1 W in OS at s>>MW2
      eps=0.1d0
      j=1
      null=0.d0
      cone=DCMPLX(1.d0,null)
      calp=DCMPLX(alp,null)
      N=4000
c     Scan with energy point raster xRdat-extended
      iomegaphidat=1
      if (iomegaphidat.eq.1) iresonances=0
      if (iomegaphidat.eq.1) IRESON=0
      ifullrange=1

c      ndat=1 low energy time-like data points extended by resonance scans
c      ndat=2 pQCD tail
c      ndat=3 test resonances at peak
c      ndat=-1 space-like region
      ndat=1
      ndat=0
c      ndat=3
c
      include 'xRdat-extended.f' ! just DATA X/.../ statement
      include 'xRdat-spacelikehigh.f' ! just DATA XXX/.../ statement
C      do i=1,Nall 
C         write (1001,*) I,X(I)
C      enddo
c      
c caution: logatithmic scaling used below: cannot choose emin=0 or emax=0 !!!!
c      
      logflag=0
      stflag =1
c     for spacelike region: stflag=-1
c      stflag =-1
c threshold 0.27914036d0
      emin=1.d-6
      emin=.27d0
      emax=11.5d0
      emin=-5.d0
      emax= 5.d0
c      emax=91.19d0
c J/psi,psi's
c      emin=3.09660 ! xmin psi1
c      emax=3.09720 ! xmax psi1
c      emin=3.68500 ! xmin psi2
c      emax=3.68700 ! xmax psi2
c      emin=3.72000 ! xmin psi3
c      emax=3.84000 ! xmax psi3
c      emin=3.96000 ! xmin psi4
c      emax=4.10000 ! xmax psi4
c Upsilon's
c 9460.30D
c      emin= 9.4600            ! xmin ups1
c      emax= 9.4606            ! xmax ups1
c      emin=10.023200 ! xmin ups2
c      emax=10.023320 ! xmax ups2
c      emin=10.354600 ! xmin ups3
c      emax=10.355800 ! xmax ups3
      write (*,*) ' Prepared scans with energy point raster ',
     &     'xRdat-extended'
      write (*,*) ' ndat=1 low energy time-like data points ',
     &     'extended by resonance scans'
      write (*,*) ' ndat=2 pQCD tail'
      write (*,*) ' ndat=3 test resonances at peak'
      write (*,*) ' ndat=-1 space-like region'
      write (*,*) ' ndat=-2 space-like region high'
      write (*,*) ' ndat=0 prompt for individual range'
      write (*,*) ' Enter ndat,iOSW:'
      read  (*,*) ndat,iOSW
      if (ndat.eq.0) then
      write (*,*) ' Enter energy range and number of points: ',
     &     'emin,emax,N' 
      read (*,*) emin,emax,N
      endif
      if (ndat.eq.1) then
      write (*,*) ' Enter energy range: ',
     &     'emin,emax' 
      read (*,*) emin,emax
      endif
      if (ndat.eq.0) then
         write (*,*) ' stflag=1/-1/-2 [time/space-like/',
     &        'space-like reverted] ',
     &        'logflag=0/1 [linear/log scale]'
         write (*,*) ' Enter stflag,logflag:'
         read (*,*) stflag,logflag
      endif
      if (ndat.eq.0) then
         N1=1
         N2=N+1
      else if (ndat.eq.1) then
         call getindex_qedc23(emin,Nall,X,N1)
         call getindex_qedc23(emax,Nall,X,N2)
         if (ifullrange.eq.1) then
            N1=1
c            N2=Nall+1
            N2=Nall
         endif
      else 
c      if (ndat.eq. 1).and N=Nall-1
         if (ndat.eq. 2) N=NB-1
         if (ndat.eq.-1) N=NA-1
         if (ndat.eq.-2) N=NB-1
c      if (ndat.eq. 3) N=45-1
         if (ndat.eq. 3) N=15-1
         N1=1
         N2=N+1
      endif
c write header for graphx plot program
      do j=1,4
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     real parts'
      enddo
      do j=11,14
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     imaginary parts'
      enddo
         write (8,*) '      3.0000'
         write (8,*) '      1.0000'
         write (8,*) '   ',float(N2-N1)
         write (8,*) '  |1-\Pi(s)|^2-1, (alpha/alpha(s))^2-1,',
     &        '|1-\Pi(s)|^2/(alpha/alpha(s))^2-1,',
     &        '|1-\Pi(s)|^2-(alpha/alpha(s))^2'
         write (9,*) '      3.0000'
         write (9,*) '      1.0000'
         write (9,*) '   ',float(N2-N1)
         write (9,*) ' |(alpha(s)_had+lep/alpha(0))|^2,',
     &        ' |(alpha(s)_lep/alpha(0))|^2'
         write (22,*) '      4.0000'
         write (22,*) '      1.0000'
         write (22,*) '   ',float(N2-N1)
         write (22,*) '     imaginary parts undressed'
         write (32,*) '      4.0000'
         write (32,*) '      1.0000'
         write (32,*) '   ',float(N2-N1)
         write (32,*) '     imaginary parts undressed'
         write (123,*) '     2.0000'
         write (123,*) '     1.0000'
         write (123,*) '   ',float(N2-N1)
         write (124,*) '     3.0000'
         write (124,*) '     1.0000'
         write (124,*) '   ',float(N2-N1)
         write (125,*) '     3.0000'
         write (125,*) '     1.0000'
         write (125,*) '   ',float(N2-N1)
c     ..................................................................
c     vacuum polarization  ...[c] central ...l low value, ...h high value
      write (1,*) ' Re: e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write (2,*) ' Re: e,alpha ,alphal ,alphah (no top)    '
C      write (2,*) ' e,alpha ,AVERAGE,ERROR,ERROR/AVERAGE    '
      write (3,*) ' Re: e,alphat,alphatl,alphath (with top)   ' 
      write (4,*) ' Re: e,dalept,dahadr,daltop,dalweak'
C      write (4,*) ' e,dahadr,cerror,cerror'
      write(11,*) ' Im: e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write(12,*) ' Im: e,alpha ,alphal ,alphah (no top)    '
      write(13,*) ' Im: e,alphat,alphatl,alphath (with top)   ' 
      write(14,*) ' Im; e,dalept,dahadr,daltop'
      write(124,*) ' e,leptons real parts 1,2,3-loops   ' 
      write(125,*) ' e,leptons imag parts 1,2,3-loops   ' 
c      write (1,*) ' e,DREAL(cvpt_new),DREAL(cvpt_low),DREAL(cvpt_hig)'
c      write (2,*) ' e,DREAL(alphac),DREAL(alphal),DREAL(alphah)'
c      write (3,*) ' e,1.d0/DREAL(alphact)'
c      write (4,*) ' e,DREAL(calept),DREAL(cahadr),DREAL(caltop)'
cc      write (4,*) ' e,DREAL(cahadr),DREAL(cerror),DREAL(cerror)'
c      write(11,*) ' e,AIMAG(cvpt_new),AIMAG(cvpt_low),AIMAG(cvpt_hig)'
c      write(12,*) ' e,AIMAG(alphac),AIMAG(alphal),AIMAG(alphah)'
c      write(13,*) ' e,1.d0/AIMAG(alphact)'
c      write(14,*) ' e,AIMAG(calept),AIMAG(cahadr),AIMAG(caltop)'
      if (ndat.eq.0) then
         de=(emax-emin)/(N-1)
         e=emin-de
      else 
         de=(emax-emin)/N
         e=emin
      endif
      elogl=dlog(dabs(emin))
      elogu=dlog(dabs(emax))
      delog=(elogu-elogl)/N
      if (ndat.eq.0) N2=N
      do 10 i=N1,N2
         if ((ndat.eq.1).and.(e.le.X(Nall))) then
            e=X(i)
            s=e*e
         else if ((ndat.eq.2).and.(e.le.ESX(NB))) then
            e=ESX(i)
            s=e*e
         else if (ndat.eq.3) then
            e=RMA(i)
            s=e*e
         else if ((ndat.eq.-1).and.(e.le.ETX(NA))) then
            e=ETX(i)
            s=e*abs(e)
         else if ((ndat.eq.-2).and.(e.le.XXX(NB))) then
            e=XXX(i)
            s=e*abs(e)
         else 
            if (logflag.eq.1) then
               if (stflag.eq.1) then
                  elog=elogl+delog*(i-1)
               else
                  elog=elogu-delog*(i-1)
               endif
               e=dexp(elog)
               s=e*e*stflag
               e=dsqrt(dabs(s))*stflag
            else
               e=e+de
               s=e*abs(e)
            endif
         endif
         if (stflag.eq.-2) s=-s
c
      cvpt_new= cggvapx_qedc23(s,cerror,cerrorsta,cerrorsys)
      cvpt_hig= cvpt_new+cerror
      cvpt_low= cvpt_new-cerror
c get hadronic error cerror is completely dominated by it
      if (iOSW.eq.1) then
         write (*,*) ' iOSW=',iOSW,'  W taken in asymtotic OS scheme'
         cDalphaweak1MSb=cDalphaweak1MSb+DCMPLX(-alp/pi/6.d0,null)
      endif
      alphac =calp/(cone-cvpt_new)
      alphah =calp/(cone-cvpt_hig)
      alphal =calp/(cone-cvpt_low)
      alphact=calp/(cone-cvpt_new-caltop-cDalphaweak1MSb)
      alphaht=calp/(cone-cvpt_hig-caltop-cDalphaweak1MSb)
      alphalt=calp/(cone-cvpt_low-caltop-cDalphaweak1MSb)
      r1c=calp/alphac
      r1h=calp/alphah
      r1l=calp/alphal
      reno=r1c*DCONJG(r1c)         ! undressing factor
      renh=r1h*DCONJG(r1h)         ! + error
      renl=r1l*DCONJG(r1l)         ! - error
c the same leptons only
      aleptc =calp/(cone-calept)
      rlc=calp/aleptc
      renolept=rlc*DCONJG(rlc)
      renfac=BWRENO_qedc23(s,fracerror)
c MW in MSbar vs MW OS for s>>MW2 -alpha/(6.d0*pi)
C      write (*,*) ' MW MSbar vs OS(HE):',Dalphaweak1MSb,Dalphaweak1MSb
C     &     -alp/(6.d0*pi)
C full vapo subtraction mainly for resonances parametrized in terms of
C "dressed" widths
c
c     delta alpha: leptons + 5 quarks no top
c
      write (1,98) e,DREAL(cvpt_new),DREAL(cvpt_low),DREAL(cvpt_hig)
      write(11,98) e,AIMAG(cvpt_new),AIMAG(cvpt_low),AIMAG(cvpt_hig)
c
c     alpha: leptons + 5 quarks no top
c
c      if (ndat.eq.0) then
c         alphip=1.d0/DREAL(alphalt)
c         alphim=1.d0/DREAL(alphaht)
c         alphic=(alphip+alphim)/2.d0
c         alphie=abs(alphip-alphim)/2.d0
c         write (2,98) e,1.d0/DREAL(alphac),
c     &        1.d0/DREAL(alphal),1.d0/DREAL(alphah),
c     &        alphic,alphie
c      else 
         write (2,98) e,DREAL(alphac),DREAL(alphal),DREAL(alphah)
C         write (2,98) e,DREAL(alphac),(DREAL(alphal)+DREAL(alphah))/2.D0
C     &        ,(DREAL(alphah)-DREAL(alphal))/2.D0,
C     &       (DREAL(alphah)-DREAL(alphal))/(DREAL(alphal)+DREAL(alphah))
         PiQ2=(0.072d0-DREAL(cahadr)/e2QED)/0.072d0*5d0
         ePiQ2=(0.05d0-(0.05d0-0.02d0)/4.d0*e**2)*PiQ2
         write (123,98) e**2,PiQ2,PiQ2-ePiQ2,PiQ2+ePiQ2
c      endif
      write(12,98) e,AIMAG(alphac),AIMAG(alphal),AIMAG(alphah)
      write(22,99) e,reno,renl,renh
      write(32,99) e,renfac,renfac*fracerror,renfac*fracerror
c      write(22,98) e,AIMAG(alphac)*reno,
c     &     AIMAG(alphal)*renl,AIMAG(alphah)*renh,reno
c
c     alpha: leptons + 5 quarks + top + W (1-loop MSbar)
c
c      if (ndat.eq.0) then
c         alphip=1.d0/DREAL(alphalt)
c         alphim=1.d0/DREAL(alphaht)
c         alphic=(alphip+alphim)/2.d0
c         alphie=abs(alphip-alphim)/2.d0
c         write (3,98) e,1.d0/DREAL(alphact),
c     &        1.d0/DREAL(alphalt),1.d0/DREAL(alphaht),
c     &        alphic,alphie
c      else 
         write (3,98) e,DREAL(alphact),
     &        DREAL(alphalt),DREAL(alphaht)
c      endif
      write(13,98) e,1.d0/AIMAG(alphact)
c
c     delta alpha: leptons, 5 quarks, top 
c
      cvpt_new= cggvapx_qedc23(s,cerror,cerrorsta,cerrorsys)

      edhadr=DREAL(cerror)
c      dahadr=DREAL(cahadr)

      write (4,98) e,DREAL(calept),DREAL(cahadr),DREAL(caltop),
     &     Dalphaweak1MSb
c      write (4,98) e,DREAL(cahadr),DREAL(cerror),DREAL(cerror)
      write(14,99) e,AIMAG(calept),AIMAG(cahadr),AIMAG(caltop)
c relative uncertainty       
      write(24,98) e,DREAL(cahadr),DREAL(cerror),
     &     DREAL(cerror)/DREAL(cahadr)
c
c     |1-\PI'(s)|
c
      comPi=calp/alphact
      cvap2 =DREAL(comPi*DCONJG(comPi))
      rvap2 =DREAL(comPi)**2
c      write (8,99) e,cvap2-1.d0,rvap2-1.d0,cvap2/rvap2-1.d0,cvap2-rvap2
      write (8,99) e,dlog10(1./cvap2),dlog10(1./rvap2),
     &     cvap2/rvap2,cvap2-rvap2
      write (9,99) e,1.d0/reno,1.d0/renh-1.d0/reno,1.d0/reno-1.d0/renl,
     &     1.d0/renolept
c
      write (124,98) e,r1real,r2real,r3reall
      write (125,98) e,r1imag,r2imag,r3imagl

 10   continue
c check Breit-Wigner renormalization factors initialized by "call resonances_renocomplex" in common 
c       common /renormBW/renpts,renofa,fracerr,nres
c must reproduce in resonance regions the following set
c      write(22,99) e,reno,renl,renh
c      write(32,99) e,renfac,renfac*fracerror,renfac*fracerror
c generated with alphaQEDcomplex with options ndat=1, emin=0.,emax=11.5 for example
      do j=2,ny
         write (50+j,*) '      3.0000'
         write (50+j,*) '      1.0000'
         write (50+j,*) '   ',float(nres(j))
         write (50+j,*) '     BW_qedc23 bare and renormalized'
         do i=1,nres(j)   
            write (50+j,99) renpts(i,j),renofa(i,j),fracerr(i,j)
         enddo
      enddo
c     ..................................................................
c detailed resonance scans require high resolution
 98   format(6(2x,1pe12.5))
 99   format(6(2x,1pe15.8))
      stop 10
      END
