      function resfitfunx_qedc23(s,a,na)
      implicit none
      integer ier,j,na
      double precision resfitfunx_qedc23,s,e,UGM2,sMeV,rbw,fac,BWfit_qedc23,
     &     M,G,P,rebw,polynomx_qedc23,amap,bmap,emin,emax,alpi,facbw,null
      double precision a(na),res(3)
      COMMON/ERR_qedc23/IER
      common /poly_qedc23/emin,emax,amap,bmap
      DATA alpi/   137.035999166d0/
      facbw=9.d0/4.d0*alpi**2
      fac=facbw
      null=0.d0
      UGM2=1.D6
      rbw =null
      e=sqrt(s)
      M=a(1)
      G=a(2)
      P=a(3)
      sMeV=s*UGM2
      if ((E.GE.emin).and.(E.LE.emax)) then
         rebw=fac*BWfit_qedc23(sMeV,M,G,P)
         resfitfunx_qedc23=rebw+polynomx_qedc23(amap*e+bmap,a,na)
      else 
         resfitfunx_qedc23=null
      endif
      return
      end
C
       FUNCTION BWfit_qedc23(S,M,G,P)
C      --------------------
C      BREIT WIGNER
C updated 09/12/14 NW --> relativistic BW_qedc23, modified s dependence extra s/M**2 
       implicit none
       REAL*8 BWfit_qedc23,S,M,M2,G,G2,P
       M2=M**2
       G2=G**2
       BWfit_qedc23=4.D0*G2*P*S/((S-M2)**2+M2*G2)*s/M2
C       BW_qedc23=G**2*P/((DSQRT(S)-M)**2+G**2/4.D0)
       RETURN
       END

      function polynomx_qedc23(x,a,na)
      implicit none
      INTEGER na,i
      REAL*8 x,ax,a,polynomx_qedc23,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      polynomx_qedc23=0.d0
      do i=1,na-3
         polynomx_qedc23=polynomx_qedc23+a(i+3)*t(i)
      enddo
      return
      end
