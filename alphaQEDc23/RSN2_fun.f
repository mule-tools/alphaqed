      FUNCTION RSN2_qedc23(s,IER)
c this function represents the I=1 (isospin symmetry limit, even number of pi's no omega) in the 33 correlator normalization 
c note the relative normalization <33>=<3g>/2=<gg>*9/40 in [ud] sector this is perturbative not accurate
c better use J_3=1/2 j_em thus Pi_33=1/4 Pi_rho,
c rho contribution in <gg> applies for all isovectro contributions
c <gg>=I_0+I_1+I_s + ...
c <3g>=1/2 I_1+3/4 I_s + ...
c <33>=1/4 I_1+9/16 I_s + ...
c <n2>=1/4 I_1 
      IMPLICIT NONE
      INTEGER i,j,jj,ini,IER,iresonances,IRESON,iintRd,
     &     NFext,NFext1,nf,ites
      DOUBLE PRECISION s,E,RS40_qedc23,RS33_qedc23,RSN2_qedc23,Rless,RN2less,R33less,RSNx_qedc23,BW_qedc23,
     &     null,fac,rbw,rebw,Rinicall,UGM2,sMeV,ELOW,EHIGH,
     &     RSGG1,RSN21,RN2,R3G,R33,F3G,FN2,RSN2pQCD_qedc23
      DOUBLE PRECISION res(3),fsta(5),fsys(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      INTEGER NN2,N
      PARAMETER(NN2=577)
      DOUBLE PRECISION XN2(NN2),YN2(NN2,3)
      DOUBLE PRECISION MOM,GOM,POM,MFI,GFI,PFI
      DOUBLE PRECISION CHPTCUTLOC,ERXYDAT,ECHARMTH,ECUTFIT
      real *8 mq(6),mp(6),th(6)
      real *8 rggnf2,rggnf2_alt,r3gnf2,r3gnf2_alt,r33nf2,r33nf2_alt
      COMMON/BWOM_qedc23/MOM,GOM,POM/BWFI_qedc23/MFI,GFI,PFI
      COMMON/RESRELERR_qedc23/FSTA,FSYS
c resonances flag iresonances=1 include resonances as data here; 0 include them
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc23/iresonances
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /RN2DAT_qedc23/XN2,YN2,N
      external RS40_qedc23,RS33_qedc23,RSNx_qedc23,BW_qedc23,RSN2pQCD_qedc23
c      common /rhadparts/ru,rd,rs,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,RN2less,R33less,NFext1,nf
      COMMON /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      common /quama_qedc23/mq,mp,th
      common /rggnf2special_qedc23/rggnf2,rggnf2_alt
      common /r3gnf2special_qedc23/r3gnf2,r3gnf2_alt
      common /r33nf2special_qedc23/r33nf2,r33nf2_alt
c      data ini /0/
c      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM_qedc23,BWFI_qedc23,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series
c
         call resonances_data_qedc23()
c
c called from resonances_dat.f
************************************************************************
         null=0.d0
         UGM2=1.D6
c         ini=1
c      endif
      J=IER
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo
      RSGG1=null
      RSN21=null
      E=SQRT(S)
      ELOW =XN2(1)                ! 0.318
c      EHIGH=XN2(NN2)              ! 2.125
c after dropping old incl data below lowest KEDR point
      EHIGH=2.12500d0                ! above EHIGH use incl data BES&KEDR
      CHPTCUTLOC=DMAX1(ELOW,CHPTCUTLOC)  ! do not go below recombined data set R3G0321.f
      ECUTFIT=DMAX1(EHIGH,ERXYDAT)       ! exhaust recombined data set R3G0321.f
      ECHARMTH=th(4)  ! 4.8 GeV
      RSN2_qedc23=null
      ites=0
      IF (E.LE.ELOW) THEN
         RSN2_qedc23=0.250d0*RS40_qedc23(s,J)  ! not 9/40 correct is 1/4
         RETURN
      ENDIF
      if ((E.GT.ELOW).and.(E.LE.EHIGH)) then
         call getindex_qedc23(E,NN2,XN2,I)
         if (I.LT.NN2) then
            res(j)=YN2(I,j)+(YN2(I+1,j)-YN2(I,j))
     &           /(XN2(I+1)-XN2(I))*(E-XN2(I))
            if (j.eq.3) then
               res(j)=res(j)*(YN2(I,1)+(YN2(I+1,1)-YN2(I,1))
     &              /(XN2(I+1)-XN2(I))*(E-XN2(I)))
            endif
         else
            res(j)=YN2(NN2,j)
            if (j.eq.3) then
               res(j)=res(j)*YN2(NN2,1)
            endif
         endif
c <N2> has no omega contribution skipped
c omega  .41871054d0,.810d0
c <N2> has no phi contribution skipped
c phi   1.00D0,1.04D0
         RSN2_qedc23=res(j)
         RETURN
      endif
      IF (E.GT.EHIGH) THEN
c test wether EHIGH is actually set in rhad produre as a stop if E < 1.8 GeV set by TH(3)
         if (E.LT.1.8d0) write (*,*) ' RSN2_qedc23: pQCD call, E,EHIGH',E,EHIGH
         IF (E.LE.ECUTFIT) THEN
            RSN2_qedc23=2.0d0/3.0d0*RS33_qedc23(s,J)
         ELSE IF (E.GT.ECUTFIT) THEN
            RSN2_qedc23=RSN2pQCD_qedc23(s,J)
         ENDIF
         RETURN
      ENDIF
      END
c
      FUNCTION RSN2pQCD_qedc23(s,IER)
c note: in data RSN2_qedc23=1/2 RS3G_qedc23 =RS33_qedc23 we must normalize it correspondingly 
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,R,R3G,R33,RSN2pQCD_qedc23,RSNx_qedc23,ALINP,EINP,MTOP,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc23/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      external RSNx_qedc23
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      if (IER.eq.1) then
         ALINP1=ALS
         R=RSNx_qedc23(S,R3G,R33)
         res(IER)=R33
      else if (IER.eq.2) then
         res(IER)=0.d0
      else if (IER.eq.3) then
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         R=RSNx_qedc23(S,R3G,R33)
         RSP=R33
         ALINP1=ALS-EST
         R=RSNx_qedc23(S,R3G,R33)
         RSM=R33
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
      else 
      endif
      RSN2pQCD_qedc23=res(IER)
      return
      end
c
      function RSNx_qedc23(s,R3Gout,R33out)
c as xRS_qedc23(s) with extended functionality calculating R3G and R33 as well
      implicit none
      logical NFlog
      integer  IOR,NF,NFext1,nf1,NFfixed,IORfixed
      real *8 pi,s,RSNx_qedc23,R,R3G,R33,xRS_qedc23,ALINP,EINP,MTOP
      real *8 Rless,R3Gless,R33less,R3Gout,R33out
      real *8 rggnf2,rggnf2_alt,r3gnf2,r3gnf2_alt,r33nf2,r33nf2_alt
      common/pqcdHS_qedc23/pi,ALINP,EINP,MTOP,IOR,NF
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf1
      common /rggnf2special_qedc23/rggnf2,rggnf2_alt
      common /r3gnf2special_qedc23/r3gnf2,r3gnf2_alt
      common /r33nf2special_qedc23/r33nf2,r33nf2_alt
      common /forcedNF_qedc23/NFlog
      NFlog=.true.
      NFfixed=2
      IORfixed=IOR
      call rqcdHS(s,R,ALINP,EINP,MTOP,IORfixed,NFfixed, 23)
      RSNx_qedc23=R
      R3Gout=0.450d0*R
      R33out=0.225d0*R
      return
      end
