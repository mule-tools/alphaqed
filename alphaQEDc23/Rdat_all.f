c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Rdat_all.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:00:42 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Tue Jun 27 01:18:31 2023
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2010 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      SUBROUTINE Rdata_qedc23()
c 2009 compilation of R(s) e^+e^- -> hadrons data
c arrays: E in GeV, R, delta R statistical, (delta R)/R systematic (fractional error) 
      IMPLICIT NONE
      integer ini,n1,n2,n2sav,nrep,i,j,ier
      INTEGER NCHP,NU0B,NU0Bd,NU1N,NU1Nx,NU2N,NBE,NR31N,NU7A,NHIGH,NU1E
      integer iniRdatupdatenoKLOE,iniRdatupdatenoBABA,Npipiom
c FJ 06/10/2009 BES region update NU1N=111 -> NU1N=130
c FJ 10/10/2010 BaBar/KLOE incl.  NU0B=128 -> 330; NLOW=24 -> 97 adjust in Rdat_fun.f 
c FJ 13/10/2012 NU0B=346->380 new omega phi background      
c FJ 04/06/2013 V01B and V01Bdat updated incl KLOE2012 data in average
c      PARAMETER(NCHP=18,NLOW=97,NU0B=337,NU0Bd=380,NU1N=134,NU2N=10,
c     &     NBE=152,NR31N=75,NU7A=46,NHIGH=258)
C mai 2021 NU0B=435 --> 476;     NU1N=147 --> NU1N=129
C     Nov 2022 U1N in Rdat_upd below 2.0 GeV (exclusive)
c     combined with XHH 2.0 - 3.2 GeV in epemrdata1n_updatehigh (inclusive)    
C in U1Nx 1.4 - 3.2 GeV  NU1Nx=136  replacing U1N in common EPEMALL  
C    NU1N=147 or 136   
      PARAMETER(NCHP=37,NU0B=476,NU0Bd=478,NU1N=147,NU1Nx=136,NU2N=13,
     &     NBE=100,NR31N=74,NU7A=47,NHIGH=266,NU1E=112,Npipiom=70)
      REAL*8 XCPT(NCHP),YCPT(NCHP,3),
     &     U0B(NU0B),V01B(NU0B,3),U0Bdat(NU0Bd),V01Bdat(NU0Bd,3),
     &     U1N(NU1N),V111N(NU1N),V121N(NU1N),V131N(NU1N),
     &     U1Nx(NU1Nx),V111Nx(NU1Nx),V121Nx(NU1Nx),V131Nx(NU1Nx),
     &     U2N(NU2N),V2N(NU2N,3),XBE(NBE),YBE(NBE,3),
     &     X31N(NR31N),Y31NA(NR31N),Y31NB(NR31N),Y31NC(NR31N),
     &     U7A(NU7A),V7A(NU7A,3),
     &     XXH(NHIGH),YYH(NHIGH,3),Xpipiom(Npipiom),Ypipiom(Npipiom,3)
      REAL*8 U1E(NU1E),V111E(NU1E),V121E(NU1E),V131E(NU1E)
      double precision null,err
      integer Nomega,Nphi
C      parameter(Nomega=133,Nphi=52)
      parameter(Nomega=82,Nphi=95)
      REAL *8 Xomega(Nomega),Yomega(Nomega,3),Xphi(Nphi),Yphi(Nphi,3)
      COMMON/EPEMALL_qedc23/XCPT,YCPT,
     &     U0B,V01B,U0Bdat,V01Bdat,
     &     U1Nx,V111Nx,V121Nx,V131Nx,
     &     U2N,V2N,XBE,YBE,
     &     X31N,Y31NA,Y31NB,Y31NC,
     &     U7A,V7A
      COMMON/RPIPIOM_qedc23/Xpipiom,Ypipiom
      COMMON/ROME_qedc23/Xomega,Yomega
      COMMON/RPHI_qedc23/Xphi,Yphi
      COMMON/EXIN_qedc23/U1E,V111E,V121E,V131E
      common /noKLOE_qedc23/iniRdatupdatenoKLOE
      common /noBABA_qedc23/iniRdatupdatenoBABA
      data ini/0/
c
C     
C  0.285       0.5 
C   |-----------|--
C       XCPT
C
C     0.318     0.61        1.4 
C      |---------|-----------|--
C
C          XLOW        U0B      
C
C  1.4         3.1         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        
c
C     1.8                  3.697 4.003  5.1                 13.5
C      |--------------------|-----|------|--------------------|--
c
c 49+1 2 pion threshold
c update feb 17 2011 FVCHPT2new 
c jan 2015 : R from dataset0821.f up to 400 MeV 17+1 point, icoulomb=2 i.e. incl pipi-FSR
c     jan 2021
c     18
c     32 -> 28 + 9 = 37
      DATA XCPT/
     &   0.27914036d0,
     &   0.28473D0,0.29031D0,0.29590D0,0.30148D0,0.30707D0,
     &   0.31266D0,0.31824D0,0.32383D0,0.32941D0,0.33500D0,
     &     0.34500D0,
     &  0.35500D0,0.35600D0,0.36000D0,0.36500D0,0.37000D0,0.37500D0,
     &  0.38000D0,0.38500D0,0.39000D0,0.39500D0,0.40000D0,0.40500D0,
     &  0.41000D0,0.41500D0,0.42000D0,0.42200D0,0.42500D0,0.43000D0,
     &  0.43500D0,0.43800D0,0.44000D0,0.44500D0,0.45000D0,0.45500D0,
     &     0.46000D0/
        DATA YCPT/   ! icoulomb=2 if =0 makes no difference
     &  0.00000D0,
     &  0.00201D0,0.00569D0,0.01046D0,0.01611D0,0.02251D0,
     &  0.02958D0,0.03724D0,0.04545D0,0.05415D0,0.06332D0,
     &     0.08420D0,
     &  0.10160D0,0.10300D0,0.11340D0,0.12010D0,0.13180D0,0.13970D0,
     &  0.15170D0,0.16410D0,0.17230D0,0.18230D0,0.19790D0,0.21290D0,
     &  0.22180D0,0.23040D0,0.24470D0,0.24740D0,0.25760D0,0.27050D0,
     &  0.28270D0,0.29220D0,0.29800D0,0.31220D0,0.32260D0,0.33240D0,
     &       0.34970D0,
c
     &  0.00000D0,
     &  0.00001D0,0.00007D0,0.00016D0,0.00031D0,0.00050D0,
     &  0.00074D0,0.00101D0,0.00131D0,0.00164D0,0.00200D0,
     &     0.00310D0,
     &  0.00360D0,0.00370D0,0.00430D0,0.00450D0,0.00410D0,0.00430D0,
     &  0.00460D0,0.00480D0,0.00490D0,0.00490D0,0.00500D0,0.00530D0,
     &  0.00540D0,0.00550D0,0.00570D0,0.00570D0,0.00590D0,0.00590D0,
     &  0.00600D0,0.00630D0,0.00640D0,0.00670D0,0.00680D0,0.00680D0,
     &       0.00710D0,
c
     &  0.00000D0,
     &  0.00000D0,0.00002D0,0.00004D0,0.00007D0,0.00011D0,
     &  0.00016D0,0.00022D0,0.00027D0,0.00034D0,0.00040D0,
     &     0.01790D0,
     &  0.01770D0,0.01720D0,0.01170D0,0.01150D0,0.00890D0,0.00910D0,
     &  0.00930D0,0.00940D0,0.00860D0,0.00850D0,0.00830D0,0.00860D0,
     &  0.00740D0,0.00670D0,0.00650D0,0.00560D0,0.00570D0,0.00570D0,
     &  0.00570D0,0.00580D0,0.00570D0,0.00580D0,0.00580D0,0.00570D0,
     &       0.00570D0/
C     
C  97 as U0A range extended from 0.81 to 0.613 GeV
C 122   IDM2: 1 INEW: 1 icmd2only: 0 isndkloe: 1 IKLOE: 0; created 08/07/2006
C new 125 update 27/02/2007
C 125 replaced 03/04/08
C FJ Aug 2009 omega subtraction below 0.81, new point close below 0.81
C = upper limit of background 0.8099 with value at previous point, previous point - 0.08
C one before - 0.02 to get smooth behavior after adding omega
c Follows phi BW_qedc23 R value at ends intreval
c  R phi at:  1.00   R=0.59249053281162756     
c  R phi at:  1.04   R=0.53204799395507063 
c phi background ajusted to endpoints taken to be linear in interval 1.00 - 1.004 
c updated 17/02/2011 renewed KLOE 2008
c         332 --> 331 (phi region modified) created 28/01/2012 incl. KLOE10 and BaBar pipi spectra
c 13/10/2012
c 346->380  omega and phi region expanded R0821 from dataset0821 run with iomegaphicut=1 OMEALL=PHIALL=0
c        DATA U0B/
c        DATA V01B/
c        DATA U0Bdat/
c        DATA V01Bdat/
c        DATA U1N/ ! created from dataset1431 with ICOM=1; excl and incl combined at end
c        DATA V111N/
c        DATA V121N/
c        DATA V131N/
c     DATA U1E/ ! created from dataset1431 with ICOM=0; 1.4 - 1.841 excl, 1.841 - 3.2 incl BES&KEDR
c        DATA V111E/
c        DATA V121E/
c        DATA V131E/
c comment on U1E version:
c     used above 1.841 combining excl and incl between 1.84 and 2.0 GeV above which pQCD is used
c     locally calculate V111N version for R3G0321.f,R330321.f,RN20321.f,RGG0321.f   
        include 'omfiRdat.f'
        include 'Rdat_upd.f'
c  incl special set pipi only in omega domain as background to omega resonance        
        data iniRdatupdatenoBABA/0/
        data iniRdatupdatenoKLOE/0/
C        include 'Rdat_upd_noBABA.f'
c        data iniRdatupdatenoBABA/1/
c        data iniRdatupdatenoKLOE/0/
cc begin no KLOE version comment away for all data version
c        include 'Rdat_upd_noKLOE.f'
c        data iniRdatupdatenoKLOE/1/
c        data iniRdatupdatenoBABA/0/
c     c end no KLOE version
c         258
c dataset0240.sh  ---> R0240.DAT ---> HADRO.DAT 

C        include 'epemrdata1n_updatehigh_noBESS22.f'
        include 'epemrdata1n_updatehigh.f'

C  1.4         3.2         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        

        save
C ranges adjusted after incl. BESIII 2022   16/10/2022 also XX -> XXH as creased by dataset0240        
        null=0.0d0
        if (ini.eq.0) then
c headers for graphx plot files [skip 3 with gnuplot]
c decomment appropriately for writing data
C           write (201,*) '        2.0000'
C           write (201,*) '        1.0000'
C           write (201,*) '     136.000000'    
C           write (202,*) '        2.0000'
C           write (202,*) '        1.0000'
C           write (202,*) '     136.000000'    
C           write (203,*) '        2.0000'
C           write (203,*) '        1.0000'
C           write (203,*) '      60.000000'    
c 
           n1=1                 ! 1.4
           n2=101               ! 2.0 - eps
           do i=n1,n2
              U1Nx(i)  =U1N(i)  
              V111Nx(i)=V111N(i)
              V121Nx(i)=V121N(i)
              V131Nx(i)=V131N(i)             
           enddo
           n2sav=n2
           n1=3                 ! 2.0
           n2=37                ! 3.2 
           do i=n1,n2
              j=n2sav+i-3+1
              U1Nx(j)  =XXH(i)  
              V111Nx(j)=YYH(i,1)
              V121Nx(j)=YYH(i,2)
              V131Nx(j)=YYH(i,3)
           enddo
Cc ckeck arrays           
C           n1=1                 ! 1.4
C           n2=136               ! 3.2
C           do j=n1,n2
C              err=sqrt(V121Nx(j)**2+(V131Nx(j)*V111Nx(j))**2)
C              write (201,'(1x,f8.3,3(2x,1pe11.3))')
C     &             U1Nx(j),V111Nx(j),err,err
C              err=sqrt(V121N(j)**2+(V131N(j)*V111N(j))**2)
C              write (202,'(1x,f8.3,3(2x,1pe11.3))')
C     &             U1N(j),V111N(j),err,err
C           enddo
C           n1=1
C           n2=266
C           do j=n1,n2
C              err=sqrt(YYH(j,2)**2+(YYH(j,3)*YYH(j,1))**2)
C              write (203,'(1x,f8.3,3(2x,1pe11.3))')
C     &            XXH(j),YYH(j,1),err,err
C           enddo
           n1=36                ! 3.1
           n2=48                ! 3.6
           do i=n1,n2
              j=i-n1+1
              U2N(j)=XXH(i)
              do ier=1,3
                 V2N(j,ier)=YYH(i,ier)
              enddo 
c              write (201,'(1x,f8.3,3(2x,1pe11.3))')
c     &             U2N(j),V2N(j,1),V2N(j,2),V2N(j,3)
           enddo 
c           write (201,'(1x,f8.3,3(2x,1pe11.3))')
c     &          null,null,null,null
c           write (201,*) ' U2N,V2N'
           n1=48                ! 3.6
           n2=147               ! 5.2
           do i=n1,n2
              j=i-n1+1
              XBE(j)=XXH(i)
              do ier=1,3
                 YBE(j,ier)=YYH(i,ier)
              enddo 
c              XBE(1)=(XBE(1)+XBE(2))/2.
C              write (202,'(1x,f8.3,3(2x,1pe11.3))')
C     &             XBE(j),YBE(j,1),YBE(j,2),YBE(j,3)
           enddo 
c           write (202,'(1x,f8.3,3(2x,1pe11.3))')
c     &          null,null,null,null
c           write (202,*) ' XBE,YBE'
           n1=147               ! 5.2
           n2=220               ! 9.5
           do i=n1,n2
              j=i-n1+1
              X31N(j)=XXH(i)
              Y31NA(j)=YYH(i,1)
              Y31NB(j)=YYH(i,2)
              Y31NC(j)=YYH(i,3)
C              write (203,'(1x,f8.3,3(2x,1pe11.3))')
C     &             X31N(j),Y31NA(j),Y31NB(j),Y31NC(j)
           enddo 
c           write (203,'(1x,f8.3,3(2x,1pe11.3))')
c     &          null,null,null,null
c           write (203,*) ' X31N,Y32NA,B,C'
           n1=220               !  9.5
           n2=266               ! 40.0
           do i=n1,n2
              j=i-n1+1
              U7A(j)=XXH(i)
              do ier=1,3
                 V7A(j,ier)=YYH(i,ier)
              enddo 
c              write (204,'(1x,f8.3,3(2x,1pe11.3))')
c     &             U7A(j),V7A(j,1),V7A(j,2),V7A(j,3)
           enddo 
c           write (204,'(1x,f8.3,3(2x,1pe11.3))')
c     &          null,null,null,null
c           write (204,*) ' U7A,V7A'
           ini=1
        endif
        RETURN
        END

