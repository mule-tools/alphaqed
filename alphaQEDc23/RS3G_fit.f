c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; R3Gdat_fit.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Mon Feb 20 16:00:34 2012
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Fri Oct 28 13:48:26 2022
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of function RS3G_fun.f interpolating R3G0321.f dataset
c using piecewise Chebyshev polynomial fits.
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc23 -> RCHPTnew_qedc23 via r3gfitchptail_qedc23
c            CHPTCUT  M_omega   =>  r3gfitrholow_qedc23
c            M_omega  EFITOMBG  =>  r3gfitrhohig_qedc23
c            EFITOMBG EFITMIN   =>  epem3gfit079081_qedc23
c            EFITMIN  1.4 GeV   =>  epem3gfit0814new_qedc23
c            1.4 GeV  3.2 GeV   =>  epem3gfit1425_qedc23
c  07/06/2013 changed flavor splittrig [us] component 9/10 --> 10/10
c  fit up to 2.125 GeV
c FJ 27/01/2023 phi resonance vs. background-only options
c major data update; new optionflags iomegaphidat,iintRd      
c new fit function omegaphir3gdat      
      function rs3gsmoothed_qedc23(s,IER)
      implicit none
      INTEGER j,IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,NFext,NFext1,nf,
     &     iomegaphidat,iintRd,IRESON,iresonances,iintRd1
      REAL *8 rs3gsmoothed_qedc23,RS3G_qedc23,RS40smoothed_qedc23,RS3x_qedc23,Rless,R3Gless,R33less,
     &     r3gfitchptail_qedc23,epem3gfit0814new_qedc23,rphifun3g_qedc23,rphifun_qedc23,frphi_qedc23,
     &     epem3gfit079081_qedc23,epem3gfit1425_qedc23,r3gfitrholow_qedc23,r3gfitrhohig_qedc23
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECHARMTH,RSBG,reno,
     &     RESULT,CHPTCUTLOC,EHIGH,efuture,futureprecision,
     &     RSGG1,RS3G1,R3G,R33,F3G,ERXYDAT
      REAL *8 EOMM1,EOMP1,EFIM1,EFIP1
      real *8 xc,xb,xt,facbw
      real *8 mq(6),mp(6),th(6)
      COMMON/MFPI_qedc23/MP2,M2/ERR_qedc23/IER1/POL_qedc23/POLP,POLM
      COMMON/CHAN_qedc23/ICH
      COMMON/RCUTS_qedc23/CHPTCUT,EC,ECUT
      COMMON/GUSA_qedc23/IGS,iso
      common/RESFIT_qedc23/facbw,IRESON,iintRd1   ! facbw defined in constants.f, common via common.h
      common /RES_qedc23/iresonances
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      EXTERNAL RS3G_qedc23,RS40smoothed_qedc23,RS3x_qedc23,r3gfitchptail_qedc23,
     &     epem3gfit0814new_qedc23,epem3gfit079081_qedc23,epem3gfit1425_qedc23,
     &     r3gfitrholow_qedc23,r3gfitrhohig_qedc23,rphifun3g_qedc23,rphifun_qedc23,frphi_qedc23
      common /future_qedc23/efuture,futureprecision,ifuture
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      common /quama_qedc23/mq,mp,th
      iintRd1=iintRd
      RSGG1=0.d0
      RS3G1=0.d0
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      xc=0.375d0
      xb=0.750d0
      xt=xc
      reno=1.d0
      IER1=IER
      E=DSQRT(S)
c common CUT set for R3Gdat_fit.f
      CHPTCUTLOC=0.320D0
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITMIN=0.81d0
      EFITMAX=1.4349d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
      ERXYDAT=2.125d0
c extend to get smooth mactch to rewighted RS40smooth
      ERXYDAT=2.130d0
      ECHARMTH=th(4)  ! 4.0 GeV
      eps=0.02d0
      RSBG=0.d0
      j=ier
C      RSBG=rphifun_qedc23(s,j)*0.63d0 ! 3/4*phi 83 % KKc+KKn 15% 3pi, 1% eta-gamma
C      RSBG=rphifun3g_qedc23(s,j)
      IF (E.LE.CHPTCUTLOC) THEN
         ICH=0
         RSBG=0.5d0*RS40smoothed_qedc23(s,IER)
      ELSE IF ((E.GT.CHPTCUTLOC).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=r3gfitrholow_qedc23(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         RSBG=r3gfitrhohig_qedc23(s)
      ELSE IF ((E.GT.EFITOMBG).AND.(E.LE.EFITMIN)) THEN
         ICH=3
         RSBG=epem3gfit079081_qedc23(s)
      ELSE IF ((E.GT.EFITMIN).AND.(E.LE.EFITMAX)) THEN
         ICH=4
         RSBG=epem3gfit0814new_qedc23(s)
      ELSE IF ((E.GT.EFITMAX).AND.(E.LE.ERXYDAT)) THEN
         ICH=5
         RSBG=epem3gfit1425_qedc23(s)
      ELSE IF ((E.GT.ERXYDAT).AND.(E.LE.ECHARMTH)) THEN
         ICH=6
         RSBG=0.5d0*RS40smoothed_qedc23(s,IER)
      ELSE IF (E.GT.ECHARMTH) THEN
         ICH=7
         if (nf.eq.3) then
            RSBG=0.5d0*RS40smoothed_qedc23(s,IER)
         else
            RSGG1=RS3x_qedc23(s,R3G,R33)
            F3G=R3G/RSGG1
            IF (IER.EQ.1) THEN
               if (nf.eq.4) then
                  RSBG=(RS40smoothed_qedc23(s,IER)-Rless)*xc+R3Gless
               else if (nf.eq.5) then
                  RSBG=(RS40smoothed_qedc23(s,IER)-Rless)*xb+R3Gless
               else if (nf.ge.6) then
                  RSBG=(RS40smoothed_qedc23(s,IER)-Rless)*xt+R3Gless
               endif
            ELSE
               RSBG=RS40smoothed_qedc23(s,IER)*F3G
            ENDIF
         endif
      ENDIF
c      CALL RENOoldnew(E,reno)
 10   RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
      rs3gsmoothed_qedc23=RESULT
      return
      end
C
      function epem3gfit0814new_qedc23(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,ier,IRESON,ini,iomegaphidat,iintRd,iintRd1
      parameter(np=8,nm=12)
      real *8 epem3gfit0814new_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc23,rphifun3g_qedc23,frphi_qedc23
      dimension aresl(nm),astal(nm),asysl(nm)
      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(np),astafr(np),asysfr(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP,CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      real *8 fac,rbw,BW_qedc23
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      real *8 emima1(2),emima2(2),emima3(2),emima4(2),emima5(2)
      external BW_qedc23,rphifun3g_qedc23,frphi_qedc23
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      COMMON/BWFI_qedc23/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd1
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd
      COMMON/ERR_qedc23/IER
      common /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      data ini /0/
      iintRd1=iintRd

c      ++++ epem3gfit0814new_qedc23 ++++

      include 'epem3gfit0814new.h'

      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.959d0
      EFIMX=1.000d0
      EFIPX=1.040d0
      EFIPP=ERXYDAT
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
C         e1=0.81d0
C         en=0.999999d0
         e1=emima1(1)
         en=emima1(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
C         e1=0.959d0
C         en=0.999999d0
         e1=emima4(1)
         en=emima4(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
C         e1=1.00001d0
C         en=1.03999D0
         e1=emima3(1)
         en=emima3(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresfr,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astafr,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysfr,np)
         endif
         if (iomegaphidat.eq.1) then
            yfit=rphifun3g_qedc23(s,ier)*1.027771d0
         else if ((iintRd.eq.0).and.(iomegaphidat.eq.2)) then
            yfit=rphifun3g_qedc23(s,ier)*1.027771d0
         else if ((IRESON.eq.1).and.(iomegaphidat.eq.0)) then
C     rbw=rphifun_qedc23(s,j)*0.63d0 ! 3/4*phi 83 % KKc+KKn 15% 3pi, 1% eta-gamma
c     but at phi peak KKc+KKn dominate               
            rbw=fac*BW_qedc23(sMeV,MFI,GFI,PFI)*0.61451d0
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
      else if (e.lt.1.435d0) then
C         e1=1.04000001d0
C         en=1.435d0
         e1=emima2(1)
         en=emima2(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: E out of fit range in epem3gfit0814new_qedc23'
      endif
      epem3gfit0814new_qedc23=yfit
      return
      end
c
      function epem3gfit079081_qedc23(s)
      implicit none
      integer np,ier
      parameter(np=7)
      real *8 epem3gfit079081_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ epem3gfit079081_qedc23 ++++

      include 'epem3gfit079081.h'

c omega  .41871054d0,.810d0
c no omega component in 3 gamma
      e=sqrt(s)
      yfit=0.d0
C      e1=0.787d0
C      en=0.80999d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.0.81d0).or.(e.lt.e1)) then
         epem3gfit079081_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      epem3gfit079081_qedc23=yfit
      return
      end
c
      function epem3gfit1425_qedc23(s)
      implicit none
      integer np,nm,nx,ier
      parameter(np=12,nm=6,nx=8)
      real *8 epem3gfit1425_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,aresmed,astamed,asysmed,
     &     ares,asta,asys,ares1,asta1,asys1,
     &     polynom_qedc23,CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      real *8 fres2023,fsta2023,fsys2023
      real *8 emima1(2),emima2(2),emima3(2),emima4(2),emima5(2)
      dimension areslow(np),astalow(np),asyslow(np)
      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(np),asta(np),asys(np)
      dimension ares1(np),asta1(np),asys1(np)
      dimension fres2023(nm),fsta2023(nm),fsys2023(nm)
      COMMON/ERR_qedc23/IER
      common /cufit_qedc23/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH

c      ++++ epem3gfit1425_qedc23 ++++

      include 'epem3gfit1425.h'

      e=sqrt(s)
      yfit=0.d0
      if ((e.lt.1.38d0).or.(e.gt.ERXYDAT)) then
         epem3gfit1425_qedc23=0.d0
         return
      else if (e.lt.1.841d0) then
c   1.375 -- 1.841 update dec 2020
C         e1=1.375d0
C         en=1.845d0
         e1=emima1(1)
         en=emima1(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asyslow,np)
         endif
      else if ((e.ge.1.841d0).and.(e.lt.2.00d0)) then
c   1.841 -- 2.000 update dec 2020
C         e1=1.841d0
C         en=2.000d0
         e1=emima2(1)
         en=emima2(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,ares,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,asta,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asys,np)
         endif
      else if ((e.ge.2.00d0).and.(e.lt.2.13d0)) then
c  2.00 -- 2.130 update jan 2021
C         e1=2.00d0
C         en=2.13d0
         e1=emima3(1)
         en=emima3(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,fres2023,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,fsta2023,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,fsys2023,nm)
         endif
      else
         write (*,*)  ' Warning: E out of fit range in epem3gfit1425_qedc23'
      endif
         epem3gfit1425_qedc23=yfit
      return
      end
c
      function r3gfitchptail_qedc23(s)
      implicit none
      integer ier
      real *8 r3gfitchptail_qedc23,s,rfitchptail_qedc23
      COMMON/ERR_qedc23/IER
      r3gfitchptail_qedc23=0.5d0*rfitchptail_qedc23(s)
      return
      end
c
      function r3gfitrholow_qedc23(s)
      implicit none
      integer np,ier
      parameter(np=14)
      real *8 r3gfitrholow_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ r3gfitrholow_qedc23 ++++

      include 'r3gfitrholow.h'

c omega  .41871054d0,.810d0
c 3G has no omega component
      e=sqrt(s)
      yfit=0.d0
C      e1=0.318d0
C      en=0.778d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         r3gfitrholow_qedc23=0.d0
         return
      endif
c 0.318 -- 0.778 updated nov 2010 (incl BaBar/KlOE) / jan 2012
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      r3gfitrholow_qedc23=yfit
      return
      end
c
      function r3gfitrhohig_qedc23(s)
      implicit none
      integer np,ier
      parameter(np=9)
      real *8 r3gfitrhohig_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ r3gfitrhohig_qedc23 ++++

      include 'r3gfitrhohig.h'

c omega  .41871054d0,.810d0
c no omega contribution in RS3G_qedc23
      e=sqrt(s)
      yfit=0.d0
C      e1=0.7690d0
C      en=0.7892d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         r3gfitrhohig_qedc23=0.d0
         return
      endif
c  0.769  -- 0.789 update jan 2012
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      r3gfitrhohig_qedc23=yfit
      return
      end

