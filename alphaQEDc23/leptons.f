c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*-
c ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; leptons.f ---
c;; Author : Friedrich Jegerlehner
c;; Created On : Thu Feb 13 01:34:24 2020
c;; Last Modified By:
c;; Last Modified On:
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2006 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
c functions for calculating the leptonic contributions to the shift of
c the fine structure constant in the physical on-shell renormalization scheme
c exact at one and two loops in the leading logarithmic approximation at three loops 
c i.e. modulo M^2_tau/s terms, the unknown exact 3-loop contribution is missing
c below the tau mass scale at about 1.8 GeV 
c dalQED1ferm_qedc23(s,m2,imag) exact 1-loop (Schwinger 1948)
c dalQED2ferm_qedc23(s,m2,imag) exact 2-loop (Kallen/Sabry 55)
c dallepQED3l_qedc23(s,r1real,r1imag,r2real,r2imag,r3imag) 3-loop (Steinhauser 98, PLB429(1998)158)
c included only above tau mass scale
c Remark about 4-loop result in the same approximation as 3-loop (Sturm 13, NPB874(2013)698)
c Delta alpha_lep (M^2_Z)= 0.0314192...+ 0.00007762...+ 0.00000106...+ 0.00000002...\,,
c i.e. the 4-loop contribution is negligible for our purpose

      function dallepQED1_qedc23(s,r1imag)
      implicit none
      integer i
      real *8 dallepQED1_qedc23,s,m2,r1real,r1imag,x0,y0,dalQED1ferm_qedc23,imag
      include 'common.h'
      x0  = 0.d0
      y0  = 0.d0
      if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
         dallepQED1_qedc23=0.d0
         r1imag=0.d0
         return
      endif
      if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'lep')) then
         do i=1,3 
            m2=ml(i)**2
            x0=x0+dalQED1ferm_qedc23(s,m2,imag)
            y0=y0+imag
         enddo
      else 
         iLEP=LFLAG_qedc23(LEPTONflag)
         m2=ml(iLEP)**2
         x0=x0+dalQED1ferm_qedc23(s,m2,imag)
         y0=y0+imag
      endif
      r1real=x0
      r1imag=y0
      dallepQED1_qedc23=r1real
      return
      end
C      
      function dalQED1ferm_qedc23(s,m2,imag)
      implicit none
      integer ini
      real *8 dalQED1ferm_qedc23,dalQED1,s,m2,imag,x
      real *8 s0,m2ds,m2ds2,lnm2ds,z,r,y,y2,y3,epy,emy,
     &        epysq,emysq,emy2,emy3,emy4,lny,abr1,ddilog_qedc23,dli3_qedc23,
     &        lnm2dsimag,lnyimag,tau,sint,sint2,sint3,cost
      real *8 null,one,two,four,eight,half,fourth,fourthird,
     &        twentyninth,sixteenthird,c1,c2
      include 'common.h'
      save
      data ini/0/
      if (ini.eq.0) goto 2
 1    continue
c switches for z=s/m2 input via common.h from constants.f
c      small=eps
c      large=one/eps
      s0=four*m2                       ! threshold
      z=s/m2
      x=s0/s
      imag=null
C LOW ENERGY ASYMPTOTE
      if (abs(z).lt.small) then 
c         write (*,*) ' LOW ENERGY ASYMPTOTE'
         dalQED1 = c1*z+c2*z*z
         goto 9
      endif
C TIME-LIKE BRANCH      
      if (z.gt.null) then
C HIGH ENERGY ASYMPTOTE (TIME-LIKE)
      if (z.gt.large) then
c         write (*,*) ' HIGH ENERGY ASYMPTOTE (TIME-LIKE)'
         m2ds=m2/s
         lnm2ds=log(m2ds)
         lnm2dsimag=null
         if (s.gt.s0) lnm2dsimag=pi
         dalQED1 =  -fourthird*lnm2ds-twentyninth
     &              +eight*m2ds*(one+m2ds*(lnm2ds-half))
         imag    =  lnm2dsimag*(-fourthird+eight*m2ds*m2ds)
      else if (s.gt.s0) then 
         r=sqrt(one-x)
         y=(r-one)/(r+one)
C for analytic continuation y = y + i epsilon
         y2=y*y
         epy=one+y
         emy=one-y
         epysq=one+y2
         emy2=emy*emy
         emy3=emy2*emy
         lny=log(abs(y))
         abr1=epysq-four*y
         lnyimag=null
         if (s.gt.s0) lnyimag=pi 
         dalQED1 = -twentyninth+sixteenthird*y/emy2
     &             -fourthird*epy*abr1/emy3*lny
         imag    = lnyimag*(-fourthird*epy*abr1/emy3)
      else 
         sint2=s/s0
         sint =dsqrt(sint2)
         sint3=sint2*sint
         tau=dasin(sint)
         cost=cos(tau)
         dalQED1 = -twentyninth + fourthird*
     &             (tau*cost*(one + two*sint2) - sint)/sint3
      endif
C SPACE-LIKE BRANCH      
      else
      if (-z.gt.large) then
C HIGH ENERGY ASYMPTOTE (SPACE-LIKE)
c         write (*,*) ' HIGH ENERGY ASYMPTOTE (SPACE-LIKE)'
         m2ds=-m2/s
         lnm2ds=log(m2ds)
         dalQED1 =  -fourthird*lnm2ds-twentyninth
     &              +eight*m2ds*(one+m2ds*(lnm2ds-half))
         imag    = null
      else 
         r=sqrt(one-x)
         y=(r-one)/(r+one)
C for analytic continuation y = y + i epsilon
         y2=y*y
         epy=one+y
         emy=one-y
         epysq=one+y2
         emy2=emy*emy
         emy3=emy2*emy
         lny=log(y)
         abr1=epysq-four*y
         lnyimag=null
         if (s.gt.s0) lnyimag=pi 
         dalQED1 = -twentyninth+sixteenthird*y/emy2
     &             -fourthird*epy*abr1/emy3*lny
         imag    = null
      endif
      endif
 9    dalQED1ferm_qedc23=dalQED1*adp*fourth
      imag=imag*adp*fourth
      RETURN
 2    null=0.d0
      one=1.d0
      two=2.d0
      four=4.d0
      eight=8.d0
      half=0.5d0
      fourth=0.25d0
      fourthird=4.d0/3.d0
      twentyninth=20.d0/9.d0
      sixteenthird=16.d0/3.d0
      c1=-4.d0/15.d0
      c2=-1.d0/35.d0
      ini=1
c      write (*,*) ' numerical constants initialized'
      goto 1
      END      


      integer function LFLAG_qedc23(Label) 
      implicit none
      character*3 Label
      integer i
      i=-3
      if (Label.eq.'had') i=-2 
      if (Label.eq.'lep') i=-1 
      if (Label.eq.'ele') i= 1 
      if (Label.eq.'muo') i= 2 
      if (Label.eq.'tau') i= 3 
      LFLAG_qedc23=i
      return
      end

      function dallepQED2_qedc23(s,r2imag)
C Kallen&Saby result 2--loop QED = 2--loop QCD divided by NC=3 and CF=4/3 
      implicit none
      integer i,nl
      real *8 dallepqed2_qedc23,s,x1,y1,x1l,r2real,r2imag,
     &        dalQED2ferm_qedc23,m2,imag,M1
      real *8 null,one,two,half,four,qua
      include 'common.h'
      data null,one,two,half,four,qua/0.d0,1.d0,2.d0,.5d0,4.d0,.25d0/
      x1  = null
      y1  = null
      if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
         dallepQED2_qedc23=0.d0
         r2imag=0.d0
         return
      endif
      nl=1
      iLEP=LFLAG_qedc23(LEPTONflag)
      if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'lep')) nl=3
      do i=1,nl
         M1=ml(i)
         if (nl.eq.1) M1=ml(iLEP)
C
         m2=M1**2
         x1=x1+dalQED2ferm_qedc23(s,m2,imag)
         y1=y1+imag
      enddo
      r2real=x1
      r2imag=y1
      dallepqed2_qedc23 = r2real
      return
      end
C      
      function dalQED2ferm_qedc23(s,m2,imag)
C Exact 2-loop result, originally by Kallen and Sabry 1955, 
C here based on a compact form worked out by M.Yu. Kalmykov 2003 
      implicit none
      integer ini
      real *8 dalQED2ferm_qedc23,dalQED2,s,m2,imag,x
      real *8 s0,m2ds,m2ds2,lnm2ds,z,omx,r,y,y2,y3,y4,epy,emy,
     &        epysq,emysq,emy2,emy3,emy4,lny,abr1,ddilog_qedc23,dli3_qedc23,
     &        lnm2dsimag,lnyimag,sint,sint2,sint3,sint4,sint4i,
     &        tau,cost,phi,chi,fac1,term2,Clausen2_qedc23,Clausen3_qedc23
      real *8 null,one,two,three,four,five,six,seven,eight,fourty,
     &        twentytwo,thirtytwo,fourtyeight,third,onesixteenth,
     &        tenthird,hundertfourthird,sixteenzeta3,eightthird,
     &        sixteenthird,thirtytwothird,sixtyfourzeta3,c1,c2,
     &        fourth,sixteen,fourteenthird,twentysixthird
      include 'common.h'
      save
      data ini/0/
      if (ini.eq.0) goto 2
 1    continue
C switches for z=s/m2 input via common.h from constants.f
c      small=eps
c      large=one/eps
      s0=four*m2                       ! threshold
      z=s/m2
      x=s0/s
      omx=one-x
      imag=null
      lnm2dsimag=null
C LOW ENERGY ASYMPTOTE
      if (abs(z).lt.small) then 
c         write (*,*) ' LOW ENERGY ASYMPTOTE'
         dalQED2 = c1*z+c2*z*z
         goto 9
      endif
C TIME-LIKE BRANCH      
      if (z.gt.null) then
C HIGH ENERGY ASYMPTOTE (TIME-LIKE)
      if (z.gt.large) then
c         write (*,*) ' HIGH ENERGY ASYMPTOTE (TIME-LIKE)'
         m2ds=m2/s
         m2ds2=m2ds*m2ds
         lnm2ds=log(m2ds)
         lnm2dsimag=pi
         dalQED2 =  -four*lnm2ds-tenthird+sixteenzeta3
     &              +fourtyeight*m2ds*lnm2ds
     &              -m2ds2*(eightthird+sixtyfourzeta3+fourty*lnm2ds
     &              -fourtyeight*(lnm2ds**2-lnm2dsimag**2))
         imag    = lnm2dsimag*(-four+fourtyeight*m2ds
     &              -m2ds2*(fourty-fourtyeight*two*lnm2ds))
      else if (s.gt.s0) then 
         r=sqrt(omx)
         y=(r-one)/(r+one)
C for analytic continuation y = y + i epsilon
         y2=y*y
         y3=y*y2
         y4=y*y3
         epy=one+y
         emy=one-y
         epysq=one+y2
         emysq=one-y2
         emy2=emy*emy
         emy3=emy2*emy
         emy4=emy2*emy2
         lny=log(abs(y))
         abr1=epysq-four*y
         lnyimag=pi 
         dalQED2 = (-tenthird + hundertfourthird*y/emy2
     &        + sixteenzeta3 *(one - four*y2/emy4)
     &    +(  + eightthird*y*(two+seven*y-twentytwo*y2+six*y3)*
     &        (lny**2-lnyimag**2)
     &        - four*emysq*(epysq-eight*y)*lny
     &      +(- sixteenthird*(log(emy)
     &          + two*log(epy))*(lny*(epysq*lny - two*emysq)
     &          -lnyimag*(epysq*lnyimag))
     &        + thirtytwothird*(ddilog_qedc23(y) + two*ddilog_qedc23(-y))
     &          *(emysq - two*epysq*lny)
     &        + thirtytwo*epysq*(dli3_qedc23(y) + two*dli3_qedc23(-y)))*abr1)/emy4)
         imag    = lnyimag*(
     &    +(  + eightthird*y*(two+seven*y-twentytwo*y2+six*y3)*two*lny
     &        - four*emysq*(epysq-eight*y)
     &      +(- sixteenthird*(log(emy) + two*log(epy))
     &        *(two*epysq*lny - two*emysq)
     &        - two*thirtytwothird*(ddilog_qedc23(y) + two*ddilog_qedc23(-y))*epysq
     &                                                  )*abr1)/emy4)
      else 
         sint2=s/s0
         sint =dsqrt(sint2)
         sint3=sint2*sint
         sint4=sint3*sint
         tau=dasin(sint)
         cost=cos(tau)
         phi=two*tau
         chi=pi-phi
         sint4i=one/sint4
         fac1=one-fourth*sint4i
         term2=cost*(one+two*sint2)/sint3
         dalQED2 = sixteen*
     &            (two*Clausen3_qedc23(phi)+four*Clausen3_qedc23(chi)+zeta3)*fac1
     &           + sixteenthird*(Clausen2_qedc23(phi)-two*Clausen2_qedc23(chi))*
     &            (eight*tau*fac1-term2)
     &           + thirtytwothird*(log(two*sint)+two*log(two*cost))*
     &            (two*tau*fac1-term2)*tau -tenthird
     &           + four*tau*cost*(three+two*sint2)/sint3
     &           - twentysixthird/sint2 + (fourteenthird/sint4
     &           + sixteenthird/sint2 - thirtytwo)*tau**2
         imag    = null
      endif
C SPACE-LIKE BRANCH      
      else
      if (-z.gt.large) then
C HIGH ENERGY ASYMPTOTE (SPACE-LIKE)
c         write (*,*) ' HIGH ENERGY ASYMPTOTE (SPACE-LIKE)'
         m2ds=-m2/s
         m2ds2=m2ds*m2ds
         lnm2ds=log(m2ds)
         dalQED2 =  -four*lnm2ds-tenthird+sixteenzeta3
     &              +fourtyeight*m2ds*lnm2ds
     &              -m2ds2*(eightthird+sixtyfourzeta3+fourty*lnm2ds
     &              -fourtyeight*lnm2ds**2)
         imag    = null
      else 
         r=sqrt(omx)
         y=(r-one)/(r+one)
C for analytic continuation y = y + i epsilon
         y2=y*y
         y3=y*y2
         epy=one+y
         emy=one-y
         epysq=one+y2
         emysq=one-y2
         emy2=emy*emy
         emy3=emy2*emy
         emy4=emy2*emy2
         lny=log(y)
         abr1=epysq-four*y
         dalQED2 = (-tenthird + hundertfourthird*y/emy2
     &        + sixteenzeta3 *(one - four*y2/emy4)
     &    +(  + eightthird*y*(two+seven*y-twentytwo*y2+six*y3)*lny**2
     &        - four*emysq*(epysq-eight*y)*lny
     &      +(- sixteenthird*(log(emy)
     &          + two*log(epy))*lny*(epysq*lny - two*emysq)
     &        + thirtytwothird*(ddilog_qedc23(y) + two*ddilog_qedc23(-y))
     &          *(emysq - two*epysq*lny)
     &        + thirtytwo*epysq*(dli3_qedc23(y) + two*dli3_qedc23(-y)))*abr1)/emy4)
         imag    = null
      endif
      endif
 9    dalQED2ferm_qedc23=dalQED2*adp2*onesixteenth
      imag=imag*adp2*onesixteenth
      RETURN
 2    null=0.d0
      one=1.d0
      two=2.d0
      three=3.d0
      four=4.d0
      five=5.d0
      six=6.d0
      seven=7.d0
      eight=8.d0
      sixteen=16.d0
      fourty=40.d0
      twentytwo=22.d0
      thirtytwo=32.d0
      fourtyeight=48.d0
      third=1.d0/3.d0
      fourth=0.25d0
      onesixteenth=1.d0/16.d0
      tenthird=10.d0*third
      hundertfourthird=104.d0*third
      sixteenzeta3=16.d0*zeta3
      eightthird=8.d0*third
      fourteenthird=14.d0*third
      sixteenthird=16.d0*third
      twentysixthird=26.d0*third
      thirtytwothird=32.d0*third
      sixtyfourzeta3=64.d0*zeta3
      c1=-328.d0/81.d0
      c2=-449.d0/675.d0
      ini=1
c      write (*,*) ' numerical constants initialized'
      goto 1
      END      
C
      function dallepQED3l_qedc23(s,r1real,r1imag,r2real,r2imag,r3imag)
C Warning: only included for large s >> m_tau**2
c      QED VP contribution 1-,2-,3-loop
c r1 1-loop contribution from light lepton
c r2 2-loop contribution from light leptons (Kallen/Sabry 55)
c r3 3-loop contribution from light leptons (Steinhauser 98, PLB429(1998)158)
c      M1,M2 on-shell masses;
c      M1 valence lepton (current-current fermion loop), M2 sea lepton (extra fermion loop)
c      Piq2  = adp/4.d0*(Pi_0+adp*Pi_1
c     &     + adp**2*(PiA_2+Pil+PiF_2+Pih_2))
      implicit none
      integer i,nl,testprint
      real *8 dallepQED3l_qedc23,s,q2,M1,M12,M22,sabs,sign,fac1,fac2,fac3
      real *8 alpha,Lnqmone,Lnqmtwo,Lnqmone2,M12dq2
      real *8 iLnqmone,iLnqmtwo
      real *8 r1real,r1imag,r2real,r2imag,r3real,r3imag
      real *8 rPi_0_qedc23,rPi_1_qedc23,rPi_2,rPiA_2_qedc23,rPil_2_qedc23,rPiF_2_qedc23,rPih_2_qedc23,rPiq2
      real *8 iPi_0,iPi_1,iPi_2,iPiA_2,iPil_2,iPiF_2,iPih_2,iPiq2
      real *8 e,x0,y0,x1,y1,z,
     &        rxa,ixa,rxf,ixf,rxle,rxhe,ipih_2m,ipih_2t,
     &        ixle,ixhe,rxlm,rxhm,ixlm,ixhm,rxlt,ipil_2e,ipil_2m,
     &        ixlt,rxht,ixht
      include 'common.h'
C testprint=1: print individual terms to fort.2, fort.3
      testprint=0
      fac1=-adp /4.d0
      fac2=-adp2/4.d0
      fac3=-adp3/4.d0
      sabs=dabs(s)
      e=sqrt(sabs)
      dallepQED3l_qedc23=0.d0
      r1real=0.d0
      r1imag=0.d0
      r2real=0.d0
      r2imag=0.d0
      r3real=0.d0
      r3imag=0.d0
      x0  = 0.d0
      y0  = 0.d0
      x1  = 0.d0
      y1  = 0.d0
      rxa = 0.d0
      ixa = 0.d0
      rxf = 0.d0
      ixf = 0.d0
      if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
         return
      endif
      nl=1
      iLEP=LFLAG_qedc23(LEPTONflag)
      if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'lep')) nl=3
      do i=1,nl
         M1=ml(i)
         if (nl.eq.1) M1=ml(iLEP)
         M12=M1**2
         z=s/M12
         if (abs(z).lt.large_3) then
           if (iwarnings.ne.0) then
             write (*,*) ' 3--loop high energy approximation'
             write (*,*) ' out of range: result=0.0 at energy= ',e
           endif
           return
         endif
         x0=x0+rpi_0_qedc23(s,M12,ipi_0)
         y0=y0+ipi_0
         x1=x1+rpi_1_qedc23(s,M12,ipi_1)
         y1=y1+ipi_1
         rxa=rxa+rpia_2_qedc23(s,M12,ipia_2)
         ixa=ixa+ipia_2
         rxf=rxf+rpif_2_qedc23(s,M12,ipif_2)
         ixf=ixf+ipif_2
         if (testprint.eq.1) then
         write (2,*) ' m:',M1,' A:',fac3*rpia_2_qedc23(s,M12,ipia_2),
     &                       ' F:',fac3*rpif_2_qedc23(s,M12,ipif_2)
         endif
      enddo
C light-heavy contribution:
C electron
      rxle= 0.d0 
      rxhe= 0.d0 
      ixle= 0.d0 
      ixhe= 0.d0 
      if ((iLEP.eq.-1).or.(iLEP.eq.1)) then
        rxhe=rpih_2_qedc23(s,ml(2)**2,ipih_2m)+rpih_2_qedc23(s,ml(3)**2,ipih_2t)
        ixhe=ipih_2m+ipih_2t
         if (testprint.eq.1) then
         write (2,*) ' ele:',
     &       ' h-m:',fac3*rpih_2_qedc23(s,ml(2)**2,ipih_2m),
     &       ' h-t:',fac3*rpih_2_qedc23(s,ml(3)**2,ipih_2t)
         endif
      endif
C muon
      rxlm= 0.d0
      rxhm= 0.d0
      ixlm= 0.d0
      ixhm= 0.d0
      if ((iLEP.eq.-1).or.(iLEP.eq.2)) then
         rxlm=rpil_2_qedc23(s,ml(2)**2,ml(1)**2,ipil_2) 
         rxhm=rpih_2_qedc23(s,ml(3)**2,ipih_2)
         ixlm=ipil_2
         ixhm=ipih_2
         if (testprint.eq.1) then
         write (2,*) ' muo:',
     &       ' l-e:',fac3*rpil_2_qedc23(s,ml(2)**2,ml(1)**2,ipil_2),
     &       ' h-t:',fac3*rpih_2_qedc23(s,ml(3)**2,ipih_2)
         endif
      endif
C tau
      rxlt= 0.d0
      ixlt= 0.d0
      rxht= 0.d0
      ixht= 0.d0
      if ((iLEP.eq.-1).or.(iLEP.eq.3)) then
        rxlt=rpil_2_qedc23(s,ml(3)**2,ml(1)**2,ipil_2e)
     &    +rpil_2_qedc23(s,ml(3)**2,ml(2)**2,ipil_2m) 
        ixlt=ipil_2e
     &    +ipil_2m
         if (testprint.eq.1) then
         write (2,*) ' tau:',
     &       ' l-e:',fac3*rpil_2_qedc23(s,ml(3)**2,ml(1)**2,ipil_2e),
     &       ' l-m:',fac3*rpil_2_qedc23(s,ml(3)**2,ml(2)**2,ipil_2m)
         endif
      endif
C summing up      
      rPi_2=rxa+rxf+rxle+rxlm+rxlt+rxhe+rxhm+rxht
      iPi_2=ixa+ixf+ixle+ixlm+ixlt+ixhe+ixhm+ixht
      if (testprint.eq.1) then
         write (3,*) LEPTONflag
         write (3,*) fac3*rxa,fac3*rxf
         write (3,*) fac3*rxle,fac3*rxlm,fac3*rxlt
         write (3,*) fac3*rxhe,fac3*rxhm,fac3*rxht 
      endif
C
      r1real=fac1*x0
      r1imag=fac1*y0
      r2real=fac2*x1
      r2imag=fac2*y1
      r3real=fac3*rPi_2
      r3imag=fac3*iPi_2
      dallepQED3l_qedc23=r3real
      return
      end
C
      function rpi_0_qedc23(s,M12,ipi_0)
      implicit none
      real *8 rpi_0_qedc23,ipi_0,s,sabs,M12,r12,Lnqmone,M12ds
      real *8 iLnqmone
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      M12ds=M12/s
      Lnqmone=log(sabs/M12)
      iLnqmone=0.d0
      if (s.gt.4.d0*M12) iLnqmone=-pi
      rPi_0_qedc23  = 20.d0/9.d0 - 4.d0/3.d0*Lnqmone+8.d0*M12ds
      iPi_0  =            - 4.d0/3.d0*iLnqmone
      return
      end

      function rpi_1_qedc23(s,M12,ipi_1)
      implicit none
      real *8 rpi_1_qedc23,ipi_1,s,sabs,M12,r12,Lnqmone,M12ds
      real *8 iLnqmone
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      M12ds=M12/s
      Lnqmone=log(sabs/M12)
      iLnqmone=0.d0
      if (s.gt.4.d0*M12) iLnqmone=-pi
      rPi_1_qedc23  = 5.d0/6.d0 - 4.d0*zeta3
     &       - Lnqmone - 12.d0*M12ds*Lnqmone
      iPi_1  =
     &       - iLnqmone - 12.d0*M12ds*iLnqmone
      return
      end

      function rpia_2_qedc23(s,M12,ipia_2)
      implicit none
      real *8 rpia_2_qedc23,ipia_2,s,sabs,M12,r12,Lnqmone
      real *8 iLnqmone
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      r12=sabs/M12
      Lnqmone=log(r12)
      iLnqmone=0.d0
      if (s.gt.4.d0*M12) iLnqmone=-pi
      rPiA_2_qedc23 = - 121.d0/48.d0 + (- 5.d0 + 8.d0*ln2)*zeta2
     &        - 99.d0/16.d0*zeta3 + 10.d0*zeta5 + 1.d0/8.d0*Lnqmone
      iPiA_2= + 1.d0/8.d0*iLnqmone
      return
      end

      function rpil_2_qedc23(s,M12,M22,ipil_2)
      implicit none
      real *8 rpil_2_qedc23,ipil_2,s,sabs,M12,r12,Lnqmone,M22,r22,Lnqmtwo
      real *8 iLnqmone,iLnqmtwo
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      r12=sabs/M12
      r22=sabs/M22
      Lnqmone=log(r12)
      Lnqmtwo=log(r22)
      iLnqmone=0.d0
      iLnqmtwo=0.d0
      if (s.gt.4.d0*M12) iLnqmone=-pi
      if (s.gt.4.d0*M22) iLnqmtwo=-pi
      rPil_2_qedc23 = - 116.d0/27.d0 + 4.d0/3.d0*zeta2 + 38.d0/9.d0*zeta3
     &        + 14.d0/9.d0*Lnqmone + (5.d0/18.d0
     &        - 4.d0/3.d0*zeta3)*Lnqmtwo
     &        + 1.d0/6.d0*(Lnqmone**2-iLnqmone**2)
     &        - 1.d0/3.d0*(Lnqmone*Lnqmtwo-iLnqmone*iLnqmtwo)
      iPil_2=
     &        + 14.d0/9.d0*iLnqmone + (5.d0/18.d0
     &        - 4.d0/3.d0*zeta3)*iLnqmtwo
     &        + 1.d0/6.d0*2.d0*Lnqmone*iLnqmone
     &        - 1.d0/3.d0*(Lnqmone*iLnqmtwo+iLnqmone*Lnqmtwo)
      return
      end

      function rpif_2_qedc23(s,M12,ipif_2)
      implicit none
      real *8 rpif_2_qedc23,ipif_2,s,sabs,M12,r12,Lnqmone
      real *8 iLnqmone
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      r12=sabs/M12
      Lnqmone=log(r12)
      iLnqmone=0.d0
      if (s.gt.4.d0*M12) iLnqmone=-pi
      rPiF_2_qedc23 = - 307.d0/216.d0 - 8.d0/3.d0*zeta2 + 545.d0/144.d0*zeta3
     &        + (11.d0/6.d0 - 4.d0/3.d0*zeta3)*Lnqmone
     &        - 1.d0/6.d0*(Lnqmone**2-iLnqmone**2)
      iPiF_2=
     &        + (11.d0/6.d0 - 4.d0/3.d0*zeta3)*iLnqmone
     &        - 1.d0/6.d0*2.d0*Lnqmone*iLnqmone
      return
      end

      function rpih_2_qedc23(s,M22,ipih_2)
      implicit none
      real *8 rpih_2_qedc23,ipih_2,s,sabs,M22,r22,Lnqmtwo
      real *8 iLnqmtwo
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      common /consts_qedc23/pi,pi2,ln2,zeta2,zeta3,zeta5
      sabs=abs(s)
      r22=sabs/M22
      Lnqmtwo=log(r22)
      iLnqmtwo=0.d0
      if (s.gt.4.d0*M22) iLnqmtwo=-pi
      rPih_2_qedc23 = - 37.d0/6.d0 + 38.d0/9.d0*zeta3
     &        + (11.d0/6.d0 - 4.d0/3.d0*zeta3)*Lnqmtwo
     &        - 1.d0/6.d0*(Lnqmtwo**2-iLnqmtwo**2)
      iPih_2 =
     &        + (11.d0/6.d0 - 4.d0/3.d0*zeta3)*Lnqmtwo
     &        - 1.d0/6.d0*2.d0*(Lnqmtwo*iLnqmtwo)
      return
      end
C
       function ddilog_qedc23(x)                            
C  ******************************************************************
C  *                                                                *
C  *                 program for calculating                        *
C  *      the real part of the dilogarithm for real arguments       *
C  *                                                                *
C  *      functions: ddilog_qedc23(x)                                      *
C  *                 rli2_qedc23(x)                                        *
C  *                 clausen2_qedc23(phi)                                  *
C  *                 cl2_qedc23(phi)                                       *
C  *                                                                *
C  *           F. Jegerlehner, Paul Scherrer Institute              *
C  *                                                                *
C  *                     Version: 25-OCT-1990                       *
C  *                                                                *
C  ******************************************************************
       implicit none
       real *8 pi6,null,one,two,half,qua,x,r
       real *8 ddilog_qedc23,rli2_qedc23,sparg,omx,clo
       common /polylog1_qedc23/pi6,null,one,two,half,qua
       save   /polylog1_qedc23/
       data pi6/1.644934066848226d0/                     
       data null,one,two,half,qua/0.d0,1.d0,2.d0,.5d0,.25d0/
       ddilog_qedc23=0.d0
       if (x.eq.one) then
         ddilog_qedc23=pi6
       return
       endif
       r=dabs(x)
       if (r.le.half) then
          ddilog_qedc23=rli2_qedc23(x)
          return
       else if (x.le.-two) then
          sparg=one/x 
          ddilog_qedc23=-rli2_qedc23(sparg)-half*dlog(-x)**2-pi6
          return
       else if (x.ge.two) then
          sparg=one/x 
          ddilog_qedc23=-rli2_qedc23(sparg)-half*dlog(x)**2+two*pi6
          return
       else if (x.lt.-one) then 
          sparg=one/(one-x)
          ddilog_qedc23=rli2_qedc23(sparg)-dlog(-x)*dlog(one-x)
     &           +half*dlog(one-x)**2-pi6
          return
       else if (x.lt.-half) then
          omx =one-x 
          sparg=one-one/omx                            
          ddilog_qedc23=-rli2_qedc23(sparg)-half*dlog(omx)**2
          return
       else if (x.gt.one) then
          clo=dlog(x) 
          sparg=one-one/x 
          ddilog_qedc23=rli2_qedc23(sparg)+half*clo**2
     &          -clo*dlog(x-one)+pi6
          return
       else if (x.gt.half) then 
          sparg=one-x                            
          ddilog_qedc23=-rli2_qedc23(sparg)-dlog(x)*dlog(sparg)+pi6
          return
       endif
       end

      function rli2_qedc23(x)
c Spence function for real arguments of modulus smaller than one
      real *8 one,half,qua,x,rli2_qedc23,b,z,z2
      integer ini
      dimension b(10)
      common /polylog2_qedc23/one,half,qua,b,ini
      save   /polylog2_qedc23/
      data ini/0/,one,half,qua/1.d0,.5d0,.25d0/
      if (ini.eq.0) goto 2
 1    z=-dlog(one-x)
      z2=z*z
      rli2_qedc23=z*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*b(10)+b(9))
     &  +b(8))+b(7))+b(6))+b(5))+b(4))+b(3))+b(2))+b(1))
     &  +one-z*qua)
      return
 2       b(1) =  2.7777777777777778d-02
         b(2) = -2.7777777777777778d-04
         b(3) =  4.7241118669690098d-06
         b(4) = -9.1857730746619635d-08
         b(5) =  1.8978869988970999d-09
         b(6) = -4.0647616451442255d-11
         b(7) =  8.9216910204564526d-13
         b(8) = -1.9939295860721076d-14
         b(9) =  4.5189800296199182d-16
         b(10)= -1.0356517612181247d-17
      ini=1
      goto 1
      end

      function clausen2_qedc23(phi)
c Clausens integral for arbitrary real arguments ( defined as the
c imaginary part of the complex Spence function on the unit circle 
c z=exp(i*phi) )  (2pi-periodic,odd)
      implicit none
      real *8 null,one,pi,zpi,phi,phiabs,cl2_qedc23,clausen2_qedc23
      common /polylog3_qedc23/null,one,pi,zpi
      save   /polylog3_qedc23/
      data pi /3.141592653589793d0/,zpi /6.283185307179586d0/    
      data null,one/0.d0,1.d0/
      phi=dmod(phi,zpi)
      if (phi.gt.pi) phi=phi-zpi
      phiabs=dabs(phi)
      if (phi.eq.null) then 
         clausen2_qedc23=null
      else
         clausen2_qedc23=phi/phiabs*cl2_qedc23(phiabs)
      endif
      return
      end
         
      function cl2_qedc23(phi)        
c Clausens integral for real arguments 0<phi<pi
      implicit none
      real *8 one,b,phi,z,z2,cl2_qedc23,pi,pi2
      integer ini
      dimension b(15)
      common /polylog4_qedc23/one,pi,pi2,b,ini
      save   /polylog4_qedc23/
      data pi /3.141592653589793d0/,pi2 /1.570796326794897d0/    
      data ini/0/,one/1.d0/
      if (ini.eq.0) goto 2
 1    z=phi
      z2=z*z
      if (phi.le.pi2) then 
      if (phi.eq.0d0) then
      cl2_qedc23=0d0
      return
      endif
      cl2_qedc23= z*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2* b(10)+ b(9))
     &  + b(8))+ b(7))+ b(6))+ b(5))+ b(4))+ b(3))+ b(2))+ b(1))
     &  +one-dlog(dabs(z)))
      else                                       
      cl2_qedc23= z*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*
     & (z2*(z2* b(15)+ b(14))+ b(13))+ b(12))+ b(11))+ b(10))+ b(9))
     &  + b(8))+ b(7))+ b(6))+ b(5))+ b(4))+ b(3))+ b(2))+ b(1))
     &  +one-dlog(dabs(z)))
      endif
      return
 2       b(1) =  1.3888888888888889E-02
         b(2) =  6.9444444444444444E-05
         b(3) =  7.8735197782816830E-07
         b(4) =  1.1482216343327454E-08
         b(5) =  1.8978869988970999E-10
         b(6) =  3.3873013709535213E-12
         b(7) =  6.3726364431831804E-14
         b(8) =  1.2462059912950673E-15
         b(9) =  2.5105444608999546E-17
         b(10)=  5.1782588060906234E-19
         b(11)=  1.0887357368300848E-20
         b(12)=  2.3257441143020872E-22
         b(13)=  5.0351952131473897E-24
         b(14)=  1.1026499294381215E-25
         b(15)=  2.4386585509007344E-27
      ini=1
      goto 1
      end
C
       function dli3_qedc23(x)                            
C  ******************************************************************
C  *                                                                *
C  *                 program for calculations of                    *
C  *              trilogatithms and related functions               *
C  *                                                                *
C  *      functions: dli3_qedc23(x)                                        *
C  *                 rli3_qedc23(x)                                        *
C  *                 rs12_qedc23(x)                                        *
C  *                 clausen3_qedc23(phi)                                  *
C  *                 cl3_qedc23(phi)                                       *
C  *                                                                *
C  *           F. Jegerlehner, Paul Scherrer Institute              *
C  *                                                                *
C  *                     Version: 25-OCT-1990                       *
C  *                                                                *
C  ******************************************************************
c double precision calculation of the real part of the trilogarithm
       implicit none
       real *8 zeta3,zeta2,null,one,two,half,qua,i3,i6,x,r
       real *8 dli3_qedc23,rli3_qedc23,rs12_qedc23,ddilog_qedc23,sparg,omx
       real *8 clo,cloy,clom
       common /polylog5_qedc23/zeta3,zeta2,null,one,two,half,qua,i3,i6
       save   /polylog5_qedc23/
       data zeta2,zeta3/1.644934066848226d0,1.20205690315959d0/
       data null,one,two,half,qua,i3,i6/0.d0,1.d0,2.d0,.5d0,.25d0
     &                   ,.3333333333333333d0,.1666666666666667d0/
       dli3_qedc23=0.d0
       if (x.eq.one) then
         dli3_qedc23=zeta3
       return
       endif
       r=dabs(x)
       if (r.le.half) then
          dli3_qedc23=rli3_qedc23(x)
          return
       else if (x.le.-two) then
          sparg=one/x 
          clo=dlog(-x)
          dli3_qedc23=rli3_qedc23(sparg)-i6*clo**3-zeta2*clo
          return
       else if (x.ge.two) then
          sparg=one/x 
          clo=dlog(x)
c imaginary part ignored
          dli3_qedc23=rli3_qedc23(sparg)-i6*clo**3+two*zeta2*clo
          return
       else if (x.lt.-one) then 
          sparg=one/(one-x)
          clo =dlog(-x)
          cloy=dlog(sparg)
          clom=dlog(one-sparg)
          dli3_qedc23= rs12_qedc23(sparg)-rli3_qedc23(sparg)+clom*ddilog_qedc23(sparg)
     &           +i6*cloy**3+half*cloy*clom*clo-zeta2*clo
          return
       else if (x.lt.-half) then
          omx =one-x 
          sparg=one-one/omx                            
          clo=dlog(omx)
          dli3_qedc23= rs12_qedc23(sparg)-rli3_qedc23(sparg)-clo*ddilog_qedc23(sparg)-i6*clo**3
          return
       else if (x.gt.one) then
          clo=dlog(x) 
          sparg=one-one/x 
c imaginary part ignored
          dli3_qedc23=-rs12_qedc23(sparg)+clo*ddilog_qedc23(sparg)+i3*clo**3
     &           -half*clo**2*dlog(x-one)+zeta2*clo+zeta3
          return
       else if (x.gt.half) then 
          clo=dlog(x) 
          sparg=one-x                            
          dli3_qedc23=-rs12_qedc23(sparg)-clo*ddilog_qedc23(sparg)
     &           -half*clo**2*dlog(sparg)+zeta2*clo+zeta3
          return
       endif
       end

      function rli3_qedc23(x)
c function Li(3) for real arguments of modulus smaller than one
      implicit none
      real *8 one,zeta3,x,rli3_qedc23,b,z
      integer ini
      dimension b(21)
      common /polylog6_qedc23/one,zeta3,b,ini
      save   /polylog6_qedc23/
      data ini/0/,one/1.d0/
      data zeta3/1.20205690315959d0/
      if (ini.eq.0) goto 2
 1    if (x.eq.one) then
         rli3_qedc23=zeta3
      return
      endif
      z=-dlog(one-x)          
      rli3_qedc23=z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*(z*
     & (z*b(20)+b(19))+b(18))+b(17))+b(16))+b(15))+b(14))+b(13))
     & +b(12))+b(11))+b(10))+b( 9))+b( 8))+b( 7))+b( 6))+b( 5))+b( 4))
     & +b(3))+b(2))+b(1))+one)
      return
C Coefficients for Li3:
C       b(0) =   1.0d0
 2      b(1) = - 3.75d-01
        b(2) =   7.8703703703703703d-02
        b(3) = - 8.6805555555555555d-03
        b(4) =   1.2962962962962963d-04
        b(5) =   8.1018518518518519d-05
        b(6) = - 3.4193571608537595d-06
        b(7) = - 1.3286564625850340d-06
        b(8) =   8.6608717561098513d-08
        b(9) =   2.5260875955320400d-08
        b(10)= - 2.1446944683640648d-09
        b(11)= - 5.1401106220129789d-10
        b(12)=   5.2495821146008294d-11
        b(13)=   1.0887754406636318d-11
        b(14)= - 1.2779396094493695d-12
        b(15)= - 2.3698241773087452d-13
        b(16)=   3.1043578879654623d-14
        b(17)=   5.2617586299125061d-15
        b(18)= - 7.5384795499492654d-16
        b(19)= - 1.1862322577752285d-16
        b(20)=   1.8316979965491383d-17
        b(21)=   2.7068171031837350d-18
      ini=1
      goto 1
      end

      function rs12_qedc23(x)
c function S(1,2) for real arguments of modulus smaller than one
      implicit none
      real *8 one,two,half,qua,i6,zeta3
      real *8 x,rs12_qedc23,dlog,dfloat,b,z,z2,ir
      integer ini,i
      common /polylog7_qedc23/one,two,half,qua,i6,zeta3,b,ini
      save   /polylog7_qedc23/
      dimension b(10)
      data ini/0/,one,two,half,qua/1.d0,2.d0,.5d0,.25d0/
      data i6,zeta3/.1666666666666667d0,1.20205690315959d0/
      if (ini.eq.0) goto 2
 1    if (x.eq.one) then
      rs12_qedc23=zeta3
      return
      endif
      z=-dlog(one-x)
      z2=z*z
      rs12_qedc23=z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*b(10)+b(9))
     &  +b(8))+b(7))+b(6))+b(5))+b(4))+b(3))+b(2))+b(1))
     &  +half-z*i6)*half
      return
 2       b(1) =  2.7777777777777778d-02
         b(2) = -2.7777777777777778d-04
         b(3) =  4.7241118669690098d-06
         b(4) = -9.1857730746619635d-08
         b(5) =  1.8978869988970999d-09
         b(6) = -4.0647616451442255d-11
         b(7) =  8.9216910204564526d-13
         b(8) = -1.9939295860721076d-14
         b(9) =  4.5189800296199182d-16
         b(10)= -1.0356517612181247d-17
      do i=1,10
         ir=dfloat(i)
         b(i)=b(i)*(two*ir+one)/(two*ir+two)
      enddo
      ini=1
      goto 1
      end

      function clausen3_qedc23(phi)
c Clausens integral for arbitrary real arguments ( defined as the
c real part of the complex trilogarithm (Li3) on the unit circle 
c z=exp(i*phi) )  (2pi-periodic,even)
      implicit none
      real *8 null,one,pi,zpi,zeta3
      real *8 phi,phiabs,cl3_qedc23,clausen3_qedc23
      common /polylog8_qedc23/null,one,pi,zpi,zeta3
      save   /polylog8_qedc23/
      data pi /3.141592653589793d0/,zpi /6.283185307179586d0/    
      data zeta3/1.20205690315959d0/
      data null,one/0.d0,1.d0/
      phi=dmod(phi,zpi)
      if (phi.gt.pi) phi=phi-zpi
      phiabs=dabs(phi)
      if (phi.eq.null) then 
         clausen3_qedc23=zeta3
      else
         clausen3_qedc23=cl3_qedc23(phiabs)
      endif
      return
      end
         
      function cl3_qedc23(phi)        
c Clausens integral of 3rd order for real arguments 0<phi<pi
      implicit none
      real *8 one,half,threequa,b,phi,z,z2,cl3_qedc23,pi,pi2,zeta3
      integer ini
      dimension b(15)
      common /polylog9_qedc23/one,half,threequa,pi,pi2,zeta3,b,ini
      save   /polylog9_qedc23/
      data zeta3/1.20205690315959d0/
      data pi /3.141592653589793d0/,pi2 /1.570796326794897d0/    
      data ini/0/,one,half,threequa/1.0d0,.50d0,.75d0/
      if (ini.eq.0) goto 2
 1    z=phi
      z2=z*z
      if (phi.le.pi2) then 
      if (phi.eq.0d0) then
      cl3_qedc23=zeta3
      return
      endif
      cl3_qedc23=-z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2* b(10)+ b(9))
     &  + b(8))+ b(7))+ b(6))+ b(5))+ b(4))+ b(3))+ b(2))+ b(1))
     &  +threequa-half*dlog(dabs(z)))+zeta3
      else                                       
      cl3_qedc23=-z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*(z2*
     & (z2*(z2* b(15)+ b(14))+ b(13))+ b(12))+ b(11))+ b(10))+ b(9))
     &  + b(8))+ b(7))+ b(6))+ b(5))+ b(4))+ b(3))+ b(2))+ b(1))
     &  +threequa-half*dlog(dabs(z)))+zeta3
      endif
      return
 2       b(1) =  3.4722222222222222E-03
         b(2) =  1.1574074074074074E-05
         b(3) =  9.8418997228521037E-08
         b(4) =  1.1482216343327454E-09
         b(5) =  1.5815724990809166E-11
         b(6) =  2.4195009792525152E-13
         b(7) =  3.9828977769894878E-15
         b(8) =  6.9233666183059291E-17
         b(9) =  1.2552722304499773E-18
         b(10)=  2.3537540027684652E-20
         b(11)=  4.5363989034586869E-22
         b(12)=  8.9451696703926431E-24
         b(13)=  1.7982840046954963E-25
         b(14)=  3.6754997647937384E-27
         b(15)=  7.6208079715647952E-29
      ini=1
      goto 1
      end
