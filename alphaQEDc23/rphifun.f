      function rphifun_qedc23(s,ier)
c     fitted R(s) in phi resonance region BW_qedc23 + background fit,
c     background fitted by 2nd order Chebyshev polynomial
      implicit none
      integer np,ier,ier1
      parameter(np=9)
      real *8 rphifun_qedc23,resfitfunx_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     afires,afista,afisys,bg,yfit1,yfit2,yfit3
      dimension afires(np),afista(np),afisys(np)
      REAL*8 FSTA(5),FSYS(5)
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/ERR_qedc23/IER1
      common /poly_qedc23/e1,en,amap,bmap
      external resfitfunx_qedc23
c      ++++ omegafit ++++  

      include 'phifit.h'

      IER1=ier
      e=sqrt(s)
      yfit=0.d0
      e1=1.000d0
      en=1.040d0
      if ((e.gt.en).or.(e.lt.e1)) then
         rphifun_qedc23=0.d0
         return
      endif
C      bg=1.26040D+00-5.52478D-01
      bg=0.0d0
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      yfit1=resfitfunx_qedc23(s,afires,np)
C      yfit2=resfitfunx_qedc23(s,afista,np)   ! + value tot error
C      yfit3=resfitfunx_qedc23(s,afisys,np)   ! - value tot error
      if (ier.eq.1) then
        yfit=yfit1+bg
      else if (ier.eq.2) then
        yfit=yfit1*fsta(3)
      else if (ier.eq.3) then
        yfit=yfit1*fsys(3)
      endif
      rphifun_qedc23=yfit
      return
      end
