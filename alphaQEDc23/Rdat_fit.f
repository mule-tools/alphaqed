c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Rdat_fit.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:02:10 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Tue Jun 27 01:29:06 2023
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2009 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of R(s) using piecewise Chebyshev polynomial fits.
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc23 -> RCHPTnew_qedc23
c            CHPTCUT  M_omega   =>  rfitrholow_qedc23
c            M_omega  EFITOMBG  =>  rfitrhohig_qedc23        IGS=1 rsigpp_qedc23
c            EFITOMBG EFITMIN   =>  epemfit079081_qedc23     IGS=1 rsigpp_qedc23
c            EFITMIN  1.4 GeV   =>  epemfit0814new_qedc23
c            1.4 GeV  3.2 GeV   =>  epemfit1432_qedc23
c            3.2 GeV  EC        =>  epemfit32cut_qedc23
c Dec 2022 iomegaphidat options -1,0,1,2; new: 2 treats omega & phi data separate from background      
c   romegafun_qedc23 & rphifun_qedc23 fits of omega & phi resonance data from frome_qedc23 & frphi_qedc23 in Rdat_fun.f
      function rs40smoothed_qedc23(s,IER)
      implicit none
      INTEGER IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,j,
     &     iomegaphidat,iomegaphidat_sav,iintRd,iintRd1,
     &     IRESON,iresonances,iresonances_sav,IRESON_SAV
      REAL *8 rs40smoothed_qedc23,RS40_qedc23,RS40x_qedc23,RCHPT_qedc23,RCHPTnew_qedc23,rfitchptail_qedc23,
     &     romegafun_qedc23,rphifun_qedc23,rsigpp_qedc23,epemfit0814new_qedc23,
     &     epemfit079081_qedc23,epemfit1432_qedc23,rfitrholow_qedc23,rfitrhohig_qedc23,epemfit32cut_qedc23
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECUTFIT,RSBG,reno,
     &     RESULT,efuture,futureprecision,facbw,highCHPTCUT,
     &     MPi,betapi,FVtoR,fpi2fitchptail_qedc23
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),rex(3)
      REAL*8 EOMM,EOMP,EFIM,EFIP,EOMM1,EOMP1,EFIM1,EFIP1
      REAL*8 FSTA(5),FSYS(5)
      COMMON/MFPI_qedc23/MP2,M2/ERR_qedc23/IER1/POL_qedc23/POLP,POLM
      COMMON/CHAN_qedc23/ICH
      COMMON/RCUTS_qedc23/CHPTCUT,EC,ECUT
      COMMON/LEDATCUT_qedc23/highCHPTCUT
      COMMON/GUSA_qedc23/IGS,iso
      common /RESFIT_qedc23/facbw,IRESON,iintRd1   ! facbw defined in constants.f, common via common.h
      common /RES_qedc23/iresonances
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT in Rdat_fun.f
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      EXTERNAL RS40_qedc23,RS40x_qedc23,RCHPT_qedc23,RCHPTnew_qedc23,rfitchptail_qedc23,
     &     romegafun_qedc23,rphifun_qedc23,rsigpp_qedc23,epemfit0814new_qedc23,
     &     epemfit079081_qedc23,epemfit1432_qedc23,rfitrholow_qedc23,rfitrhohig_qedc23,epemfit32cut_qedc23,
     &     fpi2fitchptail_qedc23
      common /future_qedc23/efuture,futureprecision,ifuture
      iintRd1=iintRd
      iomegaphidat_sav=iomegaphidat
      iresonances_sav=iresonances
      IRESON_SAV=IRESON
      iresonances=IRESON
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
C      iomegaphidat=0 ! use BW_qedc23 with PDG parameters for omega and phi
C      iomegaphidat=1 ! use omega and phi data contribution to R
C      iomegaphidat=2 ! replace BW_qedc23 resonances of option 0 by resonance data
C     setting for background fits set in fitRdata.f
C      iomegaphidat=-1 ! background only
      if (iomegaphidat.eq.0) iresonances=1
      if (iomegaphidat.eq.0) IRESON=1
      if (iomegaphidat.eq.1) iresonances=0
      if (iomegaphidat.eq.1) IRESON=0
      if (iomegaphidat.eq.2) iresonances=1
      if (iomegaphidat.eq.2) IRESON=1
      if (iomegaphidat.eq.-1) iresonances=0
      if (iomegaphidat.eq.-1) IRESON=0

      reno=1.d0
      IER1=IER
      E=DSQRT(S)
      CHPTCUT=0.318D0
c omega M-3 Gamma = 0.757 ; M+3 Gamma =0.808      
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITOMBG=0.78710000000000000d0
      EFITMIN=0.81d0 ! omega range end 
      EFITMAX=1.4349d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
      ECUTFIT=DMIN1(ECUT,40.d0)
c     highCHPYCUT=CHPTCUT set in Rdat_fun.f
c     fit ranges below
c  rfitchptail_qedc23 - rfitrholow_qedc23   - rfitrhohigh  - epemfit079081_qedc23 - epemfit0814new_qedc23  - epemfit1432_qedc23  - epemfit32cut_qedc23 - RS40_qedc23
c              CHPCUT         MOM            EFITOMBG        EFITMIN           EFITMAX        3.2            ECUTFIT
c              |--------------|--------------|---------------|-----------------|--------------|--------------|
c              0.318          0.777          0.787835        0.81       [1.375]1.4[1.4349]    3.2            ECUT<40.0
c                         EOMM                                EOMP  EFIM EFIP
c                         |------------------------------------|      |---|
c                       0.739                               0.851  1.0  1.04
c  2023 update M+-3 G   0.757                               0.810
c                                 romegafun_qedc23                          rphifun_qedc23
c   ICH =0,          1,             2,             3,              4,                5,             6,            7,
c
      eps=0.02d0
      RSBG=0.d0
      MPi=0.13957d0
      j=IER
      IF (E.LE.highCHPTCUT) THEN
         ICH=0
c take parameter uncertainty as syst error
         RSBG=rfitchptail_qedc23(s)
      ELSE IF ((E.GT.highCHPTCUT).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=rfitrholow_qedc23(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         IF (IGS.EQ.0) THEN
c     ###########  Chebyshev fit
            RSBG=rfitrhohig_qedc23(s)
         ELSE
c     ########### alternative GS fit
            RSBG=rsigpp_qedc23(s,iso)
         ENDIF
      ELSE IF ((E.GE.EFITOMBG).AND.(E.LT.EFITMIN)) THEN
         ICH=3
         IF (IGS.EQ.0) THEN
c     ###########  Chebychev fit
            RSBG=epemfit079081_qedc23(s)
         ELSE
c     ########### alternative GS fit
            RSBG=rsigpp_qedc23(s,iso)
         ENDIF
      ELSE IF ((E.GE.EFITMIN).AND.(E.LE.EFITMAX)) THEN
         ICH=4
         RSBG=epemfit0814new_qedc23(s)
      ELSE IF ((E.GE.EFITMAX).AND.(E.LE.3.2d0)) THEN
         ICH=5
         RSBG=epemfit1432_qedc23(s)
      ELSE IF ((E.GT.3.2d0).AND.(E.LE.ECUTFIT)) THEN
c     ECUTFIT don't go beyond 15 GeV
         ICH=6
         RSBG=epemfit32cut_qedc23(s)
      ELSE
         ICH=7
         RSBG=RS40_qedc23(s,IER)
      ENDIF
 10   RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
c      CALL RENOoldnew(E,reno)
      rs40smoothed_qedc23=RESULT
      iomegaphidat=iomegaphidat_sav
      iresonances=iresonances_sav
      IRESON=IRESON_SAV
      return
      end
C
      function epemfit0814new_qedc23(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,nx,ier,IRESON,iintRd,iintRd1,iomegaphidat,ini
      parameter(np=8,nm=12,nx=5)
      real *8 epemfit0814new_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc23,ares,asta,asys,rphifun_qedc23,frphi_qedc23
      dimension aresl(nm),astal(nm),asysl(nm)
C      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
C      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(nx),astafr(nx),asysfr(nx)
C      dimension ares(np),asta(np),asys(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP
      real *8 fac,rbw,BW_qedc23
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      real *8 emima1(2),emima2(2),emima3(2),emima4(2),emima5(2)
      external BW_qedc23,rphifun_qedc23,frphi_qedc23
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      COMMON/BWFI_qedc23/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd1  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT in Rdat_fun.f
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd   ! fac=facbw defined in constants.f, common via common.h
      COMMON/ERR_qedc23/IER
      data ini /0/

c      ++++ epemfit0814new_qedc23 ++++

      include 'epemfit0814new.h'

      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.95899999999999996d0
      EFIMX=1.00000000000000000d0  !   phi range
      EFIPX=1.0400000000000000d0   !
      EFIPP=1.1120000000000001d0
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
C         e1=0.81d0
C         en=0.999999d0
         e1=emima1(1)
         en=emima1(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
C         e1=0.958d0
C         en=1.000d0
         e1=emima4(1)
         en=emima4(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
c update jan 2012
C         e1=1.00001d0
C         en=1.03999D0
         e1=emima3(1)
         en=emima3(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresfr,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astafr,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysfr,nx)
         endif
         if (iomegaphidat.eq.1) then
C            rbw=frphi_qedc23(s,ier)
            yfit=rphifun_qedc23(s,ier)
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(iomegaphidat.eq.2)) then
C               rbw=frphi_qedc23(s,ier)
            yfit=rphifun_qedc23(s,ier)
C               if (ier.eq.2) then
C                  yfit=sqrt(yfit**2+rbw**2)
C               else
C                  yfit=yfit+rbw
C               endif
         else if ((IRESON.eq.1).and.(iomegaphidat.eq.0)) then
            rbw=fac*BW_qedc23(sMeV,MFI,GFI,PFI)
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
      else if (e.lt.1.435d0) then
C         e1=1.04000001d0
C         en=1.435d0
         e1=emima2(1)
         en=emima2(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: s out of fit range in epemfit0814new_qedc23'
      endif
      epemfit0814new_qedc23=yfit
      return
      end
c
      function ffppg2fit0810_qedc23(s)
c fitted multi hadron channels n>2 (G2 files from dataset0821 in ../for/vap/dat/FFPPG2.DAT)
      implicit none
      integer np,ier
      parameter(np=12)
      real *8 ffppg2fit0810_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ ffppg2fit0810_qedc23 ++++

      include 'ffppg2fit0810.h'

      e=sqrt(s)
      yfit=0.d0
C      e1=0.81d0
C      en=1.00d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         ffppg2fit0810_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      ffppg2fit0810_qedc23=yfit
      return
      end
c
      function ffppmfit0810_qedc23(s)
c fitted multi hadron channels pp + n>2 (M files from dataset0821 in ../for/vap/dat/FFPPM.DAT)
      implicit none
      integer np,ier
      parameter(np=12)
      real *8 ffppmfit0810_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ ffppmfit0810_qedc23 ++++

      include 'ffppmfit0810.h'

      e=sqrt(s)
      yfit=0.d0
C      e1=0.81d0
C      en=1.00d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         ffppmfit0810_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      ffppmfit0810_qedc23=yfit
      return
      end
c
      function epemfit079081_qedc23(s)
      implicit none
      integer np,ier,IRESON,iintRd,iintRd1,iomegaphidat
      parameter(np=6)
      real *8 epemfit079081_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23,MOM,GOM,POM,fsta2,fsys2,
     &     romegafun_qedc23,frome_qedc23
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      real *8 UGM2,sMeV
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc23
      real *8 emima1(2)
      external BW_qedc23,romegafun_qedc23,frome_qedc23
      COMMON/BWOM_qedc23/MOM,GOM,POM
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd1  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT in Rdat_fun.f
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      COMMON/ERR_qedc23/IER

c      ++++ epemfit079081_qedc23 ++++

      include 'epemfit079081.h'

c omega  .41871054d0,.810d0
      UGM2=1.D6
      sMeV=s*UGM2
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
C      e1=0.787d0
C      en=0.8099d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.0.81d0).or.(e.le.e1)) then
         epemfit079081_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
c     avoid double counting of omega and phi by intRdatx
      if ((e.ge.EOMM).and.(e.lt.EOMP)) then
         if (iomegaphidat.eq.1) then
C            rbw=frome_qedc23(s,ier)
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(iomegaphidat.eq.2)) then
C            rbw=frome_qedc23(s,ier)
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(IRESON.eq.1).and.
     &           (iomegaphidat.eq.0)) then
            rbw=fac*BW_qedc23(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys2
               yfit=yfit+rbw
            endif
         endif
      endif
      epemfit079081_qedc23=yfit
      return
      end
c
      function epemfit1432_qedc23(s)
      implicit none
      integer np,nm,nx,ier,IRESON,iintRd
      parameter(np=12,nm=6,nx=6)
      real *8 epemfit1432_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,
     &     ares,asta,asys,ares1,asta1,asys1,
C     &     arestoBES,astatoBES,asystoBES,aresmed,astamed,asysmed,
     &     polynom_qedc23,eex,ebe
      real *8 fres2330,fsta2330,fsys2330,fres3032,fsta3032,fsys3032
      dimension areslow(np),astalow(np),asyslow(np)
C      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(nx),asta(nx),asys(nx)
      dimension ares1(nx),asta1(nx),asys1(nx)
C      dimension arestoBES(nm),astatoBES(nm),asystoBES(nm)
      dimension fres2330(nm),fsta2330(nm),fsys2330(nm)
      dimension fres3032(nm),fsta3032(nm),fsys3032(nm)
      REAL*8 MPS(6),GPS(6),PPS(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      real *8 fac,rbw,BW_qedc23,UGM2,sMeV
      real *8 emima1(2),emima2(2),emima3(2),emima4(2),emima5(2)
      external BW_qedc23
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      COMMON/ERR_qedc23/IER
C common Psi and Upsilon parameters set in subroutine resonances
      COMMON/PSYPPAR_qedc23/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP

c      ++++ epemfit1432_qedc23 ++++

      include 'epemfit1432.h'

      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      eex=1.8590000000000000d0
      ebe=2.6000000000000001d0
         if ((e.gt.1.435).and.(e. lt.1.841d0)) then
c   1.375  --  1.841  updat3 jan 2021
C         e1=1.375d0
C         en=1.845d0
         e1=emima1(1)
         en=emima1(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asyslow,np)
         endif
      else if ((e.ge.1.841d0).and.(e.le.2.00d0)) then
c   1.841 -- 2.000
C         e1=1.838d0
C         en=2.00d0
         e1=emima2(1)
         en=emima2(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,ares,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,asta,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asys,nx)
         endif
      else if ((e.gt.2.00d0).and.(e.lt.2.25d0)) then
c  2.000 -- 2.25 update jan 2012
C         e1=2.000d0
C         en=2.263d0
         e1=emima3(1)
         en=emima3(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,ares1,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,asta1,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asys1,nx)
         endif
      else if ((e.ge.2.25d0).and.(e.lt.3.00d0)) then
C         e1=2.243d0
C         en=3.0d0
         e1=emima4(1)
         en=emima4(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,fres2330,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,fsta2330,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,fsys2330,nm)
         endif
      else if ((e.ge.3.00d0).and.(e.le.3.2d0)) then
C         e1=3.00d0
C         en=3.200d0
         e1=emima5(1)
         en=emima5(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,fres3032,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,fsta3032,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,fsys3032,nx)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
c psi1  3.08587D0 3.10787D0
            if ((e.ge.EMIPS(1)).and.(e.le.EMAPS(1))) then ! psi1
               rbw=fac*BW_qedc23(sMeV,MPS(1),GPS(1),PPS(1))
               if (ier.eq.2) then
                  rbw=rbw*STAPS(1)
                  yfit=sqrt(yfit**2+rbw**2)
               else
                  if (ier.eq.3) rbw=rbw*SYSPS(1)
                  yfit=yfit+rbw
               endif
            endif
         endif
      endif
         epemfit1432_qedc23=yfit
      return
      end
c
      function epemfit32cut_qedc23(s)
      implicit none
      integer i,nx,np,nm,nh,ni,nb,ier,IRESON,iintRd,iresonances,
     &     iresonances_sav,IRESON_SAV
      parameter(nx=12,nb=9,np=6,nm=6,ni=5,nh=3)
      real *8 epemfit32cut_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,rs40_qedc23,rs40x_qedc23,
     &    ares1,asta1,asys1,aresi,astai,asysi,aresin,astain,asysin,
     &    aresh,astah,asysh,aresy,astay,asysy,aresbes,astabes,asysbes,
     &    polynom_qedc23,RSpQCD_qedc23
      dimension ares1(nx),asta1(nx),asys1(nx)
      dimension aresi(ni),astai(ni),asysi(ni)
      dimension aresin(np),astain(np),asysin(np)
      dimension aresh(nh),astah(nh),asysh(nh)
      dimension aresy(ni),astay(ni),asysy(ni)
      dimension aresbes(nb),astabes(nb),asysbes(nb)
      REAL*8 MPS(6),GPS(6),PPS(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 fac,rbw,BW_qedc23,CHPTCUT,EC,ECUT,RSx,ECUTFIT1,ECUTFIT2
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      REAL*8 UGM2,sMeV
      real *8 emima1(2),emima2(2),emima3(2),emima4(2),emima5(2),
     &     emima6(2)
      external BW_qedc23,RS40_qedc23,RS40x_qedc23,RSpQCD_qedc23
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      common /RES_qedc23/iresonances
      COMMON/ERR_qedc23/IER
      COMMON/RCUTS_qedc23/CHPTCUT,EC,ECUT
C cpmmon Psi and Upsilon parameters set in subroutine resonances
      COMMON/PSYPPAR_qedc23/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP

c      ++++ epemfit32cut_qedc23 ++++

      include 'epemfit32cut.h'

      iresonances_sav=iresonances
      IRESON_SAV=IRESON
      iresonances=IRESON
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      ECUTFIT1=DMIN1(ECUT,11.0d0)
      ECUTFIT2=DMIN1(ECUT,40.0d0)
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
      if ((e.lt.3.2d0).or.(e.gt.40.d0)) then
         epemfit32cut_qedc23=0.d0
         return
      else if (e.gt.ECUT) then
         epemfit32cut_qedc23=RSpQCD_qedc23(s,IER)
         return
      else if (e.lt.3.6d0) then
C         e1=3.2d0
C         en=3.7d0
         e1=emima1(1)
         en=emima1(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,ares1,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,asta1,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asys1,nx)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
            do i=1,3
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc23(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.le.3.71d0) then
c 3.598  --  3.710
C         e1=3.598d0
C         en=3.710d0
         e1=emima2(1)
         en=emima2(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresin,ni)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astain,ni)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysin,ni)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
            do i=1,3
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc23(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.lt.3.78d0) then
c 3.598  --  3.787
C         e1=3.598d0
C         en=3.787d0
         e1=emima3(1)
         en=emima3(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresi,ni)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astai,ni)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysi,ni)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
            do i=1,3
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc23(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.lt.4.5d0) then
c it is important here to select 4.5 not 4.4 to get smooth transition
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
         yfit=RS40_qedc23(s,IER)
      else if ((e.ge.EC).and.(e.le.9.5d0)) then
         yfit=RSpQCD_qedc23(s,IER)
      else if (e.lt.9.5d0) then
C         e1=4.57d0
C         en=9.5d0
         e1=emima4(1)
         en=emima4(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresbes,nb)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astabes,nb)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysbes,nb)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
            i=1
            if ((E.GT.EMIYP(i)).and.(E.LT.EMAYP(i))) then
               rbw=fac*BW_qedc23(sMeV,MYP(i),GYP(i),PYP(i))
               if (ier.eq.2) then
                  rbw=rbw*STAYP(i)
                  yfit=sqrt(yfit**2+rbw**2)
               else
                  if (ier.eq.3) rbw=rbw*SYSYP(i)
                  yfit=yfit+rbw
               endif
            endif
         endif
      else if ((e.ge.9.5d0).and.(e.le.14.d0)) then
C         e1=09.50d0
C         en=14.00d0
         e1=emima5(1)
         en=emima5(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresy,ni)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astay,ni)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysy,ni)
         endif
         if ((IRESON.eq.1).and.(iintRd.eq.0)) then
            do i=2,6
               if ((E.GT.EMIYP(i)).and.(E.LT.EMAYP(i))) then
                  rbw=fac*BW_qedc23(sMeV,MYP(i),GYP(i),PYP(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAYP(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else
                     if (ier.eq.3) rbw=rbw*SYSYP(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if ((e.ge.14.0d0).and.(e.le.40.d0)) then
C            e1=14.04d0
C            en=38.29d0
         e1=emima6(1)
         en=emima6(2)
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc23(ex,aresh,nh)
         else if (ier.eq.2) then
            yfit=polynom_qedc23(ex,astah,nh)
         else if (ier.eq.3) then
            yfit=polynom_qedc23(ex,asysh,nh)
         endif
      endif
      epemfit32cut_qedc23=yfit
      iresonances=iresonances_sav
      IRESON=IRESON_SAV
      return
      end
c
      function rfitrholow_qedc23(s)
      implicit none
      integer np,ier,IRESON,iintRd,iintRd1,iomegaphidat
      parameter(np=14)
      real *8 rfitrholow_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23,MOM,GOM,POM,fsta2,fsys2,
     &     frome_qedc23,romegafun_qedc23
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc23
      REAL*8 UGM2,sMeV
      real *8 emima1(2)
      external BW_qedc23,frome_qedc23,romegafun_qedc23
      COMMON/BWOM_qedc23/MOM,GOM,POM
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd1  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT in Rdat_fun.f
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      COMMON/ERR_qedc23/IER

c      ++++ rfitrholow_qedc23 ++++

      include 'rfitrholow.h'

c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
C      e1=0.318d0
C      en=0.778d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitrholow_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
c     avoid double counting of omega and phi by intRdatx
      if ((e.ge.EOMM).and.(e.lt.EOMP)) then
         if (iomegaphidat.eq.1) then
C            rbw=frome_qedc23(s,ier)
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(iomegaphidat.eq.2)) then
C            rbw=frome_qedc23(s,ier)
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(IRESON.eq.1).and.
     &           (iomegaphidat.eq.0)) then
            rbw=fac*BW_qedc23(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys2
               yfit=yfit+rbw
            endif
         endif
      endif
      rfitrholow_qedc23=yfit
      return
      end
c
      function rfitrhohig_qedc23(s)
      implicit none
      integer np,ier,IRESON,iintRd,iintRd1,iomegaphidat
      parameter(np=9)
      real *8 rfitrhohig_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23,MOM,GOM,POM,fsta2,fsys2,
     &     frome_qedc23,romegafun_qedc23
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc23
      REAL*8 UGM2,sMeV
      real *8 emima1(2)
      external BW_qedc23,frome_qedc23,romegafun_qedc23
      COMMON/BWOM_qedc23/MOM,GOM,POM
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT_qedc23/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd1  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT in Rdat_fun.f
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      COMMON/ERR_qedc23/IER

c      ++++ rfitrhohig_qedc23 ++++

      include 'rfitrhohig.h'

c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
C      e1=0.769d0
C      en=0.789d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitrhohig_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
c     avoid double counting of omega and phi by intRdatx
      if ((e.ge.EOMM).and.(e.lt.EOMP)) then
         if (iomegaphidat.eq.1) then
C            rbw=frome_qedc23(s,ier)    ! provides data not the fit
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(iomegaphidat.eq.2)) then
C            rbw=frome_qedc23(s,ier)
            yfit=romegafun_qedc23(s,ier) ! fit including BG
C            if (ier.eq.2) then
C               
C               yfit=sqrt(yfit**2+rbw**2)
C            else
C               yfit=yfit+rbw
C            endif
         else if ((iintRd.eq.0).and.(IRESON.eq.1).and.
     &           (iomegaphidat.eq.0)) then
            rbw=fac*BW_qedc23(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys2
               yfit=yfit+rbw
            endif
         endif
      endif
      rfitrhohig_qedc23=yfit
      return
      end
c
      function rfitchptail_qedc23(s)
      implicit none
      integer np,ier
      parameter(np=5)
      real *8 rfitchptail_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ rfitchptail_qedc23.h ++++

      include 'rfitchptail.h'

c     0.285 - 0.325  --> 0.341  data below this seem unriliable
      e=sqrt(s)
      yfit=0.d0
C      e1=0.285d0
C      en=0.341d0
      e1=emima1(1)
      en=emima1(2)
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitchptail_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc23(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc23(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc23(ex,asys,np)
      endif
      rfitchptail_qedc23=yfit
      return
      end
c

      function polynom_qedc23(x,a,na)
C map x to interval [-1,+1]
      implicit none
      INTEGER na,i
      REAL*8 x,a,polynom_qedc23,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      polynom_qedc23=0.d0
      do i=1,na
         polynom_qedc23=polynom_qedc23+a(i)*t(i)
      enddo
      return
      end
c
      function rsigpp_qedc23(s,iso)
c modified jan 2012 ffppg2 --> ffppm incl all channels
      implicit none
      integer iso,iopt,ioptn,ioptc,IER,IRESON,iintRd
      real *8 rsigpp_qedc23,s,sMeV,e,y,beta,SM,UGM2,M2,e1,en,
     &             fpi2rnew_qedc23,ffppmfit0810_qedc23,epemfit0814new_qedc23,eps
      real *8 MOM,GOM,POM,fsta2,fsys2
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      real *8 fac,rbw,BW_qedc23
      double precision MPLUS,MZERO,MPI2
      external fpi2rnew_qedc23,ffppmfit0810_qedc23,epemfit0814new_qedc23
      external BW_qedc23
      common/pions_qedc23/MPLUS,MZERO
      COMMON/BWOM_qedc23/MOM,GOM,POM
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/RESDOMAINS_qedc23/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc23/fac,IRESON,iintRd
      COMMON/ERR_qedc23/IER
c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      if ((e.gt.1.05d0).or.(e.lt.0.32d0)) then
         write (*,*)  ' Warning: s out of fit range in rsigpp_qedc23'
      endif
      ioptc=32 ! all 3 rho's with quadratic energy dependent width as used by Belle 2008
      ioptn=11 ! rho only  with linear energy dependent width as used by CMD-2 1999 ....
      iopt=ioptn
      eps=0.02
c      MPLUS=139.56995D0 now set in constants.f
      MPI2=MPLUS**2
      UGM2=1.D6
      M2=MPI2/UGM2
      sMeV=s*UGM2
      beta=SQRT(1.D0-4.D0*MPI2/sMeV)
c join GS parametrizaton and ffpp fit
      e1=0.81d0
      en=1.00d0
      if (e.le.e1) then
         y=beta**3*fpi2rnew_qedc23(sMEV,iso,iopt)/4.d0
c     error of pipi data
         IF (IER.EQ.2) y=y*0.02d0
         IF (IER.EQ.3) y=y*0.006D0
         if ((IRESON.eq.1).and.(iintRd.eq.0).and.
     &        (e.ge.EOMM).and.(e.lt.EOMP)) then
            rbw=fac*BW_qedc23(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               y=sqrt(y**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys2
               y=y+rbw
            endif
         endif
      else if (e.gt.en) then
         y=epemfit0814new_qedc23(s)
      else
         y=beta**3/4.d0*ffppmfit0810_qedc23(s)
      endif
      rsigpp_qedc23=y
      return
      end
c
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; fpi2rnew_qedc23.f ---
c;; Author          : Fred Jegerlehner
c;; Created On      : Thu May  1 23:40:25 2008
c;; Last Modified By: Fred Jegerlehner
c;; Last Modified On: Fri Aug  1 11:07:30 2008
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2008 Fred Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
      function fpi2rnew_qedc23(s,iso,iopt)
c    PION FORM FACTOR  (absolute square)
c CMD-2 parametrization December 2001; update August 2003
c version extended by rho--omega mixing flag iso :
c iso=1 calculate I=1 part only (omega switched off), iso=0 I=0 part else sum
c 01/08/2008 new option for energy dependent widths
c iopt=12 rho only quadratic, 32 all 3 rho's quadratic
c iopt=11 rho only linear, 31 all 3 rho's linear
c iopt anything else [0,1,...] old CMD-2 version = iopt=11
      implicit none
      character*4 DSET
      integer np,iso,iopt,i,ini
      parameter(np=8)
      complex*16 cI,cone,cdels,cepdom,cbeta,cgama,cnorm,cfpis,cfpis1,
     &           cBWnu,cBWde,cBWGSr0,cBWGSr1,cBWom,cBWGSr(3)
      real *8 fpi2rnew_qedc23,fpi2rnew1,fpi2rnewtot,fpi2rnewie1,
     &        s,e,xp,rats,beps,ppis,lgs,hs,pi,ratm,bepm,ppim,lgm,hm,
     &        dhdsm,fs,d,grs,rBWnu
      real *8 a(np),mrho(3),grho(3),mr,gr,mp2,mr2,mo2,mr4,
     &     beta,phibeta,delta,phidelta,deltaarg,betaarg,gamaarg
      real *8 mpi,dmro(3),dgro(3),ddelta,dphidel,dbeta,dphibet,
     &     gama,dgama,phigama,dphigam,momega,dmomega,gomega,dgomega,
     &     mp,mo,go
      COMMON /DASE_qedc23/DSET
      data pi /3.141592653589793d0/
      data ini /0/
      save
      if (DSET.ne.'WAVE') DSET='CMD2'
c      DSET='WAVE'
c Constants, Parameters
c      alp=1.D0/137.035999084D0  ! pm (51) April 2008
C CMD-2 Fit parameters 2003 PDG 2006
c      mpi0  =134.9766           !+/- 0.0006  neutral pion
      mpi    =139.57018d0     !+/- 0.00035  mass of charged pion
      mrho(1)=775.5d0         ! PDG 2006
      dmro(1)=0.4d0
      grho(1)=143.85d0
      dgro(1)=1.5d0
      mrho(2)=1446.0d0    ! take values from Belle fit
      dmro(2)=29.0d0
      grho(2)=434.0d0
      dgro(2)=62.0d0
      mrho(3)=1728.8d0      ! take values from Belle fit
      dmro(3)=91.0d0
      grho(3)=193.0d0
      dgro(3)=62.0d0
c      delta =1.57d-3       ! CMD2 2002 not changed  (ckeck 2008)
      delta=2.000d-3 ! my value 08
      ddelta=0.16d-3         ! +/- 0.15d-3 +/- 0.05d-3   modulus of delta
      phidelta=10.4d0        ! CMD2 hep-ex/0610021v3
      dphidel=3.8d0           ! +/- 1.6 +/- 3.5       phase angle in degrees
c      beta   =0.0859d0        ! CMD2 hep-ex/0610021v3
      beta   =0.08d0        ! CMD2 hep-ex/0610021v3
      dbeta  =0.0040d0        ! +/-  .0030 +/- .0027
      phibeta=180.0d0
      dphibet=10.0d0
      gama   =0.000d0         ! */-  0.009
      gama   =0.028d0         ! */-  0.009
      dgama  =0.028d0
      phigama=24.000d0
      dphigam=24.000d0
      momega =782.65d0       ! +/- 0.12 PDG 2004/2006
      dmomega=000.12d0
      gomega =  8.49d0         ! +/- 0.08  PDG 2004/2006
      dgomega=  0.08d0
      if (ini.eq.0) then
c CMD2 fit May 2008 chi^2=29/51 and S=0.7; incl. SND would give 159/51 and S=1.7
         if (DSET.eq.'CMD2') then
            a(1)=7.7437D+02
            a(2)=1.4421D+02
            a(3)=9.9859D-02
            a(4)=1.8000D+02
            a(5)=1.4460D+03
            a(6)=4.3400D+02
            a(7)=1.8300D-03
            a(8)=1.0600D+01
c WA pipi 2023 update data Y5P in ffppdata1n_2update.f
c settings iso=2,ipot=11            
         else if (DSET.eq.'WAVE') then
            a(1)= 7.741607E+02
            a(2)= 1.491746E+02
            a(3)= 4.663395E-02
            a(4)= 1.969959E+02
            a(5)= 1.146838E-01
            a(6)=-1.295641E+02
            a(7)=-2.156348E-03
            a(8)=-1.695035E+02
         else
         endif
         ini=1
      endif

      mrho(1) =a(1)
      grho(1) =a(2)
      beta    =a(3)
      phibeta =a(4)
      gama    =a(5)
      phigama =a(6)
      delta   =a(7)
      phidelta=a(8)

      mp=mpi
      mo=momega
      go=gomega

      cI   =DCMPLX(0.D0,1.D0)
      cone =DCMPLX(1.d0,0.0d0)
      mp2  =mp*mp
      mo2  =mo*mo
c Auxiliary variables
      e    =dsqrt(s)
      xp   =4.d0*mp2
      rats =xp/s
      beps =dsqrt(1.d0-rats)
      ppis =0.5d0*e *beps
      lgs  =dlog((e +2.d0*ppis)/(2.d0*mp))
      hs   =2.d0/pi*ppis/e *lgs
      do i=1,3
        mr   =mrho(i)
        gr   =grho(i)
        mr2  =mr *mr
        mr4  =mr2*mr2
        ratm =xp/mr2
        bepm =dsqrt(1.d0-ratm)
        ppim =0.5d0*mr*bepm
        lgm  =dlog((mr+2.d0*ppim)/(2.d0*mp))
        hm   =2.d0/pi*ppim/mr*lgm
        dhdsm=hm*(0.125d0/ppim**2-0.5d0/mr2)+0.5d0/pi/mr2
        fs   =gr*mr2/ppim**3*(ppis**2*(hs-hm)+(mr2-s)*ppim**2*dhdsm)
        d    =3.d0/pi*mp2/ppim**2*lgm+mr/2.d0/pi/ppim-mp2*mr/pi/ppim**3
        if (iopt.eq.11) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)
           else
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.31) then
           grs=gr*(ppis/ppim)**3*(mr/e)
        else if (iopt.eq.12) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)**2
           else
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.32) then
           grs=gr*(ppis/ppim)**3*(mr/e)**2
        else
c original CMD-2 fit version
           if (i.eq.1) then
              grs=gr*(ppis/ppim)**3*(mr/e)
           else
              grs=gr
           endif
        endif
        rBWnu=mr2*(1.d0+d*gr/mr)
        cBWnu=DCMPLX(rBWnu,0.D0)
        cBWde=DCMPLX(mr2-s+fs,-mr*grs)
        cBWGSr(i)=cBWnu/cBWde
      enddo
c delta argument in radians
      deltaarg=phidelta*2.d0*pi/360.d0
      gamaarg=phigama*2.d0*pi/360.d0
      cdels =DCMPLX(dcos(deltaarg),dsin(deltaarg))*
     &       DCMPLX(delta*s/mo2,0.d0)
      cBWom =DCMPLX(mo2,0.d0)/DCMPLX(mo2-s,-mo*go)
      cepdom=cone+cdels*cBWom
      betaarg=phibeta*2.d0*pi/360.d0
      cbeta =DCMPLX(dcos(betaarg),dsin(betaarg))*
     &       DCMPLX(beta,0.d0)
      cgama =DCMPLX(dcos(gamaarg),dsin(gamaarg))*
     &       DCMPLX(gama,0.d0)
      cnorm =cone+cbeta+cgama
      cfpis  =(cBWGSr(1)*cepdom+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      cfpis1 =(cBWGSr(1)+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      fpi2rnewtot=cfpis *DCONJG(cfpis )
      fpi2rnewie1=cfpis1*DCONJG(cfpis1)
      if (iso.eq.0) then
        fpi2rnew_qedc23 =fpi2rnewtot-fpi2rnewie1
      else if (iso.eq.1) then
        fpi2rnew_qedc23 =fpi2rnewie1
      else
        fpi2rnew_qedc23 =fpi2rnewtot
      endif
      return
      end
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; fpi2rnew_qedc23.f --- 
c;; Author          : Fred Jegerlehner
c;; Created On      : Thu May  1 23:40:25 2008
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Dec  1 16:00:11 2016
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2008 Fred Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      function fpi2rnew_c_qedc23(s,iso,iopt)
c    PION FORM FACTOR  (absolute square)
C VERSION for charged channel (tau spectral function)
c CMD-2 parametrization December 2001; update August 2003    
c version extended by rho--omega mixing flag iso : 
c iso=1 calculate I=1 part only (omega switched off), iso=0 I=0 part else sum 
c this version which makes from the CMD2 GS fit a modified fit with mass shifted by dM
c and width [change in mrho and grho] and corrected for kinematical effects due to mpiplus vs mpizero 
c 01/08/2008 new option for energy dependent widths
c iopt=12 rho only quadratic, 32 all 3 rho's quadratic 
c iopt=11 rho only linear, 31 all 3 rho's linear
c iopt anything else [0,1,...] old CMD-2 version = iopt=11 
      implicit none
      logical lshiftparams,lwrite
      integer np,iso,iopt,i,ini,in0
      parameter(np=8)
      complex*16 cI,cone,cdels,cepdom,cbeta,cgama,cnorm,cfpis,cfpis1,
     &           cBWnu,cBWde,cBWGSr0,cBWGSr1,cBWom,cBWGSr(3)
      real *8 fpi2rnew_c_qedc23,fpi2rnew1,fpi2rnewtot,fpi2rnewie1,
     &        s,e,xp,rats,beps,ppis,lgs,hs,pi,ratm,bepm,ppim,lgm,hm,
     &        dhdsm,fs,d,grs,rBWnu
      real *8 a(np),mrho(3),grho(3),mr,gr,mp2,mr2,mo2,mr4,
     &     beta,phibeta,delta,phidelta,deltaarg,betaarg,gamaarg
      real *8 mpi,dmro(3),dgro(3),ddelta,dphidel,dbeta,dphibet,
     &     gama,dgama,phigama,dphigam,momega,dmomega,gomega,dgomega,
     &     mp,mo,go,alp,fradcor,bp3,b03,m1,m0,mpi0,ms,ms2,
     &     dGoverGrad,sig0,sigp,null
      real *8 delta0,deltap,dGfac,beps0,dM,dG
      real *8 bepn,ppin,lgn,dn,geecc,geenc
      data pi /3.141592653589793d0/
      data ini /0/
      data in0 /0/
      save
      common /shift_qedc23/dM,dG,lshiftparams
      common /fpi2rnewmessages_qedc23/lwrite
c Constants, Parameters
      null=0.d0
      alp=1.D0/137.035999084D0  ! pm (51) April 2008
C CMD-2 Fit parameters 2003 PDG 2006
      mpi0  =134.9766           !+/- 0.0006  neutral pion
      mpi   =139.57018d0        !+/- 0.00035  mass of charged pion
c      mpi=2.d0*mpi
c      mpi0=mpi
      if ((ini.eq.0).and.(mpi0.eq.mpi)) then 
         write (*,*)'+++++++++++++++++++++++++++++++++++++++++++++++++'
         write (*,*)'+ fpi2rnew_c_qedc23 WARNING: mpi0=mpi for test purpose +' 
         write (*,*)'+++++++++++++++++++++++++++++++++++++++++++++++++'
      endif
c      mrho(1)=775.5d0          ! PDG 2006
c      dmro(1)=0.4d0           
c      grho(1)=143.85d0
c      dgro(1)=1.5d0           
c      mrho(2)=1459.0d0      ! PDG 2008
c      dmro(2)=11.0d0          
c      grho(2)=171.0d0
c      dgro(2)=50.0d0          
c      mrho(3)=1688.8d0    ! PDG 2006
c      dmro(3)=2.1d0
c      grho(3)=161.0d0        
c      dgro(3)=10.0d0
c      delta =1.57d-3       ! CMD2 2002 not changed  (ckeck 2008)
c      ddelta=0.16d-3         ! +/- 0.15d-3 +/- 0.05d-3   modulus of delta
c      phidelta=10.4d0        ! CMD2 hep-ex/0610021v3
c      dphidel=3.8d0           ! +/- 1.6 +/- 3.5       phase angle in degrees
cc      beta   =0.0859d0        ! CMD2 hep-ex/0610021v3
c      beta   =0.08d0        ! CMD2 hep-ex/0610021v3
c      dbeta  =0.0040d0        ! +/-  .0030 +/- .0027
c      phibeta=180.0d0
c      dphibet=10.0d0
c      gama   =0.000d0         ! +/-  0.009
c      gama   =0.028d0         ! +/-  0.009
c      dgama  =0.028d0
c      phigama=24.000d0
c      dphigam=24.000d0
c      momega =782.65d0       ! +/- 0.12 PDG 2004/2006
c      dmomega=000.12d0      
c      gomega =  8.49d0         ! +/- 0.08  PDG 2004/2006
c      dgomega=  0.08d0        
c Belle 0805.3773v1 constrained by F(0)=1
      mrho(1)=774.6d0         ! Belle +/-0.2 +/- 0.5 
      dmro(1)=0.4d0           
      grho(1)=148.1d0         ! Belle +/-0.4 +/- 1.7
      dgro(1)=1.5d0           
      mrho(2)=1446.0d0         ! Belle +/-7 +/- 28
      dmro(2)=11.0d0          
      grho(2)=434.0d0         ! Belle +/-16 +/- 60
      dgro(2)=50.0d0          
      mrho(3)=1728.0d0         ! Belle +/-17 +/- 89
      dmro(3)=2.1d0
      grho(3)=164.0d0         ! Belle +/-21 +89 -26       
      dgro(3)=10.0d0
c in charged channel no omega mixing here used to add omega mixing contribution
      delta =1.83d-3 
      ddelta=0.39d-3 
      phidelta=0.d0
      dphidel=0.d0
      beta   =0.15d0         ! Belle +/-0.05 +0.15 -0.04       
      dbeta  =0.0040d0        ! +/-  .0030 +/- .0027
      phibeta=202.0d0         ! Belle +/-4 +41 -8       
      dphibet=10.0d0
      gama   =0.037d0          ! Belle +/- 0.006 +0.006-0.009
      dgama  =0.028d0
      phigama=24.000d0          ! Belle +/- 9 +118-28
      dphigam=24.000d0
      momega =783.0d0       ! +/- 0.12 PDG 2004/2006
      dmomega=000.12d0      
      gomega =  8.44d0         ! +/- 0.08  PDG 2004/2006
      dgomega=  0.08d0        
c Belle 0805.3773v1 fit with F(0) unconstrained
c      mrho(1)=774.9d0         ! Belle +/-0.3 +/- 0.5 
c      dmro(1)=0.4d0           
c      grho(1)=148.6d0         ! Belle +/-0.5 +/- 1.7
c      dgro(1)=1.5d0           
c      mrho(2)=1428.0d0         ! Belle +/-15 +/- 26
c      dmro(2)=11.0d0          
c      grho(2)=413.0d0         ! Belle +/-12 +/- 57
c      dgro(2)=50.0d0          
c      mrho(3)=1694.0d0         ! Belle +/-41 +/- 89
c      dmro(3)=2.1d0
c      grho(3)=135.0d0         ! Belle +/-36 +50 -26       
c      dgro(3)=10.0d0
cc in charged channel no omega mixing here used to add omega mixing contribution
c      delta =1.83d-3 
c      ddelta=0.39d-3 
c      phidelta=0.d0
c      dphidel=0.d0
c      beta   =0.13d0         ! Belle +/-0.01 +0.16 -0.04       
c      dbeta  =0.0040d0        ! +/-  .0030 +/- .0027
c      phibeta=202.0d0         ! Belle +/-9 +50 -5       
c      dphibet=10.0d0
c      gama   =0.028d0          ! Belle +/- 0.020 +0.059-0.009
c      dgama  =0.028d0
c      phigama=-3.000d0          ! Belle +/- 13 +136-29
c      dphigam=24.000d0
c      momega =783.0d0       ! +/- 0.12 PDG 2004/2006
c      dmomega=000.12d0      
c      gomega =  8.44d0         ! +/- 0.08  PDG 2004/2006
c      dgomega=  0.08d0        
      if (ini.eq.0) then
c CMD2 fit May 2008 chi^2=29/51 and S=0.7; incl. SND would give 159/51 and S=1.7
c      a(1)=7.7359D+02  
c      a(2)=1.4305D+02
c      a(3)=9.2313D-02  
c      a(4)=1.8000D+02  
c      a(5)=1.4590D+03
c      a(6)=1.7100D+02  
c      a(7)=1.5700D-03  
c      a(8)=1.0400D+01
c Belle constrained F(0)[=1 fixed] fit 
      a(1)=774.6d0        !   mrho(1) 
      a(2)=148.1d0        !   grho(1) 
      a(3)=0.15d0         !   beta    
      a(4)=202.0d0        !  phibeta 
      a(5)=1446.0d0         !  mrho(2) 
      a(6)=434.d0          !  grho(2) 
      a(7)=1.83d-3            !  delta   
      a(8)=0.d0            !  phidelta
c Belle unconstrained F(0) fit 
c      a(1)=774.9d0        !   mrho(1) 
c      a(2)=148.6d0        !   grho(1) 
c      a(3)=0.13d0         !   beta    
c      a(4)=197.0d0        !  phibeta 
c      a(5)=1428.0d0         !  mrho(2) 
c      a(6)=413.d0          !  grho(2) 
c      a(7)=1.83d-3            !  delta   
c      a(8)=0.d0            !  phidelta
c 27/10/2015 CMD2 fixed
      a(1)=7.7437D+02  
      a(2)=1.4421D+02  
c      a(3)=9.9859D-02
c      a(4)=1.8000D+02  
c      a(5)=1.4460D+03  
c      a(6)=4.3400D+02
c      a(7)=1.8300D-03  
c      a(8)=1.0600D+01
      ini=1
      endif
      mrho(1) =a(1)
      grho(1) =a(2)
      beta    =a(3)
      phibeta =a(4)
      mrho(2) =a(5)
      grho(2) =a(6)
      delta   =a(7)
      phidelta=a(8)
c change rho parameters for rho^+
      m1=mpi
      m0=mpi0
      ms   =mrho(1)  ! charged channel mass
      mr=ms
c dM now via common shift from calling program 
c      if (lshiftparams) then
         mr=ms+dM    ! neutral channel mass dM = neutral - charged
c      endif
      mr2  =mr *mr
      ms2  =ms *ms
      bp3=(sqrt(1.d0-(2.d0*m1)**2/mr2))**3
      b03=(sqrt((1.d0-(m1+m0)**2/ms2)*(1.d0-(m1-m0)**2/ms2)))**3
c Flores-Baez et al PRD 76 (2007) 096010 Eq. (20)
c radiarive shift od width
c cut at 10 MeV soft + hard added      
      sig0= 8.39d-3
      sigp=-4.03d-3
      dGoverGrad=ms/mr*b03/bp3*sigp-sig0
      if (in0.eq.0) then
         if (lwrite) then
         write (*,*)
     &        ' ***** radiative shift charged-neutral in dG/G *****'
         write (*,*) ' * dGamma/Gamma ',dGoverGrad
         write (*,'(a15,2f9.3)') ' * dGamma Gamma',
     &        dGoverGrad*grho(1),grho(1)
         write (*,*) ' ***** +++++++++++++++++++++++ *****'
         if (dM.gt.null) then
            write (*,*) ' dM has wrong sign !!!! ',mrho(1),mrho(1)+dM
            write (*,*) ' ***** +++++++++++++++++++++++ *****'
         endif
         if (dG.gt.null) then
            write (*,*) ' dG has wrong sign !!!! ',grho(1),grho(1)+dG
            write (*,*) ' ***** +++++++++++++++++++++++ *****'
         endif
         endif
         in0=1
      endif
cc Test width rscaling; assuming mass splitting of + 1 MeV
cC use QED correction of W boson for the rho^+
c      fradcor=1.d0+alp/4.d0/pi*(73.d0/6.d0-4.d0/3.d0*pi**2)
cC correction is small and somewhat uncertain, skip it
c      fradcor=1.d0
c      dM=0.d0
c      dG=0.d0
c Davier, Lopez Castro
      delta0= 8.48d-3
      deltap=-4.00d-3
      dGfac=grho(1)/mr/bp3  ! = g^2_{rhopipi}/48/pi
c      grho(1)=a(2)*b03/bp3*(mrho(1)/a(1))**2*fradcor
C
      mp=mpi
      mo=momega
      go=gomega
 
      cI   =DCMPLX(0.D0,1.D0)
      cone =DCMPLX(1.d0,0.0d0)
      mp2  =mp*mp
      mo2  =mo*mo
c Auxiliary variables
      e    =dsqrt(s)
      xp   =4.d0*mp2
      rats =xp/s
      beps0=dsqrt(1.d0-rats)
      beps=sqrt((1.d0-(m1+m0)**2/s)*(1.d0-(m1-m0)**2/s))
c Davier, Lopez Castro et al
c      dG=dGfac*(beps**3*(1.d0+deltap)-beps0**3*(1.d0+delta0))*s/mr
c I assume radiation effects in neural channel accouted for by FSR which is accounted for setarately
c same in principle true for charge channel as well; here we only shift fixed width      
c dG now via common shift from calling program
c      dG=dGfac*(b03*ms-bp3*mr)
c strictly one-loop correction to the with requires two-loop correction in complete self energy
c      write (*,*) 'e,dM,dG:',e,dM,dG
      ppis =0.5d0*e *beps
      lgs  =dlog((e +2.d0*ppis)/(2.d0*mp))
      hs   =2.d0/pi*ppis/e *lgs
      do i=1,3 
        mr   =mrho(i)
        gr   =grho(i)
        if ((lshiftparams).and.(i.eq.1)) then
c           write (*,*) ' fpi2rnew_qedc23 1 :', mr,gr
           mr=mr+dM
           gr=gr+dG
c           write (*,*) ' fpi2rnew_qedc23 2 :', mr,gr
        endif
        mr2  =mr *mr
        mr4  =mr2*mr2
c        ratm =xp/mr2
c        bepm =dsqrt(1.d0-ratm)
        bepm=sqrt((1.d0-(m1+m0)**2/mr2)*(1.d0-(m1-m0)**2/mr2))
        ppim =0.5d0*mr*bepm
        lgm  =dlog((mr+2.d0*ppim)/(2.d0*mp))
        hm   =2.d0/pi*ppim/mr*lgm
        dhdsm=hm*(0.125d0/ppim**2-0.5d0/mr2)+0.5d0/pi/mr2
        fs   =gr*mr2/ppim**3*(ppis**2*(hs-hm)+(mr2-s)*ppim**2*dhdsm)
        d    =3.d0/pi*mp2/ppim**2*lgm+mr/2.d0/pi/ppim-mp2*mr/pi/ppim**3
        if (i.eq.1) then
           bepn=sqrt((1.d0-(2.d0*m1)**2/mr2))
           ppin =0.5d0*mr*bepn
           lgn  =dlog((mr+2.d0*ppin)/(2.d0*mp))
           dn   =3.d0/pi*mp2/ppin**2*lgn+mr/2.d0/pi/ppin
     &          -mp2*mr/pi/ppin**3
           if (lwrite) then
           write (*,*) ' ######## CC d=? #######',d ,mr,gr
           write (*,*) ' ######## NC d=? #######',dn,mr,gr
           endif
c electronic widths           
           geecc=alp**2*bepm**3/36.d0/gr*mr2*(1.d0+d*gr/mr)**2
           geenc=alp**2*bepn**3/36.d0/gr*mr2*(1.d0+dn*gr/mr)**2
           if (lwrite) then
           write (*,*) ' ###G_ee[CC], G_ee[NC], CC/NC  #######',
     &          geecc,geenc,geecc/geenc
           write (*,*) ' exp:',4.74d-5*gr
           endif
        endif
        if (iopt.eq.11) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)
           else 
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.31) then
           grs=gr*(ppis/ppim)**3*(mr/e)
        else if (iopt.eq.12) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)**2
           else 
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.32) then
           grs=gr*(ppis/ppim)**3*(mr/e)**2
        else
c original CMD-2 fit version
           if (i.eq.1) then
              grs=gr*(ppis/ppim)**3*(mr/e)
           else 
              grs=gr
           endif           
        endif
        rBWnu=mr2*(1.d0+d*gr/mr)
        cBWnu=DCMPLX(rBWnu,0.D0)
        cBWde=DCMPLX(mr2-s+fs,-mr*grs)
        cBWGSr(i)=cBWnu/cBWde
      enddo  
c delta argument in radians       
      deltaarg=phidelta*2.d0*pi/360.d0
      gamaarg=phigama*2.d0*pi/360.d0
      cdels =DCMPLX(dcos(deltaarg),dsin(deltaarg))*
     &       DCMPLX(delta*s/mo2,0.d0)
      cBWom =DCMPLX(mo2,0.d0)/DCMPLX(mo2-s,-mo*go)
      cepdom=cone+cdels*cBWom
      betaarg=phibeta*2.d0*pi/360.d0
      cbeta =DCMPLX(dcos(betaarg),dsin(betaarg))*
     &       DCMPLX(beta,0.d0)
      cgama =DCMPLX(dcos(gamaarg),dsin(gamaarg))*
     &       DCMPLX(gama,0.d0)
      cnorm =cone+cbeta+cgama
c      write (*,*) ' fpi2rnew_qedc23: coefs',cepdom,cbeta,cgama
c      write (*,*) ' fpi2rnew_qedc23: BWs',cBWGSr
      cfpis  =(cBWGSr(1)*cepdom+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      cfpis1 =(cBWGSr(1)+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      fpi2rnewtot=cfpis *DCONJG(cfpis )
      fpi2rnewie1=cfpis1*DCONJG(cfpis1)
c      write (*,*) ' fpi2rnew_qedc23: ',cfpis,cfpis1
      if (iso.eq.0) then        
        fpi2rnew_c_qedc23 =fpi2rnewtot-fpi2rnewie1
      else if (iso.eq.1) then
        fpi2rnew_c_qedc23 =fpi2rnewie1
      else 
        fpi2rnew_c_qedc23 =fpi2rnewtot
      endif
c      write (*,*) ' fpi2rnew_qedc23:',e,fpi2rnew_qedc23
      return
      end

      function fpi2fitchptail_qedc23(s)
      implicit none
      integer np,ier
      parameter(np=3)
      real *8 fpi2fitchptail_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc23
      real *8 emima1(2)
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc23/IER

c      ++++ fpi2fitchptail_qedc23 ++++

      include 'fpi2fitchptail.h'
c
       e=sqrt(s)
c       write (*,*) ' fpi2fitchptail_qedc23: e',e
       yfit=0.d0
C       e1=0.27914036d0
C       en=0.460d0
       e1=emima1(1)
       en=emima1(2)
       if ((e.gt.en).or.(e.lt.e1)) then
          fpi2fitchptail_qedc23=0.d0
          return
       endif
       es=(en+e1)
       ed=(en-e1)
       amap=2.d0/ed
       bmap=-es/ed
       ex=amap*e+bmap
       if (ier.eq.1) then
          yfit=polynom_qedc23(ex,ares,np)
       else if (ier.eq.2) then
          yfit=polynom_qedc23(ex,asta,np)
       else if (ier.eq.3) then
          yfit=polynom_qedc23(ex,asys,np)
       endif
       fpi2fitchptail_qedc23=yfit
       return
       end
