      subroutine resonances_renocomplex_qedc23()
      implicit none
      integer i,j,k,count,Nall,N1,N2
      parameter(Nall=2193)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      integer nx,ny
      parameter(nx=400,ny=15)
      integer nres(ny),ini_reso_reno_dat
      double complex cone,cggvapx_qedc23,cvpt_new,cvpt_hig,cvpt_low
      double complex r1c,r1l,r1h,ccc,cerror,cerrorsta,cerrorsys
      double precision null,emin,emax,e,s
      double precision reno,renl,renh,error
      double precision renpts(nx,ny),renofa(nx,ny),fracerr(nx,ny)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc23/EMI,EMA
      common /renormBW_qedc23/renpts,renofa,fracerr,nres,ini_reso_reno_dat
      external cggvapx_qedc23,BW_qedc23
      include 'common.h'
      null=0.d0
      cone=DCMPLX(1.d0,null)
      s=2.d0**2

      ccc=cggvapx_qedc23(s,cerror,cerrorsta,cerrorsys)
c

      include 'xRdat-extended.f' ! just DATA X/.../ statement
c      
      do j=2,15
         emin=EMI(j)
         emax=EMA(j)
         call getindex_qedc23(emin,Nall,X,N1)
         call getindex_qedc23(emax,Nall,X,N2)
         count=0
         do 10 i=N1,N2
            count=count+1
            e=X(i)
            s=e*e 
            cvpt_new= cggvapx_qedc23(s,cerror,cerrorsta,cerrorsys)
            cvpt_hig= cvpt_new+cerror
            cvpt_low= cvpt_new-cerror
            r1c=cone-cvpt_new
            r1h=cone-cvpt_hig
            r1l=cone-cvpt_low
            reno=r1c*DCONJG(r1c) ! undressing factor
            renh=r1h*DCONJG(r1h) ! + error
            renl=r1l*DCONJG(r1l) ! - error11
            error=abs((renh-renl)/2.0d0)/reno
            renpts(count,j)=e
            renofa(count,j)=reno
            fracerr(count,j)=error
 10      continue
         nres(j)=count
      enddo
c     ..................................................................
      return
      end
