c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; funalpqed.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:35:18 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Tue Jul 18 13:09:23 2017
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2010 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      function funalpqedcx_qedc23(s,cerror,cerrorsta,cerrorsys,
     &     contributionflag)
c calculating alpha complex of error cerror, statstical and systematic hadronic uncertainties in
c provided separately via cerrorsta,cerrorsys
      implicit none
      character*3 contributionflag
      double precision s,error,null,rerr,ierr
      double complex funalpqedcx_qedc23,cerror,cerrorsta,cerrorsys
      double complex cggvapx_qedc23,cerrder,cerrdersta,cerrdersys,
     &     calept,cahadr,caltop,cDalphaweak1MSb
      double complex cone,calp,cvpt_new,cvpt_hig,cvpt_low,alphac,alphah
     &     ,alphal,alphact,alphaht,alphalt
      external cggvapx_qedc23
      include 'common.h'      
      common /cres_qedc23/calept,cahadr,caltop,cDalphaweak1MSb  ! complex results
      LEPTONflag=contributionflag
      iLEP=LFLAG_qedc23(LEPTONflag)
c
      null=0.d0
      cone=DCMPLX(1.d0,null)
      calp=DCMPLX(alp,null)
c
      cvpt_new= cggvapx_qedc23(s,cerrder,cerrdersta,cerrdersys)
      cvpt_hig= cvpt_new+cerrder
      cvpt_low= cvpt_new-cerrder
c
      alphac =calp/(cone-cvpt_new)
      alphah =calp/(cone-cvpt_hig)
      alphal =calp/(cone-cvpt_low)
      alphact=calp/(cone-cvpt_new-caltop-cDalphaweak1MSb)
      alphaht=calp/(cone-cvpt_hig-caltop-cDalphaweak1MSb)
      alphalt=calp/(cone-cvpt_low-caltop-cDalphaweak1MSb)
      rerr=abs(DREAL(alphaht-alphalt))/2.d0
      ierr=abs(AIMAG(alphaht-alphalt))/2.d0
      cerror=DCMPLX(rerr,ierr)
      cerrorsta=alphac/calp*alphac*cerrdersta
      cerrorsys=alphac/calp*alphac*cerrdersys
      funalpqedcx_qedc23=alphact
      return
      END
