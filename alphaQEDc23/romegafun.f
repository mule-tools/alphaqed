      function romegafun_qedc23(s,ier)
c     fitted R(s) non-pipi data in omega resonance region BW_qedc23 + background fit,
c     background fitted by 2nd order Chebyshev polynomial
c     pipi background data provided via rpipiomegabackground_qedc23 [function included in Rdat_fun.f]
      implicit none
      integer np,ier,ier1
      parameter(np=9)
      real *8 romegafun_qedc23,resfitfunx_qedc23,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aomres,aomsta,aomsys,rpipiomegabackground_qedc23,yfit1,yfit2,yfit3
      dimension aomres(np),aomsta(np),aomsys(np)
      REAL*8 FSTA(5),FSYS(5)
      COMMON/RESRELERR_qedc23/FSTA,FSYS
      COMMON/ERR_qedc23/IER1
      common /poly_qedc23/e1,en,amap,bmap
      external resfitfunx_qedc23,rpipiomegabackground_qedc23
c      ++++ omegafit ++++  

      include 'omegafit.h'

      IER1=ier
      e=sqrt(s)
      yfit=0.d0
      e1=0.739d0
      en=0.851d0
      if ((e.gt.en).or.(e.lt.e1)) then
         romegafun_qedc23=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      yfit1=resfitfunx_qedc23(s,aomres,np)
C      yfit2=resfitfunx_qedc23(s,aomsta,np)   ! + value tot error
C      yfit3=resfitfunx_qedc23(s,aomsys,np)   ! - value tot error
      if (ier.eq.1) then
        yfit=yfit1+rpipiomegabackground_qedc23(s,ier)
      else if (ier.eq.2) then
        yfit=sqrt((yfit1*fsta(2))**2+rpipiomegabackground_qedc23(s,ier)**2)
      else if (ier.eq.3) then
        yfit=yfit1*fsys(2)+rpipiomegabackground_qedc23(s,ier)
      endif
      romegafun_qedc23=yfit
      return
      end
