      parameter (np=6)
      integer  lista(np)
      dimension covar(np,np),alpha(np,np),a(np),am(np),ac(np),ap(np),
     &     gues(np),guesini(np,3)
c  3.00 -- 3.2 update jan 2021
      DATA guesini/
      include 'param_epemfit1432.h5'
      emin=3.000d0
      emax=3.200d0
      npi=6
      name='epemfit1432'
      job=5
      ich=5
