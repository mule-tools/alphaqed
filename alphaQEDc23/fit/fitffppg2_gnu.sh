#!/bin/csh -fx
if ("$1" == "") goto usage

  set KILL_FILES=( \
		fitR.dat)
    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif

if ("$1" == "1") then 
set name = ffppg2fit0810
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  update jan 2012/ june 2013   ! fit for n>2 channels only\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "2") then 
set name = ffppmfit0810
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  update jan 2012/ june 2013 ! fit for all channels\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "3") then 
set name = fpi2fitchptail
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.285 -- 0.460 updated feb 2021 ! fpp chpt tail fit\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif

set ijob = 1

echo ' job' $ijob ' is running'

  foreach i ($KILL_FILES)
    if ( -r $i ) then
      mv -f $i $i$ijob
      continue
    endif
  end

cp guess_$name.h$ijob Rdataguess.f
echo "      name='"$name"'"  >> Rdataguess.f
echo "      job=""$1"  >> Rdataguess.f
echo "      ich=""$ijob"   >> Rdataguess.f

make -f make_fitffppg2

echo ' run  fitffppg2'
time ./fitffppg2

cat array.$ijob > fitpar.dat

cat fort.7 >> fitpar.dat 
echo "      DATA ""emima$ijob /"   >> fitpar.dat
cat fort.10 >> fitpar.dat
 rm -f fort.10
cat fort.1001 >> rangelist.dat

cat fitRdat0gnuplot.in > fitRdatplot_gnu.in
cat >> fitRdatplot_gnu.in <<_EOF_
set ylabel " R fit " 
plot "fort.11" skip 3 using 1:2:(\$2-\$3):(\$2+\$4) with yerrorlines t "rs40",\
     "fort.12" skip 3 using 1:2 with lines ls 1 t "rs40smoothed", '' skip 3 using 1:3 with lines ls 1 t "", '' skip 3 using 1:4 with lines ls 1 t "",\
     "fitR.dat" skip 3 using 1:2 with lines ls 2 t "new fit", '' skip 3 using 1:5 with lines ls 2 t "", '' skip 3 using 1:6 with lines ls 2 t ""
pause -1
clear
_EOF_

gnuplot -persist fitRdatplot_gnu.in


echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

echo -n "Update Rdat_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
cp $name.h ../
cd ..
make
cd ./fit/
endif

echo "Results in fitpar.dat"

exit 0
usage:
echo "Usage fit function name labels 1,2,3"
echo "1=ffppg2fit0810"
echo "2=ffppmfit0810"
echo "3=fpi2fitchptail"
exit 2

