      program fitR3Gdat
c fitting data from R3G0325.f and R330325.f
c dec 2019 implemented new data cut excl vs incl data separated now by lowest KEDR point
      implicit none
      character*23 chan(8,6,3)
      character*14 name
      integer i,j,jj,n,jkl,Nall,Ndat,icount,IGS,iso,
     &     Ismooth,Npipi,NCOM,itruedatapoints,idat,job,ich,
     &     iomegaphidat,iintRd
      parameter(Nall=2193,Ndat=971)
      INTEGER N3G,N3GX,N33X
      PARAMETER(N3G=577)
      DOUBLE PRECISION X3G(N3G),Y3Gdat(N3G,3),Y3G(N3G,3)
      DOUBLE PRECISION X33(N3G),Y33dat(N3G,3),Y33(N3G,3)
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      real *8 e,emin,emax,s,RS3G,rs3gsmoothed,xxx
      real *8 res(4),rfi(4)
      real *8 X(Nall),XD(Ndat),XA(Ndat)
c,Xpipi(Npipi),ECOM(NCOM)
      INTEGER NA,NB,NF,IOR,ICHK,NFIN,IORIN
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      REAL *8 CHPTCUT,EC,ECUT
      REAL *8 ALS2,EST,ESY,MZINP,MTOP2
      real *8 chisq,chisqbegin,chisqend,sx,ex,de,yfit,es,ed,amap,bmap
      real *8 y,rm,rp,sig,xx,alamda,covar,alpha,a,gues,guesini
      real *8 fitpolynom,ysav,am,ac,ap,ei,si
      integer  nxa,noffset,ni,nj,nw,np,k,ns,npi
      parameter (nj=500)
      dimension e(nj),s(nj),y(nj),rm(nj),rp(nj),ysav(nj,3)
      REAL xnskip,xncurv,xNPT,yfim,yfip,yfie,errt
      INTEGER nskip,NU1N,ier,jjj,n1,n2,n12,n21,JER
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP,
     &     EOMM1,EOMP1,EFIM1,EFIP1
      REAL*8 CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH
      external funcs,RS3G,rs3gsmoothed
      COMMON /R3GDAT/X3G,Y3Gdat,Y3G,N3GX
      COMMON /R33DAT/X33,Y33dat,Y33,N33X
c      FUNCTION RSpQCD(s,IER) in Rdat_fun.f requires QCD parameters incl errors
c provided via the following common block:
      COMMON/QCDPA/ALS2,EST,ESY,MZINP,MTOP2,NF,IOR,ICHK ! global INPUT here
c to set QCD parameters alpha_s with stat and sys error
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z, top mass,
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s)
c to be calculated, ICHK is dummy here
c (in some programs used for alternative routines calculating R(s))
      common/var1/pi1,ALINP,ERRAL,EINP,MTOP1,IORIN,NFIN   ! IOR an NF as relevant for ALINP determination
      COMMON /RCUTS/CHPTCUT,EC,ECUT
      COMMON/GUSA/IGS,iso
      COMMON /ERR_qedc23/JER
      COMMON/RESDOMAINS/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT/EOMM1,EOMP1,EFIM1,EFIP1,iomegaphidat,iintRd  ! omega & phi ranges set in resonances-dat.f via RESDOMAINS tranferred to OMEPHIDAT below
      common /cufit/CHPTCUTLOC,EHIGH,ERXYDAT,ECHARMTH  ! set in RS3G_fit.f
      include '../common.h'
      include 'R3Gdataguess.f'
      call constants()
      call constants_qcd() ! fills commons var and RCUTS
      include '../xRdat-extended.f'
      include '../xRdat-nonres.f'
c      include '../dat/xset0821.f'
      call Rdata()
c extend to get smooth mactch to rewighted RS40smooth
c      ERXYDAT=2.130d0 in RSXY_fit.f
      if (emax.eq.2.125d0) emax=2.150d0
c fill common QCDPA
      ALS2=ALINP
      EST=ERRAL
      ESY=0.D0
      MZINP=EINP
      MTOP2=MTOP1
      IOR=4                     ! default
      NF =5                     ! default
      IGS=0           ! Gounaris-sakurai parametrisation for pipi channel
      iso=2           ! e^+ e^- data GS fit; iso=0,1 yields isospin 0, isovector part
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
      IRESON=0        ! include narrow resonances in fits
      iresonances=0   ! include narrow resonances in data sets
      iintRd=0
c      iresonances=1   ! include narrow resonances in data sets
      iomegaphidat=-1
      icount=0
C      iomegaphidat=0 ! use BW with PDG parameters for omega and phi
      if (iomegaphidat.eq.0) iresonances=1
      if (iomegaphidat.eq.0) IRESON=1
C      iomegaphidat=1 ! use omega and phi data contribution to R
      if (iomegaphidat.eq.1) iresonances=0
      if (iomegaphidat.eq.1) IRESON=0
C      iomegaphidat=2 ! replace BW resonances of option 0 by resonance data
      if (iomegaphidat.eq.2) iresonances=1
      if (iomegaphidat.eq.2) IRESON=1
C     setting for background fits set in fitRdata.f
C      iomegaphidat=-1 ! background only
      if (iomegaphidat.eq.-1) iresonances=0
      if (iomegaphidat.eq.-1) IRESON=0
c      CHPTCUT=0.318d0
c      EC=5.2d0
      EC=9.46d0
      ECUT=40.d0
      do j=1,8
         do i=1,6
            do n=1,3
               chan(j,i,n)='                       '
            enddo
         enddo
      enddo
c set 1 name = epem3gfit0814new
c  E= 0.810 -- 1.000  updated nov 2010 / jan 2012 / june 2013\
      chan(1,1,1)='       DATA aresl/'
      chan(1,1,2)='       DATA astal/'
      chan(1,1,3)='       DATA asysl/'
c  E=1.040 -- 1.435   updated nov 2010 / jan 2012 / june 2013\
      chan(1,2,1)='       DATA aresh/'
      chan(1,2,2)='       DATA astah/'
      chan(1,2,3)='       DATA asysh/'
c  phi resonance background [applies for 1.00 - 1.04]\
c   1.00001 -- 1.03999 update jan 2012 / june 2013 / dec 2014
      chan(1,3,1)='       DATA aresfr/'
      chan(1,3,2)='       DATA astafr/'
      chan(1,3,3)='       DATA asysfr/'
c  Fit below phi \
c   0.958 -- 1.000 update jan 2012 / june 2013 \
      chan(1,4,1)='       DATA aresfm/'
      chan(1,4,2)='       DATA astafm/'
      chan(1,4,3)='       DATA asysfm/'
c set 4 name = epem3gfit079081
c  0.787 -- 0.8099 ares epemfit079081  update jan 2012\
      chan(4,1,1)='       DATA ares/'
      chan(4,1,2)='       DATA asta/'
      chan(4,1,3)='       DATA asys/'
c set 5 name = epem3gfit1425
c   1.375 -- 2.038 update jan 2012 / june 2013\
      chan(5,1,1)='       DATA areslow/'
      chan(5,1,2)='       DATA astalow/'
      chan(5,1,3)='       DATA asyslow/'
c   1.395 -- 2.000 update jan 2012 / june 2013 \
      chan(5,2,1)='       DATA ares/'
      chan(5,2,2)='       DATA asta/'
      chan(5,2,3)='       DATA asys/'
c  1.975 -- 2.125 update jan 2012 / june 2013
      chan(5,3,1)='       DATA fres2023 /'
      chan(5,3,2)='       DATA fsta2023 /'
      chan(5,3,3)='       DATA fsys2023 /'
c set 7 name = r3gfitrholow
c 0.318 -- 0.778 updated nov 2010 (incl BaBar/KlOE) / jan 2012 / june 2013\
      chan(7,1,1)='       DATA ares/'
      chan(7,1,2)='       DATA asta/'
      chan(7,1,3)='       DATA asys/'
c set 8 name = r3gfitrhohig
c  0.769  -- 0.789 update jan 2012 /june 2013\
      chan(8,1,1)='       DATA ares/'
      chan(8,1,2)='       DATA asta/'
      chan(8,1,3)='       DATA asys/'

c      write (7,'(A,A,A)') 'c     ++++',name,'++++ '

c      do i=1,Ndat
c         write(45,*) XD(i)
c      enddo
c      do i=1,Npipi
c         write(45,*) Xpipi(i)
c      enddo
c      stop 10
c      do i=1,Nall
c         ex= X(i)
C      idat=1
      idat=0
      if (idat.eq.1) then
         write(11,*)  '       2.0000'
         write(11,*)  '       1.0000'
         write(11,*)  '   ',float(N3G)
         do i=1,N3G
            ex= X3G(i)
c         XD(i)=ex
c         if (e.gt.ECUT) goto 10
            icount=icount+1
            sx=ex*ex
            do j=1,3
               res(j)=Y3G(i,j)
               rfi(j)=rs3gsmoothed(sx,j)
            enddo
c plot total error
            res(4)=sqrt(res(2)**2+res(3)**2)
            rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
            write(11,98) x3g(i),y3g(i,1),res(4),res(4),res(3),i
         write(12,98) x3g(i),rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),rfi(3),i
         enddo
      endif
c 10   continue
      write(1,*) icount
 98   format (1X,F10.7,4(2x,1PE11.5),2x,i4)
c      iresonances=0
      ECUT=40.d0
      EC=9.46d0  ! use data below Upsilon
c initialize
      if (guesini(1,1).eq.0.d0) then
         do ier=1,3
            do i=1,np
               guesini(i,ier)=0.0d0
            enddo
         enddo
      endif

      open (UNIT=2,FILE='fitR.dat',STATUS='NEW')
C
c excluding resonances
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
c psi1  3.08587D0 3.10787D0
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
c yps1  9.44937D0 9.47137D0
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0
c yps4  10.473D0  10.687D0
c yps5  10.690D0  10.950D0
c yps6  10.975D0  11.063D0
c make numbered list of points
c      do i=1,N3G
c         write(14,'(1x,i4,3x,f12.6)') i,X3G(i)
c      enddo
c     X3G(1)          EHIGH          ERXYDAT   ECUTFIT
c   ----|---------------|--------------|---------|--------------
c chiral  R3G0321 excl      RS3G[RS40]             pQCD:RS3GpQCD
c all in merged into RS3G from RS3G_fun interpolated at given Energy
c below EHIGH (limit of trustable excl. data below KEDR incl. point) use R3G0321 array
      itruedatapoints=0
      itruedatapoints=1
      noffset=0
      if ((itruedatapoints.eq.0)
     &     .or.(emin.gt.EHIGH).or.(emax.le.X3G(1))) then
         call getindex(emin,Ndat,XD,n1)
         call getindex(emax,Ndat,XD,n2)
         icount=noffset
         do i=n1,n2
            icount=icount+1
            XA(icount)=XD(i)
         enddo
         nxa=icount
      else if (itruedatapoints.eq.1) then
         if ((emin.ge.X3G(1)).and.(emax.le.EHIGH)) then
            call getindex(emin,N3G,X3G,n1)
            call getindex(emax,N3G,X3G,n2)
            icount=0
            do i=n1,n2
               icount=icount+1
               XA(icount)=X3G(i)
            enddo
            nxa=icount
c if chiral tail
         else if (emin.lt.X3G(1)) then
            noffset=0
            call getindex(emin,Ndat,XD,n1)
            call getindex(X3G(1),Ndat,XD,n2)
            icount=noffset
            do i=n1,n2
               icount=icount+1
               XA(icount)=XD(i)
            enddo
            noffset=icount
            if (emax.le.EHIGH) then
               call getindex(emax,N3G,X3G,n1)
               icount=noffset
               do i=1,n1
                  icount=icount+1
                  XA(icount)=X3G(i)
               enddo
               nxa=icount
            else if (emax.gt.EHIGH) then
               call getindex(EHIGH,N3G,X3G,n21)
               icount=noffset
               do i=1,n21
                  icount=icount+1
                  XA(icount)=X3G(i)
               enddo
               call getindex(EHIGH,Ndat,XD,n1)
               call getindex(emax,Ndat,XD,n2)
               do i=n1,n2
                  icount=icount+1
                  XA(icount)=XD(i)
               enddo
               nxa=icount
            endif
         else if ((emin.lt.EHIGH).and.(emax.gt.EHIGH)) then
            call getindex(emin,N3G,X3G,n12)
            call getindex(EHIGH,N3G,X3G,n21)
            icount=0
            do i=n12,n21
               icount=icount+1
               XA(icount)=X3G(i)
            enddo
            call getindex(EHIGH,Ndat,XD,n1)
            call getindex(emax,Ndat,XD,n2)
            do i=n1,n2
               icount=icount+1
               XA(icount)=XD(i)
            enddo
            nxa=icount
         endif
      endif
      n1=1
      n2=nxa
c      if ((job.eq.1).and.((ich.eq.1).or.(ich.eq.4))) then
c      else if ((job.eq.4).or.((job.eq.5).and.(ich.eq.3))) then
c      else if ((job.eq.1).and.((ich.eq.2).or.(ich.eq.3))) then
c         if (XD(n1).lt.emin) n1=n1+1
c      else
c         if (emax.gt.XD(n2)) n2=n2+1
c      endif
c exclude threshold value (variance zero is singular)
c      if ((itruedatapoints.eq.0).and.(n1.le.1)) n1=2
c      if ((itruedatapoints.eq.1).and.(n2.gt.N3G)) n2=N3G
      write(*,*) ' emin,emax ',emin,emax
      write(*,*) ' actual ',n1,XA(n1),n2,XA(n2)
      nw=nxa
      if (idat.eq.0) then
         write(11,*)  '       2.0000'
         write(11,*)  '       1.0000'
         write(11,*)  '   ',float(nw)
      endif
      write(12,*)  '       2.0000'
      write(12,*)  '       1.0000'
      write(12,*)  '   ',float(nw)
c empty arrays
      do i=1,nj
         e(i)=0.d0
         y(i)=0.d0
      enddo
C error loop
      do jjj=1,3
         ier=jjj
         icount=0
         do jj=n1,n2
            ei=XA(jj)
            icount=jj
            e(icount)=ei
            si=ei*ei
            if (icount.eq.1) xxx=RS3G(si,ier)
            y(icount)=RS3G(si,ier)
            if (ier.eq.1) then
C               write (*,*) ' fitR3Gdat:',ei,y(icount)
               ier=2
               rm(icount)=RS3G(si,ier)
               if (rm(icount).eq.0.d0) then
                  ier=3
                  rm(icount)=RS3G(si,ier)
               endif
               ier=1
            endif
            ysav(icount,ier)=y(icount)
            do jkl=1,3
               rfi(jkl)=rs3gsmoothed(si,jkl)
            enddo
c plot total error
            rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
            if (ier.eq.3) then
               write(12,98) ei,rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),
     &              rfi(3),jj
            endif
         enddo
      enddo

      write(*,*) ' number of fit points[range]:',
     &     icount,'[',e(1),' -- ',e(icount),']'
      write(1001,*) ' number of fit points[range]:',
     &     icount,'[',e(1),' -- ',e(icount),']'
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',e
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',y
c      write(*,*) ' *********************************'
c      write(*,*) '  array dim, occupncy:',nj,nw
C error loop
      write(13,*)  '       2.0000'
      write(13,*)  '       1.0000'
      write(13,*)  '   ',float(icount)
c
c use when stat error zero like in pQCD or Chpt
c      do jjj=1,2
c         ier=jjj
c         if (ier.eq.2) ier=3
      do jjj=1,3
         ier=jjj
         do j=1,icount
            y(j)=ysav(j,ier)
            if (ier.eq.1) then
c adopt total error for fit variance
               rm(j)=sqrt(ysav(j,2)**2+ysav(j,3)**2)
            else
c fitting error bounds, assume 1% uncertainty of the error for fitting
               rm(j)=ysav(j,ier)/100.
            endif
c            y(j)=ysav(j,1)
c            rm(j)=sqrt(ysav(j,2)+ysav(j,3))
            write(8,*) ' input',j,ier,y(j),rm(j)
            write(13,98) e(j),y(j),rm(j),rm(j),ysav(j,3),j
            if ((idat.eq.0).and.(ier.eq.1)) then
               write(11,98) e(j),y(j),rm(j),rm(j),ysav(j,3),j
            endif
         enddo
c      Mapping to Tschebycheff interval [-1,+1]
         nw=icount
         es=(e(nw)+e(1))
         ed=(e(nw)-e(1))
         amap=2.d0/ed
         bmap=-es/ed
         alamda=-1.d0
         if (npi.lt.np) then
            do i=npi+1,np
               guesini(i,jjj)=0.d0
            enddo
         endif
         do i=1,np
            a(i)=guesini(i,jjj)
            lista(i)=i
         enddo
         write (*,*) ' initial fit coefficients',a
         chisq=0.0
         do j=1,nw
            s(j)=amap*e(j)+bmap
            yfit=fitpolynom(s(j),a,npi)
            chisq=chisq+((y(j)-yfit)/(rm(j)))**2
            write(*,*) j,s(j),yfit,chisq
         enddo
         chisqbegin=chisq
         write(*,*) ' '
         write(*,*) ' chisq of guess:',chisqbegin
         write(*,*) ' '
c      ns: number of iterrations
         ns=20000
c      npi: number of parameters to be ajusted
         do 300 k=1,ns
            alamda=-1.d0
            call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &           covar,alpha,np,chisq,funcs,alamda)
c       write(*,'(1x,6(1pe11.4))') (a(i),i=1,npi)
c       write(*,*) ' chisq,alamda',chisq,alamda
 300     continue
         write(*,'(/1x,a,i2,t18,a,f10.4,t43,a,e9.2)') 'Iteration #',ns,
     *        'Chi-squared:',chisq,'ALAMDA:',alamda
         write(*,'(1x,t5,a,t13,a,t21,a,t29,a,t37,a,
     &       t45,a,t53,a,t61,a,t69,a)') 'A(1)',
     *       'A(2)','A(3)','A(4)','A(5)','A(6)',
     *       'A(7)','A(8)','A(9)'
         write(*,'(1x,12(1pe11.4))') (a(i),i=1,npi)
c Recalculate chi2
         alamda=0.d0
         call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
         write(*,*) 'Uncertainties:'
         write(*,'(1x,12(1pe11.4)/)') (sqrt(covar(i,i)),i=1,npi)
         write(*,'(1x,a)') 'Expected results:'
         write(*,'(1x,f7.2,5f8.2/)') a
         chisq=0.0
         do j=1,nw
            yfit=fitpolynom(s(j),a,npi)
            chisq=chisq+((y(j)-yfit)/(rm(j)))**2
         enddo
         chisqend=chisq
         write(*,*) ' '
         write(*,*) chisqbegin,' -->',chisqend,'S=',
     &        sqrt(chisqend/(nw-1))
         write(*,*) ' '
         if (ier.eq.1) then
            do j=1,np
               ac(j)=a(j)
            enddo
         endif
         if (ier.eq.2) then
            do j=1,np
               am(j)=a(j)
            enddo
         endif
         if (ier.eq.3) then
            do j=1,np
               ap(j)=a(j)
            enddo
         endif
      enddo                     ! end ier loop

c Header for ploting fit
      write(2,*) '      2.0000'
      write(2,*) '      1.0000'
      write(2,*) '    100.000'
C Write plot data of fit
      de=(e(nw)-e(1))/100.
      ex=e(1)
      do j=1,100
        yfit=fitpolynom(amap*ex+bmap,ac,npi)
        yfim=fitpolynom(amap*ex+bmap,am,npi)
        yfip=fitpolynom(amap*ex+bmap,ap,npi)
        yfie=sqrt(yfim**2+yfip**2)
        write(2,124) ex,yfit,yfit-yfim,yfit+yfim,yfit-yfie,yfit+yfie
        ex  =ex+de
      enddo
c #############################
      if ((job.eq.5).and.(ich.eq.2)) then
         write (*,*) ' #############################'
         write(*,'(1x,f7.3,5f8.3/)')  (ac(j),j=1,npi)
         write(*,'(1x,f7.3,5f8.3/)')  (am(j),j=1,npi)
         write(*,'(1x,f7.3,5f8.3/)')  (ap(j),j=1,npi)
         write (*,*) ' amap,bmap:',amap,bmap
         write (*,*) ' #############################'
      endif
c #############################

c Header for ploting data
      write(3,*) '      2.0000'
      write(3,*) '      1.0000'
      write(3,*) '   ',float(nw)
      write(4,*) '      2.0000'
      write(4,*) '      1.0000'
      write(4,*) '   ',float(nw)
      do j=1,nw
         errt=sqrt(ysav(j,2)**2+ysav(j,3)**2)
         write(3,124) e(j),ysav(j,1),ysav(j,2),ysav(j,2)
         write(4,124) e(j),ysav(j,1),errt,errt
c         write(4,124) e(j),y(j),rm(j),rm(j)
      enddo
c      write (7,*) 'job,ich=',job,ich
      write (7,'(A)') chan(job,ich,1)
      if (np.eq.3)  write(7,83) ac
      if (np.eq.4)  write(7,84) ac
      if (np.eq.5)  write(7,85) ac
      if (np.eq.6)  write(7,86) ac
      if (np.eq.7)  write(7,87) ac
      if (np.eq.8)  write(7,88) ac
      if (np.eq.9)  write(7,89) ac
      if (np.eq.10) write(7,90) ac
      if (np.eq.11) write(7,91) ac
      if (np.eq.12) write(7,92) ac
      if (np.eq.13) write(7,93) ac
      if (np.eq.14) write(7,94) ac
      write (7,'(A)') chan(job,ich,2)
      if (np.eq.3)  write(7,83) am
      if (np.eq.4)  write(7,84) am
      if (np.eq.5)  write(7,85) am
      if (np.eq.6)  write(7,86) am
      if (np.eq.7)  write(7,87) am
      if (np.eq.8)  write(7,88) am
      if (np.eq.9)  write(7,89) am
      if (np.eq.10) write(7,90) am
      if (np.eq.11) write(7,91) am
      if (np.eq.12) write(7,92) am
      if (np.eq.13) write(7,93) am
      if (np.eq.14) write(7,94) am
      write (7,'(A)') chan(job,ich,3)
      if (np.eq.3)  write(7,83) ap
      if (np.eq.4)  write(7,84) ap
      if (np.eq.5)  write(7,85) ap
      if (np.eq.6)  write(7,86) ap
      if (np.eq.7)  write(7,87) ap
      if (np.eq.8)  write(7,88) ap
      if (np.eq.9)  write(7,89) ap
      if (np.eq.10) write(7,90) ap
      if (np.eq.11) write(7,91) ap
      if (np.eq.12) write(7,92) ap
      if (np.eq.13) write(7,93) ap
      if (np.eq.14) write(7,94) ap
c next guess parameters
      if (np.eq.3)  write(9,63) ac
      if (np.eq.4)  write(9,64) ac
      if (np.eq.5)  write(9,65) ac
      if (np.eq.6)  write(9,66) ac
      if (np.eq.7)  write(9,67) ac
      if (np.eq.8)  write(9,68) ac
      if (np.eq.9)  write(9,69) ac
      if (np.eq.10) write(9,70) ac
      if (np.eq.11) write(9,71) ac
      if (np.eq.12) write(9,72) ac
      if (np.eq.13) write(9,73) ac
      if (np.eq.14) write(9,74) ac
      if (np.eq.3)  write(9,63) am
      if (np.eq.4)  write(9,64) am
      if (np.eq.5)  write(9,65) am
      if (np.eq.6)  write(9,66) am
      if (np.eq.7)  write(9,67) am
      if (np.eq.8)  write(9,68) am
      if (np.eq.9)  write(9,69) am
      if (np.eq.10) write(9,70) am
      if (np.eq.11) write(9,71) am
      if (np.eq.12) write(9,72) am
      if (np.eq.13) write(9,73) am
      if (np.eq.14) write(9,74) am
      if (np.eq.3)  write(9,83) ap
      if (np.eq.4)  write(9,84) ap
      if (np.eq.5)  write(9,85) ap
      if (np.eq.6)  write(9,86) ap
      if (np.eq.7)  write(9,87) ap
      if (np.eq.8)  write(9,88) ap
      if (np.eq.9)  write(9,89) ap
      if (np.eq.10) write(9,90) ap
      if (np.eq.11) write(9,91) ap
      if (np.eq.12) write(9,92) ap
      if (np.eq.13) write(9,93) ap
      if (np.eq.14) write(9,94) ap
      write (10,125) e(1),e(nw)

 124  format(1x,f11.6,5(2x,1pe13.6))
 125  format('     &     ',f9.6,'d0,',f9.6,'d0/')
 99   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 94   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 93   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 92   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 91   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 90   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 89   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 88   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 87   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 86   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 85   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0/')
 84   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 83   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
c +++++
 74   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 73   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 72   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 71   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 70   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 69   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 68   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 67   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 66   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 65   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,')
 64   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 63   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
      write (*,*) ' -- 1 -- iomegaphidat=',iomegaphidat,
     &     ' -- iresonances=',iresonances,
     &     ' -- IRESON=',IRESON
      stop 10
      end

      function fitpolynom(x,a,na)
      implicit none
      INTEGER na,i
      REAL*8 x,ax,a,fitpolynom,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      fitpolynom=0.d0
      do i=1,na
         fitpolynom=fitpolynom+a(i)*t(i)
      enddo
      return
      end
C
      include './nur/mrqcof.f'
      include './nur/mrqmin.f'
      include './nur/covsrt.f'
      include './nur/gaussj.f'

      subroutine funcs(x,a,y,dyda,na)
      implicit none
      INTEGER na,i
      REAL*8 x,y,ax,a,dyda,fitpolynom,t
      dimension a(na),dyda(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo

      y=fitpolynom(x,a,na)

      do i=1,na
         dyda(i)=t(i)
      enddo
      return
      end

c  0.2791404      1
c  0.3150000      7
c  0.3200000      8
c  0.6100000     57
c  0.6160000     58
c  0.8100000    152
c  1.4000000    307
c  2.0000000    406
c  2.2000000    407
c  2.4000000    408
c  2.5000000    409
       FUNCTION GZINTCOR(S)
C      --------------------
C      CORRECTION FOR GAMMA-Z-INTERFERRENCE IN HIGH ENERGY DATA
       IMPLICITREAL*8 (A-Z)
       PI=4.D0*DATAN(1.D0)
       GMU=1.166390D-5
       ALP=1.D0/137.0359895D0
       MZ2=91.1887D0**2
       ST2=0.2322D0
       POL=DSQRT(2.D0)*GMU/16.D0/PI/ALP*S*MZ2/(S-MZ2)
       QFQF=11.D0/9.D0
       QFVF=QFQF*(21.D0/11.D0-4.D0*ST2)
       VVAAF=10.D0-56.D0/3.D0*ST2+176.D0/9.D0*ST2**2
       VE=-1.D0+4.D0*ST2
       VVAAE=2.D0-8.D0*ST2+16.D0*ST2**2
       C1=QFQF-2.D0*VE*QFVF*POL+VVAAE*VVAAF*POL**2
       GZINTCOR=QFQF/C1
       RETURN
       END
