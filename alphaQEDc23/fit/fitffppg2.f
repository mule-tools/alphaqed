      program fitffppg2
c fitting data from ../old/ffppdata1n_2.f
      implicit none
      character*23 chan(3,3)
      character*14 name
      integer i,j,n,Nall,Ndat,icount,Ndatx,ig2
      parameter(Ndat=174)
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      real *8 e,emin,emax,s,ffppmfit0810,ffppg2fit0810,fpi2fitchptail,
     &     MPi,betapi,FVtoR
      real *8 res(4),rfi(4),rrr(3),fff(3)
      real *8 XG2(Ndat),FFPPG2(Ndat,3),FFPPM(Ndat,3)
      INTEGER NA,NB,NF,IOR,ICHK
      REAL *8 CHPTCUT,EC,ECUT
      REAL *8 ALS2,EST,ESY,MZINP,MTOP2
      real *8 chisq,chisqbegin,chisqend,sx,ex,de,yfit,es,ed,amap,bmap
      real *8 y,rm,rp,sig,xx,alamda,covar,alpha,a,gues,guesini
      real *8 fitpolynom,am,ac,ap,ei,si,GZINTCOR
      integer  ni,nj,nw,np,k,ns,npi
      parameter (nj=500)
      dimension e(nj),s(nj),y(nj),rm(nj),rp(nj)
      real *8  ysav(nj,3)
      REAL xnskip,xncurv,xNPT,yfim,yfip,yfie,errt
      INTEGER nskip,NU1N,ier,jjj,n1,n2,nx5aoffset,nx5moffset,
     &     ii,jj,job,ich,Nmax
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      external funcs,ffppmfit0810,ffppg2fit0810,fpi2fitchptail
      COMMON/ERR/IER

      include '../dat/ffppdata1n_2.h'

      include 'Rdataguess.f'

      MPi=0.13957d0
      nx5aoffset=220
      nx5moffset= 93
      Nmax=117
      do i=1,Nmax
           ii=i+nx5aoffset
           jj=i+nx5moffset
         XG2(i)=X5M(jj)
         do ier=1,3
            FFPPG2(i,ier)=Y5M(jj,ier)
            FFPPM(i,ier)=Y5B(ii,ier)
         enddo
      enddo
c check first x value
      write (*,*) ' job=',job
      write (*,*) ' X5A vs 810.',X5A(nx5aoffset+1),XG2(1),XG2(Nmax)
      do j=1,3
         do n=1,3
            chan(j,n)='                       '
         enddo
      enddo
c set 1 name = ffppg2fit0810
c 0.81 -- 1.00  update jan 2012/ june 2013 ! fit for n>2 channels only
      chan(1,1)='       DATA ares/'
      chan(1,2)='       DATA asta/'
      chan(1,3)='       DATA asys/'
c set 2 name = ffppmfit0810
c 0.81 -- 1.00  update jan 2012/ june 2013 ! fit for all channels
      chan(2,1)='       DATA ares/'
      chan(2,2)='       DATA asta/'
      chan(2,3)='       DATA asys/'
c set 3 name = fppfitchptail
c 0.285 -- 0.355  feb 2021  fit for chpttail
      chan(3,1)='       DATA ares/'
      chan(3,2)='       DATA asta/'
      chan(3,3)='       DATA asys/'
      do j=1,3
         do n=1,3
            write (*,*) chan(j,n)
         enddo
      enddo

      if (job.eq.2) ig2=0
      if (job.eq.1) ig2=1
      if (job.eq.3) ig2=2
c emax=1.000 Ndatx=Ndat-4
      Ndatx=Nmax
      if (ig2.eq.2) Ndatx=28
      write(11,*)  '       3.0000'
      write(11,*)  '       1.0000'
      write(11,*)  '   ',float(Ndatx)
      write(12,*)  '       3.0000'
      write(12,*)  '       1.0000'
      write(12,*)  '   ',float(Ndatx)

      do i=1,Ndatx-1
         ex= XG2(i)/1000.
         if (ig2.eq.2) ex=XXchpt(i)/1000.
         icount=icount+1
         sx=ex*ex
         do j=1,3
            if (ig2.eq.0) res(j)=FFPPM(i,j)
            if (ig2.eq.1) res(j)=FFPPG2(i,j)
            if (ig2.eq.2) then
               res(j)=YYchpt(i,j)
               betapi=sqrt(1.d0-4.d0*MPi**2/sx)
               FVtoR=betapi**3/4.d0
               rrr(j)=res(j)*FVtoR
            endif
c               res(j)=rs40(sx,j)
c            rfi(j)=rs40smoothed(sx,j)
            ier=j
            if (ig2.eq.0) rfi(j)=ffppmfit0810(sx)
            if (ig2.eq.1) rfi(j)=ffppg2fit0810(sx)
            if (ig2.eq.2) then
               rfi(j)=fpi2fitchptail(sx)
               betapi=sqrt(1.d0-4.d0*MPi**2/sx)
               FVtoR=betapi**3/4.d0
               fff(j)=rfi(j)*FVtoR
            endif
         enddo
c plot total error
         res(4)=sqrt(res(2)**2+res(3)**2)
         rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
         write(11,98) ex,res(1),res(4),res(4),res(3),i
         write(12,98) ex,rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),rfi(3),i
         write(13,97) ex,rfi(1),rfi(2),rfi(3)
         write(14,97) ex,rrr(1),rrr(2),rrr(3)
         write(15,97) ex,fff(1),fff(2),fff(3)
      enddo
c 10   continue
      write(1,*) icount
 98   format (1X,F10.7,4(2x,1PE11.5),2x,i4)
 97   format (1X,F10.7,3(2x,1PE11.5))
 96   format (1X,F10.4,3(2x,1PE11.5))
c initialize
c         do ier=1,3
c            do i=1,np
c               guesini(i,ier)=0.0d0
c            enddo
c         enddo

      open (UNIT=2,FILE='fitR.dat',STATUS='NEW')
C
c empty arrays
      do i=1,nj
         e(i)=0.d0
         y(i)=0.d0
      enddo
C error loop
      do jjj=1,3
         ier=jjj
         icount=0
         write (*,*) ' ***** Ndatx=',Ndatx
         do 12 j=1,Ndatx-1
            ei=XG2(j)/1000.
            if (ig2.eq.2) ei=XXchpt(j)/1000.
            icount=icount+1
c            write(*,*) ' icount,j-n1+1',icount,j-n1+1
            e(icount)=ei
            si=ei*ei
            if (ig2.eq.0) y(icount)=FFPPM(j,ier)
            if (ig2.eq.1) y(icount)=FFPPG2(j,ier)
            if (ig2.eq.2) y(icount)=YYchpt(j,ier)
            if (ier.eq.1) then
               ier=2
               if (ig2.eq.0) rm(icount)=FFPPM(j,ier)
               if (ig2.eq.1) rm(icount)=FFPPG2(j,ier)
               if (ig2.eq.2) rm(icount)=YYchpt(j,ier)
               if (rm(icount).eq.0.d0) then
                  ier=3
                  if (ig2.eq.0) rm(icount)=FFPPM(j,ier)
                  if (ig2.eq.1) rm(icount)=FFPPG2(j,ier)
                  if (ig2.eq.2) rm(icount)=YYchpt(j,ier)
               endif
               ier=1
            endif
            ysav(icount,ier)=y(icount)
            write (*,*) e(icount),Y(icount),rm(icount)
 12      continue
      enddo
      write(*,*) ' number of fit points[range]:',
     &     icount,'[',e(1),' -- ',e(icount),']'
      write(1001,*) ' number of fit points[range]:',
     &     icount,'[',e(1),' -- ',e(icount),']'
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',e
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',y
c      write(*,*) ' *********************************'
c      write(*,*) '  array dim, occupncy:',nj,nw
C error loop
      do jjj=1,3
         ier=jjj
         do j=1,icount
            y(j)=ysav(j,ier)
            if (ier.eq.1) then
               rm(j)=sqrt(ysav(j,2)**2+ysav(j,3)**2)
            else
               rm(j)=ysav(j,ier)/100.
            endif
c            y(j)=ysav(j,1)
c            rm(j)=sqrt(ysav(j,2)+ysav(j,3))
          write(8,*) ' input',j,ier,y(j),rm(j)
         enddo
c      Mapping to Tschebycheff interval [-1,+1]
      nw=icount
      es=(e(nw)+e(1))
      ed=(e(nw)-e(1))
      amap=2.d0/ed
      bmap=-es/ed
      alamda=-1.d0
      do i=1,np
        a(i)=guesini(i,jjj)
        lista(i)=i
      enddo
C      npi=12
c      if (ig2.eq.2) npi=3
c      npi=11
c      npi=10
c      npi=9
c      npi=9
c      npi=7
c      npi=6
c      npi= 5
c      npi= 3
c      npi=14
      write (*,*) ' initial fit coefficients',a
      chisq=0.0
      do j=1,nw
        s(j)=amap*e(j)+bmap
        yfit=fitpolynom(s(j),a,npi)
        chisq=chisq+((y(j)-yfit)/(rm(j)))**2
        write(*,*) j,s(j),yfit,chisq
      enddo
      chisqbegin=chisq
      write(*,*) ' '
      write(*,*) ' chisq of guess:',chisqbegin
      write(*,*) ' '
c      ns: number of iterrations
      ns=20000
c      npi: number of parameters to be ajusted
      do 300 k=1,ns
      alamda=-1.d0
        call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
c       write(*,'(1x,6(1pe11.4))') (a(i),i=1,npi)
c       write(*,*) ' chisq,alamda',chisq,alamda
  300 continue
        write(*,'(/1x,a,i2,t18,a,f10.4,t43,a,e9.2)') 'Iteration #',ns,
     *       'Chi-squared:',chisq,'ALAMDA:',alamda
        write(*,'(1x,t5,a,t13,a,t21,a,t29,a,t37,a,
     &       t45,a,t53,a,t61,a,t69,a)') 'A(1)',
     *       'A(2)','A(3)','A(4)','A(5)','A(6)',
     *       'A(7)','A(8)','A(9)'
        write(*,'(1x,12(1pe11.4))') (a(i),i=1,npi)
c Recalculate chi2
      alamda=0.d0
        call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
        write(*,*) 'Uncertainties:'
        write(*,'(1x,12(1pe11.4)/)') (sqrt(covar(i,i)),i=1,npi)
        write(*,'(1x,a)') 'Expected results:'
        write(*,'(1x,f7.2,5f8.2/)') a
      chisq=0.0
      do j=1,nw
        yfit=fitpolynom(s(j),a,npi)
        chisq=chisq+((y(j)-yfit)/(rm(j)))**2
      enddo
      chisqend=chisq
      write(*,*) ' '
      write(*,*) chisqbegin,' -->',chisqend,'S=',sqrt(chisqend/(nw-1))
      write(*,*) ' '
      if (ier.eq.1) then
         do j=1,np
            ac(j)=a(j)
         enddo
      endif
      if (ier.eq.2) then
         do j=1,np
            am(j)=a(j)
         enddo
      endif
      if (ier.eq.3) then
         do j=1,np
            ap(j)=a(j)
         enddo
      endif
      enddo  ! end ier loop

c Header for ploting fit
      write(2,*) '      2.0000'
      write(2,*) '      1.0000'
      write(2,*) '    101.000'
C Write plot data of fit
      de=(e(nw)-e(1))/100.
      ex=e(1)
      do j=1,101
        yfit=fitpolynom(amap*ex+bmap,ac,npi)
        yfim=fitpolynom(amap*ex+bmap,am,npi)
        yfip=fitpolynom(amap*ex+bmap,ap,npi)
        yfie=sqrt(yfim**2+yfip**2)
        write(2,124) ex,yfit,yfit-yfim,yfit+yfim,yfit-yfie,yfit+yfie
        ex  =ex+de
      enddo
c     Low energy R data from FV, lowest save datapoint 0.335 GeV
c add this to Rdat_all XCPT,YCPT arrays
      de=(0.335d0-e(1))/10.
      ex=e(1)
      do j=1,11
        yfit=fitpolynom(amap*ex+bmap,ac,npi)
        yfim=fitpolynom(amap*ex+bmap,am,npi)
        yfip=fitpolynom(amap*ex+bmap,ap,npi)
        betapi=sqrt(1.d0-4.d0*MPi**2/ex**2)
        FVtoR=betapi**3/4.d0  ! wirte format 97
        FVtoR=1.d0            ! write format 96
        write(16,96) ex*1.d3,yfit*FVtoR,yfim*FVtoR,yfip*FVtoR
        ex  =ex+de
      enddo

c     Header for ploting data
      write(3,*) '      2.0000'
      write(3,*) '      1.0000'
      write(3,*) '   ',float(nw)
      write(4,*) '      2.0000'
      write(4,*) '      1.0000'
      write(4,*) '   ',float(nw)
      do j=1,nw
         errt=sqrt(ysav(j,2)**2+ysav(j,3)**2)
         write(3,124) e(j),ysav(j,1),ysav(j,2),ysav(j,2)
         write(4,124) e(j),ysav(j,1),errt,errt
      enddo
c      write (7,*) 'job,ich=',job,ich
      write (7,'(A)') chan(job,1)
      if (np.eq.3)  write(7,83) ac
      if (np.eq.4)  write(7,84) ac
      if (np.eq.5)  write(7,85) ac
      if (np.eq.6)  write(7,86) ac
      if (np.eq.7)  write(7,87) ac
      if (np.eq.8)  write(7,88) ac
      if (np.eq.9)  write(7,89) ac
      if (np.eq.10) write(7,90) ac
      if (np.eq.11) write(7,91) ac
      if (np.eq.12) write(7,92) ac
      if (np.eq.13) write(7,93) ac
      if (np.eq.14) write(7,94) ac
      write (7,'(A)') chan(job,2)
      if (np.eq.3)  write(7,83) am
      if (np.eq.4)  write(7,84) am
      if (np.eq.5)  write(7,85) am
      if (np.eq.6)  write(7,86) am
      if (np.eq.7)  write(7,87) am
      if (np.eq.8)  write(7,88) am
      if (np.eq.9)  write(7,89) am
      if (np.eq.10) write(7,90) am
      if (np.eq.11) write(7,91) am
      if (np.eq.12) write(7,92) am
      if (np.eq.13) write(7,93) am
      if (np.eq.14) write(7,94) am
      write (7,'(A)') chan(job,3)
      if (np.eq.3)  write(7,83) ap
      if (np.eq.4)  write(7,84) ap
      if (np.eq.5)  write(7,85) ap
      if (np.eq.6)  write(7,86) ap
      if (np.eq.7)  write(7,87) ap
      if (np.eq.8)  write(7,88) ap
      if (np.eq.9)  write(7,89) ap
      if (np.eq.10) write(7,90) ap
      if (np.eq.11) write(7,91) ap
      if (np.eq.12) write(7,92) ap
      if (np.eq.13) write(7,93) ap
      if (np.eq.14) write(7,94) ap
c next guess parameters
      if (np.eq.3)  write(9,63) ac
      if (np.eq.4)  write(9,64) ac
      if (np.eq.5)  write(9,65) ac
      if (np.eq.6)  write(9,66) ac
      if (np.eq.7)  write(9,67) ac
      if (np.eq.8)  write(9,68) ac
      if (np.eq.9)  write(9,69) ac
      if (np.eq.10) write(9,70) ac
      if (np.eq.11) write(9,71) ac
      if (np.eq.12) write(9,72) ac
      if (np.eq.13) write(9,73) ac
      if (np.eq.14) write(9,74) ac
      if (np.eq.3)  write(9,63) am
      if (np.eq.4)  write(9,64) am
      if (np.eq.5)  write(9,65) am
      if (np.eq.6)  write(9,66) am
      if (np.eq.7)  write(9,67) am
      if (np.eq.8)  write(9,68) am
      if (np.eq.9)  write(9,69) am
      if (np.eq.10) write(9,70) am
      if (np.eq.11) write(9,71) am
      if (np.eq.12) write(9,72) am
      if (np.eq.13) write(9,73) am
      if (np.eq.14) write(9,74) am
      if (np.eq.3)  write(9,83) ap
      if (np.eq.4)  write(9,84) ap
      if (np.eq.5)  write(9,85) ap
      if (np.eq.6)  write(9,86) ap
      if (np.eq.7)  write(9,87) ap
      if (np.eq.8)  write(9,88) ap
      if (np.eq.9)  write(9,89) ap
      if (np.eq.10) write(9,90) ap
      if (np.eq.11) write(9,91) ap
      if (np.eq.12) write(9,92) ap
      if (np.eq.13) write(9,93) ap
      if (np.eq.14) write(9,94) ap
      write (10,125) e(1),e(nw)

 124  format(1x,f11.6,5(2x,1pe13.6))
 125  format('     &     ',f9.6,'d0,',f9.6,'d0/')
 99   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 94   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 93   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 92   format ('     &   ',f10.6,'d0,',f10.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 91   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 90   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 89   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 88   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 87   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 86   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 85   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0/')
 84   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 83   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
c +++++
 74   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 73   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 72   format ('     &   ',f10.6,'d0,',f10.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 71   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 70   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 69   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 68   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 67   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 66   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 65   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,')
 64   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 63   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')

      stop 10
      end

      function fitpolynom(x,a,na)
      implicit none
      INTEGER na,i
      REAL*8 x,ax,a,fitpolynom,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      fitpolynom=0.d0
      do i=1,na
         fitpolynom=fitpolynom+a(i)*t(i)
      enddo
      return
      end
C
      subroutine funcs(x,a,y,dyda,na)
      implicit none
      INTEGER na,i
      REAL*8 x,y,ax,a,dyda,fitpolynom,t
      dimension a(na),dyda(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo

      y=fitpolynom(x,a,na)

      do i=1,na
         dyda(i)=t(i)
      enddo
      return
      end

c  0.2791404      1
c  0.3150000      7
c  0.3200000      8
c  0.6100000     57
c  0.6160000     58
c  0.8100000    152
c  1.4000000    307
c  2.0000000    406
c  2.2000000    407
c  2.4000000    408
c  2.5000000    409
       FUNCTION GZINTCOR(S)
C      --------------------
C      CORRECTION FOR GAMMA-Z-INTERFERRENCE IN HIGH ENERGY DATA
       IMPLICITREAL*8 (A-Z)
       PI=4.D0*DATAN(1.D0)
       GMU=1.166390D-5
       ALP=1.D0/137.0359895D0
       MZ2=91.1887D0**2
       ST2=0.2322D0
       POL=DSQRT(2.D0)*GMU/16.D0/PI/ALP*S*MZ2/(S-MZ2)
       QFQF=11.D0/9.D0
       QFVF=QFQF*(21.D0/11.D0-4.D0*ST2)
       VVAAF=10.D0-56.D0/3.D0*ST2+176.D0/9.D0*ST2**2
       VE=-1.D0+4.D0*ST2
       VVAAE=2.D0-8.D0*ST2+16.D0*ST2**2
       C1=QFQF-2.D0*VE*QFVF*POL+VVAAE*VVAAF*POL**2
       GZINTCOR=QFQF/C1
       RETURN
       END
