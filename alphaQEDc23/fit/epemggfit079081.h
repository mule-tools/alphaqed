
c      ++++ epemggfit079081 ++++  

c  0.787 -- 0.8099 ares epemggfit079081  update oct 2022 
       DATA ares/      
     &      5.618965d0,-0.410086d0,-0.092193d0,-0.017342d0, 0.017539d0,
     &     -0.005722d0, 0.000000d0/
       DATA asta/      
     &      0.030467d0,-0.006843d0, 0.001446d0,-0.000208d0,-0.000209d0,
     &      0.000477d0, 0.000000d0/
       DATA asys/      
     &      0.023600d0,-0.007898d0, 0.002959d0,-0.002409d0, 0.002261d0,
     &     -0.001578d0, 0.000000d0/
      DATA emima1 /
     &      0.787000d0, 0.809900d0/
