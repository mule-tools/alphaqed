
c      ++++ fpi2fitchptail ++++  

c 0.285 -- 0.460 updated feb 2021 ! fpp chpt tail fit
c obtained with fit program fitffppg2.f 
       DATA ares/      
     &      1.744527d0, 0.749274d0, 0.004799d0/
       DATA asta/      
     &      0.044230d0, 0.021233d0,-0.021987d0/
       DATA asys/      
     &      0.008295d0, 0.001360d0,-0.005880d0/
      DATA emima1 /
     &      0.279140d0, 0.438000d0/
