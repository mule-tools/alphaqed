#!/bin/csh -fx
if ("$1" == "") goto usage

  set KILL_FILES=( \
		fitR.dat)
    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif
    if ( -r  rangelist.dat ) then
	rm -f  rangelist.dat
    endif

touch  rangelist.dat

set update = 'update oct 2022'
echo "\
++ ""$update"" ++ "

if ("$1" == "1") then 
set name = epemggfit0814new
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  E= 0.810 -- 1.040  ""$update"" " >> array.1
echo "\
c  E=1.040 -- 1.435   ""$update"" " > array.2
echo "\
c  phi resonance background [applies for 1.00 - 1.04]\
c   1.00001 -- 1.03999 ""$update"" " > array.3
echo "\
c  Fit below phi \
c   0.958 -- 1.000 ""$update"" " > array.4
set njob = 4
endif
if ("$1" == "2") then 
echo " Empty option"
exit
endif
if ("$1" == "3") then 
echo " Empty option"
exit
endif
if ("$1" == "4") then 
set name = epemggfit079081
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.787 -- 0.8099 ares epemggfit079081  ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "5") then 
set name = epemggfit1425
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c   1.375 -- 1.850 ""$update"" " >> array.1
echo "\
c   1.750 -- 1.930 ""$update"" " > array.2
echo "\
c   1.845 -- 2.130 ""$update"" " > array.3
set njob = 3
endif
if ("$1" == "6") then 
echo " Empty option"
exit
endif
if ("$1" == "7") then 
set name = rggfitrholow
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.318 -- 0.778 ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "8") then 
set name = rggfitrhohig
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.769  -- 0.789 ""$update"" " >> array.1
set njob = 1
endif

set ijob = 1

while ( $ijob <= $njob )

echo ' job' $ijob ' is running'

  foreach i ($KILL_FILES)
    if ( -r $i ) then
      mv -f $i $i$ijob
      continue
    endif
  end

cp guess_$name.h$ijob RGGdataguess.f
echo "      name='"$name"'"  >> RGGdataguess.f
echo "      job=""$1"  >> RGGdataguess.f
echo "      ich=""$ijob"   >> RGGdataguess.f

make -f make_fitRGGdat

echo ' run  fitRGGdat'
time ./fitRGGdat

cat array.$ijob >> fitpar.dat

cat fort.7 >> fitpar.dat 
echo "      DATA ""emima$ijob /"   >> fitpar.dat
cat fort.10 >> fitpar.dat
 rm -f fort.10
cat fort.1001 >> rangelist.dat

cat fitRdat0gnuplot.in > fitRdatplot_gnu.in
cat >> fitRdatplot_gnu.in <<_EOF_
set ylabel " R_{{/Symbol gg}} fit " 
plot "fort.11" skip 3 using 1:2:(\$2-\$3):(\$2+\$4) with yerrorlines t "RSGG",\
     "fort.12" skip 3 using 1:2 with lines ls 1 t "rsggsmoothed", '' skip 3 using 1:3 with lines ls 1 t "", '' skip 3 using 1:4 with lines ls 1 t "",\
     "fitR.dat" skip 3 using 1:2 with lines ls 2 t "new fit", '' skip 3 using 1:5 with lines ls 2 t "", '' skip 3 using 1:6 with lines ls 2 t ""
pause -1
clear
_EOF_

gnuplot -persist fitRdatplot_gnu.in

echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

if ( $ijob == $njob ) then 
echo -n "Update RSGG_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
cp $name.h ../
cd ..
make
cd ./fit/
endif
endif

 @ ijob++

end

echo "Results in fitpar.dat"

exit 0
usage:
echo "Usage fit function name labels 1,...,8"
echo "1=epemggfit0814new"
echo "4=epemggfit079081"
echo "5=epemggfit1425"
echo "7=rggfitrholow"
echo "8=rggfitrhohig"
exit 2

