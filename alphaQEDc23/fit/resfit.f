      function resfitfun(s,a,na)
      implicit none
      integer ier,iex,j,na,np,npi,iwhich
      parameter(np=9)
      integer lista(np)
      double precision resfitfun,s,e,UGM2,EREM,EREP,sMeV,rbw,fac,BWfit,
     &     M,G,P,MR,GR,PR,rebw,polynom,sx,amap,bmap
      double precision a(na),b(np),res(3),fsta(3),fsys(3)
      common/energy/sx,b    
      common /error/iex,iwhich,fsta,fsys
      common /resbound/MR,GR,PR,EREM,EREP,UGM2,fac
      common /liste/lista,npi
      common /polyn/amap,bmap
      ier=1
      sx=s
      e=sqrt(s)
      M=a(1)
      G=a(2)
      P=a(3)
      sMeV=s*UGM2
      if ((E.GT.EREM).and.(E.LT.EREP)) then
         rbw=fac*BWfit(sMeV,M,G,P)
         if (ier.eq.2) then
            rbw=rbw*fsta(iwhich)
            res(j)=sqrt(res(j)**2+rbw**2)
         else 
            if (ier.eq.3) rbw=rbw*fsys(iwhich)
            res(j)=res(j)+rbw
         endif
         rebw=rbw
      endif
      resfitfun=rebw+polynom(amap*e+bmap,a,na)
      return
      end

c     
      function resfitfun1(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun1,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(1))=x
      resfitfun1=resfitfun(s,a,np)
      return
      end
c
      function resfitfun2(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun2,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(2))=x
      resfitfun2=resfitfun(s,a,np)
      return
      end
c
      function resfitfun3(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun3,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(3))=x
      resfitfun3=resfitfun(s,a,np)
      return
      end
c
      function resfitfun4(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun4,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(4))=x
      resfitfun4=resfitfun(s,a,np)
      return
      end
c
      function resfitfun5(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun5,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(5))=x
      resfitfun5=resfitfun(s,a,np)
      return
      end
c
      function resfitfun6(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun6,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(6))=x
      resfitfun6=resfitfun(s,a,np)
      return
      end
c
      function resfitfun7(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun7,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(7))=x
      resfitfun7=resfitfun(s,a,np)
      return
      end
c
      function resfitfun8(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun8,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(8))=x
      resfitfun8=resfitfun(s,a,np)
      return
      end
c
      function resfitfun9(x)
      implicit none
      integer np,na,i,npi
      parameter (np=9)
      integer lista(np)
      double precision resfitfun9,resfitfun,x,s
      double precision a(np),b(np)
      external resfitfun
      common/energy/s,b
      common /apar/a,na
      common /liste/lista,npi
      a(lista(9))=x
      resfitfun9=resfitfun(s,a,np)
      return
      end
c
