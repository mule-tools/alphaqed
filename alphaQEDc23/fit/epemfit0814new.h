
c      ++++ epemfit0814new ++++  

c   0.810 -- 0.999999  update oct 2022 
       DATA aresl/     
     &      2.489286d0,-2.060171d0, 0.858568d0,-0.108427d0, 0.065494d0,
     &      0.025003d0, 0.003912d0, 0.004013d0,-0.003657d0, 0.000999d0,
     &     -0.008382d0,-0.003678d0/
       DATA astal/     
     &      0.016233d0,-0.002113d0, 0.006476d0, 0.000780d0, 0.000203d0,
     &     -0.000411d0,-0.000401d0, 0.000070d0, 0.000551d0, 0.001166d0,
     &      0.000939d0, 0.000916d0/
       DATA asysl/     
     &      0.022357d0,-0.000380d0, 0.008417d0,-0.000781d0, 0.000990d0,
     &     -0.000283d0, 0.001304d0, 0.000647d0,-0.000170d0, 0.000623d0,
     &     -0.000965d0,-0.000608d0/
      DATA emima1 /
     &      0.810000d0, 0.999900d0/

c   1.040 -- 1.435   update oct 2022 
       DATA aresh/     
     &      1.279758d0, 0.291868d0, 0.313849d0,-0.113251d0, 0.095638d0,
     &     -0.054710d0, 0.040599d0,-0.028759d0, 0.018313d0,-0.011859d0,
     &      0.003851d0,-0.005444d0/
       DATA astah/     
     &      0.016140d0, 0.000994d0, 0.003963d0, 0.001401d0, 0.000606d0,
     &      0.000098d0,-0.000335d0,-0.000626d0,-0.000245d0,-0.000654d0,
     &      0.000070d0,-0.000669d0/
       DATA asysh/     
     &      0.052024d0, 0.024288d0, 0.022080d0, 0.010122d0, 0.004722d0,
     &     -0.000193d0, 0.000765d0,-0.002721d0,-0.002638d0,-0.004244d0,
     &     -0.001102d0,-0.002274d0/
      DATA emima2 /
     &      1.040010d0, 1.435000d0/

c  phi resonance background [applies for 1.00 - 1.04]
c   1.00001 -- 1.03800  update oct 2022 
       DATA aresfr/    
     &      0.903760d0, 0.203315d0, 0.007794d0, 0.019307d0, 0.001104d0/
       DATA astafr/    
     &      0.021091d0,-0.001244d0, 0.000623d0, 0.000012d0, 0.000181d0/
       DATA asysfr/    
     &      0.021729d0, 0.009694d0,-0.001026d0,-0.002074d0, 0.000251d0/
      DATA emima3 /
     &      1.000010d0, 1.039990d0/

c  Fit below phi 
c   0.958 -- 1.000 update oct 2022 
       DATA aresfm/    
     &      1.105288d0, 0.083186d0, 0.077182d0, 0.015275d0,-0.001428d0,
     &     -0.003951d0, 0.005666d0, 0.008914d0/
       DATA astafm/    
     &      0.015841d0, 0.005774d0,-0.001453d0, 0.000497d0, 0.002660d0,
     &      0.000104d0,-0.000398d0, 0.000698d0/
       DATA asysfm/    
     &      0.024982d0, 0.004084d0, 0.002098d0, 0.001098d0, 0.000448d0,
     &     -0.000421d0,-0.001695d0, 0.001383d0/
      DATA emima4 /
     &      0.958000d0, 0.999900d0/
