      program  phifit
c fitting data R data in phi resonance region 1.0 to 1.04 GeV
      implicit  none
      character*23 chan(3,2,3)
      character*14 name
      real *8 chisq,chisqbegin,chisqend,sx,ex,de,yfit,es,ed,amap,bmap
      real *8 e,et,s,y,rm,rp,sig,xx,alamda,covar,alpha,a,gues,fitpolynom
      real *8 guesini,ei,si,eps,ydat,facbw,b,Ee,resfitfun
      real *8 UGM2,sMeV,rbw,rebw,BWfit,fac,null,alpi,f2,ac,am,ap
      REAL *8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI,EOMM,EOMP,EFIM,EFIP,
     &     EREM,EREP,mR,GR,PR,emin,emax,BW,BWren
      real *8 fsta(3),fsys(3)
      integer Ndat,ier,jj,iwhich,nbeg,job,n,ich,imod
C      parameter(Ndat=898)
      integer  i,j,ni,nj,nw,np,na,k,ns,npi,icount,iresonances,nb
      parameter (nj=900,nb=9)
      dimension b(nb),
     &     e(nj),et(nj),s(nj),y(nj),rm(nj),rp(nj)
      real *8 eb(nj),sb(nj),yb(nj),rmb(nj),rpb(nj)
      real *8  ysav(nj,3),res(nj),guesiniom(nb),
     &     guesinifi(nb),hini(nb),dyda(nb),h(nb)
      real *8 aomres(nb),aomsta(nb),aomsys(nb),
     &        afires(nb),afista(nb),afisys(nb)
      REAL xnskip,xncurv,xNPT,yfim,yfip,yfie,errt
      INTEGER nskip,NU1N,n1,n2
      integer nphi,nome,ii
      parameter(nphi=95)
      double precision Xphi3gdat(nphi),Yphi3gdat(nphi,3)
     &     ,Xphi3gbgr(nphi),Yphi3gbgr(nphi,3)
      common /resbound/MR,GR,PR,EREM,EREP,UGM2,fac
      common /error/ier,iwhich,fsta,fsys
      common /polyn/amap,bmap
      common /liste/lista,npi
      common /steps/h
      external funcs,fitpolynom,BWfit,resfitfun,BW
      include 'phires3gbg.dat'
      include 'phires3g.dat'
      include 'Rdataguess.f'
      DATA alpi/   137.035999166d0/
      DATA EOMM,EOMP,EFIM,EFIP/.739d0,.851d0,1.00D0,1.04D0/
      DATA fsta/1.0D-2,0.83D-2,0.8D-2/
      DATA fsys/1.2D-2,1.96D-2,1.1D-2/
      DATA hini /1.d-2,1.d-5,
     &     1.d-5,1.d-3,1.d-5,1.d-3,
     &     1.d-5,1.d-3,0.1d0/
      DATA guesiniom/
     &       -2.7725d0,-1.058064d0, 2.506775d0, 3.188163d0, 0.806831d0,
     &     -1.630680d0,-1.673893d0, 1.027095d0, 0.000000d0/
      DATA guesinifi/
     &     1019.1819d0, 4.121886d0, 0.000301d0, 0.416548d0, 0.410309d0,
     &      0.001130d0, 0.000000d0, 0.000000d0, 0.000000d0/
c      do i=1,12
c         guesini(i)=0.0d0
c      enddo
c      guesini(1)=1.d0
c     guesini(2)=1.d0
      do j=1,3
         do i=1,2
            do ier=1,3
               chan(j,i,ier)='                       '
            enddo
         enddo
      enddo
c set 2 name = omegafit
c " Use phifit program "
c  omega range 0.739 --  0.851 update jan 2012   ! fit of non-pipi part\
      chan(2,1,1)='       DATA aomres/'
      chan(2,1,2)='       DATA aomsta/'
      chan(2,1,3)='       DATA aomsys/'
c set 3 name = phifit
c " Use phifit program "
c  phi  range 1.000 --  1.040  update jan 2012  ! fit for all channels \
      chan(3,1,1)='       DATA afires/'
      chan(3,1,2)='       DATA afista/'
      chan(3,1,3)='       DATA afisys/'

      na=nb
      do ii=1,nb
         b(ii)=0.d0
         h(ii)=hini(ii)/10.
      enddo
C      iwhich=3                 ! 2=omega,3=phi
      ier=1
      facbw=9.d0/4.d0*alpi**2
      fac=facbw
      null=0.d0
      UGM2=1.D6
      rbw =null
      MOM=782.65D0              ! +/- 0.12 PDG 2010
      GOM=8.49d0                ! +/- 0.08  PDG 2010
      POM=7.28D-5               ! pm 0.14d-5 PDG 2010
      MFI=1.019461D3            ! pm 0.019 PDG 2014
      GFI=4.266D0               ! pm 0.031   PDG 2014
      PFI=2.954D-4              ! pm 0.03d-4 PDG 2010
      imod=1
      if (imod.eq.1) then
      else
         write (*,*) ' iwhich,ier=?'
         read  (*,*) iwhich,ier
      endif
      iwhich=2
      if (iwhich.eq.2) then
         MR=MOM
         GR=GOM
         PR=POM
         EREM=EOMM
         EREP=EOMP
      else if (iwhich.eq.3) then
         MR=MFI
         GR=GFI
         PR=PFI
         EREM=EFIM
         EREP=EFIP
      else 
         write(*,*) ' Choose which resonance! iwich=2,3?',iwhich
      endif
      if (imod.eq.1) then
      else
         do 14 i=1,nb
            a(i)=null
            if (iwhich.eq.2) guesini(i,1)=guesiniom(i) 
            if (iwhich.eq.3) guesini(i,1)=guesinifi(i) 
 14      continue
c initialize
         if (guesini(1,1).eq.0.d0) then
            do i=1,np
               do ier=1,3 
                  guesini(i,ier)=0.0d0
               enddo
            enddo
         endif
      endif
C      guesini(1)=MR
C      guesini(2)=GR
C     guesini(3)=PR
      do 15 i=1,nb
         a(i)=guesini(i,ier)
 15   continue
      write (*,*) ' a:',a

      if (iwhich.eq.2) then
         open (UNIT=3,FILE='omeres.dap',STATUS='NEW')
         open (UNIT=4,FILE='omeres.out',STATUS='NEW')
      else if (iwhich.eq.3) then
         open (UNIT=3,FILE='phires.dap',STATUS='NEW')
         open (UNIT=4,FILE='phires.out',STATUS='NEW')
      else
      endif
      if (iwhich.eq.2) write (11,'(a22)') 'B    01WA #2#y#1# data'
      if (iwhich.eq.3) write (11,'(a22)') 'B    01WA #2#v#1# data'
      if (iwhich.eq.2) write (11,'(a22)') 'D    04omeres.out     '
      if (iwhich.eq.3) write (11,'(a22)') 'D    04phires.out     '
      eps=0.0001d0
      if (iwhich.eq.2) ndat=nphi
      if (iwhich.eq.3) ndat=nphi
      write (2,*) '      3.0000'
      write (2,*) '      1.0000'
      write (2,*) '   ',float(ndat)
      write (8,*) '      2.0000'
      write (8,*) '      1.0000'
      write (8,*) '   ',float(ndat)
C      do i=1,3
C         read (2,*) 
C     enddo
      if (iwhich.eq.2) then
         Ndat=nphi
         write (2,*) 'Ndat=',ndat
         do i=1,ndat
            eb(i)=Xphi3gbgr(i)
            yb(i)=Yphi3gbgr(i,1)
            errt=sqrt(Yphi3gbgr(i,2)**2+Yphi3gbgr(i,3)**2)
            rmb(i)=errt
            rpb(i)=rmb(i)
            write (2,124) eb(i),yb(i),rmb(i),rpb(i)
         enddo
      else if (iwhich.eq.3) then
         Ndat=nphi
         write (2,*) 'Ndat=',ndat
         do i=1,ndat
            eb(i)=Xphi3gdat(i)
            yb(i)=Yphi3gdat(i,1)
            errt=sqrt(Yphi3gdat(i,2)**2+Yphi3gdat(i,3)**2)
            rmb(i)=errt
            rpb(i)=rmb(i)
            write (2,124) eb(i),yb(i),rmb(i),rpb(i)
         enddo
      else
      endif
      if (iwhich.eq.2) write (*,*) ' phi bg range set',eb(1),eb(ndat)
      if (iwhich.eq.3) write (*,*) '  phi  range set',eb(1),eb(ndat)
C      write (*,*) ' select fit range emin,emax=?'
C      read  (*,*) emin,emax
C      if (emin.lt.eb(1)) emin=eb(1)
C      if (emax.gt.eb(ndat)) emax=eb(ndat)
c     restriced fit range
      if (emin.gt.eb(1)+eps) then
C         call getindex(emin,Ndat,eb,n1)
      else
         n1=1
      endif
      if (emax.lt.eb(ndat)-eps) then
C         call getindex(emax,Ndat,eb,n2)
      else
         n2=ndat
      endif
      write (*,*) ' Indexrange',n1,' -- ',n2,' ndat=',ndat
c     empty arrays
      do i=1,nj
         e(i) =0.d0
         y(i) =0.d0
         s(i) =0.d0
         rm(i)=0.d0
         rp(i)=0.d0
      enddo
      n1=1
      n2=nphi
      ndat=nphi
      icount=0
      do j=n1,n2
         icount=icount+1
         ei=eb(j)
         e(icount)=ei
         si=ei*ei
         s(icount)=si
         y(icount)=yb(j)
         rm(icount)=rmb(j)
         rp(icount)=rpb(j)
         ysav(icount,1)=y(icount)
         ysav(icount,2)=y(icount)-rm(icount)
         ysav(icount,3)=y(icount)+rm(icount)
      enddo
      do i=1,icount
         write(45,*) i,eb(i)
      enddo
      write(*,'(a30,i3,a2,f9.6,a4,f9.6,a2)')
     &     ' number of fit points[range]:',
     &     icount,' [',e(1),' -- ',e(icount),' ]'
      do ier=1,3 
C      ier=1
         do 16 i=1,nb
            a(i)=guesini(i,ier)
            lista(i)=i
 16      continue
         do i=1,icount
            y(i)=ysav(i,ier)
C            write (*,*) ' i,y(i)',i,y(i)
         enddo
         MR=a(1)
         GR=a(2)
         PR=a(3)
         write (*,*) ' in params', a
c     Mapping to Tschebycheff interval [-1,+1]
         nw=icount
         es=(e(nw)+e(1))
         ed=(e(nw)-e(1))
         amap=2.d0/ed
         bmap=-es/ed
         chisq=0.0
c phi   1.00D0,1.04D0
         do j=1,nw
            s(j)=amap*e(j)+bmap
            yfit=fitpolynom(s(j),a,npi)
            chisq=chisq+((y(j)-yfit)/(rm(j)))**2
            write (*,'(1x,i3,2x,f11.6,2(2x,1pe13.6))') j,e(j),yfit,y(j)
            write (8,124) s(j),yfit,y(j),BWren
         enddo
         chisqbegin=chisq
         write (*,*) ' '
         write (*,*) ' chisq:',chisqbegin
         write (*,*) '  a(i):',a
         write (*,*) ' '
c ns: number of iterrations
      ns=2000
c npi: number of parameters to be ajusted
C      npi=8
      do 300 k=1,ns
         alamda=-1.d0
         call mrqmin(s,y,rm,nw,a,nb,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
C       write(*,'(1x,6(1pe11.4))') (a(i),i=1,npi)
C         write(*,*) ' chisq,alamda',chisq,alamda
 300  continue
        write(*,'(/1x,a,i2,t18,a,f10.4,t43,a,e9.2)') 'Iteration #',ns,
     *       'Chi-squared:',chisq,'ALAMDA:',alamda
        write(*,'(1x,t5,a,t13,a,t21,a,t29,a,t37,a,
     &       t45,a,t53,a,t61,a,t69,a)') 'A(1)',
     *       'A(2)','A(3)','A(4)','A(5)','A(6)',
     *       'A(7)','A(8)','A(9)'
        write(*,'(1x,12(1pe11.4))') (a(i),i=1,npi)
c Recalculate chi2
      alamda=0.d0
        call mrqmin(s,y,rm,nw,a,nb,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
        write(*,*) 'Uncertainties:'
        write(*,'(1x,12(1pe11.4)/)') (sqrt(covar(i,i)),i=1,npi)
        write(*,'(1x,a)') 'Expected results:'
        write(*,'(1x,f7.2,5f8.2/)') 1.0,0.0,0.0,0.0,0.0,0.0
      chisq=0.0
      do j=1,nw
         yfit=fitpolynom(s(j),a,npi)
         chisq=chisq+((y(j)-yfit)/(rm(j)))**2
      enddo
      chisqend=chisq
      write (*,*) ' '
      write (*,*) chisqbegin,' -->',chisqend,'S=',sqrt(chisqend/(nw-1))
      write (*,*) ' '
      if (ier.eq.1) then
c     Header for ploting data
         write (3,*) '      2.0000'
         write (3,*) '      1.0000'
         write (3,*) '    100.0000'
C Write plot data of fit
         de=(e(nw)-e(1))/100.
         ex=e(1)
         do j=1,100
            s(j)=amap*ex+bmap
            sx=ex*ex
            yfit=fitpolynom(s(j),guesini,npi)
            call getindex(ex,nw,e,i)
            if (ex.eq.e(i)) then
               ydat=y(i)
            else 
               ydat=y(i)+(y(i+1)-y(i))/(e(i+1)-e(i))*(ex-e(i))
            endif
            write (3,124) ex,ydat,yfit
            ex  =ex+de
         enddo
      endif
C         write (4,*) '      2.0000'
C         write (4,*) '      3.0000'
C         write (4,*) '    100.0000'
CC Write plot data of fit
C      endif
C      de=(e(nw)-e(1))/100.
C      ex=e(1)
C      do j=1,100
C         sx=ex*ex
C         yfit=fitpolynom(s(j),a,npi)
C         write (4,124) ex,yfit
C         ex  =ex+de
C      enddo
      if (ier.eq.1) then
         do j=1,nb
            ac(j)=a(j)
         enddo
      endif
      if (ier.eq.2) then
         do j=1,nb
            am(j)=a(j)
         enddo
      endif
      if (ier.eq.3) then
         do j=1,nb
            ap(j)=a(j)
         enddo
      endif
      enddo                     ! end ier loop
      
c Header for ploting fit
      write(4,*) '      2.0000'
      write(4,*) '      1.0000'
      write(4,*) '    100.000'
C Write plot data of fit
      de=(e(nw)-e(1))/100.
      ex=e(1)
      do j=1,100
        yfit=fitpolynom(amap*ex+bmap,ac,npi)
        yfim=fitpolynom(amap*ex+bmap,am,npi)
        yfip=fitpolynom(amap*ex+bmap,ap,npi)
        yfie=sqrt(yfim**2+yfip**2)
        write(4,124) ex,yfit,yfit-yfim,yfit+yfim,yfit-yfie,yfit+yfie
        ex  =ex+de
      enddo

C     Header for ploting data
C      write (7,99) a
C      write (*,99) guesini
C      write (*,99) a
      write (7,'(A)') chan(job,ich,1)
      if (np.eq.9)  write(7,89) ac
      write (7,'(A)') chan(job,ich,2)
      if (np.eq.9)  write(7,89) am
      write (7,'(A)') chan(job,ich,3)
      if (np.eq.9)  write(7,89) ap
c next guess parameters
      if (np.eq.9)  write(9,69) ac
      if (np.eq.9)  write(9,69) am
      if (np.eq.9)  write(9,89) ap

      write (*,*) ' out params', a

      stop 10

124   format(1x,f11.6,5(2x,1pe13.4))
 99   format ('     &     ',f9.4,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 89   format ('     &     ',f9.4,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 69   format ('     &     ',f9.4,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
      end
 
      function fitpolynom(x,a,na)
      implicit none
      INTEGER na,i
      REAL*8 x,ax,a,fitpolynom,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      fitpolynom=0.d0
      do i=1,na
         fitpolynom=fitpolynom+a(i)*t(i)
      enddo
      return
      end
C
C      include './nur/mrqcof.f'
C      include './nur/mrqmin.f'
C      include './nur/covsrt.f'
C      include './nur/gaussj.f'

      subroutine funcs(x,a,y,dyda,na)
      implicit none
      INTEGER na,i
      REAL*8 x,y,ax,a,dyda,fitpolynom,t
      dimension a(na),dyda(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo

      y=fitpolynom(x,a,na)

      do i=1,na
         dyda(i)=t(i)
      enddo
      return
      end
