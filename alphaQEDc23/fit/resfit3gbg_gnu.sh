#!/bin/csh 


    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif

set update = 'update jan 2021'
echo "\
++ ""$update"" ++ "

if ("$1" == "2") then
set KILL_FILES=(omeres.dap omeres.out)
  foreach i ($KILL_FILES)
    if ( -r $i ) then
      rm -f $i
      continue
    endif
  end
set name = phifit3gbg
echo "\
c      ++++ ""$name"" ++++  " > array.2
set njob = 1
endif
if ("$1" == "3") then
set KILL_FILES=(phires.dap phires.out)
  foreach i ($KILL_FILES)
    if ( -r $i ) then
      rm -f $i
      continue
    endif
  end
set name = phifit
echo "\
c      ++++ ""$name"" ++++  " > array.3
set njob = 1
endif

set ijob = 1

#while ( $ijob <= $njob )

echo ' job' $ijob ' is running'

cp guess_$name.h$ijob Rdataguess.f
echo "      name='"$name"'"  >> Rdataguess.f
echo "      job=""$1"  >> Rdataguess.f
echo "      ich=""$ijob"   >> Rdataguess.f


make -f make_resfit prog=phifit3gbg

echo ' run  phifit3gbg'
time ./phifit3gbg

cat array.$1 >> fitpar.dat

cat fort.7 >> fitpar.dat

echo ' plotdata fort.2,3,4 and fort.10'


rm -f resfitolot_gnu.in

cat resfitgnuplot.in > resfitplot_gnu.in
if ("$1" == "2") then
cat >> resfitplot_gnu.in <<_EOF_
set ylabel " R fit " 
plot "fort.2" skip 3 using 1:2:(\$2-\$3):(\$2+\$4) with yerrorlines t "{/Symbol o,f} dat",\
     "fort.8" skip 3 using 1:2 with lines ls 3 t "old fit", '' skip 3 using 1:3 with lines ls 4 t "data",\
     "omeres.out" skip 3 using 1:2 with lines ls 5 t "new fit"
pause -1
clear
_EOF_
endif
if ("$1" == "3") then
cat >> resfitplot_gnu.in <<_EOF_
set ylabel " R fit " 
plot "fort.2" skip 3 using 1:2:(\$2-\$3):(\$2+\$4) with yerrorlines t "{/Symbol o,f} dat",\
     "fort.8" skip 3 using 1:2 with lines ls 3 t "old fit", '' skip 3 using 1:3 with lines ls 4 t "data",\
     "phires.out" skip 3 using 1:2 with lines ls 5 t "new fit"
pause -1
clear
_EOF_
endif

gnuplot resfitplot_gnu.in

echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

if ( $ijob == $njob ) then
echo -n "Update omfi_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
endif
endif

exit 0

usage:
echo "Usage fit function name labels 1 "
echo "2= phi resonance background fit"
echo "3= phi  resonance fit non-pipi"
exit 2
