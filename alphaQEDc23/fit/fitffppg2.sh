#!/bin/csh -fx
if ("$1" == "") goto usage

  set KILL_FILES=( \
		fitR.dat)
    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif

if ("$1" == "1") then 
set name = ffppg2fit0810
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  update jan 2012/ june 2013   ! fit for n>2 channels only\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "2") then 
set name = ffppmfit0810
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  update jan 2012/ june 2013 ! fit for all channels\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "3") then 
set name = fpi2fitchptail
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.285 -- 0.460 updated feb 2021 ! fpp chpt tail fit\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif

set ijob = 1

echo ' job' $ijob ' is running'

  foreach i ($KILL_FILES)
    if ( -r $i ) then
      mv -f $i $i$ijob
      continue
    endif
  end

cp guess_$name.h$ijob Rdataguess.f
echo "      name='"$name"'"  >> Rdataguess.f
echo "      job=""$1"  >> Rdataguess.f
echo "      ich=""$ijob"   >> Rdataguess.f

make -f make_fitffppg2

echo ' run  fitffppg2'
time ./fitffppg2

cat array.$ijob > fitpar.dat

cat fort.7 >> fitpar.dat 
echo "      DATA ""emima$ijob /"   >> fitpar.dat
cat fort.10 >> fitpar.dat
 rm -f fort.10
cat fort.1001 >> rangelist.dat

$HOME/andromeda/gra/fjplo3 fitRdat0

echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

echo -n "Update Rdat_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
cp $name.h ../
cd ..
make
cd ./fit/
endif

echo "Results in fitpar.dat"

exit 0
usage:
echo "Usage fit function name labels 1,2,3"
echo "1=ffppg2fit0810"
echo "2=ffppmfit0810"
echo "3=fpi2fitchptail"
exit 2

