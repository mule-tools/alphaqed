!****************************************************************************************
!     COMPLEX VACUUM POLARIZATION from DATA, Fedor's version: ALPHA(0)->ALPHA(0)/(1-VPOL(Q2))
!     data from  http://cmd.inp.nsk.su/~ignatov/vpl/
!     VP fortran function by BabaYaga@NLO
!****************************************************************************************
MODULE VPOLnsk_v29
  use vpol_nsk_data_v29, only: prec
  implicit none

contains

      SUBROUTINE VPOLnsk(Q2, full, hadr, lep, dfull)
        use vpol_nsk_data_v29
      implicit none
      real(kind=prec), intent(in) :: q2
      complex(kind=prec), intent(out), optional :: full, hadr, lep, dfull

      complex(kind=prec), parameter :: imag = (0._prec,1._prec)
      complex(kind=prec), parameter :: p(0:np-1) = pre + imag * pim
      complex(kind=prec), parameter :: ps(0:np-1) = pres
      complex(kind=prec), parameter :: lp(0:np-1) = lpre + imag * lpim
      complex(kind=prec), parameter :: lps(0:np-1) = lpres
      complex(kind=prec), parameter :: ep(0:np-1) = epre + imag * epim
      complex(kind=prec), parameter :: eps(0:np-1) = epres

      complex(kind=prec), parameter :: hp(0:np-1) = p - lp
      complex(kind=prec), parameter :: hps(0:np-1) = ps - lps

      real(kind=prec) :: aq2
      integer istart,ifinish,imid,k
      ! binary search
      aq2 = abs(q2)
      istart  = 0
      ifinish = np - 1
      imid  = np/2
      do while(.not.(s(imid+1).gt.aq2.and.s(imid).le.aq2))
         if (aq2.gt.s(imid)) then
            istart = imid + 1
         else
            ifinish = imid - 1
         endif
         imid = (istart + ifinish)/2
      enddo
      k = imid

      if (q2.gt.0.d0) then
        if(present(full)) full = p(k) + (p(k+1)-p(k))/(s(k+1)-s(k)) * (q2-s(k))
        if(present(dfull)) dfull = ep(k) + (ep(k+1)-ep(k))/(s(k+1)-s(k)) * (q2-s(k))
        if(present(lep)) lep = lp(k) + (lp(k+1)-lp(k))/(s(k+1)-s(k)) * (q2-s(k))
        if(present(hadr)) hadr = hp(k) + (hp(k+1)-hp(k))/(s(k+1)-s(k)) * (q2-s(k))
          ! rho = cv(k) + (cv(k+1)-cv(k))/(s(k+1)-s(k)) * (q2-s(k))
          ! cov = rho * evpre*evpim
      else
        if(present(full)) full = ps(k) + (ps(k+1)-ps(k))/(s(k+1)-s(k))*(aq2-s(k))
        if(present(dfull)) dfull = eps(k)+(eps(k+1)-eps(k))/(s(k+1)-s(k))*(aq2-s(k))
        if(present(lep)) lep = lps(k) + (lps(k+1)-lps(k))/(s(k+1)-s(k))*(aq2-s(k))
        if(present(hadr)) hadr = hps(k) + (hps(k+1)-hps(k))/(s(k+1)-s(k))*(aq2-s(k))
        ! rho = 0.d0
        ! cov = 0.d0
      endif
      END SUBROUTINE

END MODULE VPOLnsk_v29
