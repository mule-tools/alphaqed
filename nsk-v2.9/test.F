! compile with gfortran vpolNsk.F test.F
      program test
        use vpolnsk_m
      implicit none
      double precision q2
      double complex vpfull,vplept,vphadr

      q2=1.02d0*1.02d0
      call vpolnsk(q2, vpfull, vphadr, vplept)
      print *,"s = ",q2," GeV2 VP     = ",vpfull
      print *,"s = ",q2," GeV2 VPlept = ",vplept
      print *,"s = ",q2," GeV2 VPlhadr= ",vphadr

      q2=-1.02d0*1.02d0
      call vpolnsk(q2, vpfull, vphadr, vplept)
      print *,"s = ",q2," GeV2 VP     = ",vpfull
      print *,"s = ",q2," GeV2 VPlept = ",vplept
      print *,"s = ",q2," GeV2 VPlhadr= ",vphadr
      end 
