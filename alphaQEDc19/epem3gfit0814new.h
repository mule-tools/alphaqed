
c      ++++ epem3gfit0814new_qedc19 ++++  

c  E= 0.810 -- 1.000  update dec 2019 
       DATA aresl/     
     &      1.166218d0,-1.071386d0, 0.381586d0,-0.071092d0, 0.017145d0,
     &      0.011789d0, 0.000155d0, 0.005713d0, 0.001719d0, 0.003731d0,
     &      0.000001d0, 0.000474d0/
       DATA astal/     
     &      0.009275d0,-0.002545d0, 0.002614d0, 0.001668d0, 0.000991d0,
     &     -0.000298d0,-0.000244d0, 0.000249d0,-0.000097d0, 0.001445d0,
     &     -0.000353d0, 0.000721d0/
       DATA asysl/     
     &      0.006984d0,-0.000028d0, 0.001526d0,-0.000207d0,-0.000158d0,
     &      0.000045d0, 0.000341d0, 0.000359d0,-0.000266d0, 0.000370d0,
     &     -0.000538d0,-0.000147d0/

c  E=1.040 -- 1.435   update dec 2019 
       DATA aresh/     
     &      0.642686d0, 0.061042d0, 0.192658d0,-0.080030d0, 0.058586d0,
     &     -0.032595d0, 0.022666d0,-0.013278d0, 0.009632d0,-0.003039d0,
     &      0.002526d0,-0.001830d0/
       DATA astah/     
     &      0.013976d0,-0.011119d0, 0.011701d0,-0.007615d0, 0.006785d0,
     &     -0.005071d0, 0.003477d0,-0.002644d0, 0.001857d0,-0.001190d0,
     &      0.000645d0,-0.000370d0/
       DATA asysh/     
     &      0.022838d0,-0.001532d0, 0.010423d0,-0.003441d0, 0.003860d0,
     &     -0.003897d0, 0.003429d0,-0.001539d0, 0.000745d0,-0.000198d0,
     &      0.000262d0, 0.000038d0/

c  phi resonance background [applies for 1.00 - 1.04]
c   1.00001 -- 1.03999 update dec 2019 
       DATA aresfr/    
     &      0.337990d0, 0.327434d0,-0.003803d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
       DATA astafr/    
     &      0.039858d0, 0.026115d0, 0.000428d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
       DATA asysfr/    
     &      0.013358d0, 0.016191d0, 0.002916d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/

c  Fit below phi 
c   0.958 -- 1.000 update dec 2019 
       DATA aresfm/    
     &      0.438455d0,-0.027466d0, 0.019876d0, 0.010861d0, 0.006790d0,
     &     -0.000480d0,-0.000411d0, 0.001277d0/
       DATA astafm/    
     &      0.007425d0, 0.004151d0,-0.000027d0, 0.000963d0, 0.001736d0,
     &      0.000397d0, 0.000046d0, 0.000183d0/
       DATA asysfm/    
     &      0.007829d0, 0.000007d0, 0.000672d0, 0.000086d0, 0.000270d0,
     &     -0.000299d0,-0.000335d0, 0.000802d0/
