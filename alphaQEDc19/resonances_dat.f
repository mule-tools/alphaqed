      subroutine resonances_data_qedc19()
c resonance parameters mass M, width G (all in MeV) and branching fraction P to e^+e^- 
c resonance ranges in GeV
      implicit none
      integer i
      REAL*8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI,MPIPLUS,MPIZERO
      REAL*8 PSI(6),MPS(6),GPS(6),PPS(6),YPI(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6)
      REAL*8 EOMM,EOMP,EFIM,EFIP,EROM,EROP
      REAL*8 RMA(15),RGA(15),RPA(15),EREM(15),EREG(15),EREP(15),
     &     EMI(15),EMA(15)
      common /pions_qedc19/MPIPLUS,MPIZERO
      COMMON/BWRO_qedc19/MRO,GRO,PRO/BWOM_qedc19/MOM,GOM,POM/BWFI_qedc19/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/PSYPPAR_qedc19/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESRELERR_qedc19/FSTA,FSYS
      COMMON/RESDOMAINS_qedc19/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESALL_qedc19/RMA,RGA,RPA,EREM,EREG,EREP
      COMMON/RESRAN_qedc19/EMI,EMA
C Update PDG 1994 J/Psi and Ypsilon resonances: mass, width,
C branching fraction to ee, errors
C 2002 update 22/08/02, now PDG 2004 update PDG 2010
C Masses and width in MeV; partial width as a branching fraction
       DATA MPS /3096.916D0,3686.09D0,3772.92D0,4039.D0,4153.D0,4421.D0/
       DATA GPS /0.0929D0,0.304D0,27.3D0,80.D0,103.D0,62.D0/
       DATA PPS /5.94D-2,7.72D-3,9.7D-6,1.07D-5,8.1D-6,9.4D-6/
       DATA MYP /9460.30D0,10023.26D0,10355.2D0,10579.4D0,10865.D0,
     &      11019.D0/
       DATA GYP /54.020D-3  ,31.98D-3  ,20.32D-3,20.5D0 ,110.D0,79.D0/
       DATA PYP /2.48D-2  ,1.91D-2  ,2.18D-2  ,1.57D-5,2.8D-6 ,1.6D-6/
C Errors used here actually are not statistic and systematic but combined
C ones for width and brfrac; in order not to underestimate the error
C by adding all the combined errors quadratically we treat the first
C quadratically the second linearly
C *********        RELATIVE ERRORS for Resonances      ***********
       DATA STAPS /5.7D-2,11.2D-2,11.4D-2,19.2D-2,25.6D-2,34.9D-2/
       DATA SYSPS /4.2D-2,14.8D-2,15.2D-2,28.6D-2,40.0D-2,36.4D-2/
       DATA STAYP /3.4D-2,15.9D-2,13.3D-2,09.2D-2,11.8D-2,20.2D-2/
       DATA SYSYP /6.7D-2,16.0D-2,09.3D-2,20.8D-2,25.0D-2,31.3D-2/
C ################################################################
C *********        RELATIVE ERRORS for Resonances      ***********
C Rho: Kinoshita's fit, Omega, Phi, J/Psi, Ypsilon updated jan 2006,
c omega, phi updatad jul 2017
      DATA FSTA/1.0D-2,0.83D-2,0.8D-2,4.7D-2,4.1D-2/
      DATA FSYS/1.2D-2,1.96D-2,1.1D-2,4.6D-2,5.3D-2/
c resonance domains
c      DATA EOMM,EOMP,EFIM,EFIP/.41871054d0,.810d0,1.00D0,1.04D0/
c      DATA EMIPS /3.08587D0,3.67496D0,3.7000D0,3.960D0,4.102D0,4.315D0/
c      DATA EMAPS /3.10787D0,3.69696D0,3.8400D0,4.098D0,4.275D0,4.515D0/
c      DATA EMIYP/9.44937D0,10.01226D0,10.3442D0,
c     &     10.473D0,10.690D0,10.975D0/
c      DATA EMAYP/9.47137D0,10.03426D0,10.3662D0,
c     &     10.687D0,10.950D0,11.300D0/
c replaced feb 2012
      DATA EOMM,EOMP,EFIM,EFIP/.754d0,.810d0,1.00D0,1.04D0/
c      DATA EMIPS/3.08587D0,3.67496D0,3.63642D0,3.63900D0,3.63800D0,
c     &     4.11100D0/
c      DATA EMAPS/3.10787D0,3.69696D0,3.90942D0,4.43900D0,4.66800D0,
c     &     4.73100D0/
c      DATA EMIPS/3.09200D0,3.685D0,3.6877D0,3.9100D0,4.28100D0,4.46800D0/
c      DATA EMAPS/3.102000,3.6876D0,3.90902D0,4.27900D0,4.46200D0,4.73100D0/
c      DATA EMIYP/9.4501D0,1.002116D1,1.03521D1,1.04730D1,1.03150D1,1.06240D1/
c      DATA EMAYP/9.47D0,1.0026364D1,1.03600D1,1.06870D1,1.14150D1,1.14140D1/
      DATA EMIPS/3.09300D0,3.68457D0,3.6940D0,3.69500D0,3.69600D0,
     &     4.11100D0/
      DATA EMAPS/3.10200D0,3.68761D0,3.90942D0,4.43900D0,4.66800D0,
     &     4.73100D0/
      DATA EMIYP/9.45010D0,1.00212D1,1.03521D1,1.04730D1,1.04150D1,
     &     1.06240D1/
      DATA EMAYP/9.47000D0,1.00264D1,1.03600D1,1.06870D1,1.14150D1,
     &     1.14140D1/
c
      data MPIPLUS,MPIZERO/139.57018d0,134.9766d0/
c errors
      DATA EREM / 0.34d0, 0.12d0, 0.02d0,
     &     0.011d0,0.04d0,0.35d0,1.0d0,3.0d0,4.0d0,
     &     0.26d0,0.31d0,0.5d0,1.2d0,8.0d0,8.0d0/
      DATA EREG / 0.80d0, 0.08d0, 0.04d0,
     &     2.8d-3,9.0d-3,1.0d0,10.d0,8.0d0,20.d0,
     &     1.25d-3,2.63d-3,1.85d-3,2.5d0,13.d0,16.0d0/
      DATA EREP /0.05D-5,0.14d-5,0.03d-4,
     &     0.06d-2,0.17d-3,0.7d-6,0.16d-5,0.9d-6,3.2d-6,
     &     0.07d-2,0.16d-2,0.21d-2,0.08d-5,0.7d-6,0.5d-6/

      MRO=775.49D0               ! +/- 0.34 PDG 2010 + update neutral only
      GRO=149.10D0              ! +/- 0.80 PDG 2010 + update neutral only
      PRO=4.72D-5               ! pm 0.05d-5 PDG 2010 + update
      MOM=782.65D0              ! +/- 0.12 PDG 2010
      GOM=8.49d0                ! +/- 0.08  PDG 2010
      POM=7.28D-5               ! pm 0.14d-5 PDG 2010
      MFI=1.019461D3            ! pm 0.019 PDG 2014
      GFI=4.266D0                ! pm 0.031   PDG 2014
      PFI=2.954D-4               ! pm 0.03d-4 PDG 2010
      EROM=2.d0*MPIPLUS*1.D-3
      EROP=EOMP
c all in one array masses, widths, branching fractions, energy ranges (emin,emax)
      RMA(1)=MRO
      RMA(2)=MOM
      RMA(3)=MFI
      RGA(1)=GRO
      RGA(2)=GOM
      RGA(3)=GFI
      RPA(1)=PRO
      RPA(2)=POM
      RPA(3)=PFI
      EMI(1)=EROM
      EMA(1)=EROP
      EMI(2)=EOMM
      EMA(2)=EOMP
      EMI(3)=EFIM
      EMA(3)=EFIP
      do i=1,6
         RMA(3+i)=MPS(i)
         RGA(3+i)=GPS(i)
         RPA(3+i)=PPS(i)
         EMI(3+i)=EMIPS(i)
         EMA(3+i)=EMAPS(i)
      enddo 
      do i=1,6
         RMA(9+i)=MYP(i)
         RGA(9+i)=GYP(i)
         RPA(9+i)=PYP(i)
         EMI(9+i)=EMIYP(i)
         EMA(9+i)=EMAYP(i)
      enddo 
      return
      end
