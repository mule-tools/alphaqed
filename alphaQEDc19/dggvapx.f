c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; dggvap.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Fri Jan  2 23:52:06 2015
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Sat Jul 15 23:47:27 2017
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2015 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
c **********************************************************************
c *         F. Jegerlehner, Humboldt Univerity Berlin, Grermany        *
c **********************************************************************
       FUNCTION dggvapx_qedc19(s,error,errorsta,errorsys)
C      -----------------------------
c Updated Novem 2015: hadr5n12 --> hadr5n15
c Updated March 2012: hadr5n09 --> hadr5n12
c                     - includes BaBar and KLOE pipi ISR spectra
c                     - improved resolution of some resonance regions
c                     - 2010 Review of Particle Proprties resonace parameters
c                     - new version alpha_2 [<gamma 3> vacuum polarization] based on (ud) and (s) flavor separation        
c                     - <3 3> also available based of recombined flavor  separation
c calculate real part of 1PI photon vacuum polarization
c  --- combines real hadronic part (dhadr5n12) with leptonic part (leptons) ---
C Fermionic contributions to the running of alpha
C Function collecting 1-loop, 2-loop, 3-loop leptonic contributions (e, mu, tau) 
C + hadronic contribution from light hadrons (udscb)
C + top quark contribution (t); here at 1-loop (good up to 200 GeV LEP I/II) 
C error is errder: central value dggvap +/- errder -> upper/lower bound
c Update December 2014: hadr5n12 --> hadr5n14 
c  -- included new data by KLOE 2012, BaBar 2012...2014, Novosibirsk 2014
c  -- up-to-date PDG parameters for omega and phi, 
c  -- option for using directly the omega and phi data
c  -- using local complex VP subtraction for BW_qedc19 resonances (numerical integration by NAG routine)  
c  -- BESIII included     
       implicit none
       real*8 dggvapx_qedc19,degvapx_qedc19,s
       real*8 c0,st2,als
       real*8 mtop,mtop2
       real*8 dallepQED1_qedc19,dallepQED2_qedc19,dallepQED3l_qedc19,
     &        dalQED1ferm_qedc19,res_re,res_im
       real*8 r1real,r1imag,r2real,r2imag,imag
       real*8 r1reall,r1imagl,r2reall,r2imagl,r3reall,r3imagl
       real*8 fac,ee,e,der,errder,errdersta,errdersys
     &      ,deg,errdeg,errdegsta,errdegsys
       real*8 dalept,daltop,dahadr,dghadr,hfun,MW2,
     &      fourMW2,Dalphaweak1MSb,error,errorsta,errorsys,null
       include 'common.h'      
       common /parm_qedc19/st2,als,mtop
c keep partial results here; as calculated in this routine        
       common /resu_qedc19/dalept,dahadr,daltop,Dalphaweak1MSb
       common /rdeg_qedc19/dghadr,errdegsta,errdegsys,degvapx_qedc19
       null=0.d0
       e=dsqrt(dabs(s))
       if (s.lt.null) e=-e
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'had')) then
          call dhadr5x_qedc19(e,st2,der,errdersta,errdersys,
     &         deg,errdegsta,errdegsys)
          dahadr=der
          dghadr=deg
       else 
          dahadr=null
          dghadr=null
       endif
       if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
          dalept=null
       else 
          r1real =dallepQED1_qedc19(s,r1imag)
          r2real =dallepQED2_qedc19(s,r2imag)
          r3reall=dallepQED3l_qedc19(s,r1reall,
     &                          r1imagl,r2reall,r2imagl,r3imagl)
          res_re=r1real+r2real+r3reall
          res_im=r1imag+r2imag+r3imagl
          dalept=res_re
       endif
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'top')) then
          mtop2=mtop*mtop
c          write (*,*) ' dggvap: mtop',mtop
          daltop=dalQED1ferm_qedc19(s,mtop2,imag)*4.d0/3.d0
       else 
          daltop=null
       endif
c MSbar alpha for one-loop weak contribution [WWgamma]
c       Dalphaweak1MSb=-adp/4.d0*(7.d0*log(mu^2/MW2)+2.d0/3.d0) ! OS value for s>>MW2
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'wea')) then
c          MW=80.399d0 now set in constants.f [via common.h]
          MW2=MW*MW
          fourMW2=MW2
          if (xMW.ne.null) fourMW2=xMW**2
           if (abs(s).gt.fourMW2) then
             Dalphaweak1MSb=-adp*(7.d0/4.d0*log(abs(s)/MW2))
          else 
             Dalphaweak1MSb=0.0d0
          endif
       endif
       errorsta=errdersta
       errorsys=errdersys
       error=sqrt(errorsta**2+errorsys**2)
       dggvapx_qedc19=dalept+dahadr
       RETURN
       END

       FUNCTION degvapx_qedc19(s,error,errorsta,errorsys)
C      -----------------------------
c Delta g SM SU(2) coupling: interface for dggvap 
c Update Decembber 2014: new flavor separation and recombination 
c redesigned after cross check on the lattice by Harvey Meyer        
       implicit none
       real*8 dggvapx_qedc19,degvapx_qedc19,degvap1,s,err,errsta,errsys,xxx
       real*8 st2,als,mtop,errdegsta,errdegsys,dghadr1
       real*8 dalept,dahadr,daltop,Dalphaweak1MSb
       real*8 dglept,dghadr,dgetop,Dalpha2weak1MSb,
     &      error,errorsta,errorsys
       common /parm_qedc19/st2,als,mtop
       common /resu_qedc19/dalept,dahadr,daltop,Dalphaweak1MSb     ! input
       common /resg_qedc19/dglept,dghadr1,dgetop,Dalpha2weak1MSb   ! output
       common /rdeg_qedc19/dghadr,errdegsta,errdegsys,degvap1
       xxx=dggvapx_qedc19(s,err,errsta,errsys)  ! fills common resu and part of rdeg
       dglept=dalept/4.d0/st2
       dghadr1=dghadr
       dgetop=daltop/4.d0/st2*3.d0/2.d0
       Dalpha2weak1MSb=Dalphaweak1MSb/st2*43.d0/42.d0
       errorsta=errdegsta
       errorsys=errdegsys
       error=sqrt(errorsta**2+errorsys**2)
       degvap1=dglept+dghadr
       degvapx_qedc19=degvap1
       RETURN
       END

