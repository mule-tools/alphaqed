c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; alphaQEDr17.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sat Jul 15 19:43:55 2017
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Tue Jul 18 18:29:26 2017
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2017 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      PROGRAM alphaQEDr17
C Calculating the running QED coupling alpha(E), as a real function 
C Leptons: 1--loop, 2--loop exact, 3--loop in high energy approximation
C Quarks form routine dhadr5n including effective s--channel shifts in low energy range
C r1....,r2....,r3.... : one-loop, two-loop, three-loop result
C  ..real: real part, ..imag: imaginary part, not provided here
C ......l: light fermions only (high energy approximation for 3--loops) 
C dallepQED1n=r1real,dallepQED2n=r2real,dallepQED3l=r3reall
C dallepQED3l also returns one-loop and two-loop high energy (light fermion) 
C approximations 
c 21/09/2009 common pQCD parameter setting via constants_qcd.f      
c 21/09/2012 new option for W contribution: iOSW=0 strict MSbar, iOSW=1 including 
c            high energy asymptotic constant of the OS W contribution at s>>MW2
c 10/07/2017 providing statistical and systematic hadronic errors separately now
c            allows to set up the covariance matrix
      implicit none
      integer*4 i,j,N,stflag,iOSW
      real*8 dggvapx,s,error,errorsta,errorsys,e,null,Dalphaweak1MSb
      real*8 st2,als,mtop,dvpt_higx,dvpt_lowx,dummy
      real*8 dalept,dahadr,daltop,dvpt_new,dvpt_hig,dvpt_low
      real*8 alphac,alphah,alphal,alphacx,alphahx,alphalx,
     &     alphact,alphaht,alphalt,dvpt_newx,
     &     alphacW,alphalW,alphahW,weaksav
      real*8 emax,emin,elogl,elogu,delog,elog,fep,fem,fractionalerror
      external dggvapx
********************* part to be included in main program **************
      double precision pi1,ALINP,ERRAL,EINP,MTOP1    ! get QCD parameters as set in constants_qcd
c some QCD parameters required to calculare top contribution 
      include 'common.h'      
      common/var1/pi1,ALINP,ERRAL,EINP,MTOP1
      common /parm/st2,als,mtop
      common /resu/dalept,dahadr,daltop,Dalphaweak1MSb
      COMMON/IOSW/iOSW
      call constants()
      call constants_qcd()
c      sin2ell=0.23153 ! pm 0.00016 LEPEEWG Phys Rep 427 (2006) 257
      st2=0.23153d0   ! Reference value for weak mixing parameter
      als=ALINP       ! alpha strong
      mtop=MTOP1      ! top quark mass
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment in main the following 2 lines for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau
c      iLEP=LFLAG_qedc19(LEPTONflag)
************************************************************************
      null=0.d0
c the parameter xMW [provided via common.h] allows to change W threshold default M_W, e.g. to xMW=2.d0*MW
c      xMW=null is set in constants ! default M_W
c      xMW=2.d0*MW
c local option for including W contribution
      iOSW=0          ! =0 W in MSbar, =1 W in OS at s>>MW2
      N=1999
c write header for graphx plot program
      do j=1,4
         write (j,*) '      3.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N)
      enddo
c      do j=11,14
      j=14
         write (j,*) '      3.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N)
c      enddo
c     ..................................................................
c     vacuum polarization  ...[c] central ...l low value, ...h high value
      write (1,*) ' e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write (2,*) ' e,alpha ,alphal ,alphah (no top)    '
      write (3,*) ' e,alphat,alphatl,alphath (with top)   ' 
      write (4,*) ' e,alphacW,alphalW,alphahW'
      write(14,*) ' e,dalept,dahadr,daltop,Dalphaweak1MSb'
c      write(12,*) ' e,(alphah-alphal)/2    '
c      
c caution: logatithmic scaling used below: cannot choose emin=0 or emax=0 !!!!
c      
      stflag=1
c     for space-like region: stflag=-1
c      stflag=-1
      emin=1.d-6
c dalpha low
      emin=.27d0
c      emax=1.1d0
cc dalpha med
c      emin=1.0d0
c      emax=6.0d0
ccc dalpha hig
      emin=0.30d0
      emax=14.0d0
ccc plot EJP
cc      emin=1.d-6
cc      emax=10.0d0
ccc weak high energy
c      emin=13.9d0
c      emax=1.4d3
c      emin=100.0d0
c      emax=2.0d4
c      emin=.970d0
c      emax=2.50d0
c      emax=35.0
c      emin=9.00d0
c      emax=14.5
c      emax=91.19d0
      elogl=dlog(dabs(emin))
      elogu=dlog(dabs(emax))
      delog=(elogu-elogl)/N
      do 10 i=1,N+1
      if (stflag.eq.1) then
        elog=elogl+delog*(i-1)
      else
        elog=elogu-delog*(i-1)
      endif
      e=dexp(elog)
      s=e*e*stflag
      e=dsqrt(dabs(s))*stflag
c
      dvpt_new= dggvapx(s,error,errorsta,errorsys)
      dvpt_hig= dvpt_new+error
      dvpt_low= dvpt_new-error
      alphac =alp/(1.d0-dvpt_new)
      alphah =alp/(1.d0-dvpt_hig)
      alphal =alp/(1.d0-dvpt_low)
      alphact=alp/(1.d0-dvpt_new-daltop)
      alphaht=alp/(1.d0-dvpt_hig-daltop)
      alphalt=alp/(1.d0-dvpt_low-daltop)
      alphacW=alp/(1.d0-dvpt_new-daltop-Dalphaweak1MSb)
      alphahW=alp/(1.d0-dvpt_hig-daltop-Dalphaweak1MSb)
      alphalW=alp/(1.d0-dvpt_low-daltop-Dalphaweak1MSb)
      weaksav=Dalphaweak1MSb
c here we evaluate the space-like values at the same s
c the idea is to cross check errors from data vs errors magnified by taking Principal Value integrals 
c in the data (time-like region)
      dvpt_newx= dggvapx(-abs(s),error,errorsta,errorsys)
      dvpt_higx= dvpt_newx+error
      dvpt_lowx= dvpt_newx-error
      alphahx =alp/(1.d0-dvpt_higx)
      alphalx =alp/(1.d0-dvpt_lowx)
      alphacx =alp/(1.d0-dvpt_newx)
c fractional total error in space-like region of delta alpha
      fractionalerror=0.d0
      if (dvpt_newx.ne.null) then
         fractionalerror=(dvpt_higx-dvpt_lowx)/2.d0/dvpt_newx
         fractionalerror=abs(fractionalerror)
      endif
      fep=1.d0+fractionalerror
      fem=1.d0-fractionalerror
c
c     delta alpha: leptons + 5 quarks no top, no W
c
      write (1,99) e,dvpt_new,dvpt_low,dvpt_hig
      write(11,99) e,dvpt_new,dvpt_new*fem,dvpt_new*fep
c
c     alpha: leptons + 5 quarks no top no W
c
c fractional total error in space-like region of alpha
      fractionalerror=(alphahx-alphalx)/2.d0/alphacx
      fep=1.d0+fractionalerror
      fem=1.d0-fractionalerror

      write (2,99) e,alphac ,alphal ,alphah
      write(12,99) e,alphac ,alphac*fem,alphac*fep
c check fractional error time-like vs space-like
      write(13,99) e,(alphah-alphal)/2.d0/alphac,fractionalerror
c
c     alpha: leptons + 5 quarks + top 
c
      write (3,99) e,alphact,alphalt,alphaht
c
c     alpha: leptons + 5 quarks + top + W (1-loop MSbar) 
c
      write (14,99) e,alphacW,alphalW,alphahW
c
c     delta alpha: leptons, 5 quarks, top, W
c
      dummy=dggvapx(s,error,errorsta,errorsys)

      write(4,99) e,dalept,dahadr,daltop,weaksav
c
 10   continue
c     ..................................................................
 99   format(5(2x,1pe11.4))
      stop 10
      END
c
