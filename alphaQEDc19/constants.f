      subroutine constants_qedc19()
      implicit none
      real *8 alpi,ealpi
      include 'common.h'
      data pi,pi2,ln2/3.141592653589793d0,9.869604401089358d0,
     &                0.6931471805599453d0/    
      data zeta2,zeta3/1.644934066848226d0,1.20205690315959d0/
      data zeta5/1.036927755143370d0/
c      DATA ALP/0.0072973544D0/
c     26/05/2008 new result from Gabrielse on a_e
c update 2012 Aoyama et al arXiv:1205.5368
      DATA alpi,gmu/   137.035999166d0,1.16637D-5/
      DATA ealpi,ergmu/000.000000037d0,0.00001D-5/
      data ml /0.510998910d-3,105.658367d-3,1.77682d0/
      data erml /0.000000013d-3,0.000004d-3,0.00016d0/
c xMW is a switch which allows to specify the MSbar matchingscale for the W contribution
c the parameter xMW [provided via common.h] allows to change W threshold default M_W, e.g. to xMW=2.d0*MW
c      xMW=null is set in constants ! default M_W
c      xMW=2.d0*MW
      data MZ,MW,xMW/91.1876d0,80.385d0,0.0d0/
      data ERMZ,ERMW/00.0021d0,00.015d0/
      data small,large,large_3/1.d-4,1.d6,8.d0/
      save
      a0=sqrt(pi/alpi/sqrt(2.d0)/gmu)
      era0=a0*(ealpi/alpi+ergmu/gmu)/2.d0
      alp   =1.d0/alpi
      adp   = alp/pi
      adp2  = adp*adp
      adp3  = adp*adp2
      facbw=9.d0/4.d0*alpi**2   ! in common RESFIT
      alp1=alp
      eralp=alp*ealpi/alpi
C switch off/on warnings iwarnings=0/1 default is 1 
      iwarnings=0
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
      LEPTONflag='all'
      iLEP  = -3  ! for sum of leptons + quarks
C Overwrite default switches for replacing exact by asymptotic expansions 
C (required to enforce numerical stability)
C Do not change without checking whether new switches give acceptable results
C z=|s|/m^2 (threshold= 4 m^2), 
C low energy expansion up to O(z^3): c_1*z+c_2*z^2
C high energy expansion up to O(z^-3): c_1/z+c_2/z^2 up to logs
C
*      small=1d-6   !  default 
*      large=1d6    !  default 
C
C corresponding switch for 3--loop high energy approximation 
C where exact result is not available at lower energies 
C i.e. below large_3*|s| 3--loop contribution is taken to be zero
C
*     large_3=8.d0  ! default  
C
      return
      end

