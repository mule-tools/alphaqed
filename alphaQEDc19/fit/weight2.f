      PROGRAM WEIGHTEDAVERAGE
C rewrites a datatable            
C INPUTfFILE format inflag=0     SOROU.DAT: x,y,statvariance,systfraction
C                   inflag=1     SOROU.DAT: x,y,statpercent,systpercent
C                   inflag=2     SOROU.DAT: x,y,statvariance,systvariance
C into DATA fortran statement in HADRO.DAT: x,y,statvariance,systfraction 
C and a standard table in        
C OUTPUTFILE format ouflag=0     HATAB.DAT: x,y,statvariance,systfraction 
C    for plotfile   ouflag=1     HATAB.DAT: x,y,statvariance,statvariance,systfraction 
C calculates weighted average at coinciding points
      INTEGER NP
      PARAMETER(NP=800)
      REAL*4 XIN(NP),YIN(NP,4),XOU(NP),YOU(NP,4)
     . ,ZIN(10),SINST2(10),SINSY2(10)
      INTEGER*4 I,N,J,JCOUNT,ICOUNT,NM,K
      integer inflag,ouflag,ftflag
      COMMON/ENERGY/E
      OPEN (UNIT=1,FILE='SOROU.DAT',STATUS='OLD')
      OPEN (UNIT=2,FILE='HADRO.DAT',STATUS='NEW')
      OPEN (UNIT=3,FILE='HATAB.DAT',STATUS='NEW')
      YMAX=0.0
      mevtogev=0
      inflag=0
      ouflag=0
c      ouflag=1
      ftflag=5
c      ftflag=7
      DO 9 I=1,NP
        READ(1,*) XIN(I),YIN(I,1),YIN(I,2),YIN(I,3)
c        READ(1,*) XIN(I),YIN(I,1),YIN(I,2),YIN(I,3),YIN(I,4)
c        XIN(I)=(XIN(I)+YIN(I,1))/2.
c        YIN(I,1)=YIN(I,2)
c        YIN(I,2)=YIN(I,3)
c        YIN(I,3)=YIN(I,4)
c correction factor for 2004 published KLOE data
cc KLOE repair 
c        YIN(I,1)=YIN(I,1)*(1.d0-0.007d0)
c        YIN(I,2)=YIN(I,2)*(1.d0-0.007d0)
c        write (*,*) ' Warning: Y values rescaled for KLOE'
cc end KLOE repair
        if (mevtogev.eq.1) XIN(I)=XIN(I)/1000. 
        if (inflag.eq.1) then
           YIN(I,2)=YIN(I,1)*YIN(I,2)/100.
           YIN(I,3)=YIN(I,3)/100.
        else if (inflag.eq.2) then
           YIN(I,3)=YIN(I,3)/YIN(I,1)
        else
        endif
        WRITE(*,*) ' I:',I,' ',XIN(I),YIN(I,1),YIN(I,2),YIN(I,3)
        IF (XIN(I).EQ.0.0) THEN
          N=I-1
          GOTO 10
        ENDIF
        YMAX=AMAX1(YMAX,YIN(I,1))
  9   CONTINUE
      ICOUNT=1
      JCOUNT=1
 10   continue
      if (ouflag.eq.1) then
        WRITE(3,*) '     3.0000 '
        WRITE(3,*) '     1.0000 '
        WRITE(3,*) FLOAT(N)
        WRITE(3,*) 'C comment line'
      endif
      DO 13 I=1,N
      IF (JCOUNT.GT.1) THEN
        JCOUNT=JCOUNT-1
        GOTO 13
      ENDIF 
      J=1
      DO WHILE (I+J.LE.N+1)
        ZIN(J)=YIN(I+J-1,1)
        SINST2(J)=YIN(I+J-1,2)**2
        SINSY2(J)=(YIN(I+J-1,1)*YIN(I+J-1,3))**2
        IF ((I+J.EQ.N+1).OR.(XIN(I+J).GT.XIN(I))) GOTO 20
        J=J+1
      ENDDO
 20   JCOUNT=J
      IF (JCOUNT.GT.1) THEN
        E=XIN(I)
        CALL WIGHTMEAN(JCOUNT,ZIN,SINST2,SINSY2,ZOU,SOUST,SOUSY)
        if (ouflag.eq.1) then
           write(3,124)   xin(i),zou,soust,soust,sousy
        else
        IF (XIN(I).GT.100.D0) THEN
          IF (YMAX.GE.100.) THEN
            WRITE(3,27)   XIN(I),ZOU,SOUST,SOUSY
          ELSE
            WRITE(3,17)   XIN(I),ZOU,SOUST,SOUSY
            WRITE(*,*) XIN(I),ZOU,SOUST,SOUSY
          ENDIF
C          WRITE(3,17)  XIN(I),ZOU,SOUST,SOUSY
        ELSE
          IF (YMAX.GE.100.) THEN
            WRITE(3,21)   XIN(I),ZOU,SOUST,SOUSY
          ELSE
            WRITE(3,11)   XIN(I),ZOU,SOUST,SOUSY
          ENDIF
C          WRITE(3,11)  XIN(I),ZOU,SOUST,SOUSY
        ENDIF
        endif
C        IF ((XIN(I).GE.0.9).AND.(XIN(I).LE.2.05)) THEN
        YOU(ICOUNT,1)=XIN(I)
        YOU(ICOUNT,2)=ZOU
        YOU(ICOUNT,3)=SOUST
        YOU(ICOUNT,4)=SOUSY
        ICOUNT=ICOUNT+1
C        ENDIF
        GOTO 13
      ELSE
        ZOU=YIN(I,1)
        SOUST=YIN(I,2)
        SOUSY=YIN(I,3)
        if (ouflag.eq.1) then
           write(3,124)   xin(i),zou,soust,soust,sousy
        else
        IF (XIN(I).GT.100.D0) THEN
          IF (YMAX.GE.100.) THEN
            WRITE(3,27)   XIN(I),ZOU,SOUST,SOUSY
          ELSE
            WRITE(3,17)   XIN(I),ZOU,SOUST,SOUSY
            WRITE(*,*) XIN(I),ZOU,SOUST,SOUSY
          ENDIF
C          WRITE(3,17)  XIN(I),ZOU,SOUST,SOUSY
        ELSE
          IF (YMAX.GE.100.) THEN
            WRITE(3,21)   XIN(I),ZOU,SOUST,SOUSY
          ELSE
            WRITE(3,11)   XIN(I),ZOU,SOUST,SOUSY
          ENDIF
C          WRITE(3,11)  XIN(I),ZOU,SOUST,SOUSY
        ENDIF
        endif
C        IF ((XIN(I).GE.0.9).AND.(XIN(I).LE.2.05)) THEN
        YOU(ICOUNT,1)=XIN(I)
        YOU(ICOUNT,2)=YIN(I,1)
        YOU(ICOUNT,3)=YIN(I,2)
        YOU(ICOUNT,4)=YIN(I,3)
        ICOUNT=ICOUNT+1
C        ENDIF
      ENDIF
 13   CONTINUE
      IF (ftflag.eq.5) THEN
      NM=INT(AINT((ICOUNT-1)/5.)+1.)
      WRITE (2,*) N
      WRITE (2,15) 
      DO I=1,NM
      K=5*I-5
C WRITE X-VALUES
      IF (YOU(K,1).LT.100.) THEN
c      WRITE (2,149) YOU(K,1),YOU(K+1,1),YOU(K+2,1),YOU(K+3,1),YOU(K+4,1)
      WRITE (2,141) YOU(K,1),YOU(K+1,1),YOU(K+2,1),YOU(K+3,1),YOU(K+4,1)
      ELSE
      WRITE (2,121) YOU(K,1),YOU(K+1,1),YOU(K+2,1),YOU(K+3,1),YOU(K+4,1)
      ENDIF
      ENDDO
      WRITE (2,16) 
C WRITE Y-VALUES
      DO J=1,2
      DO I=1,NM
      K=5*I-5
      IF (YMAX.GE.100.) THEN
      WRITE (2,242) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1)
      ELSE
      WRITE (2,141) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1)
      ENDIF
      ENDDO
      ENDDO
      J=3
      DO I=1,NM
      K=5*I-5
      WRITE (2,142) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1)
      ENDDO
      ELSE 
      NM=INT(AINT((ICOUNT-1)/7.)+1.)
      WRITE (2,*) N
      WRITE (2,15) 
      DO I=1,NM
      K=7*I-7
C WRITE X-VALUES
      IF (YOU(K,1).LT.100.) THEN
      WRITE (2,14) YOU(K,1),YOU(K+1,1),YOU(K+2,1),YOU(K+3,1),YOU(K+4,1)
     .,YOU(K+5,1),YOU(K+6,1)
      ELSE
      WRITE (2,12) YOU(K,1),YOU(K+1,1),YOU(K+2,1),YOU(K+3,1),YOU(K+4,1)
     .,YOU(K+5,1),YOU(K+6,1)
      ENDIF
      ENDDO
      WRITE (2,16) 
C WRITE Y-VALUES
      DO J=1,2
      DO I=1,NM
      K=7*I-7
      IF (YMAX.GE.100.) THEN
      WRITE (2,24) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1),YOU(K+5,J+1),YOU(K+6,J+1)
      ELSE
      WRITE (2,14) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1),YOU(K+5,J+1),YOU(K+6,J+1)
      ENDIF
      ENDDO
      ENDDO
      J=3
      DO I=1,NM
      K=7*I-7
      WRITE (2,14) YOU(K,J+1),YOU(K+1,J+1),YOU(K+2,J+1),YOU(K+3,J+1)
     .,YOU(K+4,J+1),YOU(K+5,J+1),YOU(K+6,J+1)
      ENDDO
      ENDIF
 11   FORMAT (1X ,6X,F6.2,3(',',2X,F6.3))
 21   FORMAT (1X ,6X,F6.2,2(',',2X,F6.2),1(',',2X,F6.3))
 12   FORMAT (1X ,4X,'&',2X,7(F6.1,'D0,'))
 121  FORMAT (1X ,4X,'&',2X,7(F7.2,'D0,'))
 22   FORMAT (1X ,4X,'&',2X,7(F6.1,'D0,'))
 14   FORMAT (1X ,4X,'&',2X,7(F6.3,'D0,'))
 142  FORMAT (1X ,4X,'&',2X,5(F6.3,'D0,'))
c 141  FORMAT (1X ,4X,'&',2X,7(F8.5,'D0,'))
c 141  FORMAT (1X ,4X,'&',2X,5(F8.6,'D0,'))
 141  FORMAT (1X ,4X,'&',2X,5(F8.5,'D0,'))
 149  FORMAT (1X ,4X,'&',2X,5(1PE10.2,','))
 24   FORMAT (1X ,4X,'&',2X,7(F6.2,'D0,'))
 242  FORMAT (1X ,4X,'&',2X,7(F8.3,'D0,'))
 15   FORMAT (1X ,6X,' DATA XX/')
 16   FORMAT (1X ,6X,' DATA YY/')
 17   FORMAT (1X ,6X,F6.1,3(',',2X,F6.3))
C 17    format(1x,f9.3,4(2x,1pe11.4))
 27   FORMAT (1X ,6X,F6.1,2(',',2X,F6.2),1(',',2X,F6.3))
 124  FORMAT(1X,F9.3,4(2X,1PE11.4))
      END

      SUBROUTINE WIGHTMEAN(J,ZIN,SINST2,SINSY2,ZOU,SOUST,SOUSY)
C input  y_i,statvariance_i,systvariance_i i=1,2,...,j
C output weighted everage y,statvariance,systfraction
      REAL*4 ZIN(10),SINST2(10),SINSY2(10),W(10)
      INTEGER*4 I,J
      COMMON/ENERGY/E
      WRITE(*,*) J
      ST2=0.0
      SY2=0.0
      WT=0.0
      YT=0.0
      AST2=0.0
      ASY2=0.0
      DO I=1,J
      ST2=ST2+SINST2(I)
      SY2=SY2+SINSY2(I)
      SIN2=SINST2(I)+SINSY2(I)
      WRITE(*,*) SINST2(I),SINSY2(I)
      W(I)=1./SIN2
      WT=WT+W(I)
      YT=YT+W(I)*ZIN(I)
      AST2=AST2+SINST2(I)*W(I)
      ASY2=ASY2+SINSY2(I)*W(I)
      ENDDO
      ZOU=YT/WT
      SOU=1./SQRT(WT)
      SOUST=SQRT(ST2/(ST2+SY2))*SOU
      SOUSY=SQRT(SY2/(ST2+SY2))*SOU/ZOU
      WRITE(*,*) ' NULLTEST: ',(AST2+ASY2)/J-1.
      ALTST=SQRT(1./J*AST2/WT)
      ALTYT=SQRT(1./J*ASY2/WT)/ZOU
      WRITE(7,*) SOUST,ALTST,SOUSY,ALTYT
C calculate chi-square
       chi2=0.d0
       do i=1,j
          chi2=chi2+w(i)*(zou-zin(i))**2
       enddo
       S=1.d0
       if (j.gt.1) S=sqrt(chi2/(j-1))
       if (S.gt.1.d0) then
          SOUST=S*SOUST
          SOUSY=S*SOUSY
          if (S.le.1.25d0) then
             write(*,*) ' S=',S,' incompatible data at E= ',e
             write(9,*) ' S=',S,' incompatible data at E= ',e
          else
             write(*,*) ' S=',S,' incompatible data at E= ',e
             write(9,*) ' S=',S,' incompatible data at E= ',e
          endif
       endif
      RETURN
      END
