
c      ++++ ffppg2fit0810 ++++  

c 0.81 -- 1.00  update jan 2012/ june 2013   ! fit for n>2 channels only
c obtained with fit program fitffppg2.f 
       DATA ares/      
     &     1.106113d0,  0.055370d0, 0.781375d0, 0.013935d0, 0.234605d0,
     &      0.004004d0, 0.086713d0, 0.009467d0, 0.019477d0,-0.006026d0,
     &     -0.007033d0,-0.016508d0/
       DATA asta/      
     &     0.030376d0,  0.014216d0, 0.015173d0,-0.002976d0, 0.001568d0,
     &     -0.003694d0, 0.001484d0, 0.001706d0, 0.002972d0, 0.001951d0,
     &      0.001499d0, 0.000332d0/
       DATA asys/      
     &     0.057262d0,  0.020901d0,-0.015085d0,-0.020877d0,-0.007959d0,
     &      0.003006d0, 0.007108d0, 0.004228d0,-0.000339d0,-0.000981d0,
     &      0.000196d0,-0.000053d0/
