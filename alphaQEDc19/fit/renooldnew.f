       SUBROUTINE RENOoldnew(energy,reno)
c provides renormalization factor reno for R(s)=sigma_had(s)/sigma_pt(s) 
c reno = (|alpha_old/alpha_new|)^2; where alpha_old is the one from hadr5n03
c and alpha_new the current from alphaQEDcomplex; see commons for other related values 
       implicit none
       integer Ismooth
       double precision energy,e,derold,errderold,degold,errdegold,
     &      pi1,ALINP,ERRAL,EINP,MTOP1,st2,als,mtop
       double precision dalept,dahadr,daltop,Dalphaweak1MSb,dahadrold
       double precision one,null,cvap2,dvpt_new,dggvapx,s,
     &      error,errorsta,errorsys,dvpt_old,
     &      romPiold,rvap2old,reno,funalpqedrx,ralphac,ralphaold,
     &      renoold,renonew
       double complex cone,calp,calphac,funalpqedcx,
     &      cerror,cerrorsta,cerrorsys,comPi,
     &      cvpt_new,cggvapx,cerrder,cerrdersta,cerrdersys,calphanew
       external funalpqedrx,funalpqedcx,dggvapx,cggvapx
***************** part to be included also in main program **************
       include '../common.h'
       common/var1/pi1,ALINP,ERRAL,EINP,MTOP1
       common /parm/st2,als,mtop
       common /resu/dalept,dahadr,daltop,Dalphaweak1MSb
       common /rhadold/dahadrold
c renoold is 2003 (alpha(s)/alpha)**2; renonew is 2012 version of it based on complec alpha 
       common /oldnew/ralphaold,calphanew,renoold,renonew
       COMMON/SMOO/Ismooth
       call constants()
       call constants_qcd()
c      sin2ell=0.23153 ! pm 0.00016 LEPEEWG Phys Rep 427 (2006) 257
       st2=0.23153d0            ! Reference value for weak mixing parameter
       als=ALINP                ! alpha strong
       mtop=MTOP1               ! top quark mass
C LEPTONflag=all,had,lep,ele,muo,tau,lem -> iLEP=-3,-2,-1,1,2,3,4
C Some VP subtractuions only include the electron = ele or electron plus muon = lem
C Default:
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks
C Decomment in main the folowwing line for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau,lem
************************************************************************
       Ismooth=0
       e=energy
       s=e*abs(e)
c
       call dhadr5n03(e,st2,derold,errderold,degold,errdegold)
c
       one =1.d0
       null=0.d0
       cone=DCMPLX(1.d0,null)
       calp=DCMPLX(alp,null)
       calphac=funalpqedcx(s,cerror,cerrorsta,cerrorsys,'all')
       comPi=calp/calphac
       cvap2 =DREAL(comPi*DCONJG(comPi))
       dvpt_new= dggvapx(s,error,errorsta,errorsys)
       dvpt_old=dvpt_new-dahadr+derold
       romPiold=one-dvpt_old
       rvap2old =romPiold**2
       reno=cvap2/rvap2old
       calphanew=calphac
       ralphaold=alp/romPiold
       renoold=rvap2old
       renonew=cvap2
c       write (*,*) ' renooldnew:1)',derold,errderold,degold,errdegold
c       write (*,*) ' renooldnew:2)',calp,comPi
c       write (*,*) ' renooldnew:3)',dvpt_new,dvpt_old
c       write (*,*) ' renooldnew:',e,reno,derold
       RETURN
       END
      
