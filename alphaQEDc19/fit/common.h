      character*3 LEPTONflag
      integer iLEP,iwarnings,LFLAG_qedc19,IRESON,iresonances
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      real *8 alp,eralp,alp1,gmu,ergmu,a0,era0,adp,adp2,adp3
      real *8 facbw,small,large,large_3
      real *8 MZ,MW,xMW,ERMZ,ERMW,ml(3),erml(3)
      common /consts/pi,pi2,ln2,zeta2,zeta3,zeta5
      common /params/ALP,adp,adp2,adp3
      common /switch/small,large,large_3
      common /lepton/iLEP,iwarnings,LEPTONflag
      common /RESFIT/facbw,IRESON
      common /RES/iresonances
      common /alpgmu/alp1,gmu,a0,eralp,ergmu,era0
      common /physparams/MZ,MW,xMW,ERMZ,ERMW		       
      common /leptomass/ml,erml
