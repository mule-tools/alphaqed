      program fitRdata
c fitting data from Rdat_all.f
      implicit none
      character*23 chan(9,6,3)
      character*14 name
      integer i,j,n,jj,Nall,Ndat,icount,IGS,iso,Ismooth,Npipi,NCOM,
     &     iomegaphidat,itruedatapoints,idat,job,ich
      parameter(Nall=2187,Ndat=961)
c      parameter(Npipi=572,NCOM=46)
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      real *8 e,emin,emax,s,rs40,rs40smoothed
      real *8 res(4),rfi(4)
      real *8 X(Nall),XD(Ndat)
c,Xpipi(Npipi),ECOM(NCOM)
      INTEGER NA,NB,NF,IOR,ICHK
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      REAL *8 CHPTCUT,EC,ECUT
      REAL *8 ALS2,EST,ESY,MZINP,MTOP2
      real *8 chisq,chisqbegin,chisqend,sx,ex,de,yfit,es,ed,amap,bmap
      real *8 y,rm,rp,sig,xx,alamda,covar,alpha,a,gues,guesini
      real *8 fitpolynom,ysav,am,ac,ap,ei,si,GZINTCOR
      integer  ni,nj,nw,np,k,ns,npi
      parameter (nj=500)
      dimension e(nj),s(nj),y(nj),rm(nj),rp(nj),ysav(nj,3)
      REAL xnskip,xncurv,xNPT,yfim,yfip,yfie,errt
      INTEGER nskip,NU1N,ier,jjj,n1,n2,JER
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      external funcs,RS40,rs40smoothed,GZINTCOR

c      FUNCTION RSpQCD(s,IER) in Rdat_fun.f requires QCD parameters incl errors
c provided via the following common block:
      COMMON/QCDPA/ALS2,EST,ESY,MZINP,MTOP2,NF,IOR,ICHK ! global INPUT here
c to set QCD parameters alpha_s with stat and sys error
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z, top mass,
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s)
c to be calculated, ICHK is dummy here
c (in some programs used for alternative routines calculating R(s))
      common/var1/pi1,ALINP,ERRAL,EINP,MTOP1
      COMMON /RCUTS/CHPTCUT,EC,ECUT
      COMMON/GUSA/IGS,iso
      COMMON /ERR/JER
      COMMON/RESDOMAINS/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /OMEPHIDAT/iomegaphidat

      include '../common.h'
      include 'Rdataguess.f'
      call constants()
      call constants_qcd() ! fills commons var and RCUTS
      include '../xRdat-extended.f'
      include '../xRdat-nonres.f'
c      include '../dat/xset0821.f'
      call Rdata()
c fill common QCDPA
      ALS2=ALINP
      EST=ERRAL
      ESY=0.D0
      MZINP=EINP
      MTOP2=MTOP1
      IOR=4                     ! default
      NF =5                     ! default
      IGS=0           ! Gounaris-sakurai parametrisation for pipi channel
      iso=2           ! e^+ e^- data GS fit; iso=0,1 yields isospin 0, isovector part
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
c      IRESON=1        ! include narrow resonances in fits
c      iresonances=1   ! include narrow resonances in data sets
      icount=0
      iomegaphidat=0                
c      CHPTCUT=0.318d0
c      EC=5.2d0
      EC=9.5d0
      ECUT=40.d0
      do j=1,9
         do i=1,6
            do n=1,3
               chan(j,i,n)='                       '
            enddo
         enddo
      enddo
c set 1 name = epemfit0814new
c  E= 0.810 -- 1.000  updated nov 2010 / jan 2012 / june 2013\
      chan(1,1,1)='       DATA aresl/'
      chan(1,1,2)='       DATA astal/'
      chan(1,1,3)='       DATA asysl/'
c  E=1.040 -- 1.435   updated nov 2010 / jan 2012 / june 2013\
      chan(1,2,1)='       DATA aresh/'
      chan(1,2,2)='       DATA astah/'
      chan(1,2,3)='       DATA asysh/'
c  phi resonance background [applies for 1.00 - 1.04]\
c   1.00001 -- 1.03800 update jan 2012 / june 2013\
      chan(1,3,1)='       DATA aresfr/'
      chan(1,3,2)='       DATA astafr/'
      chan(1,3,3)='       DATA asysfr/'
c  Fit below phi \
c   0.958 -- 1.000 update jan 2012 / june 2013 \
      chan(1,4,1)='       DATA aresfm/'
      chan(1,4,2)='       DATA astafm/'
      chan(1,4,3)='       DATA asysfm/'
c set 2 name = ffppg2fit0810
c " Use fitffppg2 fit program in this case"
c  0.81 -- 1.00  update jan 2012   ! fit fot n>2 channels only\
      chan(2,1,1)='       DATA ares/'
      chan(2,1,2)='       DATA asta/'
      chan(2,1,3)='       DATA asys/'
c set 3 name = ffppmfit0810
c " Use fitffppg2 fit program in this case"
c 0.81 -- 1.00  update jan 2012  ! fit for all channels \
      chan(3,1,1)='       DATA ares/'
      chan(3,1,2)='       DATA asta/'
      chan(3,1,3)='       DATA asys/'
c set 4 name = epemfit079081
c  0.787 -- 0.8099 ares epemfit079081  update jan 2012\
      chan(4,1,1)='       DATA ares/'
      chan(4,1,2)='       DATA asta/'
      chan(4,1,3)='       DATA asys/'
c set 5 name = epemfit1432
c   1.375 -- 2.038 update jan 2012 / june 2013\
      chan(5,1,1)='       DATA areslow/'
      chan(5,1,2)='       DATA astalow/'
      chan(5,1,3)='       DATA asyslow/'
c   1.395 -- 2.000 update jan 2012 / june 2013 \
      chan(5,2,1)='       DATA ares/'
      chan(5,2,2)='       DATA asta/'
      chan(5,2,3)='       DATA asys/'
c  2.9 -- 3.70 updated jan 2012 / june 2013 \
      chan(5,3,1)='       DATA ares1/'
      chan(5,3,2)='       DATA asta1/'
      chan(5,3,3)='       DATA asys1/'
c  2.25 -- 3.0 checked jan 2012 / june 2013 \
      chan(5,4,1)='       DATA fres2330 /'
      chan(5,4,2)='       DATA fsta2330 /'
      chan(5,4,3)='       DATA fsys2330 /'
c  1.975 -- 2.243 update jan 2012 / june 2013 \
      chan(5,5,1)='       DATA fres2023 /'
      chan(5,5,2)='       DATA fsta2023 /'
      chan(5,5,3)='       DATA fsys2023 /'
c set 6 name = epemfit32cut
c  2.9 -- 3.70 update jab 2012 / june 2013\
      chan(6,1,1)='       DATA ares1/'
      chan(6,1,2)='       DATA asta1/'
      chan(6,1,3)='       DATA asys1/'
c  3.598  --  3.787 updated jan2012 / june 2013\
      chan(6,2,1)='       DATA aresi/'
      chan(6,2,2)='       DATA astai/'
      chan(6,2,3)='       DATA asysi/'
c 3.598 -- 3.71  feb 2012 new / june 2013\
      chan(6,3,1)='       DATA aresin/'
      chan(6,3,2)='       DATA astain/'
      chan(6,3,3)='       DATA asysin/'
c 4.57 -- 9.5 checked jan 2012 / june 2013\
      chan(6,4,1)='       DATA aresbes/'
      chan(6,4,2)='       DATA astabes/'
      chan(6,4,3)='       DATA asysbes/'
c 9.5 -- 14.0 update jan 2012 / june 2013\
      chan(6,5,1)='       DATA aresy /'
      chan(6,5,2)='       DATA astay /'
      chan(6,5,3)='       DATA asysy /'
c  14.04 -- 38.29 checked mar 2012 / june 2013\
      chan(6,6,1)='       DATA aresh /'
      chan(6,6,2)='       DATA astah /'
      chan(6,6,3)='       DATA asysh /'
c set 7 name = rfitrholow
c 0.318 -- 0.778 updated nov 2010 (incl BaBar/KlOE) / jan 2012 / june 2013\
      chan(7,1,1)='       DATA ares/'
      chan(7,1,2)='       DATA asta/'
      chan(7,1,3)='       DATA asys/'
c set 8 name = rfitrhohig
c  0.769  -- 0.789 update jan 2012 /june 2013\
      chan(8,1,1)='       DATA ares/'
      chan(8,1,2)='       DATA asta/'
      chan(8,1,3)='       DATA asys/'
c set 2 name = rfitchptail
c  0.285 -- 0.325  new feb 2012 / updated jan 2015
      chan(9,1,1)='       DATA ares/'
      chan(9,1,2)='       DATA asta/'
      chan(9,1,3)='       DATA asys/'

c      write (7,'(A,A,A)') 'c     ++++',name,'++++ ' 
c      do i=1,Nall
c         write(45,*) i,X(i)
c      enddo
      do i=1,Ndat
         write(45,*) i,XD(i)
      enddo
c      do i=1,Npipi
c         write(45,*) Xpipi(i)
c      enddo
c      stop
      idat=0
      if (idat.eq.1) then
         write(11,*)  '       2.0000'
         write(11,*)  '       1.0000'
         write(11,*)  '   ',float(Ndat-2)
         write(12,*)  '       2.0000'
         write(12,*)  '       1.0000'
         write(12,*)  '   ',float(Ndat-2)
         do i=2,Ndat
c      do i=1,Nall
            ex= X(i)
c      do i=2,Ndat
c         ex= XD(i)
c         if (e.gt.ECUT) goto 10
            icount=icount+1
            sx=ex*ex
            do j=1,3
               res(j)=rs40(sx,j)
               rfi(j)=rs40smoothed(sx,j)
            enddo
c plot total error
            res(4)=sqrt(res(2)**2+res(3)**2)
            rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
c         write(11,98) xd(i),res(1),res(4),res(4),res(3),i
c         write(12,98) xd(i),rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),rfi(3),i
            write(11,98) x(i),res(1),res(4),res(4),res(3),i
          write(12,98) x(i),rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),rfi(3),i
         enddo
         write(1,*) icount
      endif
c 10   continue
 98   format (1X,F10.7,4(2x,1PE11.4),2x,i4)
c 98   format (1X,F10.7,4(2x,1PE11.5),2x,i4)
c      iresonances=0
      ECUT=40.d0
      EC=9.5d0  ! use data below Upsilon
c initialize
      if (guesini(1,1).eq.0.d0) then
         do ier=1,3
            do i=1,np
               guesini(i,ier)=0.0d0
            enddo
         enddo
      endif

      open (UNIT=2,FILE='fitR.dat',STATUS='NEW')
C
c excluding resonances
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
c psi1  3.08587D0 3.10787D0
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
c yps1  9.44937D0 9.47137D0
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0
c yps4  10.473D0  10.687D0
c yps5  10.690D0  10.950D0
c yps6  10.975D0  11.063D0
c make numbered list of points
c      do i=1,Nall
c         write(14,'(1x,i4,3x,f12.6)') i,X(i)
c      enddo
c
      call getindex(emin,Ndat,XD,n1)
      call getindex(emax,Ndat,XD,n2)
      if ((job.eq.1).and.((ich.eq.1).or.(ich.eq.4))) then
      else if ((job.eq.1).and.((ich.eq.2).or.(ich.eq.3))) then
         if (XD(n1).lt.emin) n1=n1+1
      else if (job.eq.6) then
      else  
         if (emax.gt.XD(n2)) n2=n2+1
      endif
      write(*,*) ' actual ',n1,XD(n1),n2,XD(n2)
      nw=n2-n1+1
      if (idat.eq.0) then
         write(11,*)  '       2.0000'
         write(11,*)  '       1.0000'
         write(11,*)  '   ',float(nw)
      endif
      write(12,*)  '       2.0000'
      write(12,*)  '       1.0000'
      write(12,*)  '   ',float(nw)
c empty arrays
      do i=1,nj
         e(i)=0.d0
         y(i)=0.d0
      enddo
C error loop
      do jjj=1,3
         ier=jjj
         icount=0
         do j=n1,n2
            ei=XD(j)
c            if ((ei.ge.EMIPS(1)).and.(ei.le.EMAPS(1)).or.
c     &          (ei.ge.EMIPS(2)).and.(ei.le.EMAPS(2)).or.
c     &          (ei.ge.EMIYP(1)).and.(ei.le.EMAYP(1)).or.
c     &          (ei.ge.EMIYP(2)).and.(ei.le.EMAYP(2)).or.
c     &          (ei.ge.EMIYP(3)).and.(ei.le.EMAYP(3)).or.
c     &          (ei.ge.EMIYP(4)).and.(ei.le.EMAYP(4)).or.
c     &          (ei.ge.EMIYP(5)).and.(ei.le.EMAYP(5)).or.
c     &          (ei.ge.EMIYP(6)).and.(ei.le.EMAYP(6))) goto 12
cc         if ((ei.ge.EOMM).and.(ei.le.EOMP)) goto 12
cc         if ((ei.ge.EFIM).and.(ei.le.EFIP)) goto 12
ccc         if ((ei.le.EFIP)) goto 12
cc         if ((ei.ge.EFIM)) goto 12
cc around phi
cc         if ((ei.le.(EFIP-0.2d0))) goto 12
cc         if ((ei.ge.(EFIM+0.2d0))) goto 12
c            if (RS40(ei*ei,1).eq.0.0) goto 12
            icount=icount+1
c            write(*,*) ' icount,j-n1+1',icount,j-n1+1,j,n1
            e(icount)=ei
            si=ei*ei
            y(icount)=RS40(si,ier)
            if (ier.eq.1) then
               ier=2
               rm(icount)=RS40(si,ier)
               if (rm(icount).eq.0.d0) then
                  ier=3
                  rm(icount)=RS40(si,ier)
               endif
               do jj=1,3
                  rfi(jj)=rs40smoothed(si,jj)
c                  write (*,*) ' **** ',ei,rfi(jj),jj
               enddo
c plot total error
               rfi(4)=sqrt(rfi(2)**2+rfi(3)**2)
               write(12,98) ei,rfi(1),rfi(1)-rfi(4),rfi(1)+rfi(4),
     &              rfi(3),j
               ier=1
            endif
            ysav(icount,ier)=y(icount)
            if ((ei.gt.11.5d0).and.(ei.lt.ECUT))
     &           ysav(icount,ier)=ysav(icount,ier)*GZINTCOR(si)
         enddo
      enddo

      write(*,*) ' number of fit points[range]:',
     &     icount,'[',e(1),' -- ',e(icount),']'
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',e
c      write(*,*) ' *********************************'
c      write(*,*) ' input data:',y
c      write(*,*) ' *********************************'
c      write(*,*) '  array dim, occupncy:',nj,nw
C error loop
c      write (101,*) (ysav(j,1),j=1,icount)
c      write (101,*) (ysav(j,2),j=1,icount)
c      write (101,*) (ysav(j,3),j=1,icount)
      do jjj=1,3
         ier=jjj
         do j=1,icount
            y(j)=ysav(j,ier)
            if (ier.eq.1) then
               rm(j)=sqrt(ysav(j,2)**2+ysav(j,3)**2)
            else
               rm(j)=sqrt(ysav(j,2)**2+ysav(j,3)**2)/100.
            endif
c            y(j)=ysav(j,1)
c            rm(j)=sqrt(ysav(j,2)+ysav(j,3))
          write(8,*) ' input',j,ier,y(j),rm(j)
         enddo
         do j=1,icount
            if (idat.eq.0) then
               write(11,98) e(j),y(j),rm(j),rm(j),ysav(j,3),j
            endif
         enddo
c      Mapping to Tschebycheff interval [-1,+1]
c         write (101,*) ' icount,ier:',icount,jjj
c         write (101,*) ( y(j),j=1,icount)
c         write (101,*) (rm(j),j=1,icount)
      nw=icount
      es=(e(nw)+e(1))
      ed=(e(nw)-e(1))
      amap=2.d0/ed
      bmap=-es/ed
      alamda=-1.d0
      do i=1,np
        a(i)=guesini(i,jjj)
        lista(i)=i
      enddo
c      npi=12
c      npi=11
cc      npi=10
cc      npi=9
cc      npi=7
cc      npi=6
cc      npi=4
cc      npi= 5
c      npi= 3
cc      npi=14
      write (*,*) ' initial fit coefficients',a
      chisq=0.0
      do j=1,nw
        s(j)=amap*e(j)+bmap
        yfit=fitpolynom(s(j),a,npi)
        chisq=chisq+((y(j)-yfit)/(rm(j)))**2
        write(*,*) j,s(j),yfit,chisq
      enddo
      chisqbegin=chisq
      write(*,*) ' '
      write(*,*) ' chisq of guess:',chisqbegin
      write(*,*) ' '
c      ns: number of iterrations
      ns=20000
c      npi: number of parameters to be ajusted
      do 300 k=1,ns
      alamda=-1.d0
        call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
c       write(*,'(1x,6(1pe11.4))') (a(i),i=1,npi)
c       write(*,*) ' chisq,alamda',chisq,alamda
  300 continue
        write(*,'(/1x,a,i2,t18,a,f10.4,t43,a,e9.2)') 'Iteration #',ns,
     *       'Chi-squared:',chisq,'ALAMDA:',alamda
        write(*,'(1x,t5,a,t13,a,t21,a,t29,a,t37,a,
     &       t45,a,t53,a,t61,a,t69,a)') 'A(1)',
     *       'A(2)','A(3)','A(4)','A(5)','A(6)',
     *       'A(7)','A(8)','A(9)'
        write(*,'(111x,12(1pe11.4))') (a(i),i=1,npi)
c Recalculate chi2
      alamda=0.d0
        call mrqmin(s,y,rm,nw,a,np,lista,npi,
     &        covar,alpha,np,chisq,funcs,alamda)
        write(*,*) 'Uncertainties:'
        write(*,'(1x,12(1pe11.4)/)') (sqrt(covar(i,i)),i=1,npi)
        write(*,'(1x,a)') 'Expected results:'
        write(*,'(1x,f7.2,5f8.2/)') a
      chisq=0.0
      do j=1,nw
        yfit=fitpolynom(s(j),a,npi)
        chisq=chisq+((y(j)-yfit)/(rm(j)))**2
      enddo
      chisqend=chisq
      write(*,*) ' '
      write(*,*) chisqbegin,' -->',chisqend,'S=',sqrt(chisqend/(nw-1))
      write(*,*) ' '
      if (ier.eq.1) then
         do j=1,np
            ac(j)=a(j)
         enddo
      endif
      if (ier.eq.2) then
         do j=1,np
            am(j)=a(j)
         enddo
      endif
      if (ier.eq.3) then
         do j=1,np
            ap(j)=a(j)
         enddo
      endif
      enddo  ! end ier loop

c Header for ploting fit
      write(2,*) '      2.0000'
      write(2,*) '      1.0000'
      write(2,*) '    100.000'
C Write plot data of fit
      de=(e(nw)-e(1))/100.
      ex=e(1)
      do j=1,100
        yfit=fitpolynom(amap*ex+bmap,ac,npi)
        yfim=fitpolynom(amap*ex+bmap,am,npi)
        yfip=fitpolynom(amap*ex+bmap,ap,npi)
        yfie=sqrt(yfim**2+yfip**2)
        write(2,124) ex,yfit,yfit-yfim,yfit+yfim,yfit-yfie,yfit+yfie
        ex  =ex+de
      enddo
c Header for ploting data
      write(3,*) '      2.0000'
      write(3,*) '      1.0000'
      write(3,*) '   ',float(nw)
      write(4,*) '      2.0000'
      write(4,*) '      1.0000'
      write(4,*) '   ',float(nw)
      do j=1,nw
         errt=sqrt(ysav(j,2)**2+ysav(j,3)**2)
         write(3,124) e(j),ysav(j,1),ysav(j,2),ysav(j,2)
         write(4,124) e(j),ysav(j,1),errt,errt
      enddo
c      write (7,*) 'job,ich=',job,ich
      write (7,'(A)') chan(job,ich,1)
      if (np.eq.3)  write(7,83) ac
      if (np.eq.4)  write(7,84) ac
      if (np.eq.5)  write(7,85) ac
      if (np.eq.6)  write(7,86) ac
      if (np.eq.7)  write(7,87) ac
      if (np.eq.8)  write(7,88) ac
      if (np.eq.9)  write(7,89) ac
      if (np.eq.10) write(7,90) ac
      if (np.eq.11) write(7,91) ac
      if (np.eq.12) write(7,92) ac
      if (np.eq.13) write(7,93) ac
      if (np.eq.14) write(7,94) ac
      write (7,'(A)') chan(job,ich,2)
      if (np.eq.3)  write(7,83) am
      if (np.eq.4)  write(7,84) am
      if (np.eq.5)  write(7,85) am
      if (np.eq.6)  write(7,86) am
      if (np.eq.7)  write(7,87) am
      if (np.eq.8)  write(7,88) am
      if (np.eq.9)  write(7,89) am
      if (np.eq.10) write(7,90) am
      if (np.eq.11) write(7,91) am
      if (np.eq.12) write(7,92) am
      if (np.eq.13) write(7,93) am
      if (np.eq.14) write(7,94) am
      write (7,'(A)') chan(job,ich,3)
      if (np.eq.3)  write(7,83) ap
      if (np.eq.4)  write(7,84) ap
      if (np.eq.5)  write(7,85) ap
      if (np.eq.6)  write(7,86) ap
      if (np.eq.7)  write(7,87) ap
      if (np.eq.8)  write(7,88) ap
      if (np.eq.9)  write(7,89) ap
      if (np.eq.10) write(7,90) ap
      if (np.eq.11) write(7,91) ap
      if (np.eq.12) write(7,92) ap
      if (np.eq.13) write(7,93) ap
      if (np.eq.14) write(7,94) ap
c next guess parameters
      if (np.eq.3)  write(9,63) ac
      if (np.eq.4)  write(9,64) ac
      if (np.eq.5)  write(9,65) ac
      if (np.eq.6)  write(9,66) ac
      if (np.eq.7)  write(9,67) ac
      if (np.eq.8)  write(9,68) ac
      if (np.eq.9)  write(9,69) ac
      if (np.eq.10) write(9,70) ac
      if (np.eq.11) write(9,71) ac
      if (np.eq.12) write(9,72) ac
      if (np.eq.13) write(9,73) ac
      if (np.eq.14) write(9,74) ac
      if (np.eq.3)  write(9,63) am
      if (np.eq.4)  write(9,64) am
      if (np.eq.5)  write(9,65) am
      if (np.eq.6)  write(9,66) am
      if (np.eq.7)  write(9,67) am
      if (np.eq.8)  write(9,68) am
      if (np.eq.9)  write(9,69) am
      if (np.eq.10) write(9,70) am
      if (np.eq.11) write(9,71) am
      if (np.eq.12) write(9,72) am
      if (np.eq.13) write(9,73) am
      if (np.eq.14) write(9,74) am
      if (np.eq.3)  write(9,83) ap
      if (np.eq.4)  write(9,84) ap
      if (np.eq.5)  write(9,85) ap
      if (np.eq.6)  write(9,86) ap
      if (np.eq.7)  write(9,87) ap
      if (np.eq.8)  write(9,88) ap
      if (np.eq.9)  write(9,89) ap
      if (np.eq.10) write(9,90) ap
      if (np.eq.11) write(9,91) ap
      if (np.eq.12) write(9,92) ap
      if (np.eq.13) write(9,93) ap
      if (np.eq.14) write(9,94) ap


124   format(1x,f11.6,5(2x,1pe13.6))
 99   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 94   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 93   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 92   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 91   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 90   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 89   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 88   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
 87   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0/')
 86   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0/')
 85   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0/')
 84   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0/')
 83   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0/')
c +++++
 74   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 73   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 72   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 71   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 70   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 69   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 68   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')
 67   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,',f9.6,'d0,')
 66   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,',/,
     &     '     &     ',f9.6,'d0,')
 65   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,',f9.6,'d0,')
 64   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,',
     &     f9.6,'d0,')
 63   format ('     &     ',f9.6,'d0,',f9.6,'d0,',f9.6,'d0,')

      stop 10
      end

      function fitpolynom(x,a,na)
      implicit none
      INTEGER na,i
      REAL*8 x,ax,a,fitpolynom,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      fitpolynom=0.d0
      do i=1,na
         fitpolynom=fitpolynom+a(i)*t(i)
      enddo
      return
      end
C
      include './nur/mrqcof.f'
      include './nur/mrqmin.f'
      include './nur/covsrt.f'
      include './nur/gaussj.f'

      subroutine funcs(x,a,y,dyda,na)
      implicit none
      INTEGER na,i
      REAL*8 x,y,ax,a,dyda,fitpolynom,t
      dimension a(na),dyda(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo

      y=fitpolynom(x,a,na)

      do i=1,na
         dyda(i)=t(i)
      enddo

      return
      end

c  0.2791404      1
c  0.3150000      7
c  0.3200000      8
c  0.6100000     57
c  0.6160000     58
c  0.8100000    152
c  1.4000000    307
c  2.0000000    406
c  2.2000000    407
c  2.4000000    408
c  2.5000000    409
       FUNCTION GZINTCOR(S)
C      --------------------
C      CORRECTION FOR GAMMA-Z-INTERFERRENCE IN HIGH ENERGY DATA
       IMPLICITREAL*8 (A-Z)
       PI=4.D0*DATAN(1.D0)
       GMU=1.166390D-5
       ALP=1.D0/137.0359895D0
       MZ2=91.1887D0**2
       ST2=0.2322D0
       POL=DSQRT(2.D0)*GMU/16.D0/PI/ALP*S*MZ2/(S-MZ2)
       QFQF=11.D0/9.D0
       QFVF=QFQF*(21.D0/11.D0-4.D0*ST2)
       VVAAF=10.D0-56.D0/3.D0*ST2+176.D0/9.D0*ST2**2
       VE=-1.D0+4.D0*ST2
       VVAAE=2.D0-8.D0*ST2+16.D0*ST2**2
       C1=QFQF-2.D0*VE*QFVF*POL+VVAAE*VVAAF*POL**2
       GZINTCOR=QFQF/C1
       RETURN
       END
