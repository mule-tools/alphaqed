      parameter (np=6)
      integer  lista(np)
      dimension covar(np,np),alpha(np,np),a(np),am(np),ac(np),ap(np),
     &     gues(np),guesini(np,3)
c  1.845 -- 2.130 update dec 2019
      DATA guesini/
      include 'param_epemn2fit1425.h3'
      emin=1.845d0
      emax=2.130d0
      npi=6
      name='epemn2fit1425'
      job=5
      ich=3
