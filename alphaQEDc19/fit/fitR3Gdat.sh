#!/bin/csh -fx
if ("$1" == "") goto usage

  set KILL_FILES=( \
		fitR.dat)
    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif

set update = 'update dec 2019'
echo "\
++ ""$update"" ++ "

if ("$1" == "1") then 
set name = epem3gfit0814new
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  E= 0.810 -- 1.000  ""$update"" " >> array.1
echo "\
c  E=1.040 -- 1.435   ""$update"" " > array.2
echo "\
c  phi resonance background [applies for 1.00 - 1.04]\
c   1.00001 -- 1.03999 ""$update"" " > array.3
echo "\
c  Fit below phi \
c   0.958 -- 1.000 ""$update"" " > array.4
set njob = 4
endif
if ("$1" == "2") then 
echo " Empty option"
exit
endif
if ("$1" == "3") then 
echo " Empty option"
exit
endif
if ("$1" == "4") then 
set name = epem3gfit079081
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.787 -- 0.8099 ares epem3gfit079081  ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "5") then 
set name = epem3gfit1425
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c   1.375 -- 1.837 ""$update"" " >> array.1
echo "\
c   1.780 -- 1.880 ""$update"" " > array.2
echo "\
c   1.845 -- 2.130 ""$update"" " > array.3
set njob = 3
endif
if ("$1" == "6") then 
echo " Empty option"
exit
endif
if ("$1" == "7") then 
set name = r3gfitrholow
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.318 -- 0.778 ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "8") then 
set name = r3gfitrhohig
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.769  -- 0.789 ""$update"" " >> array.1
set njob = 1
endif

set ijob = 1

while ( $ijob <= $njob )

echo ' job' $ijob ' is running'

  foreach i ($KILL_FILES)
    if ( -r $i ) then
      mv -f $i $i$ijob
      continue
    endif
  end

cp guess_$name.h$ijob R3Gdataguess.f
echo "      name='"$name"'"  >> R3Gdataguess.f
echo "      job=""$1"  >> R3Gdataguess.f
echo "      ich=""$ijob"   >> R3Gdataguess.f

make -f make_fitR3Gdat

echo ' run  fitR3Gdat'
time ./fitR3Gdat

cat array.$ijob >> fitpar.dat

cat fort.7 >> fitpar.dat 

$HOME/andromeda/gra/fjplo3 fitRdat0

echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

if ( $ijob == $njob ) then 
echo -n "Update RS3G_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
endif
endif

 @ ijob++

end

echo "Results in fitpar.dat"

exit 0
usage:
echo "Usage fit function name labels 1,...,8"
echo "1=epem3gfit0814new"
echo "4=epem3gfit079081"
echo "5=epem3gfit1425"
echo "7=r3gfitrholow"
echo "8=r3gfitrhohig"
exit 2

