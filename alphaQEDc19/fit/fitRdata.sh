#!/bin/csh -fx
if ("$1" == "") goto usage

  set KILL_FILES=( \
		fitR.dat)
    if ( -r fitpar.dat ) then
	rm -f fitpar.dat
    endif

set update = 'update dec 2019'
echo "\
++ ""$update"" ++ "

if ("$1" == "1") then 
set name = epemfit0814new
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  E= 0.810 -- 0.999999  ""$update"" " >> array.1
echo "\
c  E=1.040 -- 1.435   ""$update"" " > array.2
echo "\
c  phi resonance background [applies for 1.00 - 1.04]\
c   1.00001 -- 1.03800  ""$update"" " > array.3
echo "\
c  Fit below phi \
c   0.958 -- 1.000 ""$update"" " > array.4
set njob = 4
endif
if ("$1" == "2") then 
set name = ffppg2fit0810
echo " Use fitffppg2 fit program in this case"
exit
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  ""$update"" ! fit for n>2 channels only\
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "3") then 
set name = ffppmfit0810
echo " Use fitffppg2 fit program in this case"
exit
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.81 -- 1.00  ""$update"" ! fit for all channels 
c obtained with fit program fitffppg2.f " >> array.1
set njob = 1
endif
if ("$1" == "4") then 
set name = epemfit079081
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.787 -- 0.8099 ares epemfit079081 ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "5") then 
set name = epemfit1432
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c   1.375 -- 2.038 ""$update"" " >> array.1
echo "\
c   1.395 -- 2.000 ""$update"" " > array.2
echo "\
c  2.9 -- 3.70 ""$update"" " > array.3
echo "\
c  2.25 -- 3.0 ""$update"" " > array.4
echo "\
c  1.975 -- 2.243 ""$update"" " > array.5
set njob = 5
endif
if ("$1" == "6") then 
set name = epemfit32cut
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  2.9 -- 3.70 ""$update"" " >> array.1
echo "\
c  3.598  --  3.787 ""$update"" " > array.2
echo "\
c 3.598 -- 3.71 ""$update"" " > array.3
echo "\
c 4.57 -- 9.5 ""$update"" " > array.4
echo "\
c 9.5 -- 14.0 ""$update"" " > array.5
echo "\
c  14.04 -- 38.29 ""$update"" " > array.6
set njob = 6
endif
if ("$1" == "7") then 
set name = rfitrholow
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.318 -- 0.778 ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "8") then 
set name = rfitrhohig
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c  0.769  -- 0.789 ""$update"" " >> array.1
set njob = 1
endif
if ("$1" == "9") then 
set name = rfitchptail
echo "\
c      ++++ ""$name"" ++++  " > array.1
echo "\
c 0.285 -- 0.325 ""$update"" " >> array.1
set njob = 1
endif

set ijob = 1

while ( $ijob <= $njob )

echo ' job' $ijob ' is running'

  foreach i ($KILL_FILES)
    if ( -r $i ) then
      mv -f $i $i$ijob
      continue
    endif
  end

cp guess_$name.h$ijob Rdataguess.f
echo "      name='"$name"'"  >> Rdataguess.f
echo "      job=""$1"  >> Rdataguess.f
echo "      ich=""$ijob"   >> Rdataguess.f

make -f make_fitRdata

echo ' run  fitRdata'
time ./fitRdata

cat array.$ijob >> fitpar.dat

cat fort.7 >> fitpar.dat 

$HOME/andromeda/gra/fjplo3 fitRdat0


echo -n "Update guess y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fort.9 > param_$name.h$ijob
endif

if ( $ijob == $njob ) then 
echo -n "Update Rdat_fit.f y/[n] ?"
set ans1=$<
if ("$ans1" == "y") then
cat fitpar.dat > $name.h
cp $name.h ../
cd ..
make
cd ./fit/
endif
endif

 @ ijob++

end

echo "Results in fitpar.dat"

if ( -r fort.201 ) then
cp fort.201 SOROU.DAT 
echo '    0.000    0.000    0.000    0.000 ' >> SOROU.DAT
rm -f HA*

echo ' run weight2'
time ./weight2

mv HADRO.DAT U2N.dat
endif

if ( -r fort.202 ) then
cp fort.202 SOROU.DAT 
echo '    0.000    0.000    0.000    0.000 ' >> SOROU.DAT
rm -f HA*

echo ' run weight2'
time ./weight2

mv HADRO.DAT XBE.dat
endif

if ( -r fort.203 ) then
cp fort.203 SOROU.DAT 
echo '    0.000    0.000    0.000    0.000 ' >> SOROU.DAT
rm -f HA*

echo ' run weight2'
time ./weight2

mv HADRO.DAT X31N.dat
endif

if ( -r fort.204 ) then
cp fort.204 SOROU.DAT 
echo '    0.000    0.000    0.000    0.000 ' >> SOROU.DAT
rm -f HA*

echo ' run weight2'
time ./weight2

mv HADRO.DAT U7A.dat
endif

exit 0

usage:
echo "Usage fit function name labels 1,...,8"
echo "1=epemfit0814new"
echo "2=ffppg2fit0810"
echo "3=ffppmfit0810"
echo "4=epemfit079081"
echo "5=epemfit1432"
echo "6=epemfit32cut"
echo "7=rfitrholow"
echo "8=rfitrhohig"
echo "9=rfitchptail"
exit 2

