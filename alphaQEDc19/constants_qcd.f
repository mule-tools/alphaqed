      subroutine constants_qcd_qedc19()
      implicit none
      integer i,j
      double precision CHPTCUT,EC,ECUT
      double precision pi,ALINP,ERRAL,EINP,MTOP
      double precision mq(6),mp(6),th(6),dmp(6,3)
      common/var1_qedc19/pi,ALINP,ERRAL,EINP,MTOP
      common /quama_qedc19/mq,mp,th
      common /quame_qedc19/dmp
c commons needed to get R(s)
      COMMON/RCUTS_qedc19/CHPTCUT,EC,ECUT
      data pi /3.141592653589793d0/
c pQCD parameters
c quark masses MSbar mq, pole mp and thresholds th me errors dmp PDG 2012 August online
c u,d,s MSbar at 2 GeV, c,b MSbar masses at the MSbar mass m_c(m_c),m_b(m_b),
c top directly measured mass identified with pole masss, 
c MSbar mass from cross section measurements m_t(m_t)=160+5-4 GeV
c      DATA MQ /0.3023D0,0.3048D0,0.395D0,1.675D0,4.880D0,160.0D0/
      DATA MQ /0.0023D0,0.0048D0,0.095D0,1.275D0,4.180D0,160.0D0/
      DATA MP /0.005D0,0.009D0,0.190D0,1.987D0,4.939D0,173.5D0/
c flavor matching "thresholds" N_f -> N_f+1 
      DATA TH /1.8D0,1.8D0,1.8D0,4.2D0,10.9D0,355.8D0/
c 1: cenral value m, 2: - dm 3: + dm  PDG 2010 TOP/MW EWWG 2012 
      DATA DMP/0.000D0,0.000D0,0.000D0,0.00D0,0.00D0,000.D0,
     &         -.0005D0,-.0003D0,-.005D0,-.025D0,-.030D0,-1.00D0,
     &         0.0007D0,0.0007D0,0.005D0,0.025D0,0.030D0,01.00D0/
      save
c these value owerwrite corresponding values set in parameters.f 
c of the Harlander-Steinhauser package rhad_qedc19
      ALINP=0.1183522d0 ! +/- 0.0007      S. Bethke Aug 2009, PDG 2010
      ALINP=0.1184d0 ! +/- 0.0007  
      ERRAL=0.0007d0
      EINP=91.1876d0
      MTOP=173.5d0   ! +/- 0.6 +/- 0.9 PDG 2012 online 
c Standard cuts for pQCD in time-like regime 
c i.e. the ones used to calcuate delta alpha hadronic
      CHPTCUT=0.318d0           ! below that use CHPT
      EC=     5.2d0             ! from here to 9.5 GeV use pQCD in place of data
      ECUT=   11.5d0             ! above that pQCD tail
      return
      end
