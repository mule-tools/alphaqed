      character*3 LEPTONflag
      integer iLEP,iwarnings,LFLAG_qedc19,IRESON,iresonances
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      real *8 alp,eralp,alp1,gmu,ergmu,a0,era0,adp,adp2,adp3
      real *8 facbw,small,large,large_3
      real *8 MZ,MW,xMW,ERMZ,ERMW,ml(3),erml(3)
      common /consts_qedc19/pi,pi2,ln2,zeta2,zeta3,zeta5
      common /params_qedc19/ALP,adp,adp2,adp3
      common /switch_qedc19/small,large,large_3
      common /lepton_qedc19/iLEP,iwarnings,LEPTONflag
      common /RESFIT_qedc19/facbw,IRESON
      common /RES_qedc19/iresonances
      common /alpgmu_qedc19/alp1,gmu,a0,eralp,ergmu,era0
      common /physparams_qedc19/MZ,MW,xMW,ERMZ,ERMW		       
      common /leptomass_qedc19/ml,erml
