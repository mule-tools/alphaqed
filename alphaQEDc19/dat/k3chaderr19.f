  
c******************************************************************************
c*                                                                            *
c*    --- intRdatx ---                              Fri Jan  3 00:06:29 2020  *
c*                                                                            *
c***************************# FJ@HU Berlin #***********************************
  
c  "amuh(K3c) 2nd integral"    Update version (30/12/2019) 
  
c ISCAN:  0, Ismooth: 0, IGS: 0, DSET: WAVE, iplot: 0 itestflavorspl: 0
c IKE,Emin,Emax,NP,N,EPSREL: 12, 0.0000E+00, 0.0000E+00,   1, 10, 1.00E-02, 
c ALS(sta)(sys):  0.1184( 0.0007)( 0.0000), MZINP:   91.1876, MTOP:    173.20
c NF,IOR,ICHK: 5,4,2,ISU2: 0,NFL: 5,INAG: 1,inotail: 0, sin2W_ref: 1.00000
c CHPTCUT,EC,ECUT: 0.3180,   5.20,  11.50  sin2W_eff: 0.23153 sin2W_ref/sin2W_eff: 4.31909
  
 Result :     0.05     0.00     0.00     0.00
  in 10^{-12} 
