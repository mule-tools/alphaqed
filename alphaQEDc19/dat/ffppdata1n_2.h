       CHARACTER*25 includeffppdataset
       INTEGER NXN,NXA,NXP,NXO,NEJ95,N5M,N5Mx,IOMPHI,NX5A,NX5P,NX5M
       PARAMETER(NX5A=395,NX5P=395,NX5M=268)
       REAL*8 X5_EJ95(64),Y5_EJ95(64,3),
     1     X5M(NX5M),Y5M(NX5M,3),Y5K(NX5M,3)
       REAL*8 X5N(95),X5A(NX5A),X5P(NX5P),X5O(70),
     &     Y5N(95,3),Y5A(NX5A,3),Y5B(NX5A,3),Y5C(NX5A,3),Y5D(NX5A,3),
     &     Y5E(NX5A,3),Y5F(NX5A,3),Y5G(NX5A,3),Y5H(NX5A,3),Y5I(NX5A,3),
     &     Y5P(NX5P,3),Y5O(70,3),
     &     Y5Adat(NX5A,3),Y5Bdat(NX5A,3),Y5Mdat(NX5M,3)
       REAL*8 XCMD2SND(105),YCMD2SND(105,3),Xscan(219),Yscan(219,3)
       COMMON/FFPPAV/X5N,Y5N,X5A,Y5A,Y5B,Y5C,Y5D,Y5E,Y5F,Y5G,Y5H,Y5I,
     &              X5P,Y5P,X5O,Y5O,Y5Adat,Y5Bdat
C 5A=WA e+e- scan data (excl KLOE/BABA/BES3/CLEO) incl n>2 modes
C 5B=WA all data 2015 incl n>2
C 5C=WA all data 2015 incl n>2 excl BABA/BES3/CLEO  (scan+KLOE only)
C 5D=WA all data 2015 incl n>2 excl KLOE/BES3/CLEO  (scan+BABA only)
C 5E=WA all data 2015 incl n>2 excl KLOE/BABA/CLEO  (scan+BES3 only)
C 5F=WA all data 2015 incl n>2 excl KLOE  (scan+BABA+BES3+CLEO)
C 5G=WA all data 2015 incl n>2 excl BABA  (scan+KLOE+BES3+CLEO)
C 5H=WA all data 2015 incl n>2 excl BES3  (scan+KLOE+BABA+CLEO)
C 5I=WA all data 2015 incl n>2 excl CLEO  (scan+KLOE+BABA+BES3)
C 5P=WA pipi only incl KLOE/BABA/BES3/CLEO
C 5O=CMD-2 2003 data plus tails (excl SND,KLOE,CMD3,BaBar,KLOE2,KLOE3 and BES3)
C 5N=cmd2only=1 option in dataset0821
       COMMON/FPSCAN/XCMD2SND,YCMD2SND,Xscan,Yscan
       COMMON/FPEJ95/X5_EJ95,Y5_EJ95
C Eidelman-Jegerlehner compilation 1995
       COMMON/FPMULT/X5M,Y5M,Y5K,Y5Mdat
C Y5M non pipi n>2;
C Y5K e^+e^- scan data only n=2 only=pipi (form combining KLOE/BaBar/Bes3 a posteriori)
       COMMON/NXY/NXN,NXA,NXP,NXO,N5M
       COMMON/NEJ/NEJ95
       COMMON /OMPHILOC/IOMPHI
       COMMON /INCffppDATASET/includeffppdataset
