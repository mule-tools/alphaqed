c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; dggvap12.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:20:10 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Mon Apr 23 14:52:06 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c **********************************************************************
c *    F. Jegerlehner, University of Silesia, PL-40007 Katowice, Poland *
c **********************************************************************
       FUNCTION dggvap_qedc12(s,error)
C      -----------------------------
c Updated March 2012: hadr5n09 --> hadr5n12
c                     - includes BaBar and KLOE pipi ISR spectra
c                     - improved resolution of some resonance regions
c                     - 2010 Review of Particle Proprties resonace parameters
c                     - new version alpha_2 [<gamma 3> vacuum polarization] based on (ud) and (s) flavor separation        
c                     - <3 3> also available based of recombined flavor  separation
c calculate real part of 1PI photon vacuum polarization
c  --- combines real hadronic part (dhadr5n12) with leptonic part (leptons) ---
C Fermionic contributions to the running of alpha
C Function collecting 1-loop, 2-loop, 3-loop leptonic contributions (e, mu, tau) 
C + hadronic contribution from light hadrons (udscb)
C + top quark contribution (t); here at 1-loop (good up to 200 GeV LEP I/II) 
C error is errder: central value dggvap_qedc12 +/- errder -> upper/lower bound
       implicit none
       real*8 dggvap_qedc12,degvap_qedc12,s
       real*8 c0,st2,als
       real*8 mtop,mtop2
       real*8 dallepQED1_qedc12,dallepQED2_qedc12,dallepQED3l_qedc12,
     &        dalQED1ferm_qedc12,res_re,res_im
       real*8 r1real,r1imag,r2real,r2imag,imag
       real*8 r1reall,r1imagl,r2reall,r2imagl,r3reall,r3imagl
       real*8 fac,ee,e,der,errder,deg,errdeg
       real*8 dalept,daltop,dahadr,dghadr,hfun,MW2,
     &      fourMW2,Dalphaweak1MSb,error,null
       include 'common.h'      
       common /parm_qedc12/st2,als,mtop
c keep partial results here; as calculated in this routine        
       common /resu_qedc12/dalept,dahadr,daltop,Dalphaweak1MSb
       common /rdeg_qedc12/dghadr,errdeg,degvap_qedc12
       null=0.d0
       e=dsqrt(dabs(s))
       if (s.lt.null) e=-e
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'had')) then
          call dhadr5n12_qedc12(e,st2,der,errder,deg,errdeg)
          dahadr=der
          dghadr=deg
       else 
          dahadr=null
          dghadr=null
       endif
       if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
          dalept=null
       else 
          r1real =dallepQED1_qedc12(s,r1imag)
          r2real =dallepQED2_qedc12(s,r2imag)
          r3reall=dallepQED3l_qedc12(s,r1reall,
     &                          r1imagl,r2reall,r2imagl,r3imagl)
          res_re=r1real+r2real+r3reall
          res_im=r1imag+r2imag+r3imagl
          dalept=res_re
       endif
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'top')) then
          mtop2=mtop*mtop
c          write (*,*) ' dggvap_qedc12: mtop',mtop
          daltop=dalQED1ferm_qedc12(s,mtop2,imag)*4.d0/3.d0
       else 
          daltop=null
       endif
c MSbar alpha for one-loop weak contribution [WWgamma]
c       Dalphaweak1MSb=-adp/4.d0*(7.d0/2.d0*log(aq2/MW2)+1.d0/3.d0)
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'wea')) then
c          MW=80.399d0 now set in constants.f [via common.h]
          MW2=MW*MW
          fourMW2=MW2
          if (xMW.ne.null) fourMW2=xMW**2
           if (abs(s).gt.fourMW2) then
             Dalphaweak1MSb=-adp*(7.d0/4.d0*log(abs(s)/MW2))
          else 
             Dalphaweak1MSb=0.0d0
          endif
       endif
       error=errder
       dggvap_qedc12=dalept+dahadr
       RETURN
       END

       FUNCTION degvap_qedc12(s,error)
C      -----------------------------
c Delta g SM SU(2) coupling: interface for dggvap_qedc12 
       implicit none
       real*8 dggvap_qedc12,degvap_qedc12,degvap1,s,err,xxx
       real*8 st2,als,mtop,errdeg,dghadr1
       real*8 dalept,dahadr,daltop,Dalphaweak1MSb
       real*8 dglept,dghadr,dgetop,Dalpha2weak1MSb,error
       common /parm_qedc12/st2,als,mtop
       common /resu_qedc12/dalept,dahadr,daltop,Dalphaweak1MSb     ! input
       common /resg_qedc12/dglept,dghadr1,dgetop,Dalpha2weak1MSb   ! output
       common /rdeg_qedc12/dghadr,errdeg,degvap1
       xxx=dggvap_qedc12(s,err)  ! fills common resu and part of rdeg
       dglept=dalept/4.d0/st2
       dghadr1=dghadr
       dgetop=daltop/4.d0/st2*3.d0/2.d0
       Dalpha2weak1MSb=Dalphaweak1MSb/st2*43.d0/42.d0
       error=errdeg
       degvap1=dglept+dghadr
       degvap_qedc12=degvap1
       RETURN
       END
