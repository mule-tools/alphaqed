      subroutine constants_qcd_qedc12()
      implicit none
      integer i,j
      double precision CHPTCUT,EC,ECUT
      double precision pi,ALINP,ERRAL,EINP,MTOP
      double precision mq(6),mp(6),th(6),dmp(6,3)
      common/var1_qedc12/pi,ALINP,ERRAL,EINP,MTOP
      common /quama_qedc12/mq,mp,th
      common /quame_qedc12/dmp
c commons needed to get R(s)
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
      data pi /3.141592653589793d0/
c pQCD parameters
c quark masses MSbar mq, pole mp and thresholds th me errors dmp PDG 2012 March online
      DATA MQ /0.0024D0,0.0049D0,0.100D0,1.290D0,4.190D0,163.265D0/
      DATA MP /0.005D0,0.009D0,0.190D0,1.987D0,4.939D0,172.9D0/
c flavor matching "thresholds" N_f -> N_f+1 
      DATA TH /1.8D0,1.8D0,1.8D0,4.2D0,10.9D0,355.8D0/
c 1: cenral value m, 2: - dm 3: + dm  PDG 2010
      DATA DMP/0.000D0,0.000D0,0.000D0,0.00D0,0.00D0,000.D0,
     &         0.000D0,0.000D0,-.020D0,-.110D0,-.060D0,-01.08D0,
     &         0.000D0,0.000D0,0.030D0,0.050D0,0.180D0,001.08D0/
      save
c these value owerwrite corresponding values set in parameters.f 
c of the Harlander-Steinhauser package rhad_qedc12
      ALINP=0.1184d0 ! +/- 0.0007      S. Bethke Aug 2009, PDG 2010
      ERRAL=0.0007d0
      EINP=91.1876d0
      MTOP=172.9d0   ! +/- 0.6 +/- 0.9 PDG 2012 online 
c Standard cuts i.e. the ones used to calcuate delta alpha hadronic
      CHPTCUT=0.318d0           ! below that use CHPT
      EC=     5.2d0             ! from here to 9.5 GeV use pQCD in place of data
      ECUT=   11.5d0             ! above that pQCD tail
      return
      end
