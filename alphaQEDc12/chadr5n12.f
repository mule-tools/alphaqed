c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; chadr5n12.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 17:53:47 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Sat Apr  7 22:26:36 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c **********************************************************************
c *    F. Jegerlehner, University of Silesia, PL-40007 Katowice, Poland *
c **********************************************************************
       subroutine chadr5n12_qedc12(de,dst2,cder,cerrder,cdeg,cerrdeg)
c Update March 2012
c Provides imaginary part of 5 flavor hadronic contribution to photon vacuum polarization
c and converts dhadr5n12 to double complex variables in chadr5n12
c
c R(s) data/fits in Rdat_all.f [data] via interface Rdat_fun.f / Rdat_fit.f [fits]
c
c Pi' (q^2) photon vacuum polarization function ~ Pi(q^2)/q^2
c Convention Re alpha = - [Re Pi (q^2)-Pi (0)];
c            Im alpha =- Im Pi' (q^2) = -1/3 Re alpha * R(q^2)
c Options for R(q^2): iresonances=0,1 switch off,on narrow resonances
c omega, phi, psi(i) i=1,2,3, Upsilon(i) i=1,2,3,4,5,6
c Ismooth=0,1 data,smoothed data [mostly Chebyshev fits], IGS=0,1
c use [1] or not [0] Gounaris-Sakurai fit for rho resonance
       implicit none
       integer i,IRESON,iresonances,iresonances1,Ismooth,
     &         IGS,IGS1,iso,IER,IOR,NF,ICHK
       real *8 de,dst2,dder,ddeg,derrder,derrdeg
       real *8 s,RSDATn_qedc12,sthres,null,fac,rder,rdeg,rerrder,rerrdeg
       data null/0./
       real *8 rer,ider,ideg,ierrder,ierrdeg,ierrder1,rrer
       real *8 RR(3),RG(3),RES(3),REBW,fac2
       double precision pi,ALINP,ERRAL,EINP,MTOP
       double precision CHPTCUT,EC,ECUT
c       double precision R3(3),rdgg,errdgg,ierrdgg,idgg
       double complex cder,cdeg,cerrder,cerrdeg
c     &      ,cdgg,cerrdgg
       double precision ALS2,EST,ESY,MZINP,MTOP2
       double precision dalept,dahadr,daltop,Dalphaweak1MSb
       double precision alp,adp,adp2,adp3,facbw,ml(3)
       double precision Ruds,Rudsc,Rudscb,Rc,Rb
       double precision cuds,cc,cb,crho,comega,cphi,cpsi,cups
       double precision MPLUS,MZERO,RS3GFUN_qedc12
       external dggvap_qedc12,RSDATn_qedc12,RS3GFUN_qedc12
       common /params_qedc12/ALP,adp,adp2,adp3,ml
       common /resu_qedc12/dalept,dahadr,daltop,Dalphaweak1MSb
c      FUNCTION RSpQCD_qedc12(s,IER) in Rdat_fun.f requires QCD parameters incl errors
c provided via the following common block:
       COMMON/QCDPA_qedc12/ALS2,EST,ESY,MZINP,MTOP2,NF,IOR,ICHK ! global INPUT here
c to set QCD parameters alpha_s with stat and sys error
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z, top mass,
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s)
c to be calculated, ICHK is dummy here
c (in some programs used for alternative routines calculating R(s))
       common/var1_qedc12/pi,ALINP,ERRAL,EINP,MTOP
c commons needed to get R(s)
       COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
c resonances flag iresonances=1 include resonances as data here;
c                            =0 include them elsewhere
       COMMON/RESFIT_qedc12/facbw,IRESON
       COMMON/RES_qedc12/iresonances
       COMMON/RBW_qedc12/REBW
       COMMON/ERR_qedc12/IER
       common/srdeg_qedc12/fac,sthres
c switch for using Gounaris-Sakurai parametrization: IGS=1, iso=2
c provides e^+e^- data fit
c iso=1 isovector part, iso=0 isoscalar part
       COMMON /GUSA_qedc12/IGS,iso
c flag for imaginary part Ismooth=0 R(s) data,=1 R(s) fits
       COMMON/SMOO_qedc12/Ismooth ! to be set in main program

       call constants_qcd_qedc12()  ! fills commons var and RCUTS

       IOR=4           ! don't change for consistency with real part calculation
       IRESON=1        ! don't change provides full R including narrow resonances in fits
       iresonances=1   ! don't change provides full R including narrow resonances in data
       iso=2           ! don't change
       IGS=0           ! Chebyshev fits to be preferred
c fill common QCDPA
       ALS2=ALINP
       EST=ERRAL
       ESY=0.D0
       MZINP=EINP
       MTOP2=MTOP
       IOR=4                    ! default
       NF =5                    ! default
       MPLUS=0.13957018D0       ! PDG 2004 now set in resonances_dat.f via common/pions/
       sthres=(2.d0*MPLUS)**2   ! threshold for imaginary part [very small between m_\pi^0 and 2 m_\pi]
       fac=-alp/3.d0
       fac2=fac/dst2
       do i=1,3
          RR(i) =null
          RG(i) =null
          RES(i)=null
       enddo 
       ider=null
       ideg=null
       ierrder=null
       ierrdeg=null
       s=de*abs(de)
c provide double precision version for real part
       call dhadr5n12_qedc12(de,dst2,dder,derrder,ddeg,derrdeg)
       if (s.gt.sthres) then
c get R(s)
          do i=1,3
             IER=i
c R(s) incl resonances (if iresonances=1)
             RR(i)=RSDATn_qedc12(s,IER,Ismooth)
             RG(i)=RS3GFUN_qedc12(s,IER,Ismooth)
C separate narrow resonance contribution needed in flavor splitting for SU(2) coupling  
             RES(i)=rebw
          enddo
          rerrder=sqrt(RR(2)**2+RR(3)**2)
          rerrdeg=sqrt(RG(2)**2+RG(3)**2)
          rder=RR(1)
          rdeg=RG(1)
       else 
          rerrder=null
          rerrdeg=null
          rder=null
          rdeg=null
       endif
       ider   =fac*rder
       ideg   =fac2*rdeg
       ierrder=abs(fac*rerrder)
       ierrdeg=abs(fac2*rerrdeg)
       cder=DCMPLX(dder,ider)
       cdeg=DCMPLX(ddeg,ideg)
       cerrder=DCMPLX(derrder,ierrder)
       cerrdeg=DCMPLX(derrdeg,ierrdeg)
       return
       end
C
      FUNCTION RSDATn_qedc12(s,IER,Ismooth)
C      --------------------------------
c interface for switching between data and fits
c returns NULL in chosen pQCD ranges [RCUTS]
      IMPLICIT NONE
      INTEGER IER,IER1,Ismooth
      double precision E,S,RSDATn_qedc12,RS40_qedc12,RS40smoothed_qedc12
      EXTERNAL RS40_qedc12,RS40smoothed_qedc12
      COMMON/ERR_qedc12/IER1
      IER1=IER
      E=SQRT(S)
      if (Ismooth.eq.1) then
         RSDATn_qedc12=RS40smoothed_qedc12(s,IER)
      else
         RSDATn_qedc12=RS40_qedc12(s,IER)
      endif
      RETURN
      END
C
       FUNCTION RS3GFUN_qedc12(s,IER,Ismooth)
C      -----------------
c interface for imaginary part of 3 gamma correlator (deg)
       IMPLICIT NONE
       INTEGER IER,Ismooth
       double precision E,S,RS3GFUN_qedc12,RS3G_qedc12,RS3Gsmoothed_qedc12
       EXTERNAL RS3G_qedc12,RS3Gsmoothed_qedc12
       E=SQRT(S)
       if (Ismooth.eq.1) then
          RS3GFUN_qedc12=RS3Gsmoothed_qedc12(s,IER)
       else
          RS3GFUN_qedc12=RS3G_qedc12(s,IER)
       endif
       RETURN
       END
C
       FUNCTION RS33FUN_qedc12(s,IER,Ismooth)
C      -----------------
c interface for imaginary part of 3 3 correlator (dgg)
       IMPLICIT NONE
       INTEGER IER,Ismooth
       double precision E,S,RS33FUN_qedc12,RS33_qedc12,RS33smoothed_qedc12
       EXTERNAL RS33_qedc12,RS33smoothed_qedc12
       E=SQRT(S)
       if (Ismooth.eq.1) then
          RS33FUN_qedc12=RS33smoothed_qedc12(s,IER)
       else
          RS33FUN_qedc12=RS33_qedc12(s,IER)
       endif
       RETURN
       END

C tools for calculating R(s) in pQCD, needed for imaginary part of complex alpha_em
c
C Download the Harlander-Steinhauser program rhad_qedc12 from http://www.rhad_qedc12.de/
c expanding the tar file creates directory ./rhad_qedc12-1.01/ with all files
c put the following subroutine into that directory and compile it like the example
c..   by Robert V. Harlander and Matthias Steinhauser,
c..   Comp. Phys. Comm. 153 (2003) 244,
c..   CERN-TH/2002-379, DESY 02-223, hep-ph/0212294.
c..   Changes: [March 30, 2009] v1.01
c..            - exact results at alphas^4 included
c
c choose option lverbose = .false. in parameters.f
c you may also want to switch off warnings on non-perturbative regions
c just before format statement 2003 in rhad_qedc12.f, I use iwarn flag to switch it off
c         if (iwarn.eq.1) then
c            write(6,2003) dsqrt(scms)
c         endif
c also change in parameters.f iunit = 6 to something not used by this package
c copy rqcdHSn.f from current directory to ./rhad_qedc12-1.01/ compile and link it
