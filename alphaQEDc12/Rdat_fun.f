c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Rdat_all.f --- 
c;; Author          : Fred Jegerlehner
c;; Created On      : Mon Apr 21 00:24:57 2008
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Apr  5 16:11:39 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2010 Fred Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c provides R(s) by data, CHPT or pQCD depending on region and given cuts
c error flag: IER=1 R vaue; IER=2 statistical error, IER=3 systematic error specified as a fraction
c narrow resonances: parametrized by Breit-Wigner: omega, phi, psi(1)-psi(3) and Upsilon(1)-(6)
c rho and psi(4)-psi(6) included in e^+e^- data set (background)
c flag iresonances=1 include narrow resonances; =0 background only  
c Theory errors are taken as syst errors
c i.e. chpt and pQCD results are assigned zero statistical error
c Note background data sets without narrow resonances are CLOSED regions (endpoints included); 
c resonances on to top of subtracted background are OPEN intervals. Avoid double counting at 
c boundary points!
      FUNCTION RS40_qedc12(s,IER)
c requires common 
c      COMMON/QCDPA/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
c to set QCD parameters in main program : alpha_s with stat and sys error 
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z top mass
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s) 
c to be calculated, ICHK is dummy here (in some programs used for alternative routines calculating R(s)); ckeck for other commons needed to be set in main program 
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01         
c               test parameters via common /future/ifuture,efuture,futureprecision
c      
      IMPLICIT NONE
      INTEGER I,J,JJ,IER,IER_SAV,iresonances,IRESON,iii,count,ipQCD,
     &     ifuture,ini
      INTEGER NCHP,NLOW,NU0B,NU1N,NU2N,NBE,NR31N,NU7A
c FJ 06/10/2009 BES region update NU1N=111 -> NU1N=130
c FJ 10/10/2010 BaBar/KLOE incl.  NU0B=128 -> 330; NLOW=24 -> 97 adjust in Rdat_fun.f 
c FJ 28/01/2012 recalculated NU0B=331
      PARAMETER(NCHP=50,NLOW=97,NU0B=331,NU1N=129,NU2N=9,NBE=152,
     &     NR31N=75,NU7A=46)
      REAL*8 XCPT(NCHP),YCPT(NCHP,3),XLOW(NLOW),YLOW(NLOW,3),
     &     U0B(NU0B),V0B(NU0B,3),
     &     U1N(NU1N),V11N(NU1N),V21N(NU1N),V31N(NU1N),
     &     U2N(NU2N),V2N(NU2N,3),XBE(NBE),YBE(NBE,3),
     &     X31N(NR31N),Y31NA(NR31N),Y31NB(NR31N),Y31NC(NR31N),
     &     U7A(NU7A),V7A(NU7A,3)
      REAL*8 E,DE,S,EOMM,EOMP,EFIM,EFIP,RS40_qedc12,RCHPT_qedc12,RCHPTnew_qedc12,AVE,VAR,
     &     RES(3),ROM(3),RFI(3),RPS(3),RYP(3),REBW,reno,
     &     efuture,futureprecision
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6) 
      REAL *8 EXCPTm,EXLOWm,EU0Bm,EU1Nm,EU2Nm,EXBEm,EX31Nm,EU7Am,
     &     EXQCD1m,EXQCD2m,EXQCD3m,EXCPTp,EXLOWp,EU0Bp,EU1Np,EU2Np,
     &     EXBEp,EX31Np,EU7Ap,EXQCD3p
      REAL *8 CHPTCUT,EC,ECUT,RSpQCD_qedc12,EINFTY
      real *8 UGM2,sMeV,rbw,BW_qedc12,fac,null,alp
      external RSpQCD_qedc12,BW_qedc12
      REAL*8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI
      REAL*8 PSI(6),MPS(6),GPS(6),PPS(6),YPI(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 FSTA(5),FSYS(5)
      COMMON/EPEMALL_qedc12/XCPT,YCPT,XLOW,YLOW,
     &     U0B,V0B,
     &     U1N,V11N,V21N,V31N,
     &     U2N,V2N,XBE,YBE,
     &     X31N,Y31NA,Y31NB,Y31NC,
     &     U7A,V7A
      COMMON/BWRO_qedc12/MRO,GRO,PRO/BWOM_qedc12/MOM,GOM,POM/BWFI_qedc12/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/PSYPPAR_qedc12/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESRELERR_qedc12/FSTA,FSYS
c energy cuts: CHPT at low end, pQCD above ECUT and between EC and Upsilon threshold 9.5 GeV; may be set in constants_qcd.f and called as "call constants_qcd()"
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
c resonances flag iresonances=1 include resonances as data here; 0 include them 
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc12/iresonances
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/RBW_qedc12/REBW
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
c       MPLUS=139.57018D0   ! PDG 2004
c       EOMM=3.D0*MPLUS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      common /future_qedc12/efuture,futureprecision,ifuture
      data count /0/ ini /0/
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM_qedc12,BWFI_qedc12,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series 
c
         call resonances_data_qedc12()
c
c called from resonances_dat.f
************************************************************************
         ini=1
      endif
c test peak imaginary parts
c      if (count.eq.0) then
cc squares of imaginary parts at resonance peaks
c         alp=1.d0/137.036d0
c         write (22,*) ' Rdat_fun: OM,FI',(3.d0/alp*POM)**2,
c     &        (3.d0/alp*PFI)**2
c         do iii=1,3
c            write (22,*) ' Rdat_fun: J/psi',(3.d0/alp*PPS(iii))**2
c         enddo 
c         do iii=1,3
c            write (22,*) ' Rdat_fun: Ups',(3.d0/alp*PYP(iii))**2
c         enddo 
c         count=count+1
c      endif
      ipQCD=0
      null=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo 
c     
C     
C  0.285       0.5 
C   |-----------|--
C       XCPT
C
C     0.318     0.61        1.4 
C      |---------|-----------|--
C
C          XLOW        U0B      
C
C  1.4         3.1         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        
c Array boundaries
C
C  0.285       0.5 
C   |-----------|--
C       XCPT
C
C     0.318     0.61        1.4 
C      |---------|-----------|--
C
C          XLOW        U0B      
C
C  1.4         3.1         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        
C
C     1.8                  3.697 4.003  5.1                 13.5
C      |--------------------|-----|------|--------------------|--
c

c      call Rdata_qedc12()

C s is function RS40_qedc12(s) argument [energy square in GeV^2]
c
C other input parameters via commons:
c       COMMON/RCUTS/CHPTCUT,EC,ECUT
c error fag ier=1,2,3 for result,staterr, syserr
c       COMMON/ERR/IER
c resonances flag iresonances=1 include resonances as data here; 0 include them elsewhere 
c       COMMON/RES/iresonances
c 
        E=sqrt(S)
c        write (*,*) ' RS40_qedc12: begin',E
        if (IER.GT.3) then
           write (*,*) ' no permitted error flag',IER
           return
        endif
        j=IER
C cut boundaries CHPTCUT,EC,ECUT
c 2m_pi - CHPTCUT (standard=0.318)
        EXCPTm=  XCPT(1) 
        EXCPTp=  CHPTCUT ! XCPT(NCHP)
c CHPTCUT - 0.61 
        EXLOWm=  CHPTCUT ! XLOW(1) 
        EXLOWp=  XLOW(NLOW) 
c omega  .41871054d0,.810d0
c 0.61 - 1.4        
        EU0Bm=   U0B(1)  
        EU0Bp=   U0B(NU0B)
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
c 1.4 - 3.2      
        EU1Nm=   U1N(1)  
        EU1Np=   U1N(NU1N)
c psi1  3.08587D0 3.10787D0
c 3.2 - 3.6
        EU2Nm=   U2N(1)  
        EU2Np=   U2N(NU2N)
c 3.6 - 5.2
        EXBEm= XBE(1)
        EXBEp= XBE(NBE)
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0 
c psi4  3.960D0   4.098D0  
c psi5  4.102D0   4.275D0  
c psi6  4.315D0   4.515D0  
c 5.2 - 9.5        
        EX31Nm=  X31N(1) 
        EX31Np=  X31N(NR31N)
c yps1  9.44937D0 9.47137D0 
c 9.5 - 13 [40]        
        EU7Am=   U7A(1)  
        EU7Ap=   ECUT ! U7A(NU7A)
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0 
c yps4  10.473D0  10.687D0  
c yps5  10.690D0  10.950D0  
c yps6  10.975D0  11.063D0  
c QCD1 and QCD 2 not used
c        EXQCD1m= XQCD1(1)
c        EXQCD1p= XQCD1(NQCD1)
c        EXQCD2m= XQCD2(1)
c        EXQCD2p= XQCD2(NQCD3)
c QCD3 used for region EC (>=5.2) to 9.46 GeV
        EXQCD3m= EC ! XQCD3(1)
        EXQCD3p= U7A(1) ! ECUT ! XQCD3(NQCD3)
c above ECUT analytic pQCD RS(s) from HS
C
c       E=DSQRT(S)
C
c       do j=1,3 
c threshold to .318          
        if ((E.GE.EXCPTm).and.(E.LT.EXCPTp)) then
           call getindex_qedc12(E,NCHP,XCPT,I)
c           if (I.GE.50)
c     &          write (*,*) ' Rdat_fun:216',I,E,XCPT(I),XCPT(NCHP)
           res(j)=YCPT(I,j)+(YCPT(I+1,j)-YCPT(I,j))
     &          /(XCPT(I+1)-XCPT(I))*(E-XCPT(I))
           if (j.eq.3) then
              res(j)=res(j)*(YCPT(I,1)+(YCPT(I+1,1)-YCPT(I,1))
     &             /(XCPT(I+1)-XCPT(I))*(E-XCPT(I)))
           endif
        endif          
c .318-.610
        if ((E.GE.EXLOWm).and.(E.LT.EXLOWp)) then
           call getindex_qedc12(E,NLOW,XLOW,I)
           res(j)=YLOW(I,j)+(YLOW(I+1,j)-YLOW(I,j))
     &          /(XLOW(I+1)-XLOW(I))*(E-XLOW(I))
           if (j.eq.3) then
              res(j)=res(j)*(YLOW(I,1)+(YLOW(I+1,1)-YLOW(I,1))
     &             /(XLOW(I+1)-XLOW(I))*(E-XLOW(I)))
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EOMM).and.(E.LT.EOMP)) then
                 rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
                 if (ier.eq.2) then
                    rbw=rbw*fsta(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*fsys(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
c omega  .41871054d0,.810d0
           endif
        endif
c omega  .41871054d0,.810d0
c .610-1.4
        if ((E.GE.EU0Bm).and.(E.LT.EU0Bp)) then
           call getindex_qedc12(E,NU0B,U0B,I)
           res(j)=V0B(I,j)+(V0B(I+1,j)-V0B(I,j))
     &          /(U0B(I+1)-U0B(I))*(E-U0B(I))
           if (j.eq.3) then
              res(j)=res(j)*(V0B(I,1)+(V0B(I+1,1)-V0B(I,1))
     &             /(U0B(I+1)-U0B(I))*(E-U0B(I)))
           endif
c omega  .41871054d0,.810d0
           if (iresonances.eq.1) then
              if ((E.GT.EOMM).and.(E.LT.EOMP)) then
                 rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
                 if (ier.eq.2) then
                    rbw=rbw*fsta(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*fsys(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
c phi   1.00D0,1.04D0
              if ((E.GT.EFIM).and.(E.LT.EFIP)) then
                 rbw=fac*BW_qedc12(sMeV,MFI,GFI,PFI)
                 if (ier.eq.2) then
                    rbw=rbw*fsta(3)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*fsys(3)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c  1.40-3.2
        if ((E.GE.EU1Nm).and.(E.LT.EU1Np)) then
           call getindex_qedc12(E,NU1N,U1N,I)
           if (j.eq.1) then
              res(j)=V11N(I)+(V11N(I+1)-V11N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I))
           else if (j.eq.2) then
              res(j)=V21N(I)+(V21N(I+1)-V21N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I))
           else if (j.eq.3) then
              res(j)=(V31N(I)+(V31N(I+1)-V31N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I)))*
     &               (V11N(I)+(V11N(I+1)-V11N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I)))
           else 
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIPS(1)).and.(E.LT.EMAPS(1))) then
c psi1  3.08587D0 3.10787D0
                 rbw=fac*BW_qedc12(sMeV,MPS(1),GPS(1),PPS(1))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(1)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(1)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c psi1  3.08587D0 3.10787D0
c 3.2 - 3.6          
        if ((E.GE.EU2NM).and.(E.LT.EU2NP)) then
           call getindex_qedc12(E,NU2N,U2N,I)
           res(j)=V2N(I,j)+(V2N(I+1,j)-V2N(I,j))
     &          /(U2N(I+1)-U2N(I))*(E-U2N(I))
           if (j.eq.3) then
              res(j)=res(j)*(V2N(I,1)+(V2N(I+1,1)-V2N(I,1))
     &             /(U2N(I+1)-U2N(I))*(E-U2N(I)))
           endif
        endif
c 3.60 - 5.20 
        if ((E.GE.EXBEM).and.(E.LT.EXBEP)) then
           if ((EC.GE.EXBEP).or.(E.LE.EC)) then         
              call getindex_qedc12(E,NBE,XBE,I)
              res(j)=YBE(I,j)+(YBE(I+1,j)-YBE(I,j))
     &             /(XBE(I+1)-XBE(I))*(E-XBE(I))
              if (j.eq.3) then
                 res(j)=res(j)*(YBE(I,1)+(YBE(I+1,1)-YBE(I,1))
     &                /(XBE(I+1)-XBE(I))*(E-XBE(I)))
              endif
           else 
              res(j)=RSpQCD_qedc12(s,j)
              rebw=null
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIPS(2)).and.(E.LT.EMAPS(2))) then
                 rbw=fac*BW_qedc12(sMeV,MPS(2),GPS(2),PPS(2))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GT.EMIPS(3)).and.(E.LT.EMAPS(3))) then
                 rbw=fac*BW_qedc12(sMeV,MPS(3),GPS(3),PPS(3))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(3)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(3)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c pQCD below Uspilon if EC < 5.2 GeV 
c        EXQCD3m= EC  to EXQCD3p= EX31Np
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0 
c psi4  3.960D0   4.098D0  
c psi5  4.102D0   4.275D0  
c psi6  4.315D0   4.515D0  
c 5.20- 9.5
        if ((E.GE.EX31NM).and.(E.LT.EX31NP)) then
           if ((EC.GE.EX31NP).or.(E.LE.EC)) then         
              call getindex_qedc12(E,NR31N,X31N,I)
              if (j.eq.1) then
                 res(j)=Y31NA(I)+(Y31NA(I+1)-Y31NA(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I))
              else if (j.eq.2) then
                 res(j)=Y31NB(I)+(Y31NB(I+1)-Y31NB(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I))
              else
                 res(j)=(Y31NC(I)+(Y31NC(I+1)-Y31NC(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I)))*
     &                  (Y31NA(I)+(Y31NA(I+1)-Y31NA(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I)))
              endif
           else 
              res(j)=RSpQCD_qedc12(s,j)
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIYP(1)).and.(E.LT.EMAYP(1))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(1),GYP(1),PYP(1))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(1)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(1)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c pQCD below Uspilon 
c        EXQCD3m= EC  to EXQCD3p= EX31Np
c yps1  9.44937D0 9.47137D0 
c 9.5-13.00         
        if ((E.GE.EU7AM).and.(E.LT.EU7AP)) then
           if (E.LE.ECUT) then         
              call getindex_qedc12(E,NU7A,U7A,I)
              res(j)=V7A(I,j)+(V7A(I+1,j)-V7A(I,j))
     &             /(U7A(I+1)-U7A(I))*(E-U7A(I))
              if (j.eq.3) then
                 res(j)=res(j)*(V7A(I,1)+(V7A(I+1,1)-V7A(I,1))
     &                /(U7A(I+1)-U7A(I))*(E-U7A(I)))
              endif
           else 
              res(j)=RSpQCD_qedc12(s,j)
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIYP(2)).and.(E.LT.EMAYP(2))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(2),GYP(2),PYP(2))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(3)).and.(E.LT.EMAYP(3))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(3),GYP(3),PYP(3))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(3)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(3)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(4)).and.(E.LT.EMAYP(4))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(4),GYP(4),PYP(4))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(4)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(4)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(5)).and.(E.LT.EMAYP(5))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(5),GYP(5),PYP(5))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(5)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(5)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(6)).and.(E.LT.EMAYP(6))) then
                 rbw=fac*BW_qedc12(sMeV,MYP(6),GYP(6),PYP(6))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(6)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(6)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c yps1  9.44937D0 9.47137D0 
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0 
c yps4  10.473D0  10.687D0  
c yps5  10.690D0  10.950D0  
c yps6  10.975D0  11.063D0  
c 13.00 - oo 
        EINFTY=1.D9             ! 1000 000 TeV
        if ((E.GE.ECUT).and.(E.LT.EINFTY)) then
           res(j)=RSpQCD_qedc12(s,j)
           rebw=null
           ipQCD=1
        endif
c      enddo
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         j=IER_SAV
         if (IER_SAV.EQ.2) res(2)=null
         if (IER_SAV.EQ.3) res(3)=res(1)*futureprecision
      endif
      rs40_qedc12=res(j)
c     if (ipQCD.eq.0) then
c     CALL RENOoldnew(E,reno)
c     rs40_qedc12=rs40_qedc12*reno
c     endif
      RETURN
      END
c     
      FUNCTION RSpQCD_qedc12(s,IER)
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,RSpQCD_qedc12,xRS_qedc12,ALINP,EINP,MTOP,pi,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc12/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      common/pqcdHS_qedc12/pi,ALINP1,EINP,MTOP1,IOR1,NF1 ! local input for Rhad_qedc12 HS etc
      external xRS_qedc12
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      ALINP1=ALS
      if (IER.eq.1) then
         res(IER)=xRS_qedc12(S)
      else if (IER.eq.2) then
         res(1)=xRS_qedc12(S)          ! function with central value alpha_s to have central values in commons filled via rqcdHS
         res(IER)=0.d0
      else 
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         RSP=xRS_qedc12(S)
         ALINP1=ALS-EST
         RSM=xRS_qedc12(S)            
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
         res(1)=xRS_qedc12(S)          ! recall function with central value alpha_s to get central values in commons filled via rqcdHS
      endif
      RSpQCD_qedc12=res(IER)
      return
      end
c
      function xRS_qedc12(s)
c interface for Harlander--Steinhauser routine for calculating R(s) in pQCD
c provide local parameters via common var: ALINP= alpha_s(EINP), IOR order of pQCD IOR=1,2,3,4
c At given energy E=sqrt(s), contribution from NF=NFext flavors only
c e.g. NFext=3 only uds quarks taken into account, NFext=5 means no top contribution
c even above top threshold, etc
c create and link object rqcdHS.o in rhad_qedc12 package directory
      implicit none
      integer  IOR,NF
      real *8 pi,s,R,xRS_qedc12,ALINP,EINP,MTOP
      common/pqcdHS_qedc12/pi,ALINP,EINP,MTOP,IOR,NF
c      write (*,*) ' xRS_qedc12: params',ALINP,EINP,MTOP,IOR,NF
      call rqcdHS(s,R,ALINP,EINP,MTOP,IOR,NF, 12) 
      xRS_qedc12=R
      return
      end
c
      SUBROUTINE getindex_qedc12(E,N,X,I)
      IMPLICIT NONE
      LOGICAL lwrite
      INTEGER N,I
      REAL*8 E
      REAL*8 X(N)
      common/pri_qedc12/lwrite
      I=N
      IF (E.GT.X(1)) THEN
         DO WHILE (E.LT.X(I))
            I=I-1
         ENDDO
      ELSE 
         I=1
         if ((lwrite).and.(E.LT.X(I))) then
            write (*,*) ' getindex: out of range E below X(1)',E,X(1)
         endif
      ENDIF
      RETURN
      END
C
      FUNCTION RCHPT_qedc12(S,IPM)
C      ---------------------------
C 2PI-Threshold:Integrand
      IMPLICITREAL*8(A-Z)
      INTEGER IPM,IPD
      MPLUS=139.57018D0         ! pm 0.00035
      URF=197.327053D0          ! MeV*fm
      URI=1.D-3                 ! convert MeV into GeV
      MPLUS=MPLUS*URI
      MP2=MPLUS*MPLUS
      IPD=0
C Chiral expansion of pion form factor to two-loop
C J. Gasser, U.-G. Meissner, Nucl. Phys. B357 (1991) 90
C <r^2>^pi_V=(0.439)0.427+/-0.010 fm^2 from space-like data
C S. R. Amendolia et al., Nucl. Phys. B277 (1986) 168 [NA7]
C c^pi_V=4.1 +0.2/-0.6 GeV^{-4}
      IF (IPM.EQ. 1) THEN
         C1=0.437D0/6.D0/URF**2/URI**2
         C2=4.3D0
      ELSE IF (IPM.EQ.-1) THEN
         C1=0.417D0/6.D0/URF**2/URI**2
         C2=3.5D0
      ELSE IF (IPM.EQ. 0) THEN
         C1=0.427D0/6.D0/URF**2/URI**2
         C2=4.1D0
      ENDIF
cC use Colangelo, Finkemeier, Urech PRD 54 (1996) 4403
c     C just use normal expansion [non-Pade]
c       IF (IPM.EQ. 1) THEN
c         C1=0.457D0/6.D0/URF**2
c         C2=4.2D0*URI**4
c       ELSE IF (IPM.EQ.-1) THEN
c         C1=0.405D0/6.D0/URF**2
c         C2=2.2D0*URI**4
c       ELSE IF (IPM.EQ. 0) THEN
c         C1=0.431D0/6.D0/URF**2
c         C2=3.2D0*URI**4
c       ENDIF
      IF (IPD.EQ.0) FVCHPT=1.D0+C1*S+C2*S**2
      IF (IPD.EQ.1) FVCHPT=(1.D0+(C1-C2/C1)*S)/(1.D0-C2/C1*S)
      IF (IPD.EQ.2) FVCHPT=1.D0/(1.D0-C1*S-(C2-C1**2)*S**2)
c       FVCHPT=1.D0+C1*S+C2*S**2
      FVCHPT2=FVCHPT**2
      IF (S.GT.4.D0*MP2) THEN
         RCHPT_qedc12=(DSQRT(1.D0-4.D0*MP2/S))**3*
     .        FVCHPT2/4.d0
      ELSE
         RCHPT_qedc12=0.D0
      ENDIF
      RETURN
      END
c      
      function RCHPTnew_qedc12(S,IPM)
C     ---------------------------
C  s in GeV^2 based on Davier et al ee fit
      IMPLICITREAL*8(A-Z)
      INTEGER IPM,IPD
      MP2 = 139.57018D-3 * 139.57018D-3

      fracterror=0.012358d0
      c1= 6.35046D+00 
      c2= -2.25567D+01
      c3= 1.40482D+02
      x=s*1.d-6
      polynom_qedc12=1.d0+c1*x+c2*x**2+c3*x**3
      FVCHPT2new=polynom_qedc12
      IF (S.GT.4.D0*MP2) THEN
         RCHPTnew_qedc12=(DSQRT(1.D0-4.D0*MP2/S))**3*
     .        FVCHPT2new/4.d0
         IF (IPM.EQ. 1) THEN
            RCHPTnew_qedc12=RCHPTnew_qedc12*(1.d0+fracterror)
         ELSE IF (IPM.EQ.-1) THEN
            RCHPTnew_qedc12=RCHPTnew_qedc12*(1.d0-fracterror)
         ELSE 
         ENDIF
      ELSE
         RCHPTnew_qedc12=0.D0
      ENDIF
      RETURN
      END
C
      function rs40x_qedc12(s)
c interface for RS40_qedc12(s,IER): function which yields R(s)[IER=1], delta R statistical [IER=2] 
c or delta R systematic [IER=3]
      implicit none
      integer IER
      real *8 rs40x_qedc12,s,RS40_qedc12
      external RS40_qedc12              
c error fag ier=1,2,3 for result,staterr, syserr
      COMMON/ERR_qedc12/IER
         rs40x_qedc12=RS40_qedc12(s,IER)
c      write (*,*) ' rs40x_qedc12:',sqrt(s),rs40x_qedc12
      return
      end
C
       FUNCTION BW_qedc12(S,M,G,P)
C      --------------------
C      BREIT WIGNER
       REAL*8 BW_qedc12,S,M,G,P,XP,GP
       real*8 BWRENO_qedc12,reno,fracerr 
       external BWRENO_qedc12
       E=DSQRT(S)
c turns physical (dressed) cross section into bare one (undressed) 
c assuming mass and width are the physical ones as given by PDG
       reno=BWRENO_qedc12(s,fracerr)
CC Subtract electromagnetic decay channels: correction XP
C       MX=E
C       IF (MX.GT.1.D2) MX=MX*1.D-3
C       SR=MX**2
C       if (MX.LT.3.6D0) then
C         RL=2.d0
C       else
C         RL=3.d0
C       endif
CC       XP=1.D0-(RL+RS(SR))*P
C       XP=1.D0-(RL)*P
       XP=1.D0
       GP=G*XP
       BW_qedc12=G*GP*P/((E-M)**2+G**2/4.D0)*reno       
C       write(*,*) MX,XP
C Relativistic form
C
C       M2=M**2
C       BW_qedc12=4.D0*G*GP*P*S/((S-M2)**2+M2*G**2)
       RETURN
       END

      FUNCTION BWRENO_qedc12(s,fracerr)
c undressing BW_qedc12 resonances X table points (resonance regions), Y=reno factor, Z fractional error of Y
      implicit none
      integer nx,ny
      parameter(nx=400,ny=15)
      integer j,I,N(ny)
c      double complex r1cx(nx,ny)
      double precision BWRENO_qedc12,s,e
      double precision renfac,fracerr
      double precision X(nx,ny),Y(nx,ny),Z(nx,ny)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc12/EMI,EMA
c      common /renormBW/X,Y,Z,r1cx,N
      common /renormBW_qedc12/X,Y,Z,N
      E=SQRT(s)
      j=ny
      do while ((E.LT.EMI(j)).and.(j.gt.2))
         j=j-1
      enddo
      if (E.LT.EMA(j)) then
         I=N(j)
         DO WHILE ((I.gt.2).and.(E.LT.X(I,j)))
            I=I-1
         ENDDO
         renfac =Y(I,j)+(Y(I+1,j)-Y(I,j))/(X(I+1,j)-X(I,j))*(E-X(I,j))
         fracerr=Z(I,j)+(Z(I+1,j)-Z(I,j))/(X(I+1,j)-X(I,j))*(E-X(I,j))
      else 
         renfac=1.d0
         fracerr=0.d0
      endif
      BWRENO_qedc12=renfac
      RETURN
      END
