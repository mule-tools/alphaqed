      subroutine resonances_renocomplex_qedc12()
      implicit none
      integer i,j,k,count,Nall,N1,N2
      parameter(Nall=2177)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      integer nx,ny
      parameter(nx=400,ny=15)
      integer nres(ny)
      double complex cone,calp,cggvap_qedc12,cvpt_new,cvpt_hig,cvpt_low
      double complex alphac,alphah,alphal,r1c,r1l,r1h,ccc,cerror
c      double complex r1cx(nx,ny)
      double precision null,emin,emax,e,s
      double precision reno,renl,renh,error
      double precision renpts(nx,ny),renofa(nx,ny),fracerr(nx,ny)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc12/EMI,EMA
c      common /renormBW/renpts,renofa,fracerr,r1cx,nres
      common /renormBW_qedc12/renpts,renofa,fracerr,nres
      external cggvap_qedc12,BW_qedc12
      include 'common.h'
      null=0.d0
      cone=DCMPLX(1.d0,null)
      calp=DCMPLX(alp,null)
      s=2.d0**2

      ccc=cggvap_qedc12(s,cerror)
c

      include 'xRdat-extended.f' ! just DATA X/.../ statement
c      
      do j=2,15
         emin=EMI(j)
         emax=EMA(j)
         call getindex_qedc12(emin,Nall,X,N1)
         call getindex_qedc12(emax,Nall,X,N2)
c         write (*,*) emin,emax,N1,N2
         count=0
         do 10 i=N1,N2
            count=count+1
            e=X(i)
            s=e*e 
            cvpt_new= cggvap_qedc12(s,cerror)
            cvpt_hig= cvpt_new+cerror
            cvpt_low= cvpt_new-cerror
            alphac =calp/(cone-cvpt_new)
            alphah =calp/(cone-cvpt_hig)
            alphal =calp/(cone-cvpt_low)
            r1c=calp/alphac
            r1h=calp/alphah
            r1l=calp/alphal
            reno=r1c*DCONJG(r1c) ! undressing factor
            renh=r1h*DCONJG(r1h) ! + error
            renl=r1l*DCONJG(r1l) ! - error11
            error=abs((renh-renl)/2.0d0)/reno
            renpts(count,j)=e
            renofa(count,j)=reno
            fracerr(count,j)=error
c            r1cx(count,j)=r1c
c            write (*,*) ' resonance_reno:',j,e,reno
 10      continue
         nres(j)=count
      enddo
c      write (*,*) ' resonance_reno:',nres
c     ..................................................................
      return
      end
