      subroutine constants_qedc12()
      implicit none
      real *8 alpi,ealpi
      include 'common.h'
      data pi,pi2,ln2/3.141592653589793d0,9.869604401089358d0,
     &                0.6931471805599453d0/    
      data zeta2,zeta3/1.644934066848226d0,1.20205690315959d0/
      data zeta5/1.036927755143370d0/
c      DATA ALP/0.0072973544D0/
c     26/05/2008 new result from Gabrielse on a_e
      DATA alpi/137.035999074d0/
      DATA ealpi/000.000000051d0/
      data ml /0.510998928d-3,105.6583715d-3,1.77682d0/
      data MZ,MW,xMW/91.1876d0,80.399d0,0.0d0/
      data small,large,large_3/1.d-4,1.d6,8.d0/
      save
      alp   =1.d0/alpi
      adp   = alp/pi
      adp2  = adp*adp
      adp3  = adp*adp2
      facbw=9.d0/4.d0*alpi**2   ! in common RESFIT
C switch off/on warnings iwarnings=0/1 default is 1 
      iwarnings=0
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
      LEPTONflag='all'
      iLEP  = -3  ! for sum of leptons + quarks
C Overwrite default switches for replacing exact by asymptotic expansions 
C (required to enforce numerical stability)
C Do not change without checking whether new switches give acceptable results
C z=|s|/m^2 (threshold= 4 m^2), 
C low energy expansion up to O(z^3): c_1*z+c_2*z^2
C high energy expansion up to O(z^-3): c_1/z+c_2/z^2 up to logs
C
*      small=1d-6   !  default 
*      large=1d6    !  default 
C
C corresponding switch for 3--loop high energy approximation 
C where exact result is not available at lower energies 
C i.e. below large_3*|s| 3--loop contribution is taken to be zero
C
*     large_3=8.d0  ! default  
C
      return
      end
