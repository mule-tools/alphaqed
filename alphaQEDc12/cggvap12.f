c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; cggvap12.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:18:34 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Mon Apr 23 03:19:18 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c **********************************************************************
c *    F. Jegerlehner, Univerity of Silesia, PL-40007 Katowice, Poland *
c **********************************************************************
       FUNCTION cggvap_qedc12(s,cerror)
C      -----------------------------
c Updated March 2012: hadr5n09 --> hadr5n12
c                     - includes BaBar and KLOE pipi ISR spectra; new compilation of Rdat_all.f
c                     - improved resolution of some resonance regions
c                     - 2010 Review of Particle Proprties resonace parameters
c                     - new version alpha_2 [<gamma 3> vacuum polarization] based on (ud) and (s) flavor separation        
c                     - <3 3> also available based of recombined flavor  separation
c calculate complex 1PI photon vacuum polarization
c  --- combines complex hadronic part (chadr5n12) with lettonic part (leptons) ---
C Fermionic contributions to the running of alpha
C Function collecting 1-loop, 2-loop, 3-loop leptonic contributions (e, mu, tau) 
C + hadronic contribution from light hadrons (udscb)
C + top quark contribution (t); here at 1-loop (good up to 200 GeV LEP I/II) 
C error is cerrder: central value cggvap_qedc12 +/- cerrder -> upper/lower bound
       implicit none
       real*8 dggvap_qedc12,s
       real*8 c0,st2,als,null
       real*8 mtop,mtop2
       real*8 dallepQED1_qedc12,dallepQED2_qedc12,dallepQED3l_qedc12,
     &        dalQED1ferm_qedc12,res_re,res_im
       real*8 r1real,r1imag,r2real,r2imag,imag
       real*8 r1reall,r1imagl,r2reall,r2imagl,r3reall,r3imagl
       real*8 fac,ee,e,der,errder,deg,errdeg
       real*8 dalept,daltop,dahadr,hfun,MW2,fourMW2,Dalphaweak1MSb
       double complex cnull,cder,cdeg,cerrder,cerrdeg,cerrdeg1,
     &      cahadr,calept,caltop,cggvap_qedc12,cDalphaweak1MSb
       double complex cerror,cghadr,cegvap_qedc12
       include 'common.h'      
       common /parm_qedc12/st2,als,mtop
c keep partial results here; as calculated in this routine        
       common /resu_qedc12/dalept,dahadr,daltop,Dalphaweak1MSb
       common /cres_qedc12/calept,cahadr,caltop,cDalphaweak1MSb
       common /cdeg_qedc12/cghadr,cerrdeg1,cegvap_qedc12
       null=0.d0
       cnull=DCMPLX(null,null)
       e=dsqrt(dabs(s))
       if (s.lt.0.d0) e=-e
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'had')) then
          call chadr5n12_qedc12(e,st2,cder,cerrder,cdeg,cerrdeg)
          cahadr=cder
          cghadr=cdeg
          cerrdeg1=cerrdeg
       else 
          cahadr=cnull
          cghadr=cnull
          cerrdeg1=cnull
       endif
       if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
          calept=cnull
       else 
          r1real =dallepQED1_qedc12(s,r1imag)
          r2real =dallepQED2_qedc12(s,r2imag)
          r3reall=dallepQED3l_qedc12(s,r1reall,
     &                          r1imagl,r2reall,r2imagl,r3imagl)
          res_re=r1real+r2real+r3reall
          res_im=r1imag+r2imag+r3imagl
          calept=DCMPLX(res_re,res_im)
       endif
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'top')) then
          mtop2=mtop*mtop
          daltop=dalQED1ferm_qedc12(s,mtop2,imag)*4.d0/3.d0
          caltop=DCMPLX(daltop,imag)
       else 
          caltop=cnull
       endif
c MSbar alpha for one-loop weak contribution [WWgamma]
c       Dalphaweak1MSb=-adp/4.d0*(7.d0/2.d0*log(aq2/MW2)+1.d0/3.d0)
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'wea')) then
c          MW=80.398d0 now set in constants.f [via common.h]
          MW2=MW*MW
          fourMW2=MW2
          if (xMW.ne.null) fourMW2=xMW**2
          if (abs(s).gt.fourMW2) then
             Dalphaweak1MSb=-adp*(7.d0/4.d0*log(abs(s)/MW2))
          else 
             Dalphaweak1MSb=0.0d0
          endif
          cDalphaweak1MSb=DCMPLX(Dalphaweak1MSb,0.0d0)
       endif
       cerror=cerrder
       cggvap_qedc12=calept+cahadr
       RETURN
       END


       FUNCTION cegvap_qedc12(s,cerror)
C      -----------------------------
c complex Delta g SM SU(2) coupling: interface for cggvap_qedc12 
       implicit none
       real*8 s,null,fac
       real*8 st2,als,mtop
       double complex cggvap_qedc12,cegvap_qedc12,cegvap1,cfac,ccc,cer,cerrdeg 
       double complex calept,cahadr,cahadr1,caltop,cDalphaweak1MSb
       double complex cglept,cghadr,cghadr1,cgetop,cDalpha2weak1MSb
       double complex cerror
       common /parm_qedc12/st2,als,mtop
       common /cres_qedc12/calept,cahadr,caltop,cDalphaweak1MSb ! input
       common /cesg_qedc12/cglept,cghadr1,cgetop,cDalpha2weak1MSb ! output
       common /cdeg_qedc12/cghadr,cerrdeg,cegvap1
       null=0.d0
       fac=1.d0/4.d0/st2
       cfac=DCMPLX(fac,null)
       ccc=cggvap_qedc12(s,cer)  ! fills common cres and part of cdeg
       cglept=cfac*calept
       cghadr1=cghadr
       cgetop=cfac*caltop*DCMPLX(3.d0/2.d0,null)
       cDalpha2weak1MSb=cfac*cDalphaweak1MSb
     &        *DCMPLX(86.d0/21.d0,null)
       cegvap1=cglept+cghadr
       cerror=cerrdeg
       cegvap_qedc12=cegvap1
       RETURN
       END
