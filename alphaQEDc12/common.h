      character*3 LEPTONflag
      integer iLEP,iwarnings,lflag_qedc12,IRESON,iresonances
      real *8 pi,pi2,ln2,zeta2,zeta3,zeta5
      real *8 alp,adp,adp2,adp3,ml(3)
      real *8 facbw,small,large,large_3
	real *8 MZ,MW,xMW
      common /consts_qedc12/pi,pi2,ln2,zeta2,zeta3,zeta5
      common /params_qedc12/ALP,adp,adp2,adp3,ml
      common /switch_qedc12/small,large,large_3
      common /lepton_qedc12/iLEP,iwarnings,LEPTONflag
      common /RESFIT_qedc12/facbw,IRESON
      common /RES_qedc12/iresonances
      common /physparams_qedc12/MZ,MW,xMW		       
