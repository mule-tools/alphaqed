c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; R3Gdat_fit.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Mon Feb 20 16:00:34 2012
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Mar 29 23:16:45 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of function RS3G_fun.f interpolating R3G0321.f dataset
c using piecewise Chebyshev polynomial fits.
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc12 -> RCHPTnew_qedc12 via r3gfitchptail_qedc12
c            CHPTCUT  M_omega   =>  r3gfitrholow_qedc12
c            M_omega  EFITOMBG  =>  r3gfitrhohig_qedc12
c            EFITOMBG EFITMIN   =>  epem3gfit079081_qedc12
c            EFITMIN  1.4 GeV   =>  epem3gfit0814new_qedc12
c            1.4 GeV  3.2 GeV   =>  epem3gfit1425_qedc12
      function rs3gsmoothed_qedc12(s,IER)
      implicit none
      INTEGER IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,NFext,NFext1,nf
      REAL *8 rs3gsmoothed_qedc12,RS3G_qedc12,RS40smoothed_qedc12,RS3x_qedc12,Rless,R3Gless,R33less,
     &     r3gfitchptail_qedc12,epem3gfit0814new_qedc12,
     &     epem3gfit079081_qedc12,epem3gfit1425_qedc12,r3gfitrholow_qedc12,r3gfitrhohig_qedc12
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECUTFIT,RSBG,reno,
     &     RESULT,CHPTCUTLOC,efuture,futureprecision,
     &     RSGG1,RS3G1,R3G,R33,F3G
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      COMMON/MFPI_qedc12/MP2,M2/ERR_QEDc12/IER1/POL_QEDc12/POLP,POLM
      COMMON/CHAN_qedc12/ICH
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
      COMMON/GUSA_qedc12/IGS,iso
      EXTERNAL RS3G_qedc12,RS40smoothed_qedc12,RS3x_qedc12,r3gfitchptail_qedc12,
     &     epem3gfit0814new_qedc12,epem3gfit079081_qedc12,epem3gfit1425_qedc12,
     &     r3gfitrholow_qedc12,r3gfitrhohig_qedc12
      common /future_qedc12/efuture,futureprecision,ifuture
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc12/CHPTCUTLOC,ECUTFIT
      common /quama_qedc12/mq,mp,th
      RSGG1=0.d0
      RS3G1=0.d0
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      xc=0.375d0
      xb=0.750d0
      xt=xc
      reno=1.d0
      IER1=IER
      E=DSQRT(S)
c      CHPTCUTLOC=0.312D0
      CHPTCUTLOC=0.320D0
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITMIN=0.81d0
      EFITMAX=1.40d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
c      ECUTFIT=DMIN1(ECUT,40.d0)
      ECUTFIT=th(4)  ! 4.2 GeV
      eps=0.02d0
      RSBG=0.d0
      IF (E.LE.CHPTCUTLOC) THEN
         ICH=0
c         RSBG=0.45d0*rs40smoothed_qedc12(s,IER)                    ! 3 gamma is 9/20 gamma gamma
         RSBG=r3gfitchptail_qedc12(s)
      ELSE IF ((E.GT.CHPTCUTLOC).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=r3gfitrholow_qedc12(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         RSBG=r3gfitrhohig_qedc12(s)
      ELSE IF ((E.GT.EFITOMBG).AND.(E.LE.EFITMIN)) THEN
         ICH=3
         RSBG=epem3gfit079081_qedc12(s)
      ELSE IF ((E.GT.EFITMIN).AND.(E.LE.1.4d0)) THEN
         ICH=4
         RSBG=epem3gfit0814new_qedc12(s)
      ELSE IF ((E.GT.1.4d0).AND.(E.LE.2.2d0)) THEN
         ICH=5
         RSBG=epem3gfit1425_qedc12(s)
      ELSE IF ((E.GT.2.2d0).AND.(E.LE.ECUTFIT)) THEN
c     ECUTFIT don't go beyond 15 GeV
         ICH=6
         RSBG=0.5d0*RS40smoothed_qedc12(s,IER)
      ELSE IF (E.GT.ECUTFIT) THEN
         ICH=7
         if (nf.eq.3) then
            RSBG=0.5d0*RS40smoothed_qedc12(s,IER)
         else 
            RSGG1=RS3x_qedc12(s,R3G,R33)
            F3G=R3G/RSGG1
            IF (IER.EQ.1) THEN
               if (nf.eq.4) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xc+R3Gless
               else if (nf.eq.5) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xb+R3Gless
               else if (nf.ge.6) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xt+R3Gless
               endif
            ELSE
               RSBG=RS40smoothed_qedc12(s,IER)*F3G
            ENDIF
         endif
      ENDIF
c      CALL RENOoldnew(E,reno)
      RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
      rs3gsmoothed_qedc12=RESULT
      return
      end
C
      function epem3gfit0814new_qedc12(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,ier,IRESON,ini
      parameter(np=8,nm=12)
      real *8 epem3gfit0814new_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc12
      dimension aresl(nm),astal(nm),asysl(nm)
      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(np),astafr(np),asysfr(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP
      real *8 fac,rbw,BW_qedc12
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      external BW_qedc12
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      COMMON/BWFI_qedc12/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
      data ini /0/
C E= 0.810 -- 1.000  updated nov 2010/ jan 2012
      DATA aresl/
     &      1.039434d0,-0.963366d0, 0.338346d0,-0.070425d0, 0.009361d0,
     &      0.003189d0,-0.002192d0, 0.000778d0, 0.000739d0,
     &      0.001731d0,-0.000292d0,
     &     -0.000154d0/
      DATA astal/
     &      0.008897d0,-0.002973d0, 0.001825d0, 0.000672d0,-0.000152d0,
     &     -0.000285d0,-0.000287d0, 0.000099d0,-0.000626d0,
     &      0.001016d0,-0.000679d0,
     &      0.000573d0/
      DATA asysl/
     &      0.006760d0,-0.002124d0, 0.001457d0, 0.000196d0,-0.000181d0,
     &      0.000047d0,-0.000016d0, 0.000179d0,-0.000621d0,
     &      0.000539d0,-0.000491d0,
     &      0.000078d0/
cC E= 1.062 -- 1.438
c E=1.040 -- 1.435   updated nov 2010 / jan 2012
      DATA aresh/
     &      0.547033d0,-0.026294d0, 0.145653d0,-0.105039d0, 0.066210d0,
     &     -0.035514d0, 0.027192d0,-0.018354d0, 0.010172d0,
     &     -0.008612d0, 0.007402d0,
     &     -0.006120d0/
      DATA astah/
     &      0.032934d0, 0.025781d0, 0.021063d0, 0.009142d0,-0.000729d0,
     &     -0.001781d0,-0.002989d0,-0.004367d0,-0.002110d0,
     &     -0.004981d0,-0.001538d0,
     &     -0.002266d0/
      DATA asysh/
     &      0.031455d0, 0.009538d0, 0.015344d0, 0.002495d0, 0.007669d0,
     &     -0.001010d0,-0.000021d0,-0.003205d0,-0.002152d0,
     &     -0.003598d0,-0.001663d0,
     &     -0.003333d0/
c phi resonance background 1.0090319999999999 - 1.0300430000000000 [applies for 1.00 - 1.04]
c modified  1.0003000000000000 -- 1.0397000000000001
c  1.00001       --    1.0380000
c update jan 2012
      DATA aresfr/
     &      0.193509d0, 0.281373d0,-0.000000d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA astafr/
     &      0.008055d0, 0.001576d0, 0.000201d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA asysfr/
     &      0.004802d0, 0.006955d0, 0.000791d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
c Fit below phi  0.95899999999999996  -- 1.00000000000000000
c  0.958  --   1.0 update jan 2012
      DATA aresfm/
     &      0.388947d0,-0.040049d0, 0.005114d0, 0.001507d0, 0.002778d0,
     &     -0.002138d0,-0.001764d0, 0.002181d0/
      DATA astafm/
     &      0.007078d0, 0.001188d0,-0.000368d0,-0.000435d0, 0.000750d0,
     &      0.000293d0,-0.000566d0, 0.000194d0/
      DATA asysfm/
     &      0.005543d0, 0.000530d0, 0.000181d0,-0.000196d0,-0.000000d0,
     &     -0.000030d0,-0.000271d0, 0.000278d0/
c
      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.95899999999999996d0
      EFIMX=1.00000000000000000d0
      EFIPX=1.0400000000000000d0
      EFIPP=1.1120000000000001d0
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
         e1=0.81d0
         en=1.00d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
         e1=0.958d0
         en=1.000d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
c update jan 2012
         e1=1.00001d0
         en=1.03500d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfr,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafr,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfr,np)
         endif
c test background subtraction for phi
c         if (ini.eq.0) then
c            ini=1
c            etest0=1.00d0
c            etest1=1.04d0
c            stest0=etest0**2
c            stest1=etest1**2
c            write (*,*) ' R phi at:',etest0,fac*BW_qedc12(stest0,MFI,GFI,PFI)
c            write (*,*) ' R phi at:',etest1,fac*BW_qedc12(stest1,MFI,GFI,PFI)
c         endif
         if (IRESON.eq.1) then
            rbw=fac*BW_qedc12(sMeV,MFI,GFI,PFI)
c for <3G> factor 3/4 to be applied
               rbw=rbw*3.d0/4.d0
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
       else if (e.lt.1.435d0) then
         e1=1.040d0
         en=1.438d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: s out of fit range in epem3gfit0814new_qedc12'
      endif
      epem3gfit0814new_qedc12=yfit
      return
      end
c
      function epem3gfit079081_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=7)
      real *8 epem3gfit079081_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.787 -- 0.8099 ares epem3gfit079081_qedc12  update jan 2012
      DATA ares/
     &      2.934684d0,-0.514241d0,-0.010683d0, 0.011977d0,-0.004930d0,
     &     -0.012588d0, 0.021541d0/
      DATA asta/
     &      0.019822d0,-0.007653d0, 0.001877d0,-0.002843d0, 0.000919d0,
     &     -0.000140d0, 0.000008d0/
      DATA asys/
     &      0.016390d0,-0.008180d0, 0.003416d0,-0.003058d0, 0.000485d0,
     &      0.000286d0, 0.000385d0/
c omega  .41871054d0,.810d0
c no omega component in 3 gamma
      e=sqrt(s)
      yfit=0.d0
      e1=0.779d0
      en=0.810d0
      if ((e.gt.0.81d0).or.(e.lt.e1)) then
         epem3gfit079081_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      epem3gfit079081_qedc12=yfit
      return
      end
c
      function epem3gfit1425_qedc12(s)
      implicit none
      integer np,nm,nx,ier,IRESON
      parameter(np=12,nm=6,nx=8)
      real *8 epem3gfit1425_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,aresmed,astamed,asysmed,
     &     ares,asta,asys,ares1,asta1,asys1,
     &     arestoBES,astatoBES,asystoBES,polynom_qedc12,eex,ebe
      real *8 fres2330,fsta2330,fsys2330,fres2023,fsta2023,fsys2023
      dimension areslow(np),astalow(np),asyslow(np)
      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(np),asta(np),asys(np)
      dimension ares1(np),asta1(np),asys1(np)
      dimension arestoBES(nm),astatoBES(nm),asystoBES(nm)
      dimension fres2330(nx),fsta2330(nx),fsys2330(nx)
      dimension fres2023(nm),fsta2023(nm),fsys2023(nm)
      COMMON/ERR_qedc12/IER
c 1.377 -- 2.000
c   1.375  --    2.038 update jan 2012
      DATA areslow/
     &      0.803108d0, 0.163101d0,-0.115836d0, 0.033882d0, 0.099047d0,
     &      0.013363d0,-0.048230d0, 0.004457d0, 0.027768d0,
     &     -0.008718d0,-0.007653d0,
     &      0.008393d0/
      DATA astalow/
     &      0.082850d0, 0.021018d0,-0.008051d0, 0.006873d0,-0.004899d0,
     &      0.002867d0,-0.004519d0,-0.003195d0,-0.004407d0,
     &      0.002980d0, 0.002556d0,
     &     -0.000300d0/
      DATA asyslow/
     &      0.102574d0, 0.052170d0,-0.021879d0, 0.008900d0, 0.009619d0,
     &      0.008295d0,-0.007824d0,-0.003293d0, 0.007807d0,
     &     -0.004741d0,-0.003700d0,
     &      0.000799d0/
c   1.4  --  2.0
c   1.395 -- 2.000 update jan 2012
      DATA ares/
     &      0.793745d0, 0.129674d0,-0.143795d0,-0.003761d0, 0.083536d0,
     &      0.014670d0,-0.049965d0,-0.008197d0, 0.023961d0,
     &     -0.006621d0, 0.002441d0,
     &      0.000000d0/
      DATA asta/
     &      0.085065d0, 0.019192d0,-0.002852d0, 0.007343d0, 0.000356d0,
     &      0.005762d0, 0.000735d0,-0.001749d0,-0.005006d0,
     &     -0.000013d0, 0.001076d0,
     &      0.000000d0/
      DATA asys/
     &      0.100937d0, 0.044547d0,-0.025297d0, 0.003099d0, 0.006949d0,
     &      0.010313d0,-0.007841d0,-0.001862d0, 0.008309d0,
     &     -0.001277d0,-0.003816d0,
     &      0.000000d0/
c  2.25 -- 2.51 checked jan 2012
      DATA fres2330 /
     &      1.149963d0,-0.062232d0, 0.004324d0, 0.000118d0,-0.000979d0,
     &     -0.000069d0, 0.000510d0,-0.000096d0/
      DATA fsta2330 /
     &      0.027535d0,-0.020376d0, 0.000816d0, 0.000200d0,-0.000170d0,
     &     -0.000129d0, 0.000089d0, 0.000044d0/
      DATA fsys2330 /
     &      0.028106d0,-0.018954d0, 0.001842d0, 0.000179d0,-0.000295d0,
     &     -0.000145d0, 0.000156d0, 0.000043d0/
c  1.981 -- 2.25
c  1.975 -- 2.243 update jan 2012
      DATA fres2023 /
     &      1.078769d0, 0.222235d0,-0.079885d0,-0.026411d0, 0.011933d0,
     &      0.015562d0/
      DATA fsta2023 /
     &      0.069097d0,-0.039640d0, 0.012170d0, 0.016216d0,-0.004335d0,
     &     -0.015969d0/
      DATA fsys2023 /
     &      0.084981d0,-0.058848d0, 0.006400d0, 0.034566d0,-0.018873d0,
     &     -0.012152d0/
      e=sqrt(s)
      yfit=0.d0
      eex=1.8590000000000000d0
      ebe=2.6000000000000001d0
c smoothing overlapps exten down to 1.38
c      if ((e.lt.1.4d0).or.(e.gt.2.5d0)) then
      if ((e.lt.1.38d0).or.(e.gt.2.5d0)) then
         epem3gfit1425_qedc12=0.d0
         return
      else if (e.le.1.45d0) then
c   1.375  --  2.038
         e1=1.375d0
         en=2.038d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asyslow,np)
         endif
      else if (e.lt.1.985d0) then
c   1.4  --  2.0
c   1.395 -- 2.000
         e1=1.395d0
         en=2.0d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,ares,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,asta,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asys,np)
         endif
      else if (e.lt.2.25d0) then
c  1.975 -- 2.243 update jan 2012
         e1=1.975d0
         en=2.263d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2023,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2023,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2023,nm)
         endif
      else if (e.lt.2.50d0) then
         e1=2.243d0
         en=2.500d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2330,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2330,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2330,nx)
         endif
      endif
         epem3gfit1425_qedc12=yfit
      return
      end
c
      function r3gfitchptail_qedc12(s)
      implicit none
      integer np,ier
      parameter(np=5)
      real *8 r3gfitchptail_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.285 - 0.325
c new feb 2012
      DATA ares/
     &      0.012477d0, 0.013866d0, 0.002939d0, 0.000589d0,-0.000073d0/
      DATA asta/
     &      0.000155d0, 0.000198d0, 0.000066d0, 0.000013d0,-0.000003d0/
      DATA asys/
     &      0.000155d0, 0.000198d0, 0.000066d0, 0.000013d0,-0.000003d0/
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.285d0
      en=0.335d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r3gfitchptail_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r3gfitchptail_qedc12=yfit
      return
      end
c
      function r3gfitrholow_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=14)
      real *8 r3gfitrholow_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.318 -- 0.778
c updated nov 2010 (incl BaBar/KlOE) / jan 2012
      DATA ares/
     &      1.172278d0, 1.765772d0, 0.819643d0, 0.191394d0,-0.106432d0,
     &     -0.167313d0,-0.121333d0,-0.058065d0,-0.011283d0,
     &      0.010002d0, 0.013231d0,
     &      0.009828d0, 0.005599d0, 0.001960d0/
      DATA asta/
     &      0.007398d0, 0.007897d0, 0.002574d0, 0.001663d0, 0.001012d0,
     &      0.000313d0,-0.000270d0,-0.000006d0, 0.000515d0,
     &      0.000203d0,-0.000013d0,
     &     -0.000103d0, 0.000222d0, 0.000094d0/
      DATA asys/
     &      0.004792d0, 0.006722d0, 0.003852d0, 0.001963d0, 0.001026d0,
     &      0.000526d0, 0.000302d0, 0.000459d0, 0.000563d0,
     &      0.000334d0, 0.000068d0,
     &     -0.000102d0,-0.000052d0,-0.000023d0/
c omega  .41871054d0,.810d0
c 3G has no omega component
c
      e=sqrt(s)
      yfit=0.d0
c      e1=0.60999999999999999d0
      e1=0.318d0
      en=0.778d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r3gfitrholow_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r3gfitrholow_qedc12=yfit
      return
      end
c
      function r3gfitrhohig_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=9)
      real *8 r3gfitrhohig_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.775 -- 0.787
c  0.769  -- 0.789 update jan 2012
      DATA ares/
     &      3.421526d0,-0.230622d0,-0.034785d0, 0.023723d0,-0.005459d0,
     &     -0.028233d0, 0.000507d0, 0.013734d0, 0.014726d0/
      DATA asta/
     &      0.020036d0, 0.001184d0,-0.005421d0,-0.000015d0,-0.000703d0,
     &      0.000371d0, 0.000070d0,-0.002086d0, 0.003520d0/
      DATA asys/
     &      0.017884d0, 0.000786d0,-0.006437d0, 0.000322d0,-0.001190d0,
     &     -0.000402d0,-0.000263d0,-0.001622d0, 0.003595d0/
c omega  .41871054d0,.810d0
c no omega contribution in RS3G_qedc12
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.76900000000000002d0
      en=0.78900000000000003d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r3gfitrhohig_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r3gfitrhohig_qedc12=yfit
      return
      end

