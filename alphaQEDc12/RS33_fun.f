      FUNCTION RS33_qedc12(s,ier)
      IMPLICIT NONE
      INTEGER i,j,jj,ini,IER,iresonances,IRESON,NFext,NFext1,nf
      DOUBLE PRECISION s,E,RS40_qedc12,RS33_qedc12,Rless,R3Gless,R33less,RS3x_qedc12,BW_qedc12,
     &     null,fac,rbw,rebw,Rinicall,UGM2,sMeV,ELOW,EHIGH,
     &     RSGG1,RS331,R3G,R33,F33
      DOUBLE PRECISION res(3),fsta(5),fsys(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      INTEGER N33,N
      PARAMETER(N33=474)
      DOUBLE PRECISION X33(N33),Y33(N33,3)
      DOUBLE PRECISION MOM,GOM,POM,MFI,GFI,PFI
      DOUBLE PRECISION CHPTCUTLOC,ECUTFIT
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      COMMON/BWOM_qedc12/MOM,GOM,POM/BWFI_qedc12/MFI,GFI,PFI
      COMMON/RESRELERR_qedc12/FSTA,FSYS
c resonances flag iresonances=1 include resonances as data here; 0 include them
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc12/iresonances
      COMMON/RESFIT_qedc12/fac,IRESON
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /R33DAT_qedc12/X33,Y33,N
      external RS40_qedc12,RS3x_qedc12,BW_qedc12
c      common /rhadparts/ru,rd,rs,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc12/CHPTCUTLOC,ECUTFIT
      common /quama_qedc12/mq,mp,th
c      data ini /0/
c      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM_qedc12,BWFI,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series
c
         call resonances_data_qedc12()
c
c called from resonances_dat.f
************************************************************************
         null=0.d0
         UGM2=1.D6
         xc=0.375d0**2
         xb=0.750d0**2
         xt=xc
c         ini=1
c      endif
      j=IER
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo
      RSGG1=null
      RS331=null
      E=SQRT(S)
      ELOW =X33(1)                ! 0.318
      EHIGH=X33(N33)              ! 2.123
      CHPTCUTLOC=DMAX1(ELOW,CHPTCUTLOC)  ! do not go below recombined data set R330321.f
      ECUTFIT=DMAX1(EHIGH,ECUTFIT)       ! exhaust recombined data set R330321.f
      RS33_qedc12=null
      IF (E.LE.ELOW) THEN
         RS33_qedc12=0.225d0*RS40_qedc12(s,IER)  ! 9/40
         RETURN
      ENDIF
      if ((E.GT.ELOW).and.(E.LE.EHIGH)) then
         call getindex_qedc12(E,N33,X33,I)
         if (I.LT.N33) then
            res(j)=Y33(I,j)+(Y33(I+1,j)-Y33(I,j))
     &           /(X33(I+1)-X33(I))*(E-X33(I))
            if (j.eq.3) then
               res(j)=res(j)*(Y33(I,1)+(Y33(I+1,1)-Y33(I,1))
     &              /(X33(I+1)-X33(I))*(E-X33(I)))
            endif
         else
            res(j)=Y33(N33,j)
            if (j.eq.3) then
               res(j)=res(j)*Y33(N33,1)
            endif
         endif
c <33> has no omega contribution skipped
c omega  .41871054d0,.810d0
         if (iresonances.eq.1) then
c phi   1.00D0,1.04D0
            if ((E.GT.EFIM).and.(E.LT.EFIP)) then
               rbw=fac*BW_qedc12(sMeV,MFI,GFI,PFI)
c for <33> factor 9/16 to be applied
               rbw=rbw*9.d0/16.d0
               if (ier.eq.2) then
                  rbw=rbw*fsta(3)
                  res(j)=sqrt(res(j)**2+rbw**2)
               else
                  if (ier.eq.3) rbw=rbw*fsys(3)
                  res(j)=res(j)+rbw
               endif
               rebw=rbw
            endif
         endif
         RS33_qedc12=res(j)
         RETURN
      endif
      IF (E.GT.EHIGH) THEN
         IF (E.LE.ECUTFIT) THEN
            RS33_qedc12=9.d0/32.d0*RS40_qedc12(s,IER)
         ELSE IF (E.GT.ECUTFIT) THEN
            if (nf.eq.3) then
               RS33_qedc12=9.d0/32.d0*RS40_qedc12(s,IER)
            else
               RSGG1=RS3x_qedc12(s,R3G,R33)
               F33=R33/RSGG1
               IF (IER.EQ.1) THEN
                  if (nf.eq.4) then
                     RS33_qedc12=(RS40_qedc12(s,IER)-Rless)*xc+R33less
                  else if (nf.eq.5) then
                     RS33_qedc12=(RS40_qedc12(s,IER)-Rless)*xb+R33less
                  else if (nf.ge.6) then
                     RS33_qedc12=(RS40_qedc12(s,IER)-Rless)*xt+R33less
                  endif
               ELSE
                  RS33_qedc12=RS40_qedc12(s,IER)*F33
               ENDIF
            endif
         ENDIF
         RETURN
      ENDIF
      END
c
      FUNCTION RS33pQCD_qedc12(s,IER)
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,R,R3G,R33,RS33pQCD_qedc12,RS3x_qedc12,ALINP,EINP,MTOP,pi,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc12/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      common/pqcdHS_qedc12/pi,ALINP1,EINP,MTOP1,IOR1,NF1 ! local input for Rhad_qedc12 HS etc
      external RS3x_qedc12
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      if (IER.eq.1) then
         ALINP1=ALS
         R=RS3x_qedc12(S,R3G,R33)
         res(IER)=R33
      else if (IER.eq.2) then
         res(IER)=0.d0
      else
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         R=RS3x_qedc12(S,R3G,R33)
         RSP=R33
         ALINP1=ALS-EST
         R=RS3x_qedc12(S,R3G,R33)
         RSM=R33
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
      endif
      RS33pQCD_qedc12=res(IER)
      return
      end
