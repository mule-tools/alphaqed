c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; R33dat_fit.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Mon Feb 20 16:00:34 2012
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Mar 29 23:18:00 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of function RS33_fun.f interpolating R330321.f dataset
c using piecewise Chebyshev polynomial fits.
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc12 -> RCHPTnew_qedc12 via r33fitchptail_qedc12
c            CHPTCUT  M_omega   =>  r33fitrholow_qedc12
c            M_omega  EFITOMBG  =>  r33fitrhohig_qedc12
c            EFITOMBG EFITMIN   =>  epem33fit079081_qedc12
c            EFITMIN  1.4 GeV   =>  epem33fit0814new_qedc12
c            1.4 GeV  3.2 GeV   =>  epem33fit1425_qedc12
      function rs33smoothed_qedc12(s,IER)
      implicit none
      INTEGER IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,NFext,NFext1,nf
      REAL *8 rs33smoothed_qedc12,RS33_qedc12,RS40smoothed_qedc12,RS3x_qedc12,Rless,R3Gless,R33less,
     &     r33fitchptail_qedc12,epem33fit0814new_qedc12,
     &     epem33fit079081_qedc12,epem33fit1425_qedc12,r33fitrholow_qedc12,r33fitrhohig_qedc12
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECUTFIT,RSBG,reno,
     &     RESULT,CHPTCUTLOC,efuture,futureprecision,
     &     RSGG1,RS331,R3G,R33,F33
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      COMMON/MFPI_qedc12/MP2,M2/ERR_QEDc12/IER1/POL_QEDc12/POLP,POLM
      COMMON/CHAN_qedc12/ICH
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
      COMMON/GUSA_qedc12/IGS,iso
      EXTERNAL RS33_qedc12,RS40smoothed_qedc12,RS3x_qedc12,r33fitchptail_qedc12,
     &     epem33fit0814new_qedc12,epem33fit079081_qedc12,epem33fit1425_qedc12,
     &     r33fitrholow_qedc12,r33fitrhohig_qedc12
      common /future_qedc12/efuture,futureprecision,ifuture
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc12/CHPTCUTLOC,ECUTFIT
      common /quama_qedc12/mq,mp,th
      RSGG1=0.d0
      RS331=0.d0
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      xc=0.375d0**2
      xb=0.750d0**2
      xt=xc
      reno=1.d0
      IER1=IER
      E=DSQRT(S)
c CUT set in R3Gdat_fit.f
c      CHPTCUTLOC=0.312D0
c      CHPTCUTLOC=0.320D0
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITMIN=0.81d0
      EFITMAX=1.40d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
c      ECUTFIT=DMIN1(ECUT,40.d0)
      ECUTFIT=th(4)  ! 4.2 GeV
      eps=0.02d0
      RSBG=0.d0
      IF (E.LE.CHPTCUTLOC) THEN
         ICH=0
c         RSBG=0.225d0*rs40smoothed_qedc12(s,IER)                    ! 3 3 is 9/40 gamma gamma
         RSBG=r33fitchptail_qedc12(s)
      ELSE IF ((E.GT.CHPTCUTLOC).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=r33fitrholow_qedc12(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         RSBG=r33fitrhohig_qedc12(s)
      ELSE IF ((E.GT.EFITOMBG).AND.(E.LE.EFITMIN)) THEN
         ICH=3
         RSBG=epem33fit079081_qedc12(s)
      ELSE IF ((E.GT.EFITMIN).AND.(E.LE.1.4d0)) THEN
         ICH=4
         RSBG=epem33fit0814new_qedc12(s)
      ELSE IF ((E.GT.1.4d0).AND.(E.LE.2.2d0)) THEN
         ICH=5
         RSBG=epem33fit1425_qedc12(s)
      ELSE IF ((E.GT.2.2d0).AND.(E.LE.ECUTFIT)) THEN
c     ECUTFIT don't go beyond 15 GeV
         ICH=6
         RSBG=9.d0/32.d0*RS40smoothed_qedc12(s,IER)
      ELSE IF (E.GT.ECUTFIT) THEN
         ICH=7
         if (nf.eq.3) then
            RSBG=9.d0/32.d0*RS40smoothed_qedc12(s,IER)
         else 
            RSGG1=RS3x_qedc12(s,R3G,R33)
            F33=R33/RSGG1
            IF (IER.EQ.1) THEN
               if (nf.eq.4) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xc+R33less
               else if (nf.eq.5) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xb+R33less
               else if (nf.ge.6) then
                  RSBG=(RS40smoothed_qedc12(s,IER)-Rless)*xt+R33less
               endif
            ELSE
               RSBG=RS40smoothed_qedc12(s,IER)*F33
            ENDIF
         endif
      ENDIF
c      CALL RENOoldnew(E,reno)
      RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
      rs33smoothed_qedc12=RESULT
      return
      end
C
      function epem33fit0814new_qedc12(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,ier,IRESON,ini
      parameter(np=8,nm=12)
      real *8 epem33fit0814new_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc12
      dimension aresl(nm),astal(nm),asysl(nm)
      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(np),astafr(np),asysfr(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP
      real *8 fac,rbw,BW_qedc12
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      external BW_qedc12
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      COMMON/BWFI_qedc12/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
      data ini /0/
C E= 0.810 -- 1.000  updated nov 2010/ jan 2012
      DATA aresl/
     &      0.519778d0,-0.481604d0, 0.169203d0,-0.035144d0, 0.004687d0,
     &      0.001605d0,-0.001185d0, 0.000450d0, 0.000423d0,
     &      0.000845d0,-0.000143d0,
     &     -0.000102d0/
      DATA astal/
     &      0.004451d0,-0.001321d0, 0.000862d0, 0.000314d0,-0.000080d0,
     &     -0.000173d0,-0.000144d0, 0.000096d0,-0.000379d0,
     &      0.000517d0,-0.000289d0,
     &      0.000255d0/
      DATA asysl/
     &      0.003383d0,-0.001060d0, 0.000726d0, 0.000097d0,-0.000088d0,
     &      0.000026d0,-0.000009d0, 0.000086d0,-0.000310d0,
     &      0.000271d0,-0.000242d0,
     &      0.000034d0/
cC E= 1.062 -- 1.438
c E=1.040 -- 1.435   updated nov 2010 / jan 2012
      DATA aresh/
     &      0.312164d0,-0.057776d0, 0.099034d0,-0.073429d0, 0.051185d0,
     &     -0.026069d0, 0.020204d0,-0.015231d0, 0.005712d0,
     &     -0.007882d0, 0.003897d0,
     &     -0.004317d0/
      DATA astah/
     &      0.022066d0, 0.015747d0, 0.013398d0, 0.004838d0,-0.001677d0,
     &     -0.001519d0,-0.001575d0,-0.002363d0,-0.000746d0,
     &     -0.002973d0,-0.000861d0,
     &     -0.001444d0/
      DATA asysh/
     &      0.017691d0, 0.001432d0, 0.008735d0, 0.000432d0, 0.005027d0,
     &     -0.000893d0, 0.000184d0,-0.001892d0,-0.001118d0,
     &     -0.002131d0,-0.000896d0,
     &     -0.002162d0/
c phi resonance background 1.0090319999999999 - 1.0300430000000000 [applies for 1.00 - 1.04]
c modified  1.0003000000000000 -- 1.0397000000000001
c  1.00001       --    1.0380000
c update jan 2012
      DATA aresfr/
     &      0.084629d0, 0.239502d0, 0.000000d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA astafr/
     &      0.004533d0, 0.000794d0, 0.000090d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA asysfr/
     &      0.002466d0, 0.005549d0, 0.000722d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
c Fit below phi  0.95899999999999996  -- 1.00000000000000000
c  0.958  --   1.0 update jan 2012
      DATA aresfm/
     &      0.194637d0,-0.019950d0, 0.002544d0, 0.000794d0, 0.001325d0,
     &     -0.001066d0,-0.000920d0, 0.001274d0/
      DATA astafm/
     &      0.003641d0, 0.000622d0,-0.000283d0,-0.000166d0, 0.000329d0,
     &     -0.000091d0,-0.000230d0, 0.000228d0/
      DATA asysfm/
     &      0.002774d0, 0.000266d0, 0.000091d0,-0.000098d0,-0.000001d0,
     &     -0.000014d0,-0.000136d0, 0.000141d0/
c
      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.95899999999999996d0
      EFIMX=1.00000000000000000d0
      EFIPX=1.0400000000000000d0
      EFIPP=1.1120000000000001d0
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
         e1=0.81d0
         en=1.00d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
         e1=0.958d0
         en=1.000d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
c update jan 2012
         e1=1.00001d0
         en=1.03500d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfr,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafr,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfr,np)
         endif
c test background subtraction for phi
c         if (ini.eq.0) then
c            ini=1
c            etest0=1.00d0
c            etest1=1.04d0
c            stest0=etest0**2
c            stest1=etest1**2
c            write (*,*) ' R phi at:',etest0,fac*BW_qedc12(stest0,MFI,GFI,PFI)
c            write (*,*) ' R phi at:',etest1,fac*BW_qedc12(stest1,MFI,GFI,PFI)
c         endif
         if (IRESON.eq.1) then
            rbw=fac*BW_qedc12(sMeV,MFI,GFI,PFI)
c for <33> factor 9/16 to be applied
               rbw=rbw*9.d0/16.d0
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
       else if (e.lt.1.435d0) then
         e1=1.040d0
         en=1.438d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: s out of fit range in epem33fit0814new_qedc12'
      endif
      epem33fit0814new_qedc12=yfit
      return
      end
c
      function epem33fit079081_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=7)
      real *8 epem33fit079081_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.787 -- 0.8099 ares epem33fit079081_qedc12  update jan 2012
      DATA ares/
     &      1.467375d0,-0.257308d0,-0.005274d0, 0.006028d0,-0.002408d0,
     &     -0.006298d0, 0.010964d0/
      DATA asta/
     &      0.009922d0,-0.003986d0, 0.001042d0,-0.001551d0, 0.000285d0,
     &      0.000182d0,-0.000074d0/
      DATA asys/
     &      0.008195d0,-0.004091d0, 0.001708d0,-0.001529d0, 0.000243d0,
     &      0.000142d0, 0.000193d0/
c omega  .41871054d0,.810d0
c no omega component in 3 3
      e=sqrt(s)
      yfit=0.d0
      e1=0.779d0
      en=0.810d0
      if ((e.gt.0.81d0).or.(e.lt.e1)) then
         epem33fit079081_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      epem33fit079081_qedc12=yfit
      return
      end
c
      function epem33fit1425_qedc12(s)
      implicit none
      integer np,nm,nx,ier,IRESON
      parameter(np=12,nm=6,nx=8)
      real *8 epem33fit1425_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,aresmed,astamed,asysmed,
     &     ares,asta,asys,ares1,asta1,asys1,
     &     arestoBES,astatoBES,asystoBES,polynom_qedc12,eex,ebe
      real *8 fres2330,fsta2330,fsys2330,fres2023,fsta2023,fsys2023
      dimension areslow(np),astalow(np),asyslow(np)
      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(np),asta(np),asys(np)
      dimension ares1(np),asta1(np),asys1(np)
      dimension arestoBES(nm),astatoBES(nm),asystoBES(nm)
      dimension fres2330(nx),fsta2330(nx),fsys2330(nx)
      dimension fres2023(nm),fsta2023(nm),fsys2023(nm)
      COMMON/ERR_qedc12/IER
c 1.377 -- 2.000
c   1.375  --    2.038 update jan 2012
      DATA areslow/
     &      0.451590d0, 0.123399d0,-0.072192d0, 0.014844d0, 0.067087d0,
     &      0.007129d0,-0.034431d0, 0.008575d0, 0.011239d0,
     &     -0.011690d0, 0.001188d0,
     &      0.000970d0/
      DATA astalow/
     &      0.055140d0, 0.012245d0,-0.005106d0, 0.004353d0,-0.004347d0,
     &      0.000815d0,-0.000809d0,-0.003116d0,-0.003742d0,
     &      0.004818d0, 0.001205d0,
     &     -0.001135d0/
      DATA asyslow/
     &      0.061587d0, 0.036369d0,-0.013822d0, 0.004366d0, 0.008589d0,
     &      0.005673d0,-0.004210d0, 0.000028d0, 0.005879d0,
     &     -0.002565d0,-0.001311d0,
     &      0.000484d0/
c   1.4  --  2.0
c   1.395 -- 2.000 update jan 2012
      DATA ares/
     &      0.445733d0, 0.105042d0,-0.087304d0,-0.005888d0, 0.061077d0,
     &      0.012471d0,-0.030564d0, 0.005808d0, 0.012903d0,
     &     -0.011370d0, 0.003112d0,
     &     -0.000194d0/
      DATA asta/
     &      0.056689d0, 0.011367d0,-0.001507d0, 0.004893d0,-0.001036d0,
     &      0.002059d0, 0.002539d0,-0.003313d0,-0.004530d0,
     &      0.002244d0, 0.001189d0,
     &     -0.000509d0/
      DATA asys/
     &      0.059746d0, 0.030724d0,-0.017492d0,-0.000690d0, 0.004918d0,
     &      0.005295d0,-0.006003d0,-0.000578d0, 0.005040d0,
     &     -0.000956d0,-0.000918d0,
     &      0.000235d0/
c  2.25 -- 2.51 checked jan 2012
      DATA fres2330 /
     &      0.646854d0,-0.035006d0, 0.002432d0, 0.000067d0,-0.000551d0,
     &     -0.000039d0, 0.000287d0,-0.000054d0/
      DATA fsta2330 /
     &      0.015488d0,-0.011462d0, 0.000459d0, 0.000113d0,-0.000095d0,
     &     -0.000073d0, 0.000050d0, 0.000025d0/
      DATA fsys2330 /
     &      0.015810d0,-0.010662d0, 0.001036d0, 0.000101d0,-0.000166d0,
     &     -0.000081d0, 0.000088d0, 0.000024d0/
c  1.981 -- 2.25
c  1.975 -- 2.243 update jan 2012
      DATA fres2023 /
     &      0.616340d0, 0.113656d0,-0.047286d0,-0.007121d0, 0.002885d0,
     &      0.008023d0/
      DATA fsta2023 /
     &      0.042854d0,-0.028581d0, 0.009628d0, 0.008759d0,-0.002758d0,
     &     -0.009056d0/
      DATA fsys2023 /
     &      0.051685d0,-0.038619d0, 0.004302d0, 0.022003d0,-0.013310d0,
     &     -0.005594d0/
      e=sqrt(s)
      yfit=0.d0
      eex=1.8590000000000000d0
      ebe=2.6000000000000001d0
c smoothing overlapps exten down to 1.38
c      if ((e.lt.1.4d0).or.(e.gt.2.5d0)) then
      if ((e.lt.1.38d0).or.(e.gt.2.5d0)) then
         epem33fit1425_qedc12=0.d0
         return
      else if (e.le.1.45d0) then
c   1.375  --  2.038
         e1=1.375d0
         en=2.038d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asyslow,np)
         endif
      else if (e.lt.1.985d0) then
c   1.4  --  2.0
c   1.395 -- 2.000
         e1=1.395d0
         en=2.0d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,ares,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,asta,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asys,np)
         endif
      else if (e.lt.2.25d0) then
c  1.975 -- 2.243 update jan 2012
         e1=1.975d0
         en=2.263d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2023,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2023,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2023,nm)
         endif
      else if (e.lt.2.50d0) then
         e1=2.243d0
         en=2.500d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2330,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2330,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2330,nx)
         endif
      endif
         epem33fit1425_qedc12=yfit
      return
      end
c
      function r33fitchptail_qedc12(s)
      implicit none
      integer np,ier
      parameter(np=5)
      real *8 r33fitchptail_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.285 - 0.325
c new feb 2012
      DATA ares/
     &      0.006065d0, 0.006665d0, 0.001351d0, 0.000309d0, 0.000005d0/
      DATA asta/
     &      0.000076d0, 0.000096d0, 0.000031d0, 0.000006d0,-0.000001d0/
      DATA asys/
     &      0.000076d0, 0.000096d0, 0.000031d0, 0.000006d0,-0.000001d0/
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.285d0
      en=0.335d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r33fitchptail_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r33fitchptail_qedc12=yfit
      return
      end
c
      function r33fitrholow_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=14)
      real *8 r33fitrholow_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.318 -- 0.778
c updated nov 2010 (incl BaBar/KlOE) / jan 2012
      DATA ares/
     &      0.586100d0, 0.883038d0, 0.409738d0, 0.095791d0,-0.053218d0,
     &     -0.083653d0,-0.060620d0,-0.029012d0,-0.005560d0,
     &      0.004928d0, 0.006786d0,
     &      0.004899d0, 0.002907d0, 0.001064d0/
      DATA asta/
     &      0.003671d0, 0.003800d0, 0.001362d0, 0.000656d0, 0.000559d0,
     &      0.000132d0,-0.000188d0, 0.000123d0, 0.000180d0,
     &      0.000104d0,-0.000044d0,
     &     -0.000066d0, 0.000019d0, 0.000151d0/
      DATA asys/
     &      0.002395d0, 0.003364d0, 0.001924d0, 0.000983d0, 0.000513d0,
     &      0.000263d0, 0.000152d0, 0.000229d0, 0.000283d0,
     &      0.000166d0, 0.000036d0,
     &     -0.000052d0,-0.000025d0,-0.000011d0/
c omega  .41871054d0,.810d0
c 33 has no omega component
c
      e=sqrt(s)
      yfit=0.d0
c      e1=0.60999999999999999d0
      e1=0.318d0
      en=0.778d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r33fitrholow_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r33fitrholow_qedc12=yfit
      return
      end
c
      function r33fitrhohig_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=9)
      real *8 r33fitrhohig_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.775 -- 0.787
c  0.769  -- 0.789 update jan 2012
      DATA ares/
     &      1.710673d0,-0.115039d0,-0.017611d0, 0.011966d0,-0.002590d0,
     &     -0.014182d0, 0.000386d0, 0.006805d0, 0.007649d0/
      DATA asta/
     &      0.009923d0, 0.000387d0,-0.002833d0,-0.000426d0,-0.000340d0,
     &      0.000382d0, 0.000133d0,-0.000866d0, 0.001620d0/
      DATA asys/
     &      0.008942d0, 0.000394d0,-0.003220d0, 0.000161d0,-0.000594d0,
     &     -0.000201d0,-0.000131d0,-0.000811d0, 0.001799d0/
c omega  .41871054d0,.810d0
c no omega contribution in RS33_qedc12
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.76900000000000002d0
      en=0.78900000000000003d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r33fitrhohig_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      r33fitrhohig_qedc12=yfit
      return
      end

