*;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; alphaQEDcomplex.f --- 
*; Author          : Fred Jegerlehner
*; Created On      : Sat Nov  1 22:49:46 2003
*; Last Modified By: Friedrich Jegerlehner
*; Last Modified On: Mon Apr 23 03:27:50 2012
*; RCS: $Id$
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; Copyright (c) 2012 Fred Jegerlehner
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; 
      PROGRAM alphaQEDcomplex
C Calculating the running QED coupling alpha(E)
C Leptons: 1--loop, 2--loop exact, 3--loop in high energy approximation
C Quarks form routine dhadr5n including effective s--channel shifts in low energy range
C r1....,r2....,r3.... : oneloop, twoloop, threeloop result
C  ..real: realpart, ..imag: imaginary part;
C ......l: light fermions only (high energy approximation for 3--loops) 
C dallepQED1n=r1real,dallepQED2n=r2real,dallepQED3l_qedc12=r3reall
C dallepQED3l_qedc12 also returns oneloop and twoloop high energy (light fermion) 
C approximations 
c 21/09/2009 common pQCD parameter setting via constants_qcd.f      
c FJ 28/01/2012 recalculated Nall=2055 --> 2177
c 29/01/2012 common resonance parameters setting via resonances_dat.f
      implicit none
      integer i,j,N,stflag,logflag,Ismooth,Nall,ndat,N1,N2
      parameter(Nall=2177)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      real*8 dggvap_qedc12,s,e,de,null,Dalphaweak1MSb,cvap2,rvap2
      real*8 st2,als,mtop,dvpt_higx,dvpt_lowx
      real*8 dalept,dahadr,daltop,dvpt_new,dvpt_hig,dvpt_low
      real*8 emax,emin,elogl,elogu,delog,elog,fep,fem,fractionalerror
      double precision CHPTCUT,EC,ECUT
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      double complex calept,cahadr,caltop,cvpt_new,cvpt_hig,cvpt_low
      double complex alphac,alphah,alphal,alphact,alphaht,alphalt,
     &     calept1,cahadr1,caltop1,cggvap_qedc12,cone,calp,cDalphaweak1MSb
      double complex cerror,comPi
      REAL*8 eps
      double complex r1c,r1l,r1h
      double precision reno,renl,renh,BWRENO_qedc12,renfac,fracerror
      external cggvap_qedc12,BWRENO_qedc12
********************* extended header for real, complex as well as resonance data access **************
      REAL*8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI
      REAL*8 PSI(6),MPS(6),GPS(6),PPS(6),YPI(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 RMA(15),RGA(15),RPA(15),EREM(15),EREG(15),EREP(15),
     &     EMI(15),EMA(15)
      integer nx,ny
      parameter(nx=400,ny=15)
      integer nres(ny)
c      double complex r1cx(nx,ny)
      double precision renpts(nx,ny),renofa(nx,ny),fracerr(nx,ny)
      include 'common.h'      
c results detailed
      common /resu_qedc12/dalept,dahadr,daltop,Dalphaweak1MSb
      common /cres_qedc12/calept,cahadr,caltop,cDalphaweak1MSb
c QCD parameters pQCD version of R(s), top contribution      
      common/var1_qedc12/pi1,ALINP,ERRAL,EINP,MTOP1
      common /parm_qedc12/st2,als,mtop
c commons needed to get R(s) , where to use pQCD, data vs fits
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
      COMMON/SMOO_qedc12/Ismooth
c parameters resonsnces
      COMMON/BWRO_qedc12/MRO,GRO,PRO/BWOM_qedc12/MOM,GOM,POM/BWFI_qedc12/MFI,GFI,PFI
      COMMON/PSYPPAR_qedc12/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESALL_qedc12/RMA,RGA,RPA,EREM,EREG,EREP
      COMMON/RESRAN_qedc12/EMI,EMA
c      common/renormBW/renpts,renofa,fracerr,r1cx,nres
      common/renormBW_qedc12/renpts,renofa,fracerr,nres
      call constants_qedc12()
      call constants_qcd_qedc12() ! fills commons var and RCUTS
      call resonances_data_qedc12() ! fills commons resonance parameters
c      sin2ell=0.23153 ! pm 0.00016 LEPEEWG Phys Rep 427 (2006) 257
      st2=0.23153d0   ! Reference value for weak mixing parameter
      als=ALINP       ! alpha strong
      mtop=MTOP1      ! top quark mass
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
c following two entried set as default in chad5n12.f
c      IRESON=1        ! include narrow resonances in fits
c      iresonances=1   ! include narrow resonances in data sets
      call Rdata_qedc12()
c      call resonances_renoreal()
      call resonances_renocomplex_qedc12()
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment in main the following 2 lines for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau
c      iLEP=lflag_qedc12(LEPTONflag)
************************************************************************
c the parameter xMW [provided via common.h] allows to change W threshold default M_W, e.g. to xMW=2.d0*MW
c      xMW=null is set in constants ! default M_W
c      xMW=2.d0*MW
      eps=0.1d0
      j=1
      null=0.d0
      cone=DCMPLX(1.d0,null)
      calp=DCMPLX(alp,null)
      N=4000
c Scan with energy point raster xRdat-extended
c      ndat=1 low energy time-like data points extended by resonance scans
c      ndat=2 pQCD tail
c      ndat=3 test resonances at peak
c      ndat=-1 space-like region
      ndat=1
c      ndat=0
c      ndat=3
c
      include 'xRdat-extended.f' ! just DATA X/.../ statement
c      
c caution: logatithmic scaling used below: cannot choose emin=0 or emax=0 !!!!
c      
      logflag=0
      stflag =1
c     for spacelike region: stflag=-1
c      stflag =-1
c threshold 0.27914036d0
      emin=1.d-6
      emin=.27d0
      emax=11.5d0
c      emax=91.19d0
c J/psi,psi's
c      emin=3.09660 ! xmin psi1
c      emax=3.09720 ! xmax psi1
c      emin=3.68500 ! xmin psi2
c      emax=3.68700 ! xmax psi2
c      emin=3.72000 ! xmin psi3
c      emax=3.84000 ! xmax psi3
c      emin=3.96000 ! xmin psi4
c      emax=4.10000 ! xmax psi4
c Upsilon's
c 9460.30D
c      emin= 9.4600            ! xmin ups1
c      emax= 9.4606            ! xmax ups1
c      emin=10.023200 ! xmin ups2
c      emax=10.023320 ! xmax ups2
c      emin=10.354600 ! xmin ups3
c      emax=10.355800 ! xmax ups3
      write (*,*) ' Prepared scans with energy point raster ',
     &     'xRdat-extended'
      write (*,*) ' ndat=1 low energy time-like data points ',
     &     'extended by resonance scans'
      write (*,*) ' ndat=2 pQCD tail'
      write (*,*) ' ndat=3 test resonances at peak'
      write (*,*) ' ndat=-1 space-like region'
      write (*,*) ' ndat=0 prompt for individual range'
      write (*,*) ' Enter ndat:'
      read  (*,*) ndat
      if (ndat.eq.0) then
      write (*,*) ' Enter energy range and number of points: ',
     &     'emin,emax,N' 
      read (*,*) emin,emax,N
      endif
      if (ndat.eq.1) then
      write (*,*) ' Enter energy range: ',
     &     'emin,emax' 
      read (*,*) emin,emax
      endif
      if (ndat.eq.0) then
         write (*,*) ' stflag=1/-1 [time/space-like] ',
     &        'logflag=0/1 [linear/log scale]'
         write (*,*) ' Enter stflag,logflag:'
         read (*,*) stflag,logflag
      endif
      if (ndat.eq.1) then
         call getindex_qedc12(emin,Nall,X,N1)
         call getindex_qedc12(emax,Nall,X,N2)
      else 
c      if (ndat.eq. 1).and N=Nall-1
         if (ndat.eq. 2) N=NB-1
         if (ndat.eq.-1) N=NA-1
c      if (ndat.eq. 3) N=45-1
         if (ndat.eq. 3) N=15-1
         N1=1
         N2=N+1
      endif
c write header for graphx plot program
      do j=1,4
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     real parts'
      enddo
      do j=11,14
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     imaginary parts'
      enddo
         write (8,*) '      3.0000'
         write (8,*) '      1.0000'
         write (8,*) '   ',float(N2-N1)
         write (8,*) '  |1-\Pi(s)|^2-1, (alpha/alpha(s))^2-1,',
     &        '|1-\Pi(s)|^2/(alpha/alpha(s))^2-1,',
     &        '|1-\Pi(s)|^2-(alpha/alpha(s))^2'
         write (22,*) '      4.0000'
         write (22,*) '      1.0000'
         write (22,*) '   ',float(N2-N1)
         write (22,*) '     imaginary parts undressed'
         write (32,*) '      4.0000'
         write (32,*) '      1.0000'
         write (32,*) '   ',float(N2-N1)
         write (32,*) '     imaginary parts undressed'
c     ..................................................................
c     vacuum polarization  ...[c] central ...l low value, ...h high value
      write (1,*) ' e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write (2,*) ' e,alpha ,alphal ,alphah (no top)    '
      write (3,*) ' e,alphat,alphatl,alphath (with top)   ' 
c      write (4,*) ' e,dalept,dahadr,daltop'
      write (4,*) ' e,dahadr,cerror,cerror'
      write(11,*) ' e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write(12,*) ' e,alpha ,alphal ,alphah (no top)    '
      write(13,*) ' e,alphat,alphatl,alphath (with top)   ' 
      write(14,*) ' e,dalept,dahadr,daltop'
c      
      de=(emax-emin)/N
      e=emin
      elogl=dlog(dabs(emin))
      elogu=dlog(dabs(emax))
      delog=(elogu-elogl)/N
      do 10 i=N1,N2
         if (ndat.eq.1) then
            e=X(i)
            s=e*e
         else if (ndat.eq.2) then
            e=ESX(i)
            s=e*e
         else if (ndat.eq.3) then
            e=RMA(i)
            s=e*e
         else if (ndat.eq.-1) then
            e=ETX(i)
            s=-e*e
         else 
            if (logflag.eq.1) then
               if (stflag.eq.1) then
                  elog=elogl+delog*(i-1)
               else
                  elog=elogu-delog*(i-1)
               endif
               e=dexp(elog)
               s=e*e*stflag
               e=dsqrt(dabs(s))*stflag
            else
               e=e+de
               s=e*abs(e)
            endif
         endif
c
      cvpt_new= cggvap_qedc12(s,cerror)
      cvpt_hig= cvpt_new+cerror
      cvpt_low= cvpt_new-cerror
      alphac =calp/(cone-cvpt_new)
      alphah =calp/(cone-cvpt_hig)
      alphal =calp/(cone-cvpt_low)
      alphact=calp/(cone-cvpt_new-caltop-cDalphaweak1MSb)
      alphaht=calp/(cone-cvpt_hig-caltop-cDalphaweak1MSb)
      alphalt=calp/(cone-cvpt_low-caltop-cDalphaweak1MSb)
      r1c=calp/alphac
      r1h=calp/alphah
      r1l=calp/alphal
      reno=r1c*DCONJG(r1c)         ! undressing factor
      renh=r1h*DCONJG(r1h)         ! + error
      renl=r1l*DCONJG(r1l)         ! - error
      renfac=BWRENO_qedc12(s,fracerror)
C full vapo subtraction mainly for resonances parametrized in terms of
C "dressed" widths
c
c     delta alpha: leptons + 5 quarks no top
c
      write (1,98) e,DREAL(cvpt_new),DREAL(cvpt_low),DREAL(cvpt_hig)
      write(11,98) e,AIMAG(cvpt_new),AIMAG(cvpt_low),AIMAG(cvpt_hig)
c
c     alpha: leptons + 5 quarks no top
c
      write (2,98) e,DREAL(alphac),DREAL(alphal),DREAL(alphah)
      write(12,98) e,AIMAG(alphac),AIMAG(alphal),AIMAG(alphah)
      write(22,99) e,reno,renl,renh
      write(32,99) e,renfac,renfac*fracerror,renfac*fracerror
c      write(22,98) e,AIMAG(alphac)*reno,
c     &     AIMAG(alphal)*renl,AIMAG(alphah)*renh,reno
c
c     alpha: leptons + 5 quarks + top + W (1-loop MSbar)
c
      write (3,98) e,DREAL(alphact),DREAL(alphalt),DREAL(alphaht)
      write(13,98) e,AIMAG(alphact),AIMAG(alphalt),AIMAG(alphaht)
c
c     delta alpha: leptons, 5 quarks, top 
c
      cvpt_new= cggvap_qedc12(s,cerror)

c      write (4,98) e,DREAL(calept),DREAL(cahadr),DREAL(caltop),
c     &     Dalphaweak1MSb
      write (4,98) e,DREAL(cahadr),DREAL(cerror),DREAL(cerror)
      write(14,99) e,AIMAG(calept)**2,AIMAG(cahadr)**2,
     &     AIMAG(caltop)**2
c
c     |1-\PI'(s)|
c
      comPi=calp/alphact
      cvap2 =DREAL(comPi*DCONJG(comPi))
      rvap2 =DREAL(comPi)**2
      write (8,99) e,cvap2-1.d0,rvap2-1.d0,cvap2/rvap2-1.d0,cvap2-rvap2
c
 10   continue
c check Breit-Wigner renormalization factors initialized by "call resonances_renocomplex" in common 
c       common /renormBW/renpts,renofa,fracerr,nres
c must reproduce in resonance regions the following set
c      write(22,99) e,reno,renl,renh
c      write(32,99) e,renfac,renfac*fracerror,renfac*fracerror
c generated with alphaQEDcomplex with options ndat=1, emin=0.,emax=11.5 for example
      do j=2,ny
         write (50+j,*) '      3.0000'
         write (50+j,*) '      1.0000'
         write (50+j,*) '   ',float(nres(j))
         write (50+j,*) '     BW_qedc12 bare and renormalized'
         do i=1,nres(j)   
            write (50+j,99) renpts(i,j),renofa(i,j),fracerr(i,j)
         enddo
      enddo
c     ..................................................................
c detailed resonance scans require high resolution
 98   format(5(2x,1pe11.4))
 99   format(5(2x,1pe15.8))
      stop
      END
c
