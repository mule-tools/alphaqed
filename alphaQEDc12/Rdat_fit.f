c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Rdat_fit.f --- 
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Sun Jan 17 05:02:10 2010
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Mon Apr  2 02:29:42 2012
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2009 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of R(s) using piecewise Chebyshev polynomial fits. 
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01         
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc12 -> RCHPTnew_qedc12
c            CHPTCUT  M_omega   =>  rfitrholow_qedc12
c            M_omega  EFITOMBG  =>  rfitrhohig_qedc12        IGS=1 rsigpp_qedc12
c            EFITOMBG EFITMIN   =>  epemfit079081_qedc12     IGS=1 rsigpp_qedc12
c            EFITMIN  1.4 GeV   =>  epemfit0814new_qedc12
c            1.4 GeV  3.2 GeV   =>  epemfit1432_qedc12
c            3.2 GeV  EC        =>  epemfir32cut
      function rs40smoothed_qedc12(s,IER)
      implicit none
      INTEGER IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,
     &     IRESON,iresonances,iresonances_sav
      REAL *8 rs40smoothed_qedc12,RS40_qedc12,RS40x_qedc12,RCHPT_qedc12,RCHPTnew_qedc12,rfitchptail_qedc12,
     &     rsigpp_qedc12,epemfit0814new_qedc12,
     &     epemfit079081_qedc12,epemfit1432_qedc12,rfitrholow_qedc12,rfitrhohig_qedc12,epemfit32cut_qedc12
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECUTFIT,RSBG,reno,
     &     RESULT,efuture,futureprecision,facbw
      COMMON/MFPI_qedc12/MP2,M2/ERR_QEDc12/IER1/POL_QEDc12/POLP,POLM
      COMMON/CHAN_qedc12/ICH
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
      COMMON/GUSA_qedc12/IGS,iso
      common /RESFIT_qedc12/facbw,IRESON
      common /RES_qedc12/iresonances
      EXTERNAL RS40_qedc12,RS40x_qedc12,RCHPT_qedc12,RCHPTnew_qedc12,rfitchptail_qedc12,
     &     rsigpp_qedc12,epemfit0814new_qedc12,
     &     epemfit079081_qedc12,epemfit1432_qedc12,rfitrholow_qedc12,rfitrhohig_qedc12,epemfit32cut_qedc12
      common /future_qedc12/efuture,futureprecision,ifuture
      iresonances_sav=iresonances
      iresonances=IRESON
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      reno=1.d0
      IER1=IER
      E=DSQRT(S)
c     CHPTCUT=0.318D0
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITMIN=0.81d0
      EFITMAX=1.40d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
      ECUTFIT=DMIN1(ECUT,40.d0)
      eps=0.02d0
      RSBG=0.d0
      IF (E.LE.CHPTCUT) THEN
         ICH=0
c take parameter uncertainty as syst error
         RSBG=rfitchptail_qedc12(s)
      ELSE IF ((E.GT.CHPTCUT).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=rfitrholow_qedc12(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         IF (IGS.EQ.0) THEN
c     ###########  Chebyshev fit
            RSBG=rfitrhohig_qedc12(s)
         ELSE
c     ########### alternative GS fit
            RSBG=rsigpp_qedc12(s,iso)
         ENDIF
      ELSE IF ((E.GT.EFITOMBG).AND.(E.LT.EFITMIN)) THEN
         ICH=3
         IF (IGS.EQ.0) THEN
c     ###########  Chebychev fit
            RSBG=epemfit079081_qedc12(s)
         ELSE
c     ########### alternative GS fit
            RSBG=rsigpp_qedc12(s,iso)
         ENDIF
      ELSE IF ((E.GE.EFITMIN).AND.(E.LE.1.4d0)) THEN
         ICH=4
         RSBG=epemfit0814new_qedc12(s)
      ELSE IF ((E.GE.1.4d0).AND.(E.LE.3.2d0)) THEN
         ICH=5
         RSBG=epemfit1432_qedc12(s)
      ELSE IF ((E.GT.3.2d0).AND.(E.LE.ECUTFIT)) THEN
c     ECUTFIT don't go beyond 15 GeV
         ICH=6
         RSBG=epemfit32cut_qedc12(s)
      ELSE
         ICH=7
         RSBG=RS40_qedc12(s,IER)
      ENDIF
      RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
c      CALL RENOoldnew(E,reno)
      rs40smoothed_qedc12=RESULT
      iresonances=iresonances_sav
      return
      end
C
      function epemfit0814new_qedc12(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,ier,IRESON,ini
      parameter(np=8,nm=12)
      real *8 epemfit0814new_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc12
      dimension aresl(nm),astal(nm),asysl(nm)
      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(np),astafr(np),asysfr(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP
      real *8 fac,rbw,BW_qedc12
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      external BW_qedc12
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      COMMON/BWFI_qedc12/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
      data ini /0/
C E= 0.810 -- 1.000  updated nov 2010/ jan 2012
      DATA aresl/
     &      2.439321d0,-2.048530d0, 0.831104d0,-0.125326d0, 0.052188d0,
     &      0.014487d0, 0.007629d0, 0.004274d0, 0.005185d0,
     &      0.003991d0,-0.001869d0,
     &     -0.003154d0/
      DATA astal/
     &      0.021464d0,-0.002787d0, 0.006162d0, 0.002872d0, 0.000851d0,
     &     -0.000289d0,-0.000297d0,-0.000080d0,-0.001533d0,
     &      0.001258d0,-0.001689d0,
     &      0.000074d0/
      DATA asysl/
     &      0.024604d0, 0.000554d0, 0.010540d0, 0.002680d0, 0.001972d0,
     &      0.000968d0, 0.000562d0, 0.000409d0,-0.001001d0,
     &      0.001660d0,-0.000960d0,
     &      0.000518d0/
cC E= 1.062 -- 1.438
c E=1.040 -- 1.435   updated nov 2010 / jan 2012
      DATA aresh/
     &      1.226691d0, 0.248344d0, 0.271946d0,-0.143934d0, 0.074335d0,
     &     -0.061532d0, 0.028585d0,-0.026130d0, 0.015735d0,
     &     -0.010816d0, 0.012122d0,
     &     -0.007737d0/
      DATA astah/
     &      0.043549d0, 0.026673d0, 0.024879d0, 0.015240d0, 0.004969d0,
     &      0.000704d0,-0.003842d0,-0.008284d0,-0.004662d0,
     &     -0.008562d0,-0.002849d0,
     &     -0.004253d0/
      DATA asysh/
     &      0.081321d0, 0.061839d0, 0.046889d0, 0.017500d0, 0.014509d0,
     &     -0.001759d0,-0.003797d0,-0.010011d0,-0.009091d0,
     &     -0.010701d0,-0.005498d0,
     &     -0.006391d0/
c phi resonance background 1.0090319999999999 - 1.0300430000000000 [applies for 1.00 - 1.04]
c modified  1.0003000000000000 -- 1.0397000000000001
c  1.00001       --    1.0380000 
c update jan 2012
      DATA aresfr/
     &      0.793152d0, 0.235214d0, 0.000006d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA astafr/
     &      0.026527d0,-0.002615d0, 0.001625d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
      DATA asysfr/
     &      0.019227d0, 0.004214d0, 0.004065d0, 0.000000d0, 0.000000d0,
     &      0.000000d0, 0.000000d0, 0.000000d0/
c Fit below phi  0.95899999999999996  -- 1.00000000000000000
c  0.958  --   1.0 update jan 2012
      DATA aresfm/
     &      1.069826d0, 0.037464d0, 0.053391d0, 0.018139d0, 0.002488d0,
     &     -0.016165d0,-0.015754d0,-0.002390d0/
      DATA astafm/
     &      0.022201d0, 0.010243d0, 0.002071d0,-0.001490d0,-0.002385d0,
     &     -0.005060d0,-0.005569d0,-0.001494d0/
      DATA asysfm/
     &      0.026476d0, 0.011579d0, 0.003208d0, 0.000891d0, 0.001624d0,
     &      0.000062d0,-0.001375d0, 0.000743d0/
c
      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.95899999999999996d0
      EFIMX=1.00000000000000000d0
      EFIPX=1.0400000000000000d0
      EFIPP=1.1120000000000001d0
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
         e1=0.81d0
         en=1.00d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
         e1=0.958d0
         en=1.000d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
c update jan 2012
         e1=1.00001d0
         en=1.03800d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresfr,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astafr,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysfr,np)
         endif
c test background subtraction for phi
c         if (ini.eq.0) then            
c            ini=1
c            etest0=1.00d0
c            etest1=1.04d0
c            stest0=etest0**2
c            stest1=etest1**2
c            write (*,*) ' R phi at:',etest0,fac*BW_qedc12(stest0,MFI,GFI,PFI)
c            write (*,*) ' R phi at:',etest1,fac*BW_qedc12(stest1,MFI,GFI,PFI)
c         endif
         if (IRESON.eq.1) then
            rbw=fac*BW_qedc12(sMeV,MFI,GFI,PFI)
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else 
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
       else if (e.lt.1.435d0) then
         e1=1.040d0
         en=1.435d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: s out of fit range in epemfit0814new_qedc12'
      endif
      epemfit0814new_qedc12=yfit
      return
      end
c
      function ffppg2fit0810_qedc12(s)
c fitted multi hadron channels n>2 (G2 files from dataset0821 in ../for/vap/dat/FFPPG2.DAT)
      implicit none
      integer np,ier
      parameter(np=12)
      real *8 ffppg2fit0810_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.81 -- 1.00  update jan 2012   ! fit fot n>2 channels only
      DATA ares/
     &      1.038075d0, 0.063119d0, 0.707565d0,-0.021556d0, 0.191664d0,
     &     -0.004425d0, 0.087555d0, 0.017327d0, 0.029879d0,
     &      0.006736d0, 0.003327d0,
     &     -0.007956d0/
      DATA asta/
     &      0.037058d0, 0.023711d0, 0.015714d0,-0.006845d0, 0.001184d0,
     &      0.000417d0, 0.006120d0, 0.002464d0, 0.001233d0,
     &     -0.000660d0, 0.000289d0,
     &     -0.000634d0/
      DATA asys/
     &      0.056102d0, 0.024661d0,-0.000855d0,-0.008029d0,-0.003141d0,
     &      0.002908d0, 0.003369d0, 0.001043d0,-0.000029d0,
     &      0.001417d0, 0.002447d0,
     &      0.001946d0/
      e=sqrt(s)
      yfit=0.d0
      e1=0.81d0
      en=1.00d0
      if ((e.gt.en).or.(e.lt.e1)) then
         ffppg2fit0810_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      ffppg2fit0810_qedc12=yfit
      return
      end
c
      function ffppmfit0810_qedc12(s)
c fitted multi hadron channels pp + n>2 (M files from dataset0821 in ../for/vap/dat/FFPPM.DAT)
      implicit none
      integer np,ier
      parameter(np=12)
      real *8 ffppmfit0810_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.81 -- 1.00  update jan 2012  ! fit fot all channels 
      DATA ares/
     &     11.522994d0,-10.04007d0, 4.064433d0,-0.688488d0, 0.251696d0,
     &      0.062622d0, 0.037640d0, 0.026871d0, 0.031144d0,
     &      0.032348d0,-0.004555d0,
     &      0.006154d0/
      DATA asta/
     &      0.100369d0,-0.017197d0, 0.028557d0, 0.012768d0, 0.003614d0,
     &     -0.001529d0,-0.001784d0,-0.000175d0,-0.007417d0,
     &      0.006099d0,-0.007899d0,
     &      0.000654d0/
      DATA asys/
     &      0.014269d0, 0.012159d0, 0.005559d0, 0.002192d0, 0.000810d0,
     &      0.000255d0, 0.000041d0,-0.000273d0,-0.000465d0,
     &      0.000206d0,-0.000295d0,
     &     -0.000267d0/
      e=sqrt(s)
      yfit=0.d0
      e1=0.81d0
      en=1.00d0
      if ((e.gt.en).or.(e.lt.e1)) then
         ffppmfit0810_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      ffppmfit0810_qedc12=yfit
      return
      end
c
      function epemfit079081_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=7)
      real *8 epemfit079081_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12,MOM,GOM,POM,fsta2,fsys2
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      real *8 UGM2,sMeV
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc12
      external BW_qedc12
      COMMON/BWOM_qedc12/MOM,GOM,POM
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
c 0.787 -- 0.8099 ares epemfit079081_qedc12  update jan 2012
      DATA ares/
     &      5.581719d0,-0.392033d0,-0.072155d0,-0.009031d0, 0.024816d0,
     &      0.003628d0,-0.003445d0/
      DATA asta/
     &      0.033043d0,-0.004121d0,-0.004202d0,-0.001458d0, 0.000874d0,
     &     -0.001032d0, 0.001473d0/
      DATA asys/
     &      0.025671d0,-0.003349d0,-0.001242d0,-0.003109d0, 0.002712d0,
     &     -0.001129d0, 0.001101d0/
c omega  .41871054d0,.810d0
      UGM2=1.D6
      sMeV=s*UGM2
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.787d0
      en=0.8099d0
      if ((e.gt.0.81d0).or.(e.lt.e1)) then
         epemfit079081_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      if ((IRESON.eq.1).and.(e.ge.EOMM).and.(e.lt.EOMP)) then
         rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               yfit=sqrt(yfit**2+rbw**2)
            else 
               if (ier.eq.3) rbw=rbw*fsys2
               yfit=yfit+rbw
            endif
      endif
      epemfit079081_qedc12=yfit
      return
      end
c
      function epemfit1432_qedc12(s)
      implicit none
      integer np,nm,nx,ier,IRESON
      parameter(np=12,nm=6,nx=8)
      real *8 epemfit1432_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,aresmed,astamed,asysmed,
     &     ares,asta,asys,ares1,asta1,asys1,
     &     arestoBES,astatoBES,asystoBES,polynom_qedc12,eex,ebe
      real *8 fres2330,fsta2330,fsys2330,fres2023,fsta2023,fsys2023
      dimension areslow(np),astalow(np),asyslow(np)
      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(np),asta(np),asys(np)
      dimension ares1(np),asta1(np),asys1(np)
      dimension arestoBES(nm),astatoBES(nm),asystoBES(nm)
      dimension fres2330(nx),fsta2330(nx),fsys2330(nx)
      dimension fres2023(nm),fsta2023(nm),fsys2023(nm)
      REAL*8 MPS(6),GPS(6),PPS(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      real *8 fac,rbw,BW_qedc12,UGM2,sMeV
      external BW_qedc12
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
C common Psi and Upsilon parameters set in subroutine resonances
      COMMON/PSYPPAR_qedc12/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
c 1.377 -- 2.000
c   1.375  --    2.038 update jan 2012
      DATA areslow/
     &      1.977167d0, 0.225078d0,-0.182452d0, 0.242622d0, 0.105562d0,
     &     -0.000856d0,-0.008874d0,-0.046840d0, 0.019918d0,
     &      0.021384d0,-0.028723d0,
     &      0.017955d0/
      DATA astalow/
     &      0.080054d0, 0.013831d0,-0.021760d0,-0.005745d0,-0.009165d0,
     &      0.008690d0,-0.001083d0, 0.009951d0, 0.001225d0,
     &     -0.000504d0, 0.003878d0,
     &     -0.001298d0/
      DATA asyslow/
     &      0.155450d0,-0.027184d0,-0.069519d0, 0.015412d0,-0.020308d0,
     &      0.004543d0,-0.003235d0, 0.012758d0, 0.007980d0,
     &     -0.003315d0, 0.004986d0,
     &      0.003720d0/
c   1.4  --  2.0
c   1.395 -- 2.000 update jan 2012
      DATA ares/
     &      1.963491d0, 0.113703d0,-0.238320d0, 0.181680d0, 0.081183d0,
     &      0.025857d0, 0.008489d0,-0.042440d0, 0.037159d0,
     &      0.015929d0,-0.007181d0,
     &      0.000000d0/
      DATA asta/
     &      0.090092d0, 0.011562d0,-0.006726d0,-0.011308d0,-0.007070d0,
     &      0.000075d0,-0.009651d0, 0.005419d0,-0.011861d0,
     &     -0.001606d0,-0.009214d0,
     &      0.000000d0/
      DATA asys/
     &      0.177501d0,-0.026116d0,-0.040600d0, 0.009703d0,-0.013553d0,
     &     -0.004073d0,-0.015692d0, 0.004596d0,-0.009677d0,
     &     -0.005816d0,-0.014458d0,
     &      0.000000d0/
c  2.9 -- 3.70 updated jan 2012
      DATA ares1/
     &      2.194522d0, 0.024297d0,-0.074867d0,-0.048941d0, 0.033601d0,
     &      0.031717d0,-0.002783d0,-0.007086d0,-0.003088d0,
     &     -0.003499d0, 0.000111d0,
     &     -0.000200d0/
      DATA asta1/
     &      0.047837d0, 0.012756d0,-0.053928d0,-0.022662d0, 0.019903d0,
     &      0.010472d0, 0.002614d0, 0.007182d0,-0.005872d0,
     &     -0.013104d0, 0.000292d0,
     &      0.006289d0/
      DATA asys1/
     &      0.053237d0, 0.014822d0,-0.049601d0,-0.022020d0, 0.017290d0,
     &      0.008353d0, 0.002799d0, 0.008277d0,-0.004892d0,
     &     -0.013124d0,-0.001201d0,
     &      0.005196d0/
c  2.25 -- 3.0 checked jan 2012
      DATA fres2330 /
     &      2.227259d0,-0.123906d0, 0.056485d0,-0.025120d0, 0.001443d0,
     &      0.008173d0,-0.005148d0,-0.000646d0/
      DATA fsta2330 /
     &      0.034657d0,-0.036767d0, 0.020173d0,-0.007745d0,-0.000017d0,
     &      0.005023d0,-0.003267d0,-0.000193d0/
      DATA fsys2330 /
     &      0.038368d0,-0.032680d0, 0.017432d0,-0.008330d0, 0.001683d0,
     &      0.003700d0,-0.002537d0,-0.000697d0/
c  1.981 -- 2.25
c  1.975 -- 2.243 update jan 2012
      DATA fres2023 /
     &      2.341421d0, 0.244132d0,-0.173871d0, 0.058753d0,-0.003435d0,
     &     -0.013606d0/
      DATA fsta2023 /
     &      0.078837d0,-0.007284d0, 0.018281d0, 0.008169d0, 0.019433d0,
     &     -0.021668d0/
      DATA fsys2023 /
     &      0.086736d0,-0.022295d0, 0.030760d0,-0.001273d0, 0.024826d0,
     &     -0.024631d0/
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      eex=1.8590000000000000d0
      ebe=2.6000000000000001d0
c smoothing overlapps exten down to 1.38
c      if ((e.lt.1.4d0).or.(e.gt.3.2d0)) then
      if ((e.lt.1.38d0).or.(e.gt.3.2d0)) then
         epemfit1432_qedc12=0.d0
         return
      else if (e.le.1.45d0) then
c   1.375  --  2.038
         e1=1.375d0
         en=2.038d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asyslow,np)
         endif 
      else if (e.lt.1.985d0) then
c   1.4  --  2.0
c   1.395 -- 2.000
         e1=1.395d0
         en=2.0d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,ares,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,asta,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asys,np)
         endif
      else if (e.lt.2.25d0) then
c  1.975 -- 2.243 update jan 2012
         e1=1.975d0
         en=2.243d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2023,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2023,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2023,nm)
         endif
      else if (e.lt.3.00d0) then
         e1=2.25d0
         en=3.0d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,fres2330,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,fsta2330,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,fsys2330,nx)
         endif
      else if (e.le.3.2d0) then
         e1=2.9d0
         en=3.69696d0
c         e1=2.0d0
c         en=3.2d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,ares1,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,asta1,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asys1,np)
         endif
         if (IRESON.eq.1) then
c psi1  3.08587D0 3.10787D0
            if ((e.ge.EMIPS(1)).and.(e.le.EMAPS(1))) then ! psi1
               rbw=fac*BW_qedc12(sMeV,MPS(1),GPS(1),PPS(1))
               if (ier.eq.2) then
                  rbw=rbw*STAPS(1)
                  yfit=sqrt(yfit**2+rbw**2)
               else 
                  if (ier.eq.3) rbw=rbw*SYSPS(1)
                  yfit=yfit+rbw
               endif
            endif
         endif
      endif
         epemfit1432_qedc12=yfit
      return
      end
c
      function epemfit32cut_qedc12(s)
      implicit none
      integer i,nx,np,nm,nh,ni,nb,ier,IRESON,iresonances,iresonances_sav
      parameter(nx=12,nb=9,np=6,nm=6,ni=5,nh=3)
      real *8 epemfit32cut_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,rs40_qedc12,rs40x_qedc12,
     &    ares1,asta1,asys1,aresi,astai,asysi,aresin,astain,asysin,
     &    aresh,astah,asysh,aresy,astay,asysy,aresbes,astabes,asysbes,
     &    polynom_qedc12,RSpQCD_qedc12
      dimension ares1(nx),asta1(nx),asys1(nx)
      dimension aresi(ni),astai(ni),asysi(ni)
      dimension aresin(np),astain(np),asysin(np)
      dimension aresh(nh),astah(nh),asysh(nh)
      dimension aresy(ni),astay(ni),asysy(ni)
      dimension aresbes(nb),astabes(nb),asysbes(nb)
      REAL*8 MPS(6),GPS(6),PPS(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 fac,rbw,BW_qedc12,CHPTCUT,EC,ECUT,RSx,ECUTFIT1,ECUTFIT2
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      REAL*8 UGM2,sMeV
      external BW_qedc12,RS40_qedc12,RS40x_qedc12,RSpQCD_qedc12
      COMMON/RESFIT_qedc12/fac,IRESON
      common /RES_qedc12/iresonances
      COMMON/ERR_qedc12/IER
      COMMON/RCUTS_qedc12/CHPTCUT,EC,ECUT
C cpmmon Psi and Upsilon parameters set in subroutine resonances
      COMMON/PSYPPAR_qedc12/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
c  2.9 -- 3.70 update jab 2012
      DATA ares1/
     &      2.194522d0, 0.024297d0,-0.074867d0,-0.048941d0, 0.033601d0,
     &      0.031717d0,-0.002783d0,-0.007086d0,-0.003088d0,
     &     -0.003499d0, 0.000111d0,
     &     -0.000200d0/
      DATA asta1/
     &      0.047837d0, 0.012756d0,-0.053928d0,-0.022662d0, 0.019903d0,
     &      0.010472d0, 0.002614d0, 0.007182d0,-0.005872d0,
     &     -0.013104d0, 0.000292d0,
     &      0.006289d0/
      DATA asys1/
     &      0.053237d0, 0.014822d0,-0.049601d0,-0.022020d0, 0.017290d0,
     &      0.008353d0, 0.002799d0, 0.008277d0,-0.004892d0,
     &     -0.013124d0,-0.001201d0,
     &      0.005196d0/
c 3.60 -- 3.784795
c  3.598  --  3.787 updated jan2012
      DATA aresi/
     &      2.230315d0, 0.126494d0, 0.056245d0,-0.002913d0,-0.014830d0/
      DATA astai/
     &      0.039262d0, 0.041398d0, 0.020083d0, 0.002638d0,-0.003981d0/
      DATA asysi/
     &      0.060126d0, 0.057355d0, 0.024308d0,-0.000984d0,-0.009376d0/
c 3.598 -- 3.71  feb 2012 new
      DATA aresin/
     &      2.146929d0,-0.000366d0,-0.000543d0,-0.000434d0, 0.000060d0,
     &      0.000301d0/
      DATA astain/
     &      0.013296d0, 0.000036d0,-0.000719d0,-0.000642d0,-0.000090d0,
     &      0.000101d0/
      DATA asysin/
     &      0.022960d0, 0.000081d0,-0.001754d0,-0.000648d0, 0.000132d0,
     &      0.000563d0/
c 4.57 -- 9.5 checked jan 2012
      DATA aresbes/
     &      3.512887d0, 0.026536d0, 0.005062d0,-0.047693d0,-0.008484d0,
     &      0.044371d0,-0.012137d0,-0.007926d0, 0.015012d0/
      DATA astabes/
     &      0.066957d0,-0.046665d0, 0.008573d0, 0.013964d0,-0.008290d0,
     &      0.002806d0,-0.001983d0, 0.008892d0,-0.009458d0/
      DATA asysbes/
     &      0.104511d0,-0.067159d0,-0.002493d0, 0.035067d0,-0.015540d0,
     &     -0.010895d0, 0.005619d0, 0.009474d0,-0.008767d0/
c 9.5 -- 14.0 update jan 2012
      DATA aresy /
     &      3.702392d0, 0.211365d0, 0.018470d0, 0.018815d0, 0.044754d0/
      DATA astay /
     &      0.106659d0, 0.069533d0,-0.022172d0,-0.022854d0,-0.000350d0/
      DATA asysy /
     &      0.093281d0, 0.008952d0,-0.043419d0, 0.006921d0, 0.031207d0/
c  14.04 -- 38.29 checked mar 2012
      DATA aresh /
     &      3.939649d0,-0.069492d0,-0.025035d0/
      DATA astah /
     &      0.112354d0,-0.034319d0,-0.011809d0/
      DATA asysh /
     &      0.103868d0, 0.010274d0, 0.015509d0/
cc  11.05 -- 40.0 checked jan 2012
c      DATA aresh /
c     &      3.891660d0, 0.005715d0,-0.064195d0/
c      DATA astah /
c     &      0.099699d0,-0.055998d0,-0.043121d0/
c      DATA asysh /
c     &      0.091422d0,-0.050119d0,-0.038485d0/
      iresonances_sav=iresonances
      iresonances=IRESON
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      ECUTFIT1=DMIN1(ECUT,11.0d0)
      ECUTFIT2=DMIN1(ECUT,40.0d0)
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
      if ((e.lt.3.2d0).or.(e.gt.40.d0)) then
         epemfit32cut_qedc12=0.d0
         return
      else if (e.gt.ECUT) then
         epemfit32cut_qedc12=RSpQCD_qedc12(s,IER)
         return
      else if (e.lt.3.6d0) then
         e1=2.9d0
         en=3.7d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,ares1,nx)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,asta1,nx)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asys1,nx)
         endif
         if (IRESON.eq.1) then
            do i=1,3 
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc12(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else 
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.lt.3.71d0) then
c 3.598  --  3.710
         e1=3.598d0
         en=3.710d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresin,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astain,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysin,np)
         endif
         if (IRESON.eq.1) then
            do i=1,3 
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc12(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else 
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.lt.3.78d0) then
c 3.598  --  3.787
         e1=3.598d0
         en=3.787d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresi,ni)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astai,ni)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysi,ni)
         endif
         if (IRESON.eq.1) then
            do i=1,3 
               if ((E.GT.EMIPS(i)).and.(E.LT.EMAPS(i))) then
                  rbw=fac*BW_qedc12(sMeV,MPS(i),GPS(i),PPS(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAPS(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else 
                     if (ier.eq.3) rbw=rbw*SYSPS(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if (e.lt.4.5d0) then
c it is important here to select 4.5 not 4.4 to get smooth transition
c psi3  3.7000D0  3.8400D0
c psi4  3.960D0   4.098D0
c psi5  4.102D0   4.275D0
c psi6  4.315D0   4.515D0
         yfit=RS40_qedc12(s,IER)
      else if ((e.ge.EC).and.(e.le.9.5d0)) then
         yfit=RSpQCD_qedc12(s,IER)
      else if (e.lt.9.5d0) then
         e1=4.57d0
         en=9.5d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresbes,nb)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astabes,nb)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysbes,nb)
         endif
         if (IRESON.eq.1) then
            i=1 
            if ((E.GT.EMIYP(i)).and.(E.LT.EMAYP(i))) then
               rbw=fac*BW_qedc12(sMeV,MYP(i),GYP(i),PYP(i))
               if (ier.eq.2) then
                  rbw=rbw*STAYP(i)
                  yfit=sqrt(yfit**2+rbw**2)
               else 
                  if (ier.eq.3) rbw=rbw*SYSYP(i)
                  yfit=yfit+rbw
               endif
            endif
         endif
      else if ((e.ge.9.5d0).and.(e.le.14.d0)) then
         e1=09.50d0
         en=14.00d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc12(ex,aresy,ni)
         else if (ier.eq.2) then
            yfit=polynom_qedc12(ex,astay,ni)
         else if (ier.eq.3) then
            yfit=polynom_qedc12(ex,asysy,ni)
         endif
         if (IRESON.eq.1) then
            do i=2,6 
               if ((E.GT.EMIYP(i)).and.(E.LT.EMAYP(i))) then
                  rbw=fac*BW_qedc12(sMeV,MYP(i),GYP(i),PYP(i))
                  if (ier.eq.2) then
                     rbw=rbw*STAYP(i)
                     yfit=sqrt(yfit**2+rbw**2)
                  else 
                     if (ier.eq.3) rbw=rbw*SYSYP(i)
                     yfit=yfit+rbw
                  endif
               endif
            enddo
         endif
      else if ((e.ge.14.0d0).and.(e.le.40.d0)) then
            e1=14.04d0
            en=38.29d0
            es=(en+e1)
            ed=(en-e1)
            amap=2.d0/ed
            bmap=-es/ed
            ex=amap*e+bmap
            if (ier.eq.1) then
               yfit=polynom_qedc12(ex,aresh,nh)
            else if (ier.eq.2) then
               yfit=polynom_qedc12(ex,astah,nh)
            else if (ier.eq.3) then
               yfit=polynom_qedc12(ex,asysh,nh)
            endif
      endif
      epemfit32cut_qedc12=yfit
      iresonances=iresonances_sav
      return
      end
c
      function rfitrholow_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=14)
      real *8 rfitrholow_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12,MOM,GOM,POM,fsta2,fsys2
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc12
      REAL*8 UGM2,sMeV
      external BW_qedc12
      COMMON/BWOM_qedc12/MOM,GOM,POM
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
c 0.318 -- 0.778
c updated nov 2010 (incl BaBar/KlOE) / jan 2012
      DATA ares/
     &      2.754373d0, 4.204656d0, 2.055732d0, 0.602528d0,-0.111083d0,
     &     -0.285268d0,-0.209948d0,-0.088469d0, 0.001929d0,
     &      0.039986d0, 0.041485d0,
     &      0.029585d0, 0.016526d0, 0.005835d0/
      DATA asta/
     &      0.017361d0, 0.018817d0, 0.007041d0, 0.004477d0, 0.003014d0,
     &      0.001324d0,-0.000296d0, 0.000447d0, 0.001254d0,
     &      0.000630d0, 0.000070d0,
     &     -0.000098d0, 0.000457d0, 0.000302d0/
      DATA asys/
     &      0.011308d0, 0.016195d0, 0.009654d0, 0.005253d0, 0.002977d0,
     &      0.001704d0, 0.001078d0, 0.001322d0, 0.001466d0,
     &      0.000885d0, 0.000241d0,
     &     -0.000179d0,-0.000097d0,-0.000045d0/
c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      e1=0.318d0
      en=0.778d0
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitrholow_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      if ((IRESON.eq.1).and.(e.ge.EOMM).and.(e.lt.EOMP)) then
         rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
         if (ier.eq.2) then
            rbw=rbw*fsta2
            yfit=sqrt(yfit**2+rbw**2)
         else 
            if (ier.eq.3) rbw=rbw*fsys2
            yfit=yfit+rbw
         endif
      endif
      rfitrholow_qedc12=yfit
      return
      end
c
      function rfitrhohig_qedc12(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=9)
      real *8 rfitrhohig_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12,MOM,GOM,POM,fsta2,fsys2
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      dimension ares(np),asta(np),asys(np)
      real *8 fac,rbw,BW_qedc12
      REAL*8 UGM2,sMeV
      external BW_qedc12
      COMMON/BWOM_qedc12/MOM,GOM,POM
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
c 0.775 -- 0.787
c  0.769  -- 0.789 update jan 2012
      DATA ares/
     &      7.697969d0,-1.690342d0,-0.628978d0, 0.350544d0, 0.338475d0,
     &     -0.032064d0,-0.126662d0,-0.062357d0, 0.047245d0/
      DATA asta/
     &      0.044414d0,-0.004755d0,-0.016680d0, 0.001380d0, 0.000941d0,
     &      0.001245d0,-0.000321d0,-0.004968d0, 0.009680d0/
      DATA asys/
     &      0.039822d0,-0.004340d0,-0.018841d0, 0.002996d0,-0.000349d0,
     &     -0.000322d0,-0.000909d0,-0.004131d0, 0.009610d0/
c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      e1=0.769
      en=0.789
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitrhohig_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      if ((IRESON.eq.1).and.(e.ge.EOMM).and.(e.lt.EOMP)) then
         rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
         if (ier.eq.2) then
            rbw=rbw*fsta2
            yfit=sqrt(yfit**2+rbw**2)
         else 
            if (ier.eq.3) rbw=rbw*fsys2
            yfit=yfit+rbw
         endif
      endif
      rfitrhohig_qedc12=yfit
      return
      end
c
      function rfitchptail_qedc12(s)
      implicit none
      integer np,ier
      parameter(np=3)
      real *8 rfitchptail_qedc12,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc12
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc12/IER
c 0.285 - 0.325
c new feb 2012
      DATA ares/
     &      0.018753d0, 0.018778d0, 0.002144d0/
      DATA asta/
     &      0.000265d0, 0.000483d0, 0.000242d0/
      DATA asys/
     &      0.000295d0, 0.000353d0, 0.000089d0/
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.285d0
      en=0.325d0
      if ((e.gt.en).or.(e.lt.e1)) then
         rfitchptail_qedc12=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc12(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc12(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc12(ex,asys,np)
      endif
      rfitchptail_qedc12=yfit
      return
      end
c

      function polynom_qedc12(x,a,na)
C map x to interval [-1,+1]
      implicit none
      INTEGER na,i
      REAL*8 x,a,polynom_qedc12,t
      dimension a(na),t(na)
c      Tschebycheff Polynomials
      t(1)=1.d0
      t(2)=x
      do i=1,na-2
         t(i+2)=2.d0*x*t(i+1)-t(i)
      enddo
      polynom_qedc12=0.d0
      do i=1,na
         polynom_qedc12=polynom_qedc12+a(i)*t(i)
      enddo
      return
      end
c
      function rsigpp_qedc12(s,iso)
c modified jan 2012 ffppg2 --> ffppm incl all channels
      implicit none
      integer iso,iopt,ioptn,ioptc,IER,IRESON
      real *8 rsigpp_qedc12,s,sMeV,e,y,beta,SM,UGM2,M2,e1,en,
     &             fpi2rnew_qedc12,ffppmfit0810_qedc12,epemfit0814new_qedc12,eps
      real *8 MOM,GOM,POM,fsta2,fsys2
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      real *8 fac,rbw,BW_qedc12
      double precision MPLUS,MZERO,MPI2
      external fpi2rnew_qedc12,ffppmfit0810_qedc12,epemfit0814new_qedc12
      external BW_qedc12
      common/pions_qedc12/MPLUS,MZERO
      COMMON/BWOM_qedc12/MOM,GOM,POM
      COMMON/RESRELERR_qedc12/FSTA,FSYS
      COMMON/RESDOMAINS_qedc12/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc12/fac,IRESON
      COMMON/ERR_qedc12/IER
c omega  .41871054d0,.810d0
      fsta2=FSTA(2)
      fsys2=FSYS(2)
c
      e=sqrt(s)
      if ((e.gt.1.d0).or.(e.lt.0.61d0)) then
         write (*,*)  ' Warning: s out of fit range in rsigpp_qedc12'
      endif
      ioptc=32 ! all 3 rho's with quadratic energy dependent width as used by Belle 2008
      ioptn=11 ! rho only  with linear energy dependent width as used by CMD-2 1999 ....
      iopt=ioptn
      eps=0.02
c      MPLUS=139.56995D0 now set in constants.f
      MPI2=MPLUS**2
      UGM2=1.D6
      M2=MPI2/UGM2
      sMeV=s*UGM2
      beta=SQRT(1.D0-4.D0*MPI2/sMeV)
c join GS parametrizaton and ffpp fit
      e1=0.81d0
      en=1.00d0
      if (e.le.e1) then
         y=beta**3*fpi2rnew_qedc12(sMEV,iso,iopt)/4.d0
c     error of pipi data
         IF (IER.EQ.2) y=y*0.02d0
         IF (IER.EQ.3) y=y*0.006D0
         if ((IRESON.eq.1).and.(e.ge.EOMM).and.(e.lt.EOMP)) then
            rbw=fac*BW_qedc12(sMeV,MOM,GOM,POM)
            if (ier.eq.2) then
               rbw=rbw*fsta2
               y=sqrt(y**2+rbw**2)
            else 
               if (ier.eq.3) rbw=rbw*fsys2
               y=y+rbw
            endif
         endif
      else if (e.gt.en) then
         y=epemfit0814new_qedc12(s)
      else
         y=beta**3/4.d0*ffppmfit0810_qedc12(s)
      endif
      rsigpp_qedc12=y
      return
      end
c
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; fpi2rnew_qedc12.f --- 
c;; Author          : Fred Jegerlehner
c;; Created On      : Thu May  1 23:40:25 2008
c;; Last Modified By: Fred Jegerlehner
c;; Last Modified On: Fri Aug  1 11:07:30 2008
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2008 Fred Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; 
      function fpi2rnew_qedc12(s,iso,iopt)
c    PION FORM FACTOR  (absolute square)
c CMD-2 parametrization December 2001; update August 2003    
c version extended by rho--omega mixing flag iso : 
c iso=1 calculate I=1 part only (omega switched off), iso=0 I=0 part else sum
c 01/08/2008 new option for energy dependent widths
c iopt=12 rho only quadratic, 32 all 3 rho's quadratic 
c iopt=11 rho only linear, 31 all 3 rho's linear
c iopt anything else [0,1,...] old CMD-2 version = iopt=11 
      implicit none
      character*4 DSET
      integer np,iso,iopt,i,ini
      parameter(np=8)
      complex*16 cI,cone,cdels,cepdom,cbeta,cgama,cnorm,cfpis,cfpis1,
     &           cBWnu,cBWde,cBWGSr0,cBWGSr1,cBWom,cBWGSr(3)
      real *8 fpi2rnew_qedc12,fpi2rnew1,fpi2rnewtot,fpi2rnewie1,
     &        s,e,xp,rats,beps,ppis,lgs,hs,pi,ratm,bepm,ppim,lgm,hm,
     &        dhdsm,fs,d,grs,rBWnu
      real *8 a(np),mrho(3),grho(3),mr,gr,mp2,mr2,mo2,mr4,
     &     beta,phibeta,delta,phidelta,deltaarg,betaarg,gamaarg
      real *8 mpi,dmro(3),dgro(3),ddelta,dphidel,dbeta,dphibet,
     &     gama,dgama,phigama,dphigam,momega,dmomega,gomega,dgomega,
     &     mp,mo,go
      COMMON /DASE_qedc12/DSET
      data pi /3.141592653589793d0/
      data ini /0/
      save
      if (DSET.ne.'WAVE') DSET='CMD2'
c      DSET='WAVE'
c Constants, Parameters
c      alp=1.D0/137.035999084D0  ! pm (51) April 2008
C CMD-2 Fit parameters 2003 PDG 2006
c      mpi0  =134.9766           !+/- 0.0006  neutral pion
      mpi    =139.57018d0     !+/- 0.00035  mass of charged pion
      mrho(1)=775.5d0         ! PDG 2006
      dmro(1)=0.4d0           
      grho(1)=143.85d0
      dgro(1)=1.5d0           
      mrho(2)=1446.0d0    ! take values from Belle fit
      dmro(2)=29.0d0          
      grho(2)=434.0d0
      dgro(2)=62.0d0          
c      mrho(2)=1459.0d0      ! PDG 2008
c      dmro(2)=11.0d0          
c      grho(2)=171.0d0
c      dgro(2)=50.0d0          
      mrho(3)=1728.8d0      ! take values from Belle fit
      dmro(3)=91.0d0
      grho(3)=193.0d0        
      dgro(3)=62.0d0
c      mrho(3)=1688.8d0    ! PDG 2006
c      dmro(3)=2.1d0
c      grho(3)=161.0d0        
c      dgro(3)=10.0d0
      delta =1.57d-3       ! CMD2 2002 not changed  (ckeck 2008)
      ddelta=0.16d-3         ! +/- 0.15d-3 +/- 0.05d-3   modulus of delta
      phidelta=10.4d0        ! CMD2 hep-ex/0610021v3
      dphidel=3.8d0           ! +/- 1.6 +/- 3.5       phase angle in degrees
c      beta   =0.0859d0        ! CMD2 hep-ex/0610021v3
      beta   =0.08d0        ! CMD2 hep-ex/0610021v3
      dbeta  =0.0040d0        ! +/-  .0030 +/- .0027
      phibeta=180.0d0
      dphibet=10.0d0
      gama   =0.000d0         ! */-  0.009
      gama   =0.028d0         ! */-  0.009
      dgama  =0.028d0
      phigama=24.000d0
      dphigam=24.000d0
      momega =782.65d0       ! +/- 0.12 PDG 2004/2006
      dmomega=000.12d0      
      gomega =  8.49d0         ! +/- 0.08  PDG 2004/2006
      dgomega=  0.08d0        
      if (ini.eq.0) then
c CMD2 fit May 2008 chi^2=29/51 and S=0.7; incl. SND would give 159/51 and S=1.7
      if (DSET.eq.'CMD2') then
      a(1)=7.7437D+02  
      a(2)=1.4421D+02  
      a(3)=9.9859D-02
      a(4)=1.8000D+02  
      a(5)=1.4460D+03  
      a(6)=4.3400D+02
      a(7)=1.8300D-03  
      a(8)=1.0600D+01
      else if (DSET.eq.'WAVE') then
      a(1)=7.7449D+02  
      a(2)=1.5063D+02  
      a(3)=1.2615D-01
      a(4)=1.8000D+02  
      a(5)=1.4460D+03  
      a(6)=4.3400D+02
      a(7)=1.988D-03  
      a(8)=0.900D+01
      else
      endif
c      a(1)=7.7359D+02    ! mrho(1)   
c      a(2)=1.4305D+02    ! grho(1) 
c      a(3)=9.2313D-02    ! beta    
c      a(4)=1.8000D+02    ! phibeta 
c      a(5)=1.4590D+03    ! mrho(2) 
c      a(6)=1.7100D+02    ! grho(2) 
c      a(7)=1.5700D-03    ! delta   
c      a(8)=1.0400D+01    ! phidelta
      ini=1
      endif

      mrho(1) =a(1)
      grho(1) =a(2)
      beta    =a(3)
      phibeta =a(4)
      mrho(2) =a(5)
      grho(2) =a(6)
      delta   =a(7)
      phidelta=a(8)
      
      mp=mpi
      mo=momega
      go=gomega
 
      cI   =DCMPLX(0.D0,1.D0)
      cone =DCMPLX(1.d0,0.0d0)
      mp2  =mp*mp
      mo2  =mo*mo
c Auxiliary variables
      e    =dsqrt(s)
      xp   =4.d0*mp2
      rats =xp/s
      beps =dsqrt(1.d0-rats)
      ppis =0.5d0*e *beps
      lgs  =dlog((e +2.d0*ppis)/(2.d0*mp))
      hs   =2.d0/pi*ppis/e *lgs
      do i=1,3 
        mr   =mrho(i)
        gr   =grho(i)
        mr2  =mr *mr
        mr4  =mr2*mr2
        ratm =xp/mr2
        bepm =dsqrt(1.d0-ratm)
        ppim =0.5d0*mr*bepm
        lgm  =dlog((mr+2.d0*ppim)/(2.d0*mp))
        hm   =2.d0/pi*ppim/mr*lgm
        dhdsm=hm*(0.125d0/ppim**2-0.5d0/mr2)+0.5d0/pi/mr2
        fs   =gr*mr2/ppim**3*(ppis**2*(hs-hm)+(mr2-s)*ppim**2*dhdsm)
        d    =3.d0/pi*mp2/ppim**2*lgm+mr/2.d0/pi/ppim-mp2*mr/pi/ppim**3
        if (iopt.eq.11) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)
           else 
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.31) then
           grs=gr*(ppis/ppim)**3*(mr/e)
        else if (iopt.eq.12) then
           if (i.eq.1) then
c     GS parametrization R.R. Akhmetshin hep-ex/9904027 take linear mr/e factor
c     in Belle 2008 M.~Fujikawa et al arXiv:0805.3773 [hep-ex] fit a quadratic (mr/e)**2 dependence is assumed
c     only rho taken with energy dependent width
c     changed from linear to quadratic mr/e 24/06/2008
              grs=gr*(ppis/ppim)**3*(mr/e)**2
           else 
              grs=gr
           endif
c option used by Belle 2008 all 3 rho's have same type of energy dependent with
        else if (iopt.eq.32) then
           grs=gr*(ppis/ppim)**3*(mr/e)**2
        else
c original CMD-2 fit version
           if (i.eq.1) then
              grs=gr*(ppis/ppim)**3*(mr/e)
           else 
              grs=gr
           endif           
        endif
        rBWnu=mr2*(1.d0+d*gr/mr)
        cBWnu=DCMPLX(rBWnu,0.D0)
        cBWde=DCMPLX(mr2-s+fs,-mr*grs)
        cBWGSr(i)=cBWnu/cBWde
      enddo  
c delta argument in radians       
      deltaarg=phidelta*2.d0*pi/360.d0
      gamaarg=phigama*2.d0*pi/360.d0
      cdels =DCMPLX(dcos(deltaarg),dsin(deltaarg))*
     &       DCMPLX(delta*s/mo2,0.d0)
      cBWom =DCMPLX(mo2,0.d0)/DCMPLX(mo2-s,-mo*go)
      cepdom=cone+cdels*cBWom
      betaarg=phibeta*2.d0*pi/360.d0
      cbeta =DCMPLX(dcos(betaarg),dsin(betaarg))*
     &       DCMPLX(beta,0.d0)
      cgama =DCMPLX(dcos(gamaarg),dsin(gamaarg))*
     &       DCMPLX(gama,0.d0)
      cnorm =cone+cbeta+cgama
      cfpis  =(cBWGSr(1)*cepdom+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      cfpis1 =(cBWGSr(1)+cbeta*cBWGSr(2)+cgama*cBWGSr(3))/cnorm
      fpi2rnewtot=cfpis *DCONJG(cfpis )
      fpi2rnewie1=cfpis1*DCONJG(cfpis1)
      if (iso.eq.0) then        
        fpi2rnew_qedc12 =fpi2rnewtot-fpi2rnewie1
      else if (iso.eq.1) then
        fpi2rnew_qedc12 =fpi2rnewie1
      else 
        fpi2rnew_qedc12 =fpi2rnewtot
      endif
      return
      end
