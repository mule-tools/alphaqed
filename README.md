# HVP contributions in McMule

This repository contains all HVP contributions used in McMule.
Historically, this was just [`alphaQED`](http://www-com.physik.hu-berlin.de/~fjeger/software.html) but it now also contains Fedor Ignatov's [NSK table](https://cmd.inp.nsk.su/~ignatov/vpl/).

Note: this repository is not the official `alphaQED` package but a mirror maintained by the [McMule collaboration](https://gitlab.com/mule-tools/) for usage in McMule.
Please [click here](http://www-com.physik.hu-berlin.de/~fjeger/software.html) for the official website of `alphaQED`.
