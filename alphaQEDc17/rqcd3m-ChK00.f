      subroutine rqcd3mn(s,R,ALINP,EINP,MTOP,FTQ,IOR,NF)
Calculation of R-value to 4-loop=O(\alpha_s^3): ior=0,...,3
C link RGQCD4 to get running parameters
c Based on:
c K.G. Chetyrkin, R.V. Harlander, J.H. K\"uhn,
c "Quartic mass corrections to R_had at O(\alpha_s^3)",
c hep-ph/0005139,
c
c ql massles (light) quarks
c Qh massive (heavy) quark
c renormalization scale mu^2=s
c nf = number of active flavors
c formflag: fflag='ana'(=analytic form), else numerical approximation
c The notation is as follows:
c The notation is as follows:
c
c r0,rq,... J_ql x J_ql  non-singlet
c rQv2,...  J_Qh x J_Qh  non-singlet     
c
c r0 = r_0
c rq2 = r_{q,2}
c rq4 = r_{q,4}
c rQv2 = r_{Q,2}^{(v)}
c rQv4 = r_{Q,4}^{(v)}
c rQa2 = r_{Q,2}^{(a)}
c rQa4 = r_{Q,4}^{(a)}
c rvsing = r^{(v)}_{sing}
c rsing = r_{sing}
c rsingp = r^\prime_{sing}
c
c api == \frac{\alpha_s}{\pi}
c ms2 == \frac{m_Q^2}{s}
c nf  == n_f
c lms == l_{ms}
c
      implicit none
      character*3 fflag
      integer NFX,FTQ,IOR,ini,NF,nql,i,loops,nf1
      real *8 r0,rq2,rq4,rQv2,rQv4,rQa2,rQa4,rvsing,rsingtt
      real *8 s,R,ALINP,EINP,MTOP,sw2,AL,E
      real *8 api,mQh2,ms2,spql,svql,saql,spq,svq,saq,
     .        qQ,vQ,aQ,rvQ,raQ,Rvec,Raxi,Rqed,r04,r05,
     .        sx,mthc,mthb,mtht
      REAL *8 MQ(6),MP(6),TH(6),rq(6),eq(6),ve(6),ax(6),mqout(6)
      real *8 null,half,third,twothird
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta4,Zeta5
      real *8 alpha, ARUN, alpha0, mu0, mu1, Mth(6), muth(6)
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
c      common /quama_qedc17/mq,mp,th
      COMMON /QUARK/MQ,MP,TH
      common /results/Rvec,Raxi,Rqed
      data pi /3.141592653589793d0/
      data zeta2,zeta3,zeta4,zeta5/1.644934066848226d0,
     . 1.202056903159594d0,1.0823232d0,1.036927755143370d0/
      data ini/0/
      save
C
      if (ini.eq.0) goto 2
 1    E=dsqrt(s)
      sx=s
      if (E.LT.1.D0) then
         R=5.d0/3.d0
         write (*,*) ' R_QCD=5/3 for E < 1 GeV used! '
         return
      elseif (E.LE.3.1d0) then
         R=2.d0   ! lowest order estimate
         write (*,*) ' R_QCD= 2  for 1 < E <= 3.1 GeV used! '
         return
      endif
c
c     fflag='   '
      fflag='ana'
c      fflag='num'
c alpha_s^{n_f}: matching conditions n_f=4 -> 5 at m_b(m_b) = 4.2 GeV
c                                        5 -> 6 at m_t(m_t) = 165 GeV
c Warnings for threshold and resonance regions, 
c where approximation is not reliable
c Approximation: If n_f is number of active flavors at energy E treat
c heaviest Q with flavor n_f massive; all n_f-1 lighter one's massless      
c      Mthc =  3.10d0        ! M(J/psi)  =3.09687d0
c      Mthb =  9.46d0        ! M(Upsilon)=9.46030d0
c      Mtht =  349.d0        ! 2 x M_top = 2 x 174.3d0
c 
      Mthc =  th(4)      
      Mthb =  th(5)
      Mtht =  th(6)
c applicability of pQCD 5.5 to 9.6 and above 12 GeV
c extend to 5 to 9.6 and above 10 GeV
      if (((E.gt.Mthc).and.(E.lt.5.00d0)).or.
     .    ((E.gt.Mthb).and.(E.lt.10.0d0))) then
c         write (*,*) '  pQCD not reliable at E:',E
      endif
* Version 1 RG
c      call ALQCD1(sx,ALINP,EINP,MTOP,AL,nfx,FTQ,IOR)
c      call ALQCD2(S,ALINP,EINP,MTOP,AL,NFX,FTQ,IOR)
c      mQh2=mq(nf)**2
* Version 2 RG
c new version using RGQCD4.f version Sep. 98
c use 4-loop RG ior -> ior+1
      call rgqcd4(sx,alinp,einp,al,mqout,nfx,ftq,ior+1)
      nf=nfx
      api=al/pi
      mQh2=mqout(nf)**2
      if (nf.eq.6) then
        mQh2=mtop**2
      endif
* Version 3 RG Oleg Veretin's routine
c  Given: 
c       alpha0(mu0) --- initial value at reference point
c       mu0         --- reference point
c       Mth(1:6)    --- array of thresholds (in On-Shell scheme) 
c       muth(1:6)   --- array of decoupling scales
c       mu1         --- new point
c       loops       --- number of loops (1,2,3,4) 
c       
c         do i=1,6 
c           Mth(i)  = mp(i)
c           muth(i) = th(i)
c         enddo
c         alpha0=alinp
c         mu0   =einp
c         loops =ior
c         mu1   =sqrt(s)
c         alpha = ARUN ( alpha0, mu0, Mth, muth, mu1, loops, nf1 )
c      nf=nf1
c      api=alpha/pi
c      mQh2=mq(nf)**2
c* end alternative RG's
      ms2=mQh2/s
      nql=nf-1             ! number of massless (light) quark flavors 
      do i=1,nql 
        rq(i) = r0(api,nf,fflag) + 
     &          rq2(ms2,api,nf,fflag) + rq4(ms2,api,nf,fflag) 
c        write (2,*) nf,mu1,r0(api,nf,fflag),
c     &          rq2(ms2,api,nf,fflag),rq4(ms2,api,nf,fflag) 
      enddo
       spql=null
       svql=null
       saql=null
       do i=1,nql
          spql=spql+eq(i)**2*rq(i)
          svql=svql+ve(i)**2*rq(i)
          saql=saql+ax(i)**2*rq(i)
       enddo
      qQ = eq(nf)
      vQ = ve(nf)
      aQ = ax(nf)
      spq=null
      svq=null
      saq=null
      do i=1,nf
         spq=spq+eq(i)
         svq=svq+ve(i)
         saq=saq+ax(i)
      enddo 
      rvQ= r0(api,nf,fflag) + 
     &     rQv2(ms2,api,nf,fflag) + rQv4(ms2,api,nf,fflag)
      write (3,*) nf,mu1,r0(api,nf,fflag), 
     &     rQv2(ms2,api,nf,fflag),rQv4(ms2,api,nf,fflag)
      raQ= r0(api,nf,fflag) + 
     &     rQa2(ms2,api,nf,fflag) + rQa4(ms2,api,nf,fflag)
c vector Z couplings
      Rvec=3.d0*(svql+vQ**2*rvQ+rvsing(ms2,api,vQ,svq,fflag))
c axial vector Z couplings
c !!!!!!! singlet contribution unclear check this
      Raxi=3.d0*(saql+aQ**2*raQ+rsingtt(ms2,api,fflag))
c vector photon couplings (QED)
      Rqed=3.d0*(spql+qQ**2*rvQ+rvsing(ms2,api,qQ,spq,fflag))
      R   = Rqed
c      write (2,*) nf,mu1,spql,qQ**2*rvQ,rvsing(ms2,api,qQ,spq,fflag) 
      return
 2    Pi2 =pi*pi
c     zeta2=pi2/6.d0
      null=0.d0
      half=0.5d0
      third=1.d0/3.d0
      twothird=2.d0*third
      R04  =10.d0*third
      R05  =11.d0*third
c q=u,d,s,c,b,t (=1,2,3,4,5,6)
c
      sw2  = 0.2230d0      ! weak mixing parameter PDG 2000
c
      sw2=0.0d0
      if (sw2.eq.0.d0) write (*,*) ' Warning sw2=0 set '
      eq(1)= twothird
      eq(2)=-third
      eq(3)=-third
      eq(4)= twothird
      eq(5)=-third
      eq(6)= twothird
      ve(1)= half-2.d0*eq(1)*sw2
      ve(2)=-half-2.d0*eq(2)*sw2
      ve(3)= ve(2)
      ve(4)= ve(1)
      ve(5)= ve(2)
      ve(6)= ve(1)
      ax(1)= half
      ax(2)=-half
      ax(3)=-half
      ax(4)= half
      ax(5)=-half
      ax(6)= half
      ini=1
      goto 1
      END

      function r0(api,nf,flag)
c Eq.3 massles contribution of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 api,r0
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      if (flag.eq.'ana') then
        r0 = (  1.d0 + api + api**2*(365.d0/24.d0 + nf*(-11.d0/12.d0 +
     &  (2.d0*Zeta3)/3.d0) - 11.d0*Zeta3) +  api**3*(87029.d0/288.d0 -
     &  (121.d0*Pi2)/48 + nf**2*(151.d0/162.d0 - Pi2/108.d0 -
     &  (19.d0*Zeta3)/27.d0) - (1103.d0*Zeta3)/4.d0 +
     &   nf*(-7847.d0/216.d0 + (11.d0*Pi2)/36.d0 + (262.d0*Zeta3)/9.d0 -
     &  (25.d0*Zeta5)/9.d0) + (275.d0*Zeta5)/6.d0))
      else
        r0 = (  1.d0 + api + api**2*(1.98571d0 - 0.115295d0*nf)
     &  +  api**3*( -6.63694d0 - 1.20013d0*nf - 0.00517836d0*nf**2))
      endif
      return
      end

      function rq2(ms2,api,nf,flag)
c Eq.4 quadratic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rq2
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      if (flag.eq.'ana') then
        rq2 = (api**3*ms2*(-80.d0 + nf*(32.d0/9.d0 - 
     &  (8.d0*Zeta3)/3.d0) + 60.d0*Zeta3))
      else
        rq2 = (api**3*ms2*(-7.87659d0 + 0.35007d0*nf))
      endif
      return
      end

      function rQv2(ms2,api,nf,flag)
c Eq.5 quadratic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rQv2
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      if (flag.eq.'ana') then
        rQv2 = (  ms2*(12.d0*api + api**2*(253.d0/2.d0 -
     &  (13.d0*nf)/3.d0) + api**3*(2442.d0 - (285.d0*Pi2)/4.d0 +
     &   nf**2*(125.d0/54.d0 - Pi2/9.d0) + (490.d0*Zeta3)/3.d0 -
     &  (5225.d0*Zeta5)/6.d0 + nf*(-4846.d0/27.d0 + (17.d0*Pi2)/3.d0 -
     &  (466.d0*Zeta3)/27.d0 + (1045.d0*Zeta5)/27.d0))))
      else
        rQv2 = (  ms2*(12.d0*api + api**2*(126.5d0 - 4.33333d0*nf)
     &   + api**3*(1032.14d0 - 104.167d0*nf + 1.21819d0*nf**2)))
      endif
      return
      end

      function rQa2(ms2,api,nf,flag)
c Eq.6 quadratic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rQa2
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5,Pi4
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      Pi4=Pi2**2
      if (flag.eq.'ana') then
        rQa2 = ( ms2*(-6.d0 - 22.d0*api + api**2*(-8221.d0/24.d0 +
     &  (19.d0*Pi2)/2.d0 + nf*(151.d0/12.d0 - Pi2/3.d0 - 4.d0*Zeta3) +
     &   117.d0*Zeta3) + api**3*(-4613165.d0/864.d0 + (670.d0*Pi2)/3.d0
     &  + (121075.d0*Zeta3)/36.d0 + nf**2*(-13171.d0/1944.d0 +
     &  (8.d0*Pi2)/27.d0 + (26.d0*Zeta3)/9.d0) - 1270.d0*Zeta5 +
     &   nf*(72197.d0/162.d0 - (209.d0*Pi2)/12.d0 + Pi4/18.d0 -
     &  (656.d0*Zeta3)/3.d0 + 55.d0*Zeta5))))
      else
        rQa2 = ( ms2*(-6.d0 - 22.d0*api + api**2*(-108.140d0 + 
     &   4.48524d0*nf) + api**3*(-409.247d0 + 73.3578d0*nf 
     &  - 0.378270d0*nf**2)))
      endif
      return
      end

      function rq4(ms2,api,nf,flag)
c Eq.8 quartic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rq4,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rq4 = ( ms4*(api**2*(13.d0/3.d0 - lms - 4.d0*Zeta3) + 
     &   api**3*(-9707.d0/144.d0 + 2.d0*lms**2 + (5.d0*Pi2)/2.d0 +
     &   lms*(43.d0/12.d0 - 22.d0*Zeta3) + 25.d0*Zeta3 
     &  + nf*(457.d0/108.d0 - Pi2/9.d0 - (22.d0*Zeta3)/9.d0 + 
     &   lms*(-13.d0/18.d0 + (4.d0*Zeta3)/3.d0)) + (50.d0*Zeta5)/3.d0)))
      else
        rq4 = ( ms4*(api**2*(-0.474894 - lms) + api**3*( 4.59784d0 
     &  - 22.8619d0*lms + 2.d0*lms**2 + (0.196497d0 + 0.88052d0*lms)*nf)
     &  ))
      endif
      return
      end

      function rQv4(ms2,api,nf,flag)
c Eq.9 quartic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rQv4,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5,Pi4
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      Pi4=Pi2**2
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rQv4 = ( ms4*(-6.d0 - 22.d0*api + api**2*(-2977.d0/12.d0 - 
     &  (13.d0*lms)/2.d0 + 27.d0*Pi2 + nf*(143.d0/18.d0 + lms/3.d0 -
     &  (2.d0*Pi2)/3.d0 - (8.d0*Zeta3)/3.d0) + 108.d0*Zeta3) + 
     &   api**3*(-1274461.d0/432.d0 + 13.d0*lms**2 + 
     &  (4033.d0*Pi2)/8.d0 + lms*(-1309.d0/6.d0 - 22.d0*Zeta3) +
     &  (64123.d0*Zeta3)/18.d0 + nf**2*(-463.d0/972.d0 - 
     &  (5.d0*lms)/27.d0 + (23.d0*Pi2)/54.d0 + (28.d0*Zeta3)/27.d0)
     &  - (13285.d0*Zeta5)/9.d0 + nf*(130009.d0/648.d0 - 
     &  (2.d0*lms**2)/3.d0 - (287.d0*Pi2)/9.d0 + Pi4/9.d0 - 
     &  (1672.d0*Zeta3)/9.d0 + lms*(199.d0/12.d0 + (4.d0*Zeta3)/3.d0) + 
     &  (440.d0*Zeta5)/9.d0))))
      else
        rQv4 = ( ms4*(-6.d0 - 22.d0*api + api**2*(148.218d0 
     &  - 6.5d0*lms + (-1.84078d0 + 0.333333d0*lms)*nf) +  api**3*(
     &   4776.95d0 - 244.612d0*lms + 13.d0*lms**2 + (-275.898d0 
     &  + 18.1861d0*lms - 0.666667d0*lms**2)*nf + (4.97396d0 
     &  - 0.185185d0*lms)*nf**2)))
      endif
      return
      end

      function rQa4(ms2,api,nf,flag)
c Eq.10 quartic mass correction of nf active flavors to 3--loop
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rQa4,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5,Pi4
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      Pi4=Pi2**2
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rQa4 = ( ms4*(6.d0 + 10.d0*api + api**2*(1147.d0/4.d0 + 
     &  (75.d0*lms)/2.d0 - 27.d0*Pi2 - 224.d0*Zeta3 + nf*(-41.d0/6.d0 -
     &  (7.d0*lms)/3.d0 + (2.d0*Pi2)/3.d0 + (16.d0*Zeta3)/3.d0)) + 
     &   api**3*(222421.d0/48.d0 - 75.d0*lms**2 - (3627.d0*Pi2)/8.d0 + 
     &   lms*(4385.d0/6.d0 - 22.d0*Zeta3) + nf**2*(2995.d0/972.d0 + 
     &  (23.d0*lms)/27.d0 - (29.d0*Pi2)/54.d0 - (20.d0*Zeta3)/27.d0) - 
     &  (20147.d0*Zeta3)/6.d0 - (2225.d0*Zeta5)/3.d0 + 
     &   nf*(-200923.d0/648.d0 + (14.d0*lms**2)/3.d0 + 
     &  (98.d0*Pi2)/3.d0 - Pi4/9.d0 + (5002.d0*Zeta3)/27.d0 + 
     &   lms*(-2323.d0/36.d0 + (4.d0*Zeta3)/3.d0) + 
     &  (1040.d0*Zeta5)/27.d0))))
      else
        rQa4 = ( ms4*(6.d0 + 10.d0*api + api**2*(-248.99d0 + 37.5d0*lms 
     &  + (6.15737d0 - 2.33333d0*lms)*nf) + api**3*(-4646.22d0 + 
     &   704.388d0*lms  - 75d0*lms**2 + (264.151d0 - 62.9250d0*lms + 
     &   4.66667d0*lms**2)*nf + (-3.10948d0 + 0.851852d0*lms)*nf**2)))
      endif
      return
      end

      function rvsing(ms2,api,vQ,svq,flag)
c Eq.11 singlet contribution in the vector case
c svq = vQ + sumvq
      implicit none
      character*3 flag
      real *8 vQ,svq
      real *8 ms2,api,rvsing,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rvsing = ( api**3*((svq)**2*(55.d0/216.d0 - 
     &   5.d0/9.d0*Zeta3) + ms4*vQ*(svq)*(-20.d0/9.d0 + 
     &   50.d0/3.d0*Zeta3)))
      else
        rvsing = ( api**3*((svq)**2*(-0.413180d0)
     &  + ms4*vQ*(svq)*(17.8121d0)))
      endif
      return
      end

c      function rasing(ms2,api,vQ,svq,flag)
cc singlet contribution in the axial case is small
c      end

      function rsingtt(ms2,api,flag)
c Eq.12 singlet contribution to top-antitop production
      implicit none
      character*3 flag
      real *8 ms2,api,rsingtt,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rsingtt = ( 1.d0/4.d0*api**2*(5.d0/4.d0 - 2.d0/3.d0*Zeta2 + 
     &   ms2*(-4.d0/3.d0 + 20.d0/3.d0*Zeta2 + 4.d0*lms - 
     &   2.d0/3.d0*lms**2) + ms4*(13.d0/2.d0 + 2.d0/3.d0*Zeta2 - 
     &   16.d0/3.d0*Zeta3 + lms*(-5.d0 + 4.d0/3.d0*Zeta2) -
     &   1.d0/3.d0*lms**2 - 2.d0/9.d0*lms**3)))
      else
        rsingtt = ( 1.d0/4.d0*api**2*(0.153377d0 + ms2*(9.63289d0 + 
     &   4.d0*lms - 0.666667d0*lms**2) + ms4*(1.18565d0 - 
     &   2.80675d0*lms - 0.333333d0*lms**2  - 0.222222d0*lms**3)))
      endif
      return
      end

      function rsingptt(ms2,api,nf,flag)
c Eq.13 singlet contribution to top-antitop production
c ' = includes contributiond from purely massless states
c applies only to top production, includes full (t,b) doublet contribution
      implicit none
      character*3 flag
      integer nf
      real *8 ms2,api,rsingptt,ms4,lms
      real *8 Pi,Pi2,Zeta2,Zeta3,Zeta5
      common /numcst/Pi,Pi2,Zeta2,Zeta3,Zeta5
      ms4=ms2**2
      lms=dlog(ms2)
      if (flag.eq.'ana') then
        rsingptt = ( 1.d0/4.d0*api**3*ms4*(-380.d0/3.d0*Zeta3 + 
     &   1520.d0/3.d0*Zeta5 + nf*(40.d0/9.d0*Zeta3 -
     &   160.d0/9.d0*Zeta5)))
      else
        rsingptt = ( 0.25d0*api**3*ms4*(373.116d0 - 13.0918d0*nf))
      endif
      return
      end
