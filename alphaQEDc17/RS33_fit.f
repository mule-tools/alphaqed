c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; R33dat_fit.f ---
c;; Author          : Friedrich Jegerlehner
c;; Created On      : Mon Feb 20 16:00:34 2012
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Jan  8 03:42:14 2015
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2012 Friedrich Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c smoothed version of function RS33_fun.f interpolating R330321.f dataset
c using piecewise Chebyshev polynomial fits.
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01
c               test parameters via common /future/ifuture,efuture,futureprecision
c fitranges: 2 m_pi   CHPTCUT   =>  RCHPT_qedc17 -> RCHPTnew_qedc17 via r33fitchptail_qedc17
c            CHPTCUT  M_omega   =>  r33fitrholow_qedc17
c            M_omega  EFITOMBG  =>  r33fitrhohig_qedc17
c            EFITOMBG EFITMIN   =>  epem33fit079081_qedc17
c            EFITMIN  1.4 GeV   =>  epem33fit0814new_qedc17
c            1.4 GeV  3.2 GeV   =>  epem33fit1425_qedc17
c  07/06/2013 changed flavor splittrig [us] component 9/10 --> 10/10
c fit up to 2.125 GeV
      function rs33smoothed_qedc17(s,IER)
      implicit none
      INTEGER IER,IER1,IER_SAV,ICH,IGS,iso,ifuture,NFext,NFext1,nf
      REAL *8 rs33smoothed_qedc17,RS33_qedc17,RS40smoothed_qedc17,RS3x_qedc17,Rless,R3Gless,R33less,
     &     r33fitchptail_qedc17,epem33fit0814new_qedc17,
     &     epem33fit079081_qedc17,epem33fit1425_qedc17,r33fitrholow_qedc17,r33fitrhohig_qedc17
      REAL *8 S,MP2,M2,POLP,POLM,E,EFITOMBG,EFITMIN,EFITMAX,ESAV,RES,
     &     EP,EMA,EMI,eps,CHPTCUT,RP,RM,MOM,EC,ECUT,ECHARMTH,RSBG,reno,
     &     RESULT,CHPTCUTLOC,efuture,futureprecision,
     &     RSGG1,RS331,R3G,R33,F33,ERXYDAT
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      COMMON/MFPI_qedc17/MP2,M2/ERR_qedc17/IER1/POL_qedc17/POLP,POLM
      COMMON/CHAN_qedc17/ICH
      COMMON/RCUTS_qedc17/CHPTCUT,EC,ECUT
      COMMON/GUSA_qedc17/IGS,iso
      EXTERNAL RS33_qedc17,RS40smoothed_qedc17,RS3x_qedc17,r33fitchptail_qedc17,
     &     epem33fit0814new_qedc17,epem33fit079081_qedc17,epem33fit1425_qedc17,
     &     r33fitrholow_qedc17,r33fitrhohig_qedc17
      common /future_qedc17/efuture,futureprecision,ifuture
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc17/CHPTCUTLOC,ERXYDAT,ECHARMTH
      common /quama_qedc17/mq,mp,th
      RSGG1=0.d0
      RS331=0.d0
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      xc=0.375d0**2
      xb=0.750d0**2
      xt=xc
      reno=1.d0
      IER1=IER
      E=DSQRT(S)
c CUT set in R3Gdat_fit.f
c      CHPTCUTLOC=0.312D0
c      CHPTCUTLOC=0.320D0
      MOM=0.77703500000000003d0
      IF (IGS.EQ.1) MOM=0.61d0
      EFITOMBG=0.78783499999999995d0
      EFITMIN=0.81d0
      EFITMAX=1.40d0
      EMA=sqrt(POLP)
      EMI=sqrt(POLM)
      EP=sqrt(M2)
      ERXYDAT=2.125d0
c extend to get smooth mactch to rewighted RS40smooth
      ERXYDAT=2.150d0
      ECHARMTH=th(4)  ! 4.2 GeV
      eps=0.02d0
      RSBG=0.d0
      IF (E.LE.CHPTCUTLOC) THEN
         ICH=0
c         RSBG=r33fitchptail_qedc17(s)
         RSBG=0.25d0*rs40smoothed_qedc17(s,IER) 
      ELSE IF ((E.GT.CHPTCUTLOC).AND.(E.LE.MOM)) THEN
         ICH=1
         RSBG=r33fitrholow_qedc17(s)
      ELSE IF ((E.GT.MOM).AND.(E.LE.EFITOMBG)) THEN
         ICH=2
         RSBG=r33fitrhohig_qedc17(s)
      ELSE IF ((E.GT.EFITOMBG).AND.(E.LE.EFITMIN)) THEN
         ICH=3
         RSBG=epem33fit079081_qedc17(s)
      ELSE IF ((E.GT.EFITMIN).AND.(E.LE.EFITMAX)) THEN
         ICH=4
         RSBG=epem33fit0814new_qedc17(s)
      ELSE IF ((E.GT.EFITMAX).AND.(E.LE.ERXYDAT)) THEN
         ICH=5
         RSBG=epem33fit1425_qedc17(s)
      ELSE IF ((E.GT.ERXYDAT).AND.(E.LE.ECHARMTH)) THEN
         ICH=6
         RSBG=9.d0/32.d0*RS40smoothed_qedc17(s,IER)
      ELSE IF (E.GT.ECHARMTH) THEN
         ICH=7
         if (nf.eq.3) then
            RSBG=9.d0/32.d0*RS40smoothed_qedc17(s,IER)
         else
            RSGG1=RS3x_qedc17(s,R3G,R33)
            F33=R33/RSGG1
            IF (IER.EQ.1) THEN
               if (nf.eq.4) then
                  RSBG=(RS40smoothed_qedc17(s,IER)-Rless)*xc+R33less
               else if (nf.eq.5) then
                  RSBG=(RS40smoothed_qedc17(s,IER)-Rless)*xb+R33less
               else if (nf.ge.6) then
                  RSBG=(RS40smoothed_qedc17(s,IER)-Rless)*xt+R33less
               endif
            ELSE
               RSBG=RS40smoothed_qedc17(s,IER)*F33
            ENDIF
         endif
      ENDIF
c      CALL RENOoldnew(E,reno)
      RESULT=RSBG*reno
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         if (IER_SAV.EQ.2) RESULT=0.d0
         if (IER_SAV.EQ.3) RESULT=RESULT*futureprecision
      endif
      rs33smoothed_qedc17=RESULT
      return
      end
C
      function epem33fit0814new_qedc17(s)
c Chebyshev Polynomial fits are for R-value (IER=1), statistical (IER=2) and
c systematic (IER=3) errors;
c Note: syst error is the "true" one, not represented as a fraction as in the data sets
      implicit none
      integer np,nm,ier,IRESON,ini
      parameter(np=8,nm=12)
      real *8 epem33fit0814new_qedc17,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     aresl,astal,asysl,aresm,astam,asysm,aresh,astah,asysh,
     &     aresfm,astafm,asysfm,aresfp,astafp,asysfp,
     &     aresfr,astafr,asysfr,polynom_qedc17
      dimension aresl(nm),astal(nm),asysl(nm)
      dimension aresm(np),astam(np),asysm(np)
      dimension aresh(nm),astah(nm),asysh(nm)
      dimension aresfm(np),astafm(np),asysfm(np)
      dimension aresfp(np),astafp(np),asysfp(np)
      dimension aresfr(np),astafr(np),asysfr(np)
      real *8 MFI,GFI,PFI,fsta3,fsys3
      real *8 EFIMM,EFIMX,EFIPX,EFIPP,CHPTCUTLOC,ERXYDAT,ECHARMTH
      real *8 fac,rbw,BW_qedc17
      REAL *8 UGM2,sMeV
      real *8 etest0,etest1,stest0,stest1
      external BW_qedc17
      REAL*8 FSTA(5),FSYS(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      COMMON/BWFI_qedc17/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/RESRELERR_qedc17/FSTA,FSYS
      COMMON/RESDOMAINS_qedc17/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON/RESFIT_qedc17/fac,IRESON
      COMMON/ERR_qedc17/IER
      common /cufit_qedc17/CHPTCUTLOC,ERXYDAT,ECHARMTH
      data ini /0/

c      ++++ epem33fit0814new_qedc17 ++++

      include 'epem33fit0814new.h'

      fsta3=FSTA(3)
      fsys3=FSYS(3)
      e=sqrt(s)
      yfit=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      EFIMM=0.959d0
      EFIMX=1.000d0
      EFIPX=1.040d0
      EFIPP=ERXYDAT
c Fit ranges phi region
C  0.810 -- 1.000
C  0.843 -- 1.197
C  1.062 -- 1.438
      if ((e.ge.0.81d0).and.(e.le.EFIMM)) then
         e1=0.81d0
         en=0.999999d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,aresl,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,astal,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asysl,nm)
         endif
      else if ((e.gt.EFIMM).and.(e.le.EFIMX)) then
         e1=0.959d0
         en=0.999999d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,aresfm,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,astafm,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asysfm,np)
         endif
      else if ((e.gt.EFIMX).and.(e.lt.EFIPX)) then
c update jan 2012
         e1=1.00001d0
         en=1.03999D0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,aresfr,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,astafr,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asysfr,np)
         endif
         if (IRESON.eq.1) then
            rbw=fac*BW_qedc17(sMeV,MFI,GFI,PFI)
c for <33> factor 9/16 to be applied
               rbw=rbw*9.d0/16.d0
            if (ier.eq.2) then
               rbw=rbw*fsta3
               yfit=sqrt(yfit**2+rbw**2)
            else
               if (ier.eq.3) rbw=rbw*fsys3
               yfit=yfit+rbw
            endif
         endif
       else if (e.lt.1.435d0) then
         e1=1.04000001d0
         en=1.435d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,aresh,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,astah,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asysh,np)
         endif
      else
         write (*,*)  ' Warning: E out of fit range in epem33fit0814new_qedc17'
      endif
      epem33fit0814new_qedc17=yfit
      return
      end
c
      function epem33fit079081_qedc17(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=7)
      real *8 epem33fit079081_qedc17,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc17
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc17/IER

c      ++++ epem33fit079081_qedc17 ++++

      include 'epem33fit079081.h'

c omega  .41871054d0,.810d0
c no omega component in 3 3
      e=sqrt(s)
      yfit=0.d0
      e1=0.787d0
      en=0.80999d0
      if ((e.gt.0.81d0).or.(e.lt.e1)) then
         epem33fit079081_qedc17=0.d0
         return
      endif
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc17(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc17(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc17(ex,asys,np)
      endif
      epem33fit079081_qedc17=yfit
      return
      end
c
      function epem33fit1425_qedc17(s)
      implicit none
      integer np,nm,nx,ier,IRESON
      parameter(np=12,nm=6,nx=8)
      real *8 epem33fit1425_qedc17,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     areslow,astalow,asyslow,aresmed,astamed,asysmed,
     &     ares,asta,asys,ares1,asta1,asys1,
     &     polynom_qedc17,CHPTCUTLOC,ERXYDAT,ECHARMTH
      real *8 fres2023,fsta2023,fsys2023
      dimension areslow(np),astalow(np),asyslow(np)
      dimension aresmed(nx),astamed(nx),asysmed(nx)
      dimension ares(np),asta(np),asys(np)
      dimension ares1(np),asta1(np),asys1(np)
      dimension fres2023(nm),fsta2023(nm),fsys2023(nm)
      COMMON/ERR_qedc17/IER
      common /cufit_qedc17/CHPTCUTLOC,ERXYDAT,ECHARMTH

c      ++++ epem33fit1425_qedc17 ++++

      include 'epem33fit1425.h'

      e=sqrt(s)
      yfit=0.d0
      if ((e.lt.1.38d0).or.(e.gt.ERXYDAT)) then
         epem33fit1425_qedc17=0.d0
         return
      else if (e.le.1.45d0) then
c   1.375  --  2.038
         e1=1.375d0
         en=2.038d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,areslow,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,astalow,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asyslow,np)
         endif
      else if (e.lt.1.975d0) then
c   1.395 -- 2.000
         e1=1.395d0
         en=2.000d0
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,ares,np)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,asta,np)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,asys,np)
         endif
      else if (e.le.ERXYDAT) then
c  1.975 -- 2.125 update jan 2012 / jan 2015
         e1=1.975d0
         en=ERXYDAT
         es=(en+e1)
         ed=(en-e1)
         amap=2.d0/ed
         bmap=-es/ed
         ex=amap*e+bmap
         if (ier.eq.1) then
            yfit=polynom_qedc17(ex,fres2023,nm)
         else if (ier.eq.2) then
            yfit=polynom_qedc17(ex,fsta2023,nm)
         else if (ier.eq.3) then
            yfit=polynom_qedc17(ex,fsys2023,nm)
         endif
      else
         write (*,*)  ' Warning: E out of fit range in epem33fit1425_qedc17'
      endif
         epem33fit1425_qedc17=yfit
      return
      end
c
      function r33fitchptail_qedc17(s)
      implicit none
      integer ier
      real *8 r33fitchptail_qedc17,s,rfitchptail_qedc17
      COMMON/ERR_qedc17/IER
      r33fitchptail_qedc17=0.25d0*rfitchptail_qedc17(s)
      return
      end
c
      function r33fitrholow_qedc17(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=14)
      real *8 r33fitrholow_qedc17,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc17
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc17/IER

c      ++++ r33fitrholow_qedc17 ++++

      include 'r33fitrholow.h'

c omega  .41871054d0,.810d0
c 33 has no omega component
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.318d0
      en=0.778d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r33fitrholow_qedc17=0.d0
         return
      endif
c 0.318 -- 0.778 updated nov 2010 (incl BaBar/KlOE) / jan 2012
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc17(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc17(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc17(ex,asys,np)
      endif
      r33fitrholow_qedc17=yfit
      return
      end
c
      function r33fitrhohig_qedc17(s)
      implicit none
      integer np,ier,IRESON
      parameter(np=9)
      real *8 r33fitrhohig_qedc17,s,e,ex,es,ed,e1,en,amap,bmap,yfit,
     &     ares,asta,asys,polynom_qedc17
      dimension ares(np),asta(np),asys(np)
      COMMON/ERR_qedc17/IER

c      ++++ r33fitrhohig_qedc17 ++++

      include 'r33fitrhohig.h'

c omega  .41871054d0,.810d0
c no omega contribution in RS33_qedc17
c
      e=sqrt(s)
      yfit=0.d0
      e1=0.769000000000000d0
      en=0.789000000000000d0
      if ((e.gt.en).or.(e.lt.e1)) then
         r33fitrhohig_qedc17=0.d0
         return
      endif
c  0.769  -- 0.789 update jan 2012
      es=(en+e1)
      ed=(en-e1)
      amap=2.d0/ed
      bmap=-es/ed
      ex=amap*e+bmap
      if (ier.eq.1) then
        yfit=polynom_qedc17(ex,ares,np)
      else if (ier.eq.2) then
        yfit=polynom_qedc17(ex,asta,np)
      else if (ier.eq.3) then
        yfit=polynom_qedc17(ex,asys,np)
      endif
      r33fitrhohig_qedc17=yfit
      return
      end

