c;;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Rdat_all.f --- 
c;; Author          : Fred Jegerlehner
c;; Created On      : Mon Apr 21 00:24:57 2008
c;; Last Modified By: Friedrich Jegerlehner
c;; Last Modified On: Thu Aug 10 12:38:04 2017
c;; RCS: $Id$
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;; Copyright (C) 2010 Fred Jegerlehner
c;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
c;;
c provides R(s) by data, CHPT or pQCD depending on region and given cuts
c error flag: IER=1 R vaue; IER=2 statistical error, IER=3 systematic error specified as a fraction
c narrow resonances: parametrized by Breit-Wigner: omega, phi, psi(1)-psi(3) and Upsilon(1)-(6)
c rho and psi(4)-psi(6) included in e^+e^- data set (background)
c flag iresonances=1 include narrow resonances; =0 background only  
c Theory errors are taken as syst errors
c i.e. chpt and pQCD results are assigned zero statistical error
c Note background data sets without narrow resonances are CLOSED regions (endpoints included); 
c resonances on to top of subtracted background are OPEN intervals. Avoid double counting at 
c boundary points!
      FUNCTION RS40_qedc17(s,IER)
c requires common 
c      COMMON/QCDPA/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
c to set QCD parameters in main program : alpha_s with stat and sys error 
c (one may be zero the other the tot error), scale sqrt(s) usually = M_Z top mass
c number of external flavors e.g 5 no top, 6 incl top, ior=number of loops in R(s) 
c to be calculated, ICHK is dummy here (in some programs used for alternative routines calculating R(s)); ckeck for other commons needed to be set in main program 
c  19/04/2010   add option for error estimates with future experimental error reduction
c               if errors up to energy efuture are reduced to futureprecision
c               ifuture=1, efuture=2.5 GeV, futureprecision=0.01         
c               test parameters via common /future/ifuture,efuture,futureprecision
c      
      IMPLICIT NONE
      INTEGER I,J,JJ,IER,IER_SAV,iresonances,IRESON,IRENBW,iii,count,
     &     ipQCD,ifuture,ini,iomegaphidat
      INTEGER NCHP,NLOW,NU0B,NU0Bd,NU1N,NU2N,NBE,NR31N,NU7A
      integer iniRdatupdatenoKLOE,iniRdatupdatenoBABA
c FJ 06/10/2009 BES region update NU1N=111 -> NU1N=130
c FJ 10/10/2010 BaBar/KLOE incl.  NU0B=128 -> 330; NLOW=24 -> 97 adjust in Rdat_fun.f 
c FJ 28/01/2012 recalculated NU0B=331; 13/10/2012 NU0B=346->380
c      PARAMETER(NCHP=18,NLOW=97,NU0B=337,NU0Bd=380,NU1N=134,NU2N=10,
c     &     NBE=152,NR31N=75,NU7A=46)
      PARAMETER(NCHP=18,NLOW=97,NU0B=337,NU0Bd=380,NU1N=134,NU2N=10,
     &     NBE=99,NR31N=75,NU7A=46)
      REAL*8 XCPT(NCHP),YCPT(NCHP,3),XLOW(NLOW),YLOW(NLOW,3),
     &     U0B(NU0B),V01B(NU0B,3),U0Bdat(NU0Bd),V01Bdat(NU0Bd,3),
     &     U1N(NU1N),V111N(NU1N),V121N(NU1N),V131N(NU1N),
     &     U2N(NU2N),V2N(NU2N,3),XBE(NBE),YBE(NBE,3),
     &     X31N(NR31N),Y31NA(NR31N),Y31NB(NR31N),Y31NC(NR31N),
     &     U7A(NU7A),V7A(NU7A,3)
c
      REAL*8 E,DE,S,EOMM,EOMP,EFIM,EFIP,RS40_qedc17,RCHPT_qedc17,RCHPTnew_qedc17,AVE,VAR,
     &     RES(3),ROM(3),RFI(3),RPS(3),RYP(3),REBW,reno,
     &     efuture,futureprecision
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6) 
      REAL *8 EXCPTm,EXLOWm,EU0Bm,EU1Nm,EU2Nm,EXBEm,EX31Nm,EU7Am,
     &     EXQCD1m,EXQCD2m,EXQCD3m,EXCPTp,EXLOWp,EU0Bp,EU1Np,EU2Np,
     &     EXBEp,EX31Np,EU7Ap,EXQCD3p
      REAL *8 CHPTCUT,EC,ECUT,RSpQCD_qedc17,EINFTY
      real *8 UGM2,sMeV,rbw,BW_qedc17,fac,null,alp
      
      external RSpQCD_qedc17,BW_qedc17
      REAL*8 MRO,MOM,MFI,GRO,GOM,GFI,PRO,POM,PFI
      REAL*8 PSI(6),MPS(6),GPS(6),PPS(6),YPI(6),MYP(6),GYP(6),PYP(6)
      REAL*8 STAPS(6),SYSPS(6),STAYP(6),SYSYP(6)
      REAL*8 FSTA(5),FSYS(5)
      COMMON/EPEMALL_qedc17/XCPT,YCPT,XLOW,YLOW,
     &     U0B,V01B,U0Bdat,V01Bdat,
     &     U1N,V111N,V121N,V131N,
     &     U2N,V2N,XBE,YBE,
     &     X31N,Y31NA,Y31NB,Y31NC,
     &     U7A,V7A
      COMMON/BWRO_qedc17/MRO,GRO,PRO/BWOM_qedc17/MOM,GOM,POM/BWFI_qedc17/MFI,GFI,PFI
C common Psi and Upsilon parameters set in this subroutine
      COMMON/PSYPPAR_qedc17/MPS,GPS,PPS,MYP,GYP,PYP,STAPS,SYSPS,STAYP,SYSYP
      COMMON/RESRELERR_qedc17/FSTA,FSYS
c energy cuts: CHPT at low end, pQCD above ECUT and between EC and Upsilon threshold 9.5 GeV; may be set in constants_qcd.f and called as "call constants_qcd()"
      COMMON/RCUTS_qedc17/CHPTCUT,EC,ECUT
c resonances flag iresonances=1 include resonances as data here; 0 include them 
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON /OMEPHIDAT_qedc17/iomegaphidat
      COMMON/RES_qedc17/iresonances
      COMMON/RESFIT_qedc17/fac,IRESON
      COMMON/RBW_qedc17/REBW
      COMMON /renoBW_qedc17/IRENBW
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
c       MPLUS=139.57018D0   ! PDG 2004
c       EOMM=3.D0*MPLUS
      COMMON/RESDOMAINS_qedc17/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      common /future_qedc17/efuture,futureprecision,ifuture
      common /noKLOE_qedc17/iniRdatupdatenoKLOE
      common /noBABA_qedc17/iniRdatupdatenoBABA
      data count /0/ ini /0/
      IER_SAV=1
      if ((IER.GT.1).and.(ifuture.eq.1).and.(sqrt(s).le.efuture)) then
         IER_SAV=IER
         IER=1
      endif
      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM,BWFI,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series 
c
         call resonances_data_qedc17()
c
c called from resonances_dat.f
************************************************************************
         ini=1
c begin no KLOE version comment away for all data version
c        include 'Rdat_upd_noKLOE.f'  in Rdat_all.f line 171
         if (iniRdatupdatenoKLOE.eq.1) then
            write (*,102)
         endif
         if (iniRdatupdatenoBABA.eq.1) then
            write (*,103)
         endif
c end no KLOE version
      endif
 102  FORMAT(1X,68('*'),/,1X,'*',66X,'*',/,1X,'*  WARNING:',
     &       '    Rdat_all.f is including update Rdat_upd_noKLOE.f    *'
     .      ,/,1X,'*',66X,'*',/,1X,'* change for all data WA',
     &       ' in Rdat_all at line 171 include Rdat_upd.f *'
     .      ,/,1X,'*',66X,'*',/,1X,68('*'))
 103  FORMAT(1X,68('*'),/,1X,'*',66X,'*',/,1X,'*  WARNING:',
     &       '    Rdat_all.f is including update Rdat_upd_noBABA.f    *'
     .      ,/,1X,'*',66X,'*',/,1X,'* change for all data WA',
     &       ' in Rdat_all at line 171 include Rdat_upd.f *'
     .      ,/,1X,'*',66X,'*',/,1X,68('*'))
c test peak imaginary parts
c      if (count.eq.0) then
cc squares of imaginary parts at resonance peaks
c         alp=1.d0/137.036d0
c         write (22,*) ' Rdat_fun: OM,FI',(3.d0/alp*POM)**2,
c     &        (3.d0/alp*PFI)**2
c         do iii=1,3
c            write (22,*) ' Rdat_fun: J/psi',(3.d0/alp*PPS(iii))**2
c         enddo 
c         do iii=1,3
c            write (22,*) ' Rdat_fun: Ups',(3.d0/alp*PYP(iii))**2
c         enddo 
c         count=count+1
c      endif
      ipQCD=0
      null=0.d0
      UGM2=1.D6
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo 
c     
C     
C  0.285       0.5 
C   |-----------|--
C       XCPT
C
C     0.318     0.61        1.4 
C      |---------|-----------|--
C
C          XLOW        U0B      
C
C  1.4         3.1         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        
c Array boundaries
C
C  0.285       0.5 
C   |-----------|--
C       XCPT
C
C     0.318     0.61        1.4 
C      |---------|-----------|--
C
C          XLOW        U0B      
C
C  1.4         3.1         3.6          5.2           9.46           40.0
C   |-----------|-----------|------------|-------------|---------------|
C        U1N        U2N         XBE          X31N           U7A        
C
C     1.8                  3.697 4.003  5.1                 13.5
C      |--------------------|-----|------|--------------------|--
c

c      call Rdata_qedc17()

C s is function RS40_qedc17(s) argument [energy square in GeV^2]
c
C other input parameters via commons:
c       COMMON/RCUTS/CHPTCUT,EC,ECUT
c error fag ier=1,2,3 for result,staterr, syserr
c       COMMON/ERR/IER
c resonances flag iresonances=1 include resonances as data here; 0 include them elsewhere 
c       COMMON/RES/iresonances
c 
        E=sqrt(S)
c        write (*,*) ' RS40_qedc17: begin',E
        if ((IER.LT.1).OR.(IER.GT.3)) then
           write (*,*) ' no permitted error flag',IER
           return
        endif
        j=IER
C cut boundaries CHPTCUT,EC,ECUT
c 2m_pi - CHPTCUT (standard=0.318)
        EXCPTm=  XCPT(1) 
        EXCPTp=  CHPTCUT ! XCPT(NCHP)
c CHPTCUT - 0.61 
        EXLOWm=  CHPTCUT ! XLOW(1) 
        EXLOWp=  XLOW(NLOW) 
c omega  .41871054d0,.810d0
c 0.61 - 1.4        
        EU0Bm=   U0B(1)  
        EU0Bp=   U0B(NU0B)
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
c 1.4 - 3.2      
        EU1Nm=   U1N(1)  
        EU1Np=   U1N(NU1N)
c psi1  3.08587D0 3.10787D0
c 3.2 - 3.6
        EU2Nm=   U2N(1)  
        EU2Np=   U2N(NU2N)
c 3.6 - 5.2
        EXBEm= XBE(1)
        EXBEp= XBE(NBE)
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0 
c psi4  3.960D0   4.098D0  
c psi5  4.102D0   4.275D0  
c psi6  4.315D0   4.515D0  
c 5.2 - 9.5        
        EX31Nm=  X31N(1) 
        EX31Np=  X31N(NR31N)
c yps1  9.44937D0 9.47137D0 
c 9.5 - 13 [40]        
        EU7Am=   U7A(1)  
        EU7Ap=   ECUT ! U7A(NU7A)
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0 
c yps4  10.473D0  10.687D0  
c yps5  10.690D0  10.950D0  
c yps6  10.975D0  11.063D0  
c QCD1 and QCD 2 not used
c        EXQCD1m= XQCD1(1)
c        EXQCD1p= XQCD1(NQCD1)
c        EXQCD2m= XQCD2(1)
c        EXQCD2p= XQCD2(NQCD3)
c QCD3 used for region EC (>=5.2) to 9.46 GeV
        EXQCD3m= EC ! XQCD3(1)
        EXQCD3p= U7A(1) ! ECUT ! XQCD3(NQCD3)
c above ECUT analytic pQCD RS(s) from HS
C
c       E=DSQRT(S)
C
c       do j=1,3 
c threshold to .318          
        if ((E.GE.EXCPTm).and.(E.LT.EXCPTp)) then
           call getindex_qedc17(E,NCHP,XCPT,I)
c           if (I.GE.50)
c     &          write (*,*) ' Rdat_fun:216',I,E,XCPT(I),XCPT(NCHP)
           res(j)=YCPT(I,j)+(YCPT(I+1,j)-YCPT(I,j))
     &          /(XCPT(I+1)-XCPT(I))*(E-XCPT(I))
           if (j.eq.3) then
              res(j)=res(j)*(YCPT(I,1)+(YCPT(I+1,1)-YCPT(I,1))
     &             /(XCPT(I+1)-XCPT(I))*(E-XCPT(I)))
           endif
        endif          
c .318-.610
        if ((E.GE.EXLOWm).and.(E.LT.EXLOWp)) then
           call getindex_qedc17(E,NLOW,XLOW,I)
           res(j)=YLOW(I,j)+(YLOW(I+1,j)-YLOW(I,j))
     &          /(XLOW(I+1)-XLOW(I))*(E-XLOW(I))
           if (j.eq.3) then
              res(j)=res(j)*(YLOW(I,1)+(YLOW(I+1,1)-YLOW(I,1))
     &             /(XLOW(I+1)-XLOW(I))*(E-XLOW(I)))
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EOMM).and.(E.LT.EOMP)) then
                 rbw=fac*BW_qedc17(sMeV,MOM,GOM,POM)
                 if (ier.eq.2) then
                    rbw=rbw*fsta(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*fsys(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
c omega  .41871054d0,.810d0
           endif
        endif
c omega  .41871054d0,.810d0
c .610-1.4
        if ((E.GE.EU0Bm).and.(E.LT.EU0Bp)) then
c option omega and phi in terms of datasets, alternatively using BW_qedc17 shape with PDG parameters 
           if (iomegaphidat.eq.1) then
              call getindex_qedc17(E,NU0Bd,U0Bdat,I)
              res(j)=V01Bdat(I,j)+(V01Bdat(I+1,j)-V01Bdat(I,j))
     &             /(U0Bdat(I+1)-U0Bdat(I))*(E-U0Bdat(I))
              if (j.eq.3) then
                 res(j)=res(j)*(V01Bdat(I,1)
     &                +(V01Bdat(I+1,1)-V01Bdat(I,1))
     &                /(U0Bdat(I+1)-U0Bdat(I))*(E-U0Bdat(I)))
              endif
           else 
              call getindex_qedc17(E,NU0B,U0B,I)
              res(j)=V01B(I,j)+(V01B(I+1,j)-V01B(I,j))
     &             /(U0B(I+1)-U0B(I))*(E-U0B(I))
              if (j.eq.3) then
                 res(j)=res(j)*(V01B(I,1)+(V01B(I+1,1)-V01B(I,1))
     &                /(U0B(I+1)-U0B(I))*(E-U0B(I)))
        endif
c omega  .41871054d0,.810d0
                 if (iresonances.eq.1) then
                    if ((E.GT.EOMM).and.(E.LT.EOMP)) then
                       rbw=fac*BW_qedc17(sMeV,MOM,GOM,POM)
                       if (ier.eq.2) then
                          rbw=rbw*fsta(2)
                          res(j)=sqrt(res(j)**2+rbw**2)
                       else 
                          if (ier.eq.3) rbw=rbw*fsys(2)
                          res(j)=res(j)+rbw
                       endif
                       rebw=rbw
                    endif
c phi   1.00D0,1.04D0
                    if ((E.GT.EFIM).and.(E.LT.EFIP)) then
                       rbw=fac*BW_qedc17(sMeV,MFI,GFI,PFI)
                       if (ier.eq.2) then
                          rbw=rbw*fsta(3)
                          res(j)=sqrt(res(j)**2+rbw**2)
                       else 
                          if (ier.eq.3) rbw=rbw*fsys(3)
                          res(j)=res(j)+rbw
                       endif
                       rebw=rbw
                    endif
                 endif
              endif
           endif
c  1.40-3.2
        if ((E.GE.EU1Nm).and.(E.LT.EU1Np)) then
           call getindex_qedc17(E,NU1N,U1N,I)
           if (j.eq.1) then
              res(j)=V111N(I)+(V111N(I+1)-V111N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I))
           else if (j.eq.2) then
              res(j)=V121N(I)+(V121N(I+1)-V121N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I))
           else if (j.eq.3) then
              res(j)=(V131N(I)+(V131N(I+1)-V131N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I)))*
     &               (V111N(I)+(V111N(I+1)-V111N(I))
     &             /(U1N(I+1)-U1N(I))*(E-U1N(I)))
           else 
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIPS(1)).and.(E.LT.EMAPS(1))) then
c psi1  3.08587D0 3.10787D0
                 rbw=fac*BW_qedc17(sMeV,MPS(1),GPS(1),PPS(1))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(1)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(1)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
c           if (ier.eq.1)   write (4,*) e,res(j)
        endif
c psi1  3.08587D0 3.10787D0
c 3.2 - 3.6          
        if ((E.GE.EU2NM).and.(E.LT.EU2NP)) then
           call getindex_qedc17(E,NU2N,U2N,I)
           res(j)=V2N(I,j)+(V2N(I+1,j)-V2N(I,j))
     &          /(U2N(I+1)-U2N(I))*(E-U2N(I))
           if (j.eq.3) then
              res(j)=res(j)*(V2N(I,1)+(V2N(I+1,1)-V2N(I,1))
     &             /(U2N(I+1)-U2N(I))*(E-U2N(I)))
           endif
        endif
c 3.60 - 5.20 
        if ((E.GE.EXBEM).and.(E.LT.EXBEP)) then
           if ((EC.GE.EXBEP).or.(E.LE.EC)) then         
              call getindex_qedc17(E,NBE,XBE,I)
              res(j)=YBE(I,j)+(YBE(I+1,j)-YBE(I,j))
     &             /(XBE(I+1)-XBE(I))*(E-XBE(I))
              if (j.eq.3) then
                 res(j)=res(j)*(YBE(I,1)+(YBE(I+1,1)-YBE(I,1))
     &                /(XBE(I+1)-XBE(I))*(E-XBE(I)))
              endif
           else 
              if (E.LT.1.8d0) write (*,*) ' RS40_qedc17 [3.6-5.2]:', E
              res(j)=RSpQCD_qedc17(s,j)
              rebw=null
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIPS(2)).and.(E.LT.EMAPS(2))) then
                 rbw=fac*BW_qedc17(sMeV,MPS(2),GPS(2),PPS(2))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GT.EMIPS(3)).and.(E.LT.EMAPS(3))) then
                 rbw=fac*BW_qedc17(sMeV,MPS(3),GPS(3),PPS(3))
                 if (ier.eq.2) then
                    rbw=rbw*STAPS(3)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSPS(3)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c pQCD below Uspilon if EC < 5.2 GeV 
c        EXQCD3m= EC  to EXQCD3p= EX31Np
c psi2  3.67496D0 3.69696D0
c psi3  3.7000D0  3.8400D0 
c psi4  3.960D0   4.098D0  
c psi5  4.102D0   4.275D0  
c psi6  4.315D0   4.515D0  
c 5.20- 9.5
        if ((E.GE.EX31NM).and.(E.LT.EX31NP)) then
           if ((EC.GE.EX31NP).or.(E.LE.EC)) then         
              call getindex_qedc17(E,NR31N,X31N,I)
              if (j.eq.1) then
                 res(j)=Y31NA(I)+(Y31NA(I+1)-Y31NA(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I))
              else if (j.eq.2) then
                 res(j)=Y31NB(I)+(Y31NB(I+1)-Y31NB(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I))
              else
                 res(j)=(Y31NC(I)+(Y31NC(I+1)-Y31NC(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I)))*
     &                  (Y31NA(I)+(Y31NA(I+1)-Y31NA(I))
     &                /(X31N(I+1)-X31N(I))*(E-X31N(I)))
              endif
c              write (203,'(1x,f8.3,3(2x,1pe11.3))')
c     &             E,res(1),res(2),res(3)
           else 
              if (E.LT.1.8d0) write (*,*) ' RS40_qedc17 [5.2-9.5]:', E
              res(j)=RSpQCD_qedc17(s,j)
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIYP(1)).and.(E.LT.EMAYP(1))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(1),GYP(1),PYP(1))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(1)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(1)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c pQCD below Uspilon 
c        EXQCD3m= EC  to EXQCD3p= EX31Np
c yps1  9.44937D0 9.47137D0 
c 9.5-13.00         
        if ((E.GE.EU7AM).and.(E.LT.EU7AP)) then
           if (E.LE.ECUT) then         
              call getindex_qedc17(E,NU7A,U7A,I)
              if (I.LT.NU7A) then
                 res(j)=V7A(I,j)+(V7A(I+1,j)-V7A(I,j))
     &                /(U7A(I+1)-U7A(I))*(E-U7A(I))
                 if (j.eq.3) then
                    res(j)=res(j)*(V7A(I,1)+(V7A(I+1,1)-V7A(I,1))
     &                   /(U7A(I+1)-U7A(I))*(E-U7A(I)))
                 endif
              else if (I.EQ.NU7A) then
                 res(j)=V7A(I,j)
                 if (j.eq.3) then
                    res(j)=res(j)*V7A(I,1)
                 endif
              endif
           else 
              if (E.LT.1.8d0) write (*,*) ' RS40_qedc17 [9.5-13.]:', E
              res(j)=RSpQCD_qedc17(s,j)
              ipQCD=1
           endif
           if (iresonances.eq.1) then
              if ((E.GT.EMIYP(2)).and.(E.LT.EMAYP(2))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(2),GYP(2),PYP(2))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(2)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(2)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(3)).and.(E.LT.EMAYP(3))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(3),GYP(3),PYP(3))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(3)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(3)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(4)).and.(E.LT.EMAYP(4))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(4),GYP(4),PYP(4))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(4)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(4)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(5)).and.(E.LT.EMAYP(5))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(5),GYP(5),PYP(5))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(5)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(5)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
              if ((E.GE.EMIYP(6)).and.(E.LT.EMAYP(6))) then
                 rbw=fac*BW_qedc17(sMeV,MYP(6),GYP(6),PYP(6))
                 if (ier.eq.2) then
                    rbw=rbw*STAYP(6)
                    res(j)=sqrt(res(j)**2+rbw**2)
                 else 
                    if (ier.eq.3) rbw=rbw*SYSYP(6)
                    res(j)=res(j)+rbw
                 endif
                 rebw=rbw
              endif
           endif
        endif
c yps1  9.44937D0 9.47137D0 
c yps2  10.01226D010.03426D0
c yps3  10.3442D0 10.3662D0 
c yps4  10.473D0  10.687D0  
c yps5  10.690D0  10.950D0  
c yps6  10.975D0  11.063D0  
c 13.00 - oo 
        EINFTY=1.D9             ! 1000 000 TeV
        if ((E.GE.ECUT).and.(E.LT.EINFTY)) then
           if (E.LT.1.8d0) write (*,*) ' RS40_qedc17 [HE tail]:', E
           res(j)=RSpQCD_qedc17(s,j)
           rebw=null
           ipQCD=1
        endif
c      enddo
      if (IER_SAV.GT.1) then
         IER=IER_SAV
         j=IER_SAV
         if (IER_SAV.EQ.2) res(2)=null
         if (IER_SAV.EQ.3) res(3)=res(1)*futureprecision
      endif
      rs40_qedc17=res(j)
c     if (ipQCD.eq.0) then
c     CALL RENOoldnew(E,reno)
c     rs40_qedc17=rs40_qedc17*reno
c     endif
      RETURN
      END
c     
      FUNCTION RSpQCD_qedc17(s,IER)
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,RSpQCD_qedc17,xRS_qedc17,ALINP,EINP,MTOP,pi,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc17/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      common/pqcdHS_qedc17/pi,ALINP1,EINP,MTOP1,IOR1,NF1 ! local input for Rhad HS etc
      external xRS_qedc17
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      ALINP1=ALS
      if (IER.eq.1) then
         res(IER)=xRS_qedc17(S)
      else if (IER.eq.2) then
         res(1)=xRS_qedc17(S)          ! function with central value alpha_s to have central values in commons filled via rqcdHS
         res(IER)=0.d0
      else 
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         RSP=xRS_qedc17(S)
         ALINP1=ALS-EST
         RSM=xRS_qedc17(S)            
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
         res(1)=xRS_qedc17(S)          ! recall function with central value alpha_s to get central values in commons filled via rqcdHS
      endif
      RSpQCD_qedc17=res(IER)
      return
      end
c
      function xRS_qedc17(s)
c interface for Harlander--Steinhauser routine for calculating R(s) in pQCD
c provide local parameters via common var: ALINP= alpha_s(EINP), IOR order of pQCD IOR=1,2,3,4
c At given energy E=sqrt(s), contribution from NF=NFext flavors only
c e.g. NFext=3 only uds quarks taken into account, NFext=5 means no top contribution
c even above top threshold, etc
c create and link object rqcdHS.o in rhad package directory
      implicit none
      integer  IOR,NF
      real *8 pi,s,R,xRS_qedc17,ALINP,EINP,MTOP
      common/pqcdHS_qedc17/pi,ALINP,EINP,MTOP,IOR,NF
c      write (*,*) ' xRS_qedc17: params',ALINP,EINP,MTOP,IOR,NF
      call rqcdHS(s,R,ALINP,EINP,MTOP,IOR,NF, 17) 
      xRS_qedc17=R
      return
      end
c
      SUBROUTINE getindex_qedc17(E,N,X,I)
      IMPLICIT NONE
      LOGICAL lwrite
      INTEGER N,I
      REAL*8 E
      REAL*8 X(N)
      common/pri_qedc17/lwrite
      I=N
      IF (E.GT.X(1)) THEN
         DO WHILE (E.LT.X(I))
            I=I-1
         ENDDO
      ELSE 
         I=1
         if ((lwrite).and.(E.LT.X(I))) then
            write (*,*) ' getindex: out of range E below X(1)',E,X(1)
         endif
      ENDIF
      RETURN
      END
C
      FUNCTION RCHPT_qedc17(S,IPM)
C      ---------------------------
C 2PI-Threshold:Integrand
      IMPLICITREAL*8(A-Z)
      INTEGER IPM,IPD
      MPLUS=139.57018D0         ! pm 0.00035
      URF=197.327053D0          ! MeV*fm
      URI=1.D-3                 ! convert MeV into GeV
      MPLUS=MPLUS*URI
      MP2=MPLUS*MPLUS
      IPD=0
C Chiral expansion of pion form factor to two-loop
C J. Gasser, U.-G. Meissner, Nucl. Phys. B357 (1991) 90
C <r^2>^pi_V=(0.439)0.427+/-0.010 fm^2 from space-like data
C S. R. Amendolia et al., Nucl. Phys. B277 (1986) 168 [NA7]
C c^pi_V=4.1 +0.2/-0.6 GeV^{-4}
      IF (IPM.EQ. 1) THEN
         C1=0.437D0/6.D0/URF**2/URI**2
         C2=4.3D0
      ELSE IF (IPM.EQ.-1) THEN
         C1=0.417D0/6.D0/URF**2/URI**2
         C2=3.5D0
      ELSE IF (IPM.EQ. 0) THEN
         C1=0.427D0/6.D0/URF**2/URI**2
         C2=4.1D0
      ENDIF
cC use Colangelo, Finkemeier, Urech PRD 54 (1996) 4403
c     C just use normal expansion [non-Pade]
c       IF (IPM.EQ. 1) THEN
c         C1=0.457D0/6.D0/URF**2
c         C2=4.2D0*URI**4
c       ELSE IF (IPM.EQ.-1) THEN
c         C1=0.405D0/6.D0/URF**2
c         C2=2.2D0*URI**4
c       ELSE IF (IPM.EQ. 0) THEN
c         C1=0.431D0/6.D0/URF**2
c         C2=3.2D0*URI**4
c       ENDIF
      IF (IPD.EQ.0) FVCHPT=1.D0+C1*S+C2*S**2
      IF (IPD.EQ.1) FVCHPT=(1.D0+(C1-C2/C1)*S)/(1.D0-C2/C1*S)
      IF (IPD.EQ.2) FVCHPT=1.D0/(1.D0-C1*S-(C2-C1**2)*S**2)
c       FVCHPT=1.D0+C1*S+C2*S**2
      FVCHPT2=FVCHPT**2
      IF (S.GT.4.D0*MP2) THEN
         RCHPT_qedc17=(DSQRT(1.D0-4.D0*MP2/S))**3*
     .        FVCHPT2/4.d0
      ELSE
         RCHPT_qedc17=0.D0
      ENDIF
      RETURN
      END
c      
      function RCHPTnew_qedc17(S,IPM)
C     ---------------------------
C  s in GeV^2 based on Davier et al ee fit
      IMPLICITREAL*8(A-Z)
      INTEGER IPM,IPD
      MP2 = 139.57018D-3 * 139.57018D-3
      fracterror=0.012358d0
      c1= 6.35046D+00 
      c2= -2.25567D+01
      c3= 1.40482D+02
      x=s*1.d-6
      polynom_qedc17=1.d0+c1*x+c2*x**2+c3*x**3
      FVCHPT2new=polynom_qedc17
      IF (S.GT.4.D0*MP2) THEN
         RCHPTnew_qedc17=(DSQRT(1.D0-4.D0*MP2/S))**3*
     .        FVCHPT2new/4.d0
         IF (IPM.EQ. 1) THEN
            RCHPTnew_qedc17=RCHPTnew_qedc17*(1.d0+fracterror)
         ELSE IF (IPM.EQ.-1) THEN
            RCHPTnew_qedc17=RCHPTnew_qedc17*(1.d0-fracterror)
         ELSE 
         ENDIF
      ELSE
         RCHPTnew_qedc17=0.D0
      ENDIF
      RETURN
      END
C
      function rs40x_qedc17(s)
c interface for RS40_qedc17(s,IER): function which yields R(s)[IER=1], delta R statistical [IER=2] 
c or delta R systematic [IER=3]
      implicit none
      integer IER
      real *8 rs40x_qedc17,s,RS40_qedc17
      external RS40_qedc17              
c error fag ier=1,2,3 for result,staterr, syserr
      COMMON/ERR_qedc17/IER
         rs40x_qedc17=RS40_qedc17(s,IER)
c      write (*,*) ' rs40x_qedc17:',sqrt(s),rs40x_qedc17
      return
      end
C
       FUNCTION BW_qedc17(S,M,G,P)
C      --------------------
C      BREIT WIGNER
C updated 09/12/14 NW --> relativistic BW_qedc17, modified s dependence extra s/M**2 
       implicit none
       INTEGER IRENBW
       REAL*8 BW_qedc17,S,E,M,M2,G,P,XP,GP
       real*8 BWRENO_qedc17,BWrenofar_qedc17,reno,fracerr,UGM2,sGeV
       external BWRENO_qedc17,BWrenofar_qedc17
       common /renoBW_qedc17/IRENBW
       E=DSQRT(S)
       UGM2=1.D6
       sGeV=s/UGM2
c turns physical (dressed) cross section into bare one (undressed) 
c assuming mass and width are the physical ones as given by PDG
       if (IRENBW.eq.2) then
          reno=BWRENO_qedc17(sGEV,fracerr)
       else if (IRENBW.eq.1) then
          reno=BWrenofar_qedc17(-sGEV,fracerr)
       else
          reno=1.d0
       endif
CC Subtract electromagnetic decay channels: correction XP
C       MX=E
C       IF (MX.GT.1.D2) MX=MX*1.D-3
C       SR=MX**2
C       if (MX.LT.3.6D0) then
C         RL=2.d0
C       else
C         RL=3.d0
C       endif
CC       XP=1.D0-(RL+RS(SR))*P
C       XP=1.D0-(RL)*P
       XP=1.D0
       GP=G*XP
c       BW_qedc17=G*GP*P/((E-M)**2+G**2/4.D0)*reno       
C       write(*,*) MX,XP
C Relativistic form
C
       M2=M**2
       BW_qedc17=4.D0*G*GP*P*S/((S-M2)**2+M2*G**2)*s/M2*reno
       RETURN
       END

       FUNCTION BWdressed_qedc17(S,M,G,P)
C      --------------------
C      BREIT WIGNER
C updated 09/12/14 NW --> relativistic BW_qedc17; s-dependence such that Froissart bound holds for 
C s --> infty i.e cross section propto 1/s as well as decoupling for s --> 0 
C sigma = 3 pi/M2 BW_qedc17 to get R fac x s/M2 BW_qedc17  so we redefine BW_qedc17 --> BW_qedc17*s/M2 i.e. R=fac*BW_qedc17  
       implicit none
       REAL*8 BWdressed_qedc17,S,M,M2,G,P
       M2=M**2
       BWdressed_qedc17=4.D0*G**2*P*S/((S-M2)**2+M2*G**2)*s/M2
       RETURN
       END

      FUNCTION BWRENO_qedc17(s,fracerr)
c undressing BW_qedc17 resonances X table points (resonance regions), Y=reno factor, Z fractional error of Y
c 31/12/2014 implemented ini renotables for narrow resonances here
      implicit none
      integer nx,ny,ini,iome,iphi,npps(6),npyp(6),nres(15)
      parameter(nx=400,ny=15)
      integer j,I,N(ny),ii,jj,ini_reso_reno_dat
c      double complex r1cx(nx,ny)
      double precision BWRENO_qedc17,s,e,null
      double precision renfac,fracerr
      double precision X(nx,ny),Y(nx,ny),Z(nx,ny)
      double precision xome(131),yome(131,2),xphi(95),yphi(95,2),
     &     xpsi1(359),xpsi2(213),xpsi3(222),xpsi4(325),
     &     xpsi5(341),xpsi6(81),
     &     xyps1(202),xyps2(155),xyps3(163),xyps4(71),
     &     xyps5(388),xyps6(160),
     &     ypsi1(359,2),ypsi2(213,2),ypsi3(222,2),ypsi4(325,2),
     &     ypsi5(341,2),ypsi6(81,2),
     &     yyps1(202,2),yyps2(155,2),yyps3(163,2),yyps4(71,2),
     &     yyps5(388,2),yyps6(160,2)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc17/EMI,EMA
c      common /renormBW/X,Y,Z,r1cx,N
c following common filled by call resonances_renocomplex()
      common /renormBW_qedc17/X,Y,Z,Nres,ini_reso_reno_dat
      DATA iome,iphi /131,95/
      DATA NPPS /359,213,222,325,341, 81/
      DATA NPYP /202,155,163, 71,388,160/
      DATA ini /0/
      include 'reso_reno.dat'
c      ini_reso_reno_dat=0 ! recalculate renormalization factors via alphaQED
      if (ini.eq.0) goto 2
 1    continue
      null=0.d0
      E=SQRT(s)
      j=ny
      do while ((E.LT.EMI(j)).and.(j.gt.2))
         j=j-1
      enddo
      if (E.LT.EMA(j)) then
         I=Nres(j)
         DO WHILE ((I.gt.2).and.(E.LT.X(I,j)))
            I=I-1
         ENDDO
         renfac =Y(I,j)+(Y(I+1,j)-Y(I,j))/(X(I+1,j)-X(I,j))*(E-X(I,j))
         fracerr=Z(I,j)+(Z(I+1,j)-Z(I,j))/(X(I+1,j)-X(I,j))*(E-X(I,j))
      else 
         renfac=1.d0
         fracerr=0.d0
      endif
      BWRENO_qedc17=renfac
 2    continue
      if (ini.eq.0) then
c         if (ini_reso_reno_dat.eq.0) then
            jj=2
            do ii=1,iome
               x(ii,jj)=xome(ii)
               y(ii,jj)=yome(ii,1)
               z(ii,jj)=yome(ii,2)
            enddo
            nres(jj)=iome
            jj=3
            do ii=1,iphi
               x(ii,jj)=xphi(ii)
               y(ii,jj)=yphi(ii,1)
               z(ii,jj)=yphi(ii,2)
            enddo
            nres(jj)=iphi
            do i=1,6
               jj=jj+1
               do ii=1,npps(i)
                  if (i.eq.1) x(ii,jj)=xpsi1(ii)
                  if (i.eq.1) y(ii,jj)=ypsi1(ii,1)
                  if (i.eq.1) z(ii,jj)=ypsi1(ii,2)
                  if (i.eq.2) x(ii,jj)=xpsi2(ii)
                  if (i.eq.2) y(ii,jj)=ypsi2(ii,1)
                  if (i.eq.2) z(ii,jj)=ypsi2(ii,2)
                  if (i.eq.3) x(ii,jj)=xpsi3(ii)
                  if (i.eq.3) y(ii,jj)=ypsi3(ii,1)
                  if (i.eq.3) z(ii,jj)=ypsi3(ii,2)
                  if (i.eq.4) x(ii,jj)=xpsi4(ii)
                  if (i.eq.4) y(ii,jj)=ypsi4(ii,1)
                  if (i.eq.4) z(ii,jj)=ypsi4(ii,2)
                  if (i.eq.5) x(ii,jj)=xpsi5(ii)
                  if (i.eq.5) y(ii,jj)=ypsi5(ii,1)
                  if (i.eq.5) z(ii,jj)=ypsi5(ii,2)
                  if (i.eq.6) x(ii,jj)=xpsi6(ii)
                  if (i.eq.6) y(ii,jj)=ypsi6(ii,1)
                  if (i.eq.6) z(ii,jj)=ypsi6(ii,2)
               enddo
               nres(jj)=npps(i)
            enddo
            do i=1,6
               jj=jj+1
               do ii=1,npyp(i)
                  if (i.eq.1) x(ii,jj)=xyps1(ii)
                  if (i.eq.1) y(ii,jj)=yyps1(ii,1)
                  if (i.eq.1) z(ii,jj)=yyps1(ii,2)
                  if (i.eq.2) x(ii,jj)=xyps2(ii)
                  if (i.eq.2) y(ii,jj)=yyps2(ii,1)
                  if (i.eq.2) z(ii,jj)=yyps2(ii,2)
                  if (i.eq.3) x(ii,jj)=xyps3(ii)
                  if (i.eq.3) y(ii,jj)=yyps3(ii,1)
                  if (i.eq.3) z(ii,jj)=yyps3(ii,2)
                  if (i.eq.4) x(ii,jj)=xyps4(ii)
                  if (i.eq.4) y(ii,jj)=yyps4(ii,1)
                  if (i.eq.4) z(ii,jj)=yyps4(ii,2)
                  if (i.eq.5) x(ii,jj)=xyps5(ii)
                  if (i.eq.5) y(ii,jj)=yyps5(ii,1)
                  if (i.eq.5) z(ii,jj)=yyps5(ii,2)
                  if (i.eq.6) x(ii,jj)=xyps6(ii)
                  if (i.eq.6) y(ii,jj)=yyps6(ii,1)
                  if (i.eq.6) z(ii,jj)=yyps6(ii,2)
               enddo
               nres(jj)=npyp(i)
            enddo
            do i=1,15
               N(i)=nres(i)
            enddo 
            ini=ini+1
            write (*,*) ' nres=',nres
c         else if (ini_reso_reno_dat.eq.1) then
cc            call resonances_renocomplex() ! fill common renormBW VP subtraction on physical resonances
c         endif
         goto 1
      endif
      RETURN
      END

      FUNCTION BWrenofar_qedc17(s,fracerror)
c get vacuum polarization subtraction for Breit-Wigner resonances
c only implemented for space-like case as an average charge in the time-like region
c as for Zweig supressed resonaces J/psi, psi1, Upsilon 1,2,3 time-like subtraction iteration does not converge
c energy unit GeV
      implicit none
      double precision BWrenofar_qedc17,s,fracerror,null
      double precision r1r,r1h,r1l,reno,renl,renh,errorx
      double precision dalept,dahadr,daltop,Dalphaweak1MSb
      double precision one,dggvapx_qedc17,dvpt_new,dvpt_hig,dvpt_low
     &     ,error,errorsta,errorsys,renoreal,e
      common /resu_qedc17/dalept,dahadr,daltop,Dalphaweak1MSb 
      external dggvapx_qedc17
      include 'common.h'
      LEPTONflag='all'
      iLEP  = -3  ! for sum of leptons + quarks              
      null=0.d0
      one=1.d0
      dvpt_new= dggvapx_qedc17(s,errorx,errorsta,errorsys)
      dvpt_hig= dvpt_new+errorx
      dvpt_low= dvpt_new-errorx
      r1r=one-dvpt_new
      r1h=one-dvpt_hig
      r1l=one-dvpt_low
      reno=r1r**2
      renh=r1h**2
      renl=r1l**2
      error=abs((renh-renl)/2.0d0)/reno
      BWrenofar_qedc17=reno
      fracerror=error
c      e=s/abs(s)*sqrt(abs(s))
c      write (231,'(2x,F12.6,4(2x,1pe11.4))') e,
c     &     reno,dalept,dahadr
      return
      end
