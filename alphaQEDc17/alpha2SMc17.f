*;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; alphaQED.f --- 
*; Author          : Fred Jegerlehner
*; Created On      : Sat Nov  1 22:49:46 2003
*; Last Modified By: Friedrich Jegerlehner
*; Last Modified On: Fri Jul 21 14:59:53 2017
*; RCS: $Id$
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; Copyright (c) 2017 Fred Jegerlehner
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; 
      PROGRAM alpha2SMc17
C Calculating the running QED coupling alpha(E)
C Leptons: 1--loop, 2--loop exact, 3--loop in high energy approximation
C Quarks form routine dhadr5n including effective s--channel shifts in low energy range
C r1....,r2....,r3.... : oneloop, twoloop, threeloop result
C  ..real: realpart, ..imag: imaginary part;
C ......l: light fermions only (high energy approximation for 3--loops) 
C dallepQED1n=r1real,dallepQED2n=r2real,dallepQED3l=r3reall
C dallepQED3l also returns oneloop and twoloop high energy (light fermion) 
C approximations 
c 21/09/2009 common pQCD parameter setting via constants_qcd.f      
c FJ 28/01/2012 recalculated Nall=2055 --> 2177
c FJ March 2012: new (ud) and (s) flavor separation of exclusive channel data up to 2 GeV
c                now obsolete (corresponded to perturbative reweighting assuming OZI violation to be negiligible
c                which by lattice QCD checks turns out to be off by 5% the 9/20 --> 10/20 problem)
c 20/12/2016 vversion with new rewighting (ud) contribution from 9/20 to 1/2 isospin + rho dominance argument
c 10/07/2017 providing statistical and systematic hadronic errors separately now
c            allows to set up the covariance matrix
      implicit none
      integer i,j,N,stflag,logflag,Ismooth,Nall,ndat,N1,N2
      parameter(Nall=2187)
      REAL *8 X(Nall)
      INTEGER NA,NB
      PARAMETER(NA=979,NB=200)
      REAL ETX(NA),ESX(NB)
      real*8 s,e,de,null,cvap2,rvap2
      real*8 st2,als,mtop,alp2
      real*8 emax,emin,elogl,elogu,delog,elog
      double precision CHPTCUT,EC,ECUT
      double precision pi1,ALINP,ERRAL,EINP,MTOP1
      double complex cglept,cghadr,cgetop,cvpt_new,cvpt_hig,cvpt_low
      double complex alpha2c,alpha2h,alpha2l,alpha2ct,alpha2ht,alpha2lt,
     &     cDalpha2weak1MSb,cone,calp2
      double complex cerror,cerrorsta,cerrorsys,cegvapx,comPi
      external cegvapx
********************* part to be included in main program **************
      include 'common.h'      
      common /parm/st2,als,mtop
      common /cesg/cglept,cghadr,cgetop,cDalpha2weak1MSb
      common/var1/pi1,ALINP,ERRAL,EINP,MTOP1
c commons needed to get R(s)
      COMMON/RCUTS/CHPTCUT,EC,ECUT
      COMMON/SMOO/Ismooth
      call constants()
      call constants_qcd() ! fills commons var and RCUTS
c      sin2ell=0.23153 ! pm 0.00016 LEPEEWG Phys Rep 427 (2006) 257
      st2=0.23153d0   ! Reference value for weak mixing parameter
      als=ALINP       ! alpha strong
      mtop=MTOP1      ! top quark mass
      Ismooth=0       ! flag for imaginary part: 0= R(s) data; 1= R(s) fits
c following two entried set as default in chad5n14.f
c      IRESON=1        ! include narrow resonances in fits
c      iresonances=1   ! include narrow resonances in data sets
      call Rdata()
c      call resonances_renoreal()
      call resonances_renocomplex()
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment in main the following 2 lines for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau
c      iLEP=LFLAG(LEPTONflag)
************************************************************************
c
      null=0.d0
      alp2=alp/st2
      cone=DCMPLX(1.d0,null)
      calp2=DCMPLX(alp2,null)
      N=2000
c Scan with energy point raster xRdat-extended
c      ndat=1 low energy time-like data points extended by resonance scans
c      ndat=2 pQCD tail
c      ndat=-1 space-like region
      ndat=1
c      ndat=0
c
      include 'xRdat-extended.f' ! just DATA X/.../ statement
c      
c caution: logatithmic scaling used below: cannot choose emin=0 or emax=0 !!!!
c      
      logflag=0
      stflag =1
c     for spacelike region: stflag=-1
c      stflag =-1
      emin=1.d-6
      emin=.28d0
c      emax=1.0750d0
c      emin=.390d0
c      emax=100.0d0
c      emin=.970d0
c      emin=1.30d0
c      emax=5.50d0
c      emin=9.40d0
c      emax=12.50d0
c      emax=35.0
c      emin=1.d-6
c      emax=9.00d0
c      emax=11.5d0
c      emax=3.6d0
c      emin=3.0d0
c      emin=3.2d0
c      emin=9.4d0
      emax=11.5d0
c      emin=9.5d0
c      emax=13.d0
c      emin=3.6d0
c      emax=5.2d0
c      emin=5.2d0
c      emax=9.5d0
c      emax=91.19d0
c J/psi,psi's
c      emin=3.09680 ! xmin psi1
c      emax=3.09890 ! xmax psi1
c      emin=3.68500 ! xmin psi2
c      emax=3.68700 ! xmax psi2
c      emin=3.75000 ! xmin psi3
c      emax=3.80000 ! xmax psi3
c      emin=3.96000 ! xmin psi4
c      emax=4.10000 ! xmax psi4
c Upsilon's
c 9460.30D
c      emin= 9.460250            ! xmin ups1
c      emax= 9.460350            ! xmax ups1
c      emin=10.023200 ! xmin ups2
c      emax=10.023320 ! xmax ups2
c      emin=10.354200 ! xmin ups3
c      emax=10.356200 ! xmax ups3
      write (*,*) ' Prepared scans with energy point raster ',
     &     'xRdat-extended'
      write (*,*) ' ndat=1 low energy time-like data points ',
     &     'extended by resonance scans'
      write (*,*) ' ndat=2 pQCD tail'
      write (*,*) ' ndat=3 test resonances at peak'
      write (*,*) ' ndat=-1 space-like region'
      write (*,*) ' ndat=0 prompt for individual range'
      write (*,*) ' Enter ndat:'
      read  (*,*) ndat
      if (ndat.eq.0) then
      write (*,*) ' Enter energy range and number of points: ',
     &     'emin,emax,N' 
      read (*,*) emin,emax,N
      endif
      if (ndat.eq.1) then
      write (*,*) ' Enter energy range: ',
     &     'emin,emax' 
      read (*,*) emin,emax
      endif
      if (ndat.eq.0) then
         write (*,*) ' stflag=1/-1 [time/space-like] ',
     &        'logflag=0/1 [linear/log scale]'
         write (*,*) ' Enter stflag,logflag:'
         read (*,*) stflag,logflag
      endif
      if (ndat.eq.1) then
         call getindex(emin,Nall,X,N1)
         call getindex(emax,Nall,X,N2)
      else 
c      if (ndat.eq. 1).and N=Nall-1
         if (ndat.eq. 2) N=NB-1
         if (ndat.eq.-1) N=NA-1
c      if (ndat.eq. 3) N=45-1
         if (ndat.eq. 3) N=15-1
         N1=1
         N2=N+1
      endif
c write header for graphx plot program
      do j=1,4
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     real parts'
      enddo
      do j=11,14
         write (j,*) '      4.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N2-N1)
         write (j,*) '     imaginary parts'
      enddo
         write (8,*) '      3.0000'
         write (8,*) '      1.0000'
         write (8,*) '   ',float(N2-N1)
         write (8,*) '  |1-\Pi(s)|^2-1, (alpha2/alpha2(s))^2-1,',
     &        '|1-\Pi(s)|^2/(alpha2/alpha2(s))^2-1,',
     &        '|1-\Pi(s)|^2-(alpha2/alpha2(s))^2'
c     ..................................................................
c     vacuum polarization  ...[c] central ...l low value, ...h high value
      write (1,*) ' e,dalpha2,dalpha2_low,dalpha2_hig (no top)' 
      write (2,*) ' e,alpha2 ,alpha2l ,alpha2h (no top)    '
      write (3,*) ' e,alpha2t,alpha2tl,alpha2th (with top)   ' 
      write (4,*) ' e,dglept,dghadr,dgetop'
      write(11,*) ' e,dalpha2,dalpha2_low,dalpha2_hig (no top)' 
      write(12,*) ' e,alpha2 ,alpha2l ,alpha2h (no top)    '
      write(13,*) ' e,alpha2t,alpha2tl,alpha2th (with top)   ' 
      write(14,*) ' e,dglept,dghadr,dgetop'
c
      de=(emax-emin)/N
      e=emin
      elogl=dlog(dabs(emin))
      elogu=dlog(dabs(emax))
      delog=(elogu-elogl)/N
c
      do 10 i=N1,N2
         if (ndat.eq.1) then
            e=X(i)
            s=e*e
         else if (ndat.eq.2) then
            e=ESX(i)
            s=e*e
         else if (ndat.eq.-1) then
            e=ETX(i)
            s=-e*e
         else 
            if (logflag.eq.1) then
               if (stflag.eq.1) then
                  elog=elogl+delog*(i-1)
               else
                  elog=elogu-delog*(i-1)
               endif
               e=dexp(elog)
               s=e*e*stflag
               e=dsqrt(dabs(s))*stflag
            else
               e=e+de
               s=e*abs(e)
            endif
         endif
c
      cvpt_new= cegvapx(s,cerror,cerrorsta,cerrorsys)
      cvpt_hig= cvpt_new+cerror
      cvpt_low= cvpt_new-cerror
      alpha2c =calp2/(cone-cvpt_new)
      alpha2h =calp2/(cone-cvpt_hig)
      alpha2l =calp2/(cone-cvpt_low)
      alpha2ct=calp2/(cone-cvpt_new-cgetop-cDalpha2weak1MSb)
      alpha2ht=calp2/(cone-cvpt_hig-cgetop-cDalpha2weak1MSb)
      alpha2lt=calp2/(cone-cvpt_low-cgetop-cDalpha2weak1MSb)
c
c     delta alpha2: leptons + 5 quarks no top
c
      write (1,98) e,DREAL(cvpt_new),DREAL(cvpt_low),DREAL(cvpt_hig)
      write(11,98) e,AIMAG(cvpt_new),AIMAG(cvpt_low),AIMAG(cvpt_hig)
c
c     alpha2: leptons + 5 quarks no top
c
      write (2,98) e,DREAL(alpha2c),DREAL(alpha2l),DREAL(alpha2h)
      write(12,98) e,AIMAG(alpha2c),AIMAG(alpha2l),AIMAG(alpha2h)
c
c     alpha2: leptons + 5 quarks + top + W (1-loop MSbar)
c
      write (3,98) e,DREAL(alpha2ct),DREAL(alpha2lt),DREAL(alpha2ht)
      write(13,98) e,AIMAG(alpha2ct),AIMAG(alpha2lt),AIMAG(alpha2ht)
c
c     delta alpha2: leptons, top , 5 quarks, 5 quarks using effective parameters
c
c      write (4,98) e,DREAL(cglept),DREAL(cghadr),DREAL(cgetop),
c     &     DREAL(cDalpha2weak1MSb)
      write (4,98) e,DREAL(cghadr),DREAL(cerror),DREAL(cerror)
      write(14,98) e,AIMAG(cglept),AIMAG(cghadr),AIMAG(cgetop)
c
c     |1-\PI'(s)|
c
      comPi=calp2/alpha2ct
      comPi=cone/comPi
      cvap2 =DREAL(comPi*DCONJG(comPi))
      rvap2 =(DREAL(alpha2ct)/alp2)**2
      write (8,99) e,cvap2-1.d0,rvap2-1.d0,cvap2/rvap2-1.d0,cvap2-rvap2
c
 10   continue
c     ..................................................................
c detailed resonance scans require high resolution
 98   format(5(2x,1pe11.4))
 99   format(5(2x,1pe15.8))
      stop
      END
c
