      subroutine cerrortot_qedc17(cerror,cerrorsta,cerrorsys)
c calculates total complex error from complex stat and syst errors
      implicit none
      double complex cerror,cerrorsta,cerrorsys
      double precision realerrorsta2,imagerrorsta2,
     &     realerrorsys2,imagerrorsys2,realerrortot,imagerrortot
      common /realtoterr_qedc17/realerrortot,imagerrortot
c
      realerrorsta2=DREAL(cerrorsta)**2
      imagerrorsta2=AIMAG(cerrorsta)**2
      realerrorsys2=DREAL(cerrorsys)**2
      imagerrorsys2=AIMAG(cerrorsys)**2
      realerrortot=sqrt(realerrorsta2+realerrorsys2)
      imagerrortot=sqrt(imagerrorsta2+imagerrorsys2)
      cerror=DCMPLX(realerrortot,imagerrortot)
      return
      end
