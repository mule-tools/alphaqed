rhad = 'rhad-1.01'
import re
import tqdm

# src = [
#   'leptons.f',
#   'hadr5n17.f',
#   'dggvapx.f',
#   'constants.f',
#   'constants_qcd.f',
#   'Rdat_all.f',
#   'Rdat_fun.f',
#   'Rdat_fit.f',
#   'resonances_dat.f',
#   'chadr5n17.f',
#   'cggvapx.f',
#   'resonances_renc.f',
#   'RS3G_fun.f',
#   'RS33_fun.f',
#   'RS3G_fit.f',
#   'RS33_fit.f',
#   'cerrortot.f',
#   'alphaQEDc17.f',
# ]

# src += [
#   'common.h',
#   'dalhadshigh17.f',
#   'dalhadslow17.f',
#   'dalhadt17.f',
#   'epem33fit079081.h',
#   'epem33fit0814new.h',
#   'epem33fit1425.h',
#   'epem3gfit079081.h',
#   'epem3gfit0814new.h',
#   'epem3gfit1425.h',
#   'epemfit079081.h',
#   'epemfit0814new.h',
#   'epemfit1432.h',
#   'epemfit32cut.h',
#   'ffppg2fit0810.h',
#   'ffppmfit0810.h',
#   'r33fitrhohig.h',
#   'r33fitrholow.h',
#   'r3gfitrhohig.h',
#   'r3gfitrholow.h',
#   'Rdat_upd.f',
#   'reso_reno.dat',
#   'rfitrhohig.h',
#   'rfitrholow.h',
#   'xRdat-extended.f',
#   'xRdat-spacelikehigh.f',
# ]

src = [
  rhad + '/r012.f',
  rhad + '/r34.f',
  rhad + '/runal.f',
  rhad + '/funcs.f',
  rhad + '/vegas-rhad.f',
  rhad + '/parameters.f',
  rhad + '/rhad.f',
  rhad + '/rqcdHSn.f'
]


def read(fn):
    print(fn)
    with open(fn, "r") as fp:
        return fp.read()


def search(needle):
    return set(
        map(
            lambda x: x.lower(),
            re.findall(needle, allcode, re.M | re.I),
        )
    )


allcode = "".join([read(f) for f in src])

subroutines = search(r"^ *subroutine +([^\(\n]*)[\(\n]")
called_subroutines = search(r"^[^c].*call +([^\(\n ]*)[\(\n]") - {'derivs', 'vegas1'}
functions = search(r"^ *function +([^\(\n]*)[\(\n]")
commonblocks = search(r"common */([^/]*)/")

print(functions)
def add_suffix(s):
    return s.group(1) + "_qedc17" + s.group(2)
def add_suffix1(s):
    return s.group(1) + "_qedc17"


for i in tqdm.tqdm(src):
    with open(i) as fp:
        code = fp.read()

    code = re.sub(r"^([^c] *subroutine +[^\(\n]*)([\(\n ])", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c].*\bcall +[^\(\n ]*)([\(\n ])", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c] *common */[^/]*)(/)", add_suffix, code, flags=re.I|re.M)
    code = re.sub(r"^([^c] *save */[^/]*)(/)", add_suffix, code, flags=re.I|re.M)
    for fn in sorted(functions, key=lambda a: -len(a)):
        code = re.sub(r"(\b"+fn+r"\b)", add_suffix1, code, flags=re.I|re.M)

    with open(i, "w") as fp:
        fp.write(code)
