c  30 JAN 01 O.Veretin
c
c  This implements the function ARUN
c  running MSbar alphaQCD with decoupling of heavy
c  quarks as descibed in
c  
c  
c  K.G. Chetyrkin, J.H. Kuhn and, M. Steinhauser
c
c  "RUNDEC: A Mathematica Package for Running and Decoupling
c   of the Strong Coupling and Quark Masses.",
c
c   Comput.Phys.Commun.133 (2000) 43;
c   hep-ph/0004189 
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c  This is the main function!

      FUNCTION  ARUN ( alpha0, mu0, Mth, muth, mu1, loops, nf1 )
      implicit none
      real*8 ARUN, alpha0, mu0, Mth(6), muth(6), mu1
      integer loops
c
c  Given: 
c       alpha0(mu0) --- initial value at reference point
c       mu0         --- reference point
c       Mth(1:6)    --- array of thresholds (in On-Shell scheme) 
c       muth(1:6)   --- array of decoupling scales
c       mu1         --- new point
c       loops       --- number of loops (1,2,3,4) 
c       


      real*8 muini, aini, alpha1, alpha2
      integer nnf, nf0, nf1, i

      real*8   LAMEXP, ALAM, ADECU, ADECD, AS1
      external LAMEXP, ALAM, ADECU, ADECD, AS1

c-- determine first the number of active flavours 
c   at the reference points  'mu0'  and  'mu1'

      nf0 = 0
      nf1 = 0
        do i = 1, 6
        if ( mu0 .GT. Mth(i) ) nf0 = nf0 + 1
        if ( mu1 .GT. Mth(i) ) nf1 = nf1 + 1
        enddo    
        if (loops.eq.0) then
           ARUN =alpha0
           RETURN
        endif
c  NO DECOPLING

        if ( nf1 .EQ. nf0 ) then
        alpha1 = AS1 ( alpha0, mu0, mu1, nf0, loops )
        ARUN = alpha1
c        write(6,*) 'no decoupling, nf = ',nf0
        RETURN
        endif

c  nf1 > nf0. Evolution upward

        if ( nf1 .GT. nf0 ) then
        muini = mu0
        aini = alpha0
          DO nnf = nf0, nf1-1
          alpha1 = AS1 ( aini, muini, muth(nnf+1), nnf, loops )
          alpha2 = ADECU ( alpha1, Mth(nnf+1), muth(nnf+1), nnf, loops) 
          aini = alpha2
          muini = muth(nnf+1)
          ENDDO
        ARUN = AS1 ( aini, muini, mu1, nf1, loops )
c        write(6,*) 'evolution up'
        RETURN         
        endif

c  nf1 < nf0. Evolution downward

        if ( nf1 .LT. nf0 ) then
        muini = mu0
        aini = alpha0
          DO nnf = nf0, nf1+1, -1
          alpha1 = AS1 ( aini, muini, muth(nnf), nnf, loops )
          alpha2 = ADECD ( alpha1, Mth(nnf), muth(nnf), nnf, loops) 
          aini = alpha2
          muini = muth(nnf)
          ENDDO
        ARUN = AS1 ( aini, muini, mu1, nf1, loops )
c        write(6,*) 'evolution down'
        RETURN         
        endif

      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  BETA ( order, nf )
      implicit none
      real*8 BETA
      integer order, nf

c Returns the coefficients of beta function in QCD b0,b1,b2 and b3
c Arguments:
c       order = 0,1,2 or 3  --- order of perturbation theory
c       nf    -- number of active flavours
c
c  Analytically coefficients are given by
c
c    b0 -> 11/4 - nf/6, 
c    b1 -> 51/8 - (19*nf)/24,
c    b2 -> 2857/128 - (5033*nf)/1152 + (325*nf^2)/3456,
c    b3 -> 149753/1536 - (1078361*nf)/41472 + (50065*nf^2)/41472 +
c	(1093*nf^3)/186624 + (891*Zeta[3])/64 
c       - (1627*nf*Zeta[3])/1728 +
c	    (809*nf^2*Zeta[3])/2592
c
      real*8 nfbeta

      nfbeta = nf
      goto (10,11,12,13)  order+1
      write(6,*) 'BETA: Wrong first argument.'
      stop    
 10   BETA = 2.75D0 - 0.1666666666666667D0 * nfbeta
      return
 11   BETA = 6.375D0 - 0.7916666666666666D0 * nfbeta
      return
 12   BETA = 22.3203125D0 - 4.368923611111111D0 * nfbeta 
     $       + 0.09403935185185185D0 * nfbeta**2
      return
 13   BETA = 114.2303286570083D0 - 27.13394381642013D0 * nfbeta
     $       + 1.582379064296339D0 * nfbeta**2 
     $       + 0.005856695816186557D0 * nfbeta**3
      return
      END



ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  LAMEXP ( alpha, mu, nf, loops )
      implicit none
      real*8 LAMEXP, alpha, mu 
      integer nf, loops

c  Given 
c       alpha(mu)
c       mu  (in GeV)
c       nf -- number of flavours
c       loops number of loops  = 1,2,3,4
c
c    this function returns \Lambda_QCD in MSbar scheme (in GeV).
c    The expanded formulae version is used.
 
      real*8 as, b0, b1, b2, b3, LL
      real*8   BETA
      external BETA

      b0 = BETA( 0, nf )
      b1 = BETA( 1, nf )
      b2 = BETA( 2, nf )
      b3 = BETA( 3, nf )

      if ( ( loops .LT. 1 ) .OR. ( loops .GT. 4 ) ) then
      write(6,*) 'LAMEXP: wrong argument: loops'
      stop
      endif

      as = alpha/3.14159265358979D0

c  LL = log(mu^2/Lambda^2)
 
      LL = 1/(as*b0) 
      if ( loops .GT. 1 ) then
      LL = LL + b1*Log(b0)/b0**2 + b1*Log(as)/b0**2 
      endif
      if ( loops .GT. 2 ) then
      LL = LL + as*(-b1**2 + b0*b2)/b0**3  
      endif
      if ( loops .GT. 3 ) then
      LL = LL + as**2*(b1**3 - 2*b0*b1*b2 + b0**2*b3)/(2.D0*b0**4) 
      endif

      LAMEXP = mu * exp( -LL/2 )
      return
      END


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  ALAM ( Lambda, mu, nf, loops )
      implicit none
      real*8 ALAM, Lambda, mu 
      integer nf, loops

c  Given:
c      Lambda (in GeV)
c      mu     (in GeV)
c      nf     number of flavours
c      loops  number of loops
c
c    this function  returns alpha_s QCD in MS scheme
c
      real*8   as, b0, b1, b2, b3, L, LL
      real*8   BETA
      external BETA

      b0 = BETA( 0, nf )
      b1 = BETA( 1, nf )
      b2 = BETA( 2, nf )
      b3 = BETA( 3, nf )

      if ( ( loops .LT. 1 ) .OR. ( loops .GT. 4 ) ) then
      write(6,*) 'ALAM: wrong argument: loops'
      stop
      endif

      L = log( mu**2/Lambda**2)
      LL = log(L)
      as = 1.0D0/L/b0
      if ( loops .GT. 1 ) then
      as = as - 1.0D0/(b0*L)**2 * b1/b0*LL
      endif
      if ( loops .GT. 2 ) then
      as = as + 1.0D0/(b0*L)**3 * (b1**2/b0**2*(LL**2-LL-1.0D0)+b2/b0)
      endif
      if ( loops .GT. 3 ) then
      as = as + (b3/(2*b0**5) - (3*b1*b2*LL)/b0**6 
     $	   + (b1**3*(-0.5D0 + 2*LL + (5*LL**2)/2 - LL**3))/b0**7)/L**4
      endif      

      ALAM = as*3.14159265358979D0
      return
      END


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  AS1 ( alpha0, mu0, mu1, nf, loops )
      implicit none
      real*8  AS1, alpha0, mu0, mu1
      integer nf, loops

c  Given:
c      alpha0 
c      mu0     (in GeV)
c      mu1     (in GeV)
c      nf     number of flavours
c      loops  number of loops
c
c    this evolve alpha from "mu0" to "mu1" using
c    initial condition alpha(mu0) = alpha0
c
c  This routine uses the solution as a series expansion.
c  See also AS2 which uses "exact" solution by solving 
c  the differential equation directly

      real*8 Lambda

      real*8 LAMEXP, ALAM
      external LAMEXP, ALAM     
      
      Lambda = LAMEXP ( alpha0, mu0, nf, loops )
      AS1 = ALAM ( Lambda, mu1, nf, loops ) 
      return
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  AS2 ( alpha0, mu0, mu1, nf, loops )
      implicit none
      real*8  AS2, alpha0, mu0, mu1
      integer nf, loops

c  Given:
c      alpha0 
c      mu0     (in GeV)
c      mu1     (in GeV)
c      nf     number of flavours
c      loops  number of loops
c
c    this evolve alpha from "mu0" to "mu1" using
c    initial condition alpha(mu0) = alpha0
c
c  This solves directly differential equation
c  for the coupling constatn. We use here NAG library.
c  See also AS1 which uses series expansion for alpha

      integer N, IFAIL
      real*8 X, XEND, Y(1), TOL, W(1,7)
      external FCN

      integer nfxxx, loopxx
      common /FCNDAT/nfxxx,loopxx

      nfxxx = nf
      loopxx = loops

      N = 1
      IFAIL = 0
      TOL = 1.0D-5
      X = mu0**2
      XEND = mu1**2
      Y(1) = alpha0/3.1415925D0
c      call D02BAF( X, XEND, N, Y, TOL, FCN, W, IFAIL )
      call D02PCF( X, XEND, N, Y, TOL, FCN, W, IFAIL )
      AS2 = Y(1)*3.1415926D0
      return
      END

c here is the external subroutine
      subroutine FCN( X, Y, F)
      implicit none
      real*8 X, Y(1), F(1)

      real*8 BETA
      integer nfxxx,loopxx
      common /FCNDAT/nfxxx,loopxx

c Below X denotes mu^2 not mu !

      F(1) = BETA( 0, nfxxx )
        if ( loopxx .GT. 1) then
        F(1) = F(1)  + BETA( 1, nfxxx ) * Y(1)
        endif
        if ( loopxx .GT. 2) then
        F(1) = F(1)  + BETA( 2, nfxxx ) * Y(1)**2
        endif
        if ( loopxx .GT. 3) then
        F(1) = F(1)  + BETA( 3, nfxxx ) * Y(1)**3
        endif
      F(1) = - F(1) * Y(1)**2 / X
      return

c      b1 = BETA( 1, nf )
c      b2 = BETA( 2, nf )
c      b3 = BETA( 3, nf )
      end
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  ADECU ( alpha, massth, muth, nl, loops )
      implicit none
      real*8 ADECU, alpha,  muth, massth
      integer nl, loops

c Given
c    alpha(mu)^{(nl)}
c    massth      --- threshold (in OS scheme)
c    muth      
c    nl          --- number of light flavours
c    loops
c
c  this function returns
c              alpha(mu)^{(nl+1)}
c

c  Compute decoupling factor now. It is defined as
c
c  as(n)to_as(n+1)_OS->
c    as6to5os ->
c  1 + (7*api^2)/24 + (58933*api^3)/124416 + (api*lmm)/6 + (19*api^2*lmm)/24 +
c   (8941*api^3*lmm)/1728 + (api^2*lmm^2)/36 + (511*api^3*lmm^2)/576 +
c   (api^3*lmm^3)/216 - (2479*api^3*nl)/31104 - (409*api^3*lmm*nl)/1728 +
c   (2*api^3*z2)/3 - (api^3*nl*z2)/9 + (2*api^3*z2*Log[2])/9 +
c   (80507*api^3*Zeta[3])/27648,
c
c  where lmm = log(mu^2/M_pole^2).
c
c  Note that OS masses are used!
c  Note we need decoupling factor to N-1 loops! Not to N loop!
c##
      real*8 lmm, dec, as

      as = alpha/3.14159265358979D0
      lmm = log( muth**2/massth**2 )

      dec = 1.0D0 
      if ( loops .GT. 1 ) then
      dec = dec + 0.1666666666666666D0 * as *lmm 
      endif
      if ( loops .GT. 2 ) then
      dec = dec + ( 0.2916666666666666D0 
     $              + 0.79166666666666D0 * lmm 
     $              + 0.02777777777777D0 * lmm**2 ) * as**2
      endif
      if ( loops .GT. 3 ) then
      dec = dec + ( 5.32389021383202D0
     $             + 5.1741898148148D0 * lmm 
     $             + 0.8871527777777D0 * lmm**2 
     $             + 0.0046296296296D0 * lmm**3 
     $             + ( - 0.2624708119543296D0
     $                 - 0.2366898148148148 * lmm)*nl
     $            ) * as**3
      endif

      ADECU = alpha * dec
      return
      END


ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      FUNCTION  ADECD ( alpha, massth, muth, nl, loops )
      implicit none
      real*8 ADECD, alpha,  muth, massth
      integer nl, loops

c Given
c    alpha(mu)^{(nl)}
c    massth      --- threshold (in OS scheme)
c    muth      
c    nl          --- number of light flavours
c    loops
c
c  this function returns
c              alpha(mu)^{(nl-1)}
c

c  Compute decoupling factor now. It is defined as
c
c  as(n)to_as(n-1)_OS->
c  1 - (api*lmm)/6 + api^2*(-7/24 - (19*lmm)/24 + lmm^2/36) +
c   api^3*(-58933/124416 - (8521*lmm)/1728 - (131*lmm^2)/576 - lmm^3/216 +
c      nl*(2479/31104 + (409*lmm)/1728 + z2/9) - (2*z2)/3 - (2*z2*Log[2])/9 -
c      (80507*Zeta[3])/27648),
c
c  where lmm = log(mu^2/M_pole^2).
c
c  Note that OS masses are used!
c  Note we need decoupling factor to N-1 loops! Not to N loop!

      real*8 lmm, dec, as

      as = alpha/3.14159265358979D0
      lmm = log( muth**2/massth**2 )

      dec = 1.0D0 
      if ( loops .GT. 1 ) then
      dec = dec - 0.1666666666666666D0 * as *lmm 
      endif
      if ( loops .GT. 2 ) then
      dec = dec + ( -0.2916666666666666D0 
     $              - 0.791666666666666D0 * lmm 
     $              + 0.027777777777777D0 * lmm**2 ) * as**2 
      endif
      if ( loops .GT. 3 ) then
      dec = dec + ( -5.323890213832026D0
     $              - 4.93113425925925D0 * lmm 
     $              - 0.22743055555555D0 * lmm**2 
     $              - 0.00462962962963D0 * lmm**3 
     $              + ( 0.2624708119543296D0 
     $                 + 0.2366898148148148D0 * lmm) * nl
     $            ) * as**3
      endif

      ADECD = alpha * dec
      return
      end
