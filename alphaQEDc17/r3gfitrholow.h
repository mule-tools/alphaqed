
c      ++++ r3gfitrholow_qedc17 ++++  

c 0.318 -- 0.778 updated nov 2010 (incl BaBar/KlOE) / jan 2012 / june 2013 
       DATA ares/      
     &      1.300818d0, 1.959109d0, 0.908761d0, 0.211595d0,-0.119133d0,
     &     -0.186989d0,-0.135978d0,-0.065694d0,-0.013624d0, 0.010170d0,
     &      0.014356d0, 0.010960d0, 0.006539d0, 0.002576d0/
       DATA asta/      
     &      0.008203d0, 0.008683d0, 0.002997d0, 0.002005d0, 0.001396d0,
     &      0.000624d0,-0.000025d0, 0.000452d0, 0.000897d0, 0.000497d0,
     &      0.000128d0,-0.000045d0, 0.000256d0, 0.000151d0/
       DATA asys/      
     &      0.005124d0, 0.007151d0, 0.004175d0, 0.002284d0, 0.001336d0,
     &      0.000793d0, 0.000513d0, 0.000612d0, 0.000664d0, 0.000394d0,
     &      0.000117d0,-0.000042d0, 0.000015d0, 0.000021d0/
