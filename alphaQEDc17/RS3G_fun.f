      FUNCTION RS3G_qedc17(s,ier)
c DEC 2014 update: 
c initialization of common rhadnfmin1 supplemented:  
c      IF (E.GE.EHIGH) THEN
cc call R to initialize some parameters
c         RSGG1=RS3x_qedc17(s,R3G,R33)
c proper PHI background directly implemented in R3G0321.f 
      IMPLICIT NONE
      INTEGER i,j,jj,ini,IER,iresonances,IRESON,NFext,NFext1,nf
      DOUBLE PRECISION s,E,RS40_qedc17,RS3G_qedc17,Rless,R3Gless,R33less,RS3x_qedc17,BW_qedc17,
     &     null,fac,rbw,rebw,Rinicall,UGM2,sMeV,ELOW,EHIGH,
     &     RSGG1,RS3G1,R3G,R33,F3G
      DOUBLE PRECISION res(3),fsta(5),fsys(5)
      REAL*8 EMIPS(6),EMAPS(6),EMIYP(6),EMAYP(6),EOMM,EOMP,EFIM,EFIP
      INTEGER N3G,N
      PARAMETER(N3G=479)
      DOUBLE PRECISION X3G(N3G),Y3G(N3G,3)
      DOUBLE PRECISION MOM,GOM,POM,MFI,GFI,PFI
      DOUBLE PRECISION CHPTCUTLOC,ERXYDAT,ECHARMTH,ECUTFIT
      real *8 xc,xb,xt
      real *8 mq(6),mp(6),th(6)
      integer  itestphi,jphi
      real *8  E0,E1,EX,dephi,SX
      integer ny
      parameter(ny=15)
      REAL*8 EMI(ny),EMA(ny)
      COMMON/RESRAN_qedc17/EMI,EMA
      COMMON/BWOM_qedc17/MOM,GOM,POM/BWFI_qedc17/MFI,GFI,PFI
      COMMON/RESRELERR_qedc17/FSTA,FSYS
c resonances flag iresonances=1 include resonances as data here; 0 include them
c elsewhere; iresonances=1 Rdat_all.f yields R value including narrow resonances
      COMMON/RES_qedc17/iresonances
      COMMON/RESFIT_qedc17/fac,IRESON
c fac converts Breit-Wigner resonance contribution to R value; IRESON dummy here
c IRESON=1 Rdat_fit.f yields R value including narrow resonances
      COMMON/RESDOMAINS_qedc17/EMIPS,EMAPS,EMIYP,EMAYP,EOMM,EOMP,EFIM,EFIP
      COMMON /R3GDAT_qedc17/X3G,Y3G,N
      external RS40_qedc17,RS3x_qedc17,BW_qedc17
c      common /rhadparts/ru,rd,rs,rc,rb,rt,rsg,rem
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf
      common /cufit_qedc17/CHPTCUTLOC,ERXYDAT,ECHARMTH
      common /quama_qedc17/mq,mp,th
c      data ini /0/
c      if (ini.eq.0) then
************************************************************************
c Set parameters in above common blocks: BWOM,BWFI,PSYPPAR,RESRELERR
c rho, omega, phi, J/psi and Upsilon series
c
         call resonances_data_qedc17()
c
c called from resonances_dat.f
************************************************************************
         null=0.d0
         UGM2=1.D6
         xc=0.375d0
         xb=0.750d0
         xt=xc
c         ini=1
c      endif
      j=IER
      sMeV=s*UGM2
      rbw =null
      REBW=null
      do jj=1,3
         res(jj) =null
      enddo
      itestphi=0
      if (itestphi.eq.1) then
         E0=EFIM-GFI
         E1=EFIP+GFI
         EMI(3)=E0
         EMA(3)=E1
         dephi=(E1-E0)/50.d0
         EX=E0
         do jphi=1,50 
            SX=EX**2
            rbw=fac*BW_qedc17(sX,MFI,GFI,PFI)
            rbw=3.d0/4.d0*rbw
            write (99,*) ex,rbw,rbw*fsta(3),rbw*fsys(3)
            ex=ex+dephi
         enddo
         EMI(3)=EFIM
         EMA(3)=EFIP
      endif
      RSGG1=null
      RS3G1=null
      E=SQRT(S)
      ELOW =X3G(1)                ! 0.318
      EHIGH=X3G(N3G)              ! 2.125
      CHPTCUTLOC=DMAX1(ELOW,CHPTCUTLOC)  ! do not go below recombined data set R3G0321.f
      ECUTFIT=DMAX1(EHIGH,ERXYDAT)       ! exhaust recombined data set R3G0321.f
      ECHARMTH=th(4)  ! 4.2 GeV
      RS3G_qedc17=null
      IF (E.LE.ELOW) THEN
         RS3G_qedc17=0.5d0*RS40_qedc17(s,IER)  ! 9/20 pQCD --> 10/20 from isospin decomposition
         RETURN
      ENDIF
      if ((E.GT.ELOW).and.(E.LT.EHIGH)) then
         call getindex_qedc17(E,N3G,X3G,I)
         if (I.LT.N3G) then
            res(j)=Y3G(I,j)+(Y3G(I+1,j)-Y3G(I,j))
     &           /(X3G(I+1)-X3G(I))*(E-X3G(I))
            if (j.eq.3) then
               res(j)=res(j)*(Y3G(I,1)+(Y3G(I+1,1)-Y3G(I,1))
     &              /(X3G(I+1)-X3G(I))*(E-X3G(I)))
            endif
         else
            res(j)=Y3G(N3G,j)
            if (j.eq.3) then
               res(j)=res(j)*Y3G(N3G,1)
            endif
         endif
c <3G> has no omega contribution skipped
c omega  .41871054d0,.810d0
c phi   1.00D0,1.04D0
c use background only
         if (iresonances.eq.1) then
c phi   1.00D0,1.04D0
            if ((E.GT.EFIM).and.(E.LT.EFIP)) then
               rbw=fac*BW_qedc17(sMeV,MFI,GFI,PFI)
c for <3G> factor 3/4 to be applied
               rbw=rbw*3.d0/4.d0
               if (ier.eq.2) then
                  rbw=rbw*fsta(3)
                  res(j)=sqrt(res(j)**2+rbw**2)
               else
                  if (ier.eq.3) rbw=rbw*fsys(3)
                  res(j)=res(j)+rbw
               endif
               rebw=rbw
            endif
         endif
         RS3G_qedc17=res(j)
         RETURN
      endif
      IF (E.GE.EHIGH) THEN
c call R to initialize some parameters
         RSGG1=RS3x_qedc17(s,R3G,R33)
         IF (E.LE.ECUTFIT) THEN
            RS3G_qedc17=0.5d0*RS40_qedc17(s,IER)
         ELSE IF (E.GT.ECUTFIT) THEN
            if (nf.le.3) then
               nf=3
               RS3G_qedc17=0.5d0*RS40_qedc17(s,IER)
            else
               RSGG1=RS3x_qedc17(s,R3G,R33)
               F3G=R3G/RSGG1
               IF (IER.EQ.1) THEN
                  if (nf.eq.4) then
                     RS3G_qedc17=(RS40_qedc17(s,IER)-Rless)*xc+R3Gless
                  else if (nf.eq.5) then
                     RS3G_qedc17=(RS40_qedc17(s,IER)-Rless)*xb+R3Gless
                  else if (nf.ge.6) then
                     RS3G_qedc17=(RS40_qedc17(s,IER)-Rless)*xt+R3Gless
                  endif
               ELSE
                  RS3G_qedc17=RS40_qedc17(s,IER)*F3G
               ENDIF
            endif
         ENDIF
         RETURN
      ENDIF
      if ((E.GT.EFIM).and.(E.LT.EFIP)) then
         rbw=fac*BW_qedc17(sMeV,MFI,GFI,PFI)
         if (ier.eq.2) then
            rbw=rbw*fsta(3)
            res(j)=sqrt(res(j)**2+rbw**2)
         else 
            if (ier.eq.3) rbw=rbw*fsys(3)
            res(j)=res(j)+rbw
         endif
         rebw=rbw
      endif
      END
c
      FUNCTION RS3GpQCD_qedc17(s,IER)
c normalization of weak current j=1/4(u-d) etc
      implicit none
      integer  IER,IOR,NF,ICHK,IOR1,NF1
      real *8 s,R,R3G,R33,RS3GpQCD_qedc17,RS3x_qedc17,ALINP,EINP,MTOP,pi,
     &        ALS,EST,ESY,MZINP,ALINP1,MTOP1,RSP,RSM
      real *8 res(3)
      COMMON/QCDPA_qedc17/ALS,EST,ESY,MZINP,MTOP,NF,IOR,ICHK ! global INPUT from main
      common/pqcdHS_qedc17/pi,ALINP1,EINP,MTOP1,IOR1,NF1 ! local input for Rhad HS etc
      external RS3x_qedc17
      EINP =MZINP
      MTOP1=MTOP
      IOR1 =IOR
      NF1  =NF
      if (IER.eq.1) then
         ALINP1=ALS
         R=RS3x_qedc17(S,R3G,R33)
         res(IER)=R3G
      else if (IER.eq.2) then
         res(IER)=0.d0
      else  if (IER.eq.3) then
c treat QCD error as a systematic error
         ALINP1=ALS+EST
         R=RS3x_qedc17(S,R3G,R33)
         RSP=R3G
         ALINP1=ALS-EST
         R=RS3x_qedc17(S,R3G,R33)
         RSM=R3G
         res(IER)=ABS(RSP-RSM)/2.d0
         ALINP1=ALS
      else
      endif
      RS3GpQCD_qedc17=res(IER)
      return
      end
c
      function RS3x_qedc17(s,R3Gout,R33out)
c as xRS_qedc17(s) with extended functionality calculating R3G and R33 as well
      implicit none
      integer  IOR,NF,NFext1,nf1
      real *8 pi,s,RS3x_qedc17,R,R3G,R33,xRS_qedc17,ALINP,EINP,MTOP
      real *8 Rless,R3Gless,R33less,R3Gout,R33out
      common/pqcdHS_qedc17/pi,ALINP,EINP,MTOP,IOR,NF
      common /rhadnfmin1/Rless,R3Gless,R33less,NFext1,nf1
c adopte normalization j=1/4(u-d) etc for weak current i.e. 1/2  normal isospin
      call rqcdHS3x(s,R,R3G,R33,ALINP,EINP,MTOP,IOR,NF,17)
      RS3x_qedc17=R
      R3Gout=R3G
      R33out=R33
      return
      end
