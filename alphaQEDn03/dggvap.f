       FUNCTION dggvap(s,erfl)
C      -----------------------
C Fermionic contributions to the running of alpha
C Function collecting 1-loop, 2-loop, 3-loop leptonic contributions (e, mu, tau) 
C + hadronic contribution from light hadrons (udscb)
C + top quark contribution (t); here at 1-loop (good up to 200 GeV LEP I/II) 
C erfl is the errorflag: 0.d0->central value +/- 1.d0 -> upper/lower bound
       implicit none
       real*8 dggvap,s,erfl
       real*8 c0,st2,als
       real*8 mtop,mtop2
       real*8 dallepQED1,dallepQED2,dallepQED3l,
     &        dalQED1ferm,res_re,res_im
       real*8 r1real,r1imag,r2real,r2imag,imag
       real*8 r1reall,r1imagl,r2reall,r2imagl,r3reall,r3imagl
       real*8 fac,ee,e,der,errder,deg,errdeg
       real*8 dalept,daltop,dahadr,hfun
       include 'common.f'      
       common /parm/st2,als,mtop
       common /resu/dalept,dahadr,daltop
       e=dsqrt(dabs(s))
       if (s.lt.0.d0) e=-e
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'had')) then
          call dhadr5n03(e,st2,der,errder,deg,errdeg)
          dahadr=der
       else 
          dahadr=0.d0
       endif
       if ((LEPTONflag.eq.'had').or.(LEPTONflag.eq.'top')) then
          dalept=0.d0
       else 
          r1real =dallepQED1(s,r1imag)
          r2real =dallepQED2(s,r2imag)
          r3reall=dallepQED3l(s,r1reall,
     &                          r1imagl,r2reall,r2imagl,r3imagl)
          res_re=r1real+r2real+r3reall
          res_im=r1imag+r2imag+r3imagl
          dalept=res_re
       endif
       if ((LEPTONflag.eq.'all').or.(LEPTONflag.eq.'top')) then
          mtop2=mtop*mtop
          daltop=dalQED1ferm(s,mtop2,imag)*4.d0/3.d0
       else 
          daltop=0.d0
       endif
       dggvap=dalept+dahadr+erfl*errder
       RETURN
       END
