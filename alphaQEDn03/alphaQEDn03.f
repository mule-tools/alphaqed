*;;;;;;;;;;;;;;;;;;;;;;;;;;;; -*- Mode: Fortran -*- ;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; alphaQED.f --- 
*; Author          : Fred Jegerlehner
*; Created On      : Sat Nov  1 22:49:46 2003
*; Last Modified By: Fred Jegerlehner
*; Last Modified On: Mon Nov  3 02:23:59 2003
*; RCS: $Id$
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; Copyright (c) 2003 Fred Jegerlehner
*;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
*; 
      PROGRAM alphaQEDn
C Calculating the running QED coupling alpha(E)
C Leptons: 1--loop, 2--loop exact, 3--loop in high energy approximation
C Quarks form routine dhadr5n including effective s--channel shifts in low energy range
C r1....,r2....,r3.... : oneloop, twoloop, threeloop result
C  ..real: realpart, ..imag: imaginary part;
C ......l: light fermions only (high energy approximation for 3--loops) 
C dallepQED1n=r1real,dallepQED2n=r2real,dallepQED3l=r3reall
C dallepQED3l also returns oneloop and twoloop high energy (light fermion) approximations 
      implicit none
      integer*4 i,j,N,stflag
      real*8 dggvap,s,erfl,e
      real*8 st2,als,mtop
      real*8 dalept,dahadr,daltop,dvpt_new,dvpt_hig,dvpt_low
      real*8 alphac,alphah,alphal,alphact,alphaht,alphalt
      real*8 emax,emin,elogl,elogu,delog,elog
********************* part to be included in main program **************
      include 'common.f'      
      common /parm/st2,als,mtop
      common /resu/dalept,dahadr,daltop
      call constants()
      st2=0.23d0        ! Reference value for weak mixing parameter
      als=0.118d0       ! alpha strong
      mtop=175.d0       ! top quark mass
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment the folowwing two lines for changing the default
c      LEPTONflag='tau'
c      iLEP=LFLAG(LEPTONflag)
************************************************************************
      N=10
      do j=1,4
         write (j,*) '      3.0000'
         write (j,*) '      1.0000'
         write (j,*) '   ',float(N)
      enddo
c     ..................................................................
c     vacuum polarization
      write (1,*) ' e,dalpha,dalpha_low,dalpha_hig (no top)' 
      write (2,*) ' e,alpha ,alphal ,alphah (no top)    '
      write (3,*) ' e,alphat,alphatl,alphath (with top)   ' 
      write (4,*) ' e,dalept,dahadr,daltop'
      stflag= 1
c     for spacelike region: stflag=-1
c      stflag=-1
      emin=1.d-6
      emax=91.19d0
      elogl=dlog(dabs(emin))
      elogu=dlog(dabs(emax))
      delog=(elogu-elogl)/N
      do 10 i=1,N+1
      if (stflag.eq.1) then
        elog=elogl+delog*(i-1)
      else
        elog=elogu-delog*(i-1)
      endif
      e=dexp(elog)
      s=e*e*stflag
      e=dsqrt(dabs(s))*stflag
c
      dvpt_new= dggvap(s, 0.d0)
      dvpt_hig= dggvap(s, 1.d0)
      dvpt_low= dggvap(s,-1.d0)
      alphac =alp/(1.d0-dvpt_new)
      alphah =alp/(1.d0-dvpt_hig)
      alphal =alp/(1.d0-dvpt_low)
      alphact=alp/(1.d0-dvpt_new-daltop)
      alphaht=alp/(1.d0-dvpt_hig-daltop)
      alphalt=alp/(1.d0-dvpt_low-daltop)
c
c     delta alpha: leptons + 5 quarks no top
c
      write (1,99) e,dvpt_new,dvpt_low,dvpt_hig
c
c     alpha: leptons + 5 quarks no top
c
      write (2,99) e,alphac ,alphal ,alphah
c
c     alpha: leptons + 5 quarks + top
c
      write (3,99) e,alphact,alphalt,alphaht
c
c     delta alpha: leptons, top , 5 quarks, 5 quarks using effective parameters
c
      write (4,99) e,dalept,dahadr,daltop
c
 10   continue
c     ..................................................................
 99   format(5(2x,1pe11.4))
      stop
      END
c
