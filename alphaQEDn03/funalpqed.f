      function funalpqed(s,error,contributionflag)
      implicit none
      character*3 contributionflag
      real*8 funalpqed,dggvap,s,error
      real*8 st2,als,mtop
      real*8 dalept,dahadr,daltop,dvpt_new,dvpt_hig,dvpt_low
      real*8 alphact,alphaht,alphalt
***************** part to be included also in main program **************
      include 'common.f'      
      common /parm/st2,als,mtop
      common /resu/dalept,dahadr,daltop
      call constants()
C Decomment in main next 3 lines 
c      st2=0.23d0        ! Reference value for weak mixing parameter
c      als=0.118d0       ! alpha strong
c      mtop=175.d0       ! top quark mass
C LEPTONflag=all,had,lep,ele,muo,tau -> iLEP=-3,-2,-1,1,2,3
C Default: 
C      LEPTONflag='all'
C      iLEP  = -3  ! for sum of leptons + quarks              
C Decomment in main the folowwing line for changing the default
c      LEPTONflag='all'   ! choose had,lep,top,ele,muo,tau
************************************************************************
      LEPTONflag=contributionflag
      iLEP=LFLAG(LEPTONflag)
      dvpt_new= dggvap(s, 0.d0)
      dvpt_hig= dggvap(s, 1.d0)
      dvpt_low= dggvap(s,-1.d0)
      alphact=alp/(1.d0-dvpt_new-daltop)
      alphaht=alp/(1.d0-dvpt_hig-daltop)
      alphalt=alp/(1.d0-dvpt_low-daltop)
      error=abs((alphaht-alphalt))/2.d0
      funalpqed=alphact
      return
      END
